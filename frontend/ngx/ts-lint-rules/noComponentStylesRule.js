"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Lint = require("tslint");
class Rule extends Lint.Rules.AbstractRule {
    apply(sourceFile) {
        const walker = new NoComponentStylesWalker(sourceFile, this.getOptions());
        return this.applyWithWalker(walker);
    }
}
Rule.FAILURE_STRING = 'component styles forbidden :(';
exports.Rule = Rule;
class NoComponentStylesWalker extends Lint.RuleWalker {
    constructor(sourceFile, options) {
        super(sourceFile, options);
    }
    visitClassDeclaration(node) {
        if (node.decorators && node.decorators.length > 0) {
            node.decorators.forEach(elem => {
                let baseExpr = elem.expression || {};
                let arg = baseExpr.arguments ? baseExpr.arguments[0] : undefined;
                if (arg) {
                    const map = arg.symbol.members;
                    if (map.has('styleUrls') || map.has('styles')) {
                        this.addFailureAt(node.getStart(), node.getWidth(), Rule.FAILURE_STRING);
                    }
                }
            });
        }
    }
}
//# sourceMappingURL=noComponentStylesRule.js.map
