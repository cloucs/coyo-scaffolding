import {NgModule} from '@angular/core';
import {messagesDe} from '@app/filepicker/de.filepicker.messages';
import {messagesEn} from '@app/filepicker/en.filepicker.messages';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {ContextMenuModule} from '@shared/context-menu/context-menu.module';
import {FileModule} from '@shared/files/file.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {PreviewModule} from '@shared/preview/preview.module';
import {FilepickerBreadcrumbComponent} from './filepicker-modal/filepicker-breadcrumb/filepicker-breadcrumb.component';
import {FilepickerIconComponent} from './filepicker-modal/filepicker-icon/filepicker-icon.component';
import {FilepickerModalComponent} from './filepicker-modal/filepicker-modal.component';
import {FilepickerSearchComponent} from './filepicker-modal/filepicker-search/filepicker-search.component';

/**
 * The filepicker module
 */
@NgModule({
  declarations: [FilepickerModalComponent, FilepickerBreadcrumbComponent, FilepickerIconComponent, FilepickerSearchComponent],
  imports: [
    CoyoCommonsModule,
    CoyoFormsModule,
    ContextMenuModule,
    FileModule,
    PreviewModule
  ],
  entryComponents: [FilepickerModalComponent],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn}
  ]
})
export class FilepickerModule {
}
