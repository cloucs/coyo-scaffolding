import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {
  FilepickerModalComponent,
  FilePickerModalData
} from '@app/filepicker/filepicker-modal/filepicker-modal.component';
import {MatDialogSize} from '@coyo/ui';
import {Observable} from 'rxjs';

/**
 * This service handles the filepicker.
 */
@Injectable({
  providedIn: 'root'
})
export class FilepickerService {

  constructor(private dialog: MatDialog) {
  }

  /**
   * Opens a filepicker.
   *
   * @param data The information that are used to display the filepicker
   * @return Observable of selected items
   */
  openFilepicker(data: FilePickerModalData): Observable<FilepickerItem[]> {
    return this.dialog.open<FilepickerModalComponent, FilePickerModalData>(FilepickerModalComponent, {
      data,
      width: MatDialogSize.Large,
      height: 'calc(65vh + 171px)'
    }).afterClosed();
  }
}
