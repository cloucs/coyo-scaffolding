import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {FilepickerSearchComponent} from './filepicker-search.component';

describe('FilepickerSearchComponent', () => {
  let component: FilepickerSearchComponent;
  let fixture: ComponentFixture<FilepickerSearchComponent>;
  let queryChange: Subject<string>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilepickerSearchComponent],
      providers: [{
        provide: TranslateService,
        useValue: jasmine.createSpyObj('TranslateService', ['instant'])
      }]
    }).overrideTemplate(FilepickerSearchComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilepickerSearchComponent);
    component = fixture.componentInstance;
    queryChange = new Subject<string>();
    component.fileSearchInput = {valueChanges: queryChange} as unknown as FormControl;
    spyOn(component.searchResult, 'emit');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit a folder when the query is not empty', fakeAsync(() => {
    // given
    const query = 'search term';

    // when
    queryChange.next(query);
    tick(300);
    fixture.detectChanges();

    // then
    fixture.whenStable().then(() => {
      expect(component.searchResult.emit).toHaveBeenCalled();
      expect(component.searchResult.emit).not.toHaveBeenCalledWith(null);
    });
  }));

  it('should emit null when the query is empty', fakeAsync(() => {
    // given
    const query = '';

    // when
    queryChange.next(query);
    tick(300); // wait for the first task to get done
    fixture.detectChanges(); // update view with quote

    // then
    fixture.whenStable().then(() => {
      expect(component.searchResult.emit).toHaveBeenCalledWith(null);
    });
  }));
});
