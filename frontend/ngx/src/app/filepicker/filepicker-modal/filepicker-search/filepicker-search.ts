import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {Observable} from 'rxjs';

export interface FilepickerSearch {
  inputPlaceholder: string;

  execute(phrase: string): Observable<FilepickerItem[]>;
}
