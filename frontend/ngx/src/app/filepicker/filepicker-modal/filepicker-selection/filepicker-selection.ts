import {FormControl} from '@angular/forms';
import * as _ from 'lodash';
import {Observable} from 'rxjs';
import {FilepickerItem} from '../filepicker-item';

export class FilepickerSelection {
  currentItems: FilepickerItem[] = [];
  selectAllControl: FormControl;
  selectControls: {
    [key: string]: FormControl
  } = {};
  selectedFiles: FilepickerItem[] = [];

  constructor(items$: Observable<FilepickerItem[]>) {
    items$.subscribe(items => {
      this.currentItems = items;
      this.selectAllControl = new FormControl();
      this.initializeFileSelectControls(items);
    });
  }

  get numberOfSelectedFilesInCurrentFolder(): number {
    return _.filter(this.selectControls, (formControl, fileId) =>
      formControl.value && !!_.find(this.currentItems, ['id', fileId])).length;
  }

  get numberOfSelectedFilesInTotal(): number {
    return this.selectedFiles.length;
  }

  get numberOfFilesInCurrentFolder(): number {
    return this.currentItems.filter(value => !value.isFolder).length;
  }

  /**
   * Selects/deselects file depending on the current value of the selectAll form control:
   * true -> the file will be selected
   * false -> the file will be deselected
   *
   * @param item a file picker item
   */
  toggleFileSelection(item: FilepickerItem): void {
    const alreadySelected = !!_.find(this.selectedFiles, ['id', item.id]);
    this.selectControls[item.id].setValue(!alreadySelected);
  }

  /**
   * Selects/deselects all files depending on the current value of the selectAll form control:
   * true -> all files will be selected
   * false -> all files will be deselected
   */
  toggleAllFileSelectionsInCurrentFolder(): void {
    this.currentItems.forEach(item => {
      if (this.selectControls[item.id]) {
        this.selectControls[item.id].setValue(this.selectAllControl.value);
      }
    });
  }

  /**
   * Checks if at least one file but not all files are selected in the current folder
   *
   * @returns true if one or more but not all files are selected.
   */
  isAtLeastOneButNotAllFilesSelectedInCurrentFolder(): boolean {
    return this.isAtLeastOneFileInCurrentFolderSelected()
      && !this.areAllFilesSelectedInCurrentFolder();
  }

  /**
   * Checks if at least one file is selected in the current folder.
   *
   * @returns true if at least one file is selected
   */
  isAtLeastOneFileInCurrentFolderSelected(): boolean {
    return this.numberOfSelectedFilesInCurrentFolder > 0;
  }

  /**
   * Checks if all files in current folder is selected.
   *
   * @returns true if all files in current folder is selected.
   */
  areAllFilesSelectedInCurrentFolder(): boolean {
    return this.numberOfFilesInCurrentFolder === this.numberOfSelectedFilesInCurrentFolder;
  }

  private initializeFileSelectControls(items: FilepickerItem[]): void {
    items.forEach(item => {
      if (!item.isFolder && !this.selectControls[item.id]) {
        const formControl = new FormControl();
        formControl.registerOnChange((checked: boolean) => this.fileSelectionHandler(checked, item));
        this.selectControls[item.id] = formControl;
      }
    });
    if (this.areAllFilesSelectedInCurrentFolder()) {
      this.selectAllControl.setValue(true);
    } else {
      this.selectAllControl.setValue(false);
    }
  }

  private fileSelectionHandler(checked: boolean, item: FilepickerItem): void {
    if (checked) {
      if (!_.includes(this.selectedFiles, item)) {
        this.selectedFiles.push(item);
        if (this.areAllFilesSelectedInCurrentFolder()) {
          this.selectAllControl.setValue(true);
        }
      }
    } else {
      _.remove(this.selectedFiles, item);
      this.selectAllControl.setValue(false);
    }
  }
}
