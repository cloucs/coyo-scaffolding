import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'FILEPICKER.BREADCRUMB.CONTEXT_MENU.TOOLTIP' : 'Ausgeblendete Ordner anzeigen',
    'FILEPICKER.BUTTON.SELECT': 'Auswählen ({count})',
    'FILEPICKER.BUTTON.BACK': 'Zurück',
    'FILEPICKER.BUTTON.BACK.ARIA': 'Zurück',
    'FILEPICKER.COLUMN.FILENAME': 'Name',
    'FILEPICKER.COLUMN.SIZE': 'Größe',
    'FILEPICKER.COLUMN.MODIFIED': 'Bearbeitet',
    'FILEPICKER.COLUMN.SELECT': 'Alles auswählen/abwählen',
    'FILEPICKER.FILE.ARIA': 'Datei "{file}"',
    'FILEPICKER.FILE.SELECT': '"{file}" auswählen',
    'FILEPICKER.FOLDER.ARIA': 'Ordner "{folder}". Klicken, um in diesen Ordner zu navigieren.',
    'FILEPICKER.FOLDER.CURRENT.ARIA': 'Aktueller Ordner "{folder}"',
    'FILEPICKER.FOLDER.EMPTY': 'Hier gibt es nichts zu sehen',
    'FILEPICKER.HEADLINE': 'Datei auswählen',
    'FILEPICKER.SEARCH_RESULTS.TITLE': 'Suchergebnisse'
  }
};
