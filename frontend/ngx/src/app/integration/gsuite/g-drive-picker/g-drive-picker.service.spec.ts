import {fakeAsync, getTestBed, inject, TestBed, tick} from '@angular/core/testing';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';
import {AuthService} from '@core/auth/auth.service';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {User} from '@domain/user/user';
import {TranslateService} from '@ngx-translate/core';
import {WINDOW} from '@root/injection-tokens';
import {of} from 'rxjs';
import {GoogleApiService} from '../google-api/google-api.service';
import {GDrivePickerService} from './g-drive-picker.service';

describe('GDrivePickerService', () => {
  let injector: TestBed;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;
  let googleApiService: jasmine.SpyObj<GoogleApiService>;
  let integrationApiService: jasmine.SpyObj<IntegrationApiService>;
  let docsView: jasmine.SpyObj<any>;
  let pickerBuilder: jasmine.SpyObj<any>;
  const authService = jasmine.createSpyObj('authService', ['getUser']);
  const user = {language: 'de'} as User;

  beforeEach(() => {

    docsView = jasmine.createSpyObj('DocsView',
        ['setIncludeFolders', 'setSelectFolderEnabled', 'setOwnedByMe', 'setEnableTeamDrives']);
    docsView.setIncludeFolders.and.returnValue(docsView);
    docsView.setSelectFolderEnabled.and.returnValue(docsView);
    docsView.setOwnedByMe.and.returnValue(docsView);
    docsView.setEnableTeamDrives.and.returnValue(docsView);
    pickerBuilder = jasmine.createSpyObj('PickerBuilder',
        ['addView', 'setOAuthToken', 'setCallback', 'enableFeature', 'setSize', 'build', 'setVisible', 'setLocale']);

    window['google'] = {
      picker: {
        DocsView: function(constructorArg: any): any {
          docsView.constructorArg = constructorArg;
          return docsView;
        },
        DocsUploadView: function(): any {
          return {type: 'DocsUploadView'};
        },
        ViewId: {
          DOCS: 'DOCS',
          DOCS_IMAGES: 'DOCS_IMAGES',
          DOCS_VIDEOS: 'DOCS_VIDEOS'
        },
        Feature: {
          MULTISELECT_ENABLED: 'MULTISELECT_ENABLED',
          SUPPORT_TEAM_DRIVES: 'SUPPORT_TEAM_DRIVES'
        },
        Response: {
          ACTION: 'ACTION',
          DOCUMENTS: 'DOCUMENTS'
        },
        Action: {
          PICKED: 'PICKED'
        },
        Document: {
          ID: 'ID',
          URL: 'URL',
          NAME: 'NAME'
        }
      }
    };

    TestBed.configureTestingModule({
      providers: [GDrivePickerService, {
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['isXs', 'isSm'])
      }, {
        provide: WINDOW, useValue: jasmine.createSpyObj('windowService', ['innerWidth'])
      }, {
        provide: GoogleApiService,
        useValue: jasmine.createSpyObj('googleApiService', ['createPickerBuilder'])
      }, {
        provide: IntegrationApiService,
        useValue: jasmine.createSpyObj('integrationApiService', ['getToken'])
      }, {
        provide: AuthService,
        useValue: authService
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }, ]
    });

    const accessToken = {
      token: 'token',
      expiresIn: 60000
    };

    injector = getTestBed();
    windowSizeService = injector.get(WindowSizeService);
    googleApiService = injector.get(GoogleApiService);
    integrationApiService = injector.get(IntegrationApiService);

    googleApiService.createPickerBuilder.and.returnValue(pickerBuilder);
    integrationApiService.getToken.and.returnValue(of(accessToken));
    pickerBuilder.addView.and.returnValue(pickerBuilder);
    pickerBuilder.setOAuthToken.and.returnValue(pickerBuilder);
    pickerBuilder.enableFeature.and.returnValue(pickerBuilder);
    pickerBuilder.setSize.and.returnValue(pickerBuilder);
    pickerBuilder.setLocale.and.returnValue(pickerBuilder);
    pickerBuilder.build.and.returnValue(pickerBuilder);
    pickerBuilder.setVisible.and.returnValue(pickerBuilder);
    pickerBuilder.setCallback.and.returnValue(pickerBuilder);
    authService.getUser.and.returnValue(of(user));
  });

  it('should be created', inject([GDrivePickerService], (service: GDrivePickerService) => {
    expect(service).toBeTruthy();
  }));

  it('should come to an error if no google api is loaded', inject([GDrivePickerService], (service: GDrivePickerService) => {
    service.open().catch((result: string) => {
      expect(result).toEqual('google api is not loaded.');
    });
  }));
  ['DOCS', 'DOCS_IMAGES', 'DOCS_VIDEOS'].forEach(documentType =>
    it('should open the file picker with the correct settings for type ' + documentType, fakeAsync(
      inject([GDrivePickerService], (service: GDrivePickerService) => {
        // given
        window['gapi'] = {
          load(type: string, callback: Function): void {
            expect(type).toEqual('picker');
            callback();
          }
        };
        windowSizeService.isSm.and.returnValue(false);
        windowSizeService.isXs.and.returnValue(false);
        authService.getUser.and.returnValue(of(user));

        // when
        service.open({
          multipleSelect: false,
          view: documentType as any,
          uploadView: false,
          recentFilesView: false
        });
        tick();

        // then
        expect(googleApiService.createPickerBuilder).toHaveBeenCalled();
        expect(docsView.setIncludeFolders).toHaveBeenCalledTimes(3);
        expect(docsView.setIncludeFolders).toHaveBeenCalledWith(true);
        expect(docsView.setIncludeFolders).toHaveBeenCalledWith(true);
        expect(docsView.setIncludeFolders).toHaveBeenCalledWith(true);
        expect(docsView.setSelectFolderEnabled).toHaveBeenCalledTimes(3);
        expect(docsView.setSelectFolderEnabled).toHaveBeenCalledWith(false);
        expect(docsView.setSelectFolderEnabled).toHaveBeenCalledWith(false);
        expect(docsView.setSelectFolderEnabled).toHaveBeenCalledWith(false);
        expect(docsView.constructorArg).toEqual(documentType);
        expect(docsView.setOwnedByMe).toHaveBeenCalledTimes(2);
        expect(docsView.setOwnedByMe).toHaveBeenCalledWith(true); // personal drive
        expect(docsView.setOwnedByMe).toHaveBeenCalledWith(false); // shared with me drive
        expect(docsView.setEnableTeamDrives).toHaveBeenCalledTimes(3);
        expect(docsView.setEnableTeamDrives).toHaveBeenCalledWith(false); // personal drive
        expect(docsView.setEnableTeamDrives).toHaveBeenCalledWith(true); // team drive
        expect(docsView.setEnableTeamDrives).toHaveBeenCalledWith(false); // shared with me drive
        expect(pickerBuilder.setOAuthToken).toHaveBeenCalledWith('token');
        expect(pickerBuilder.setCallback).toHaveBeenCalled();
        expect(pickerBuilder.enableFeature).toHaveBeenCalledTimes(1);
        expect(pickerBuilder.enableFeature).toHaveBeenCalledWith('SUPPORT_TEAM_DRIVES');
        expect(pickerBuilder.setLocale).toHaveBeenCalledWith('de');
        expect(pickerBuilder.setVisible).toHaveBeenCalledWith(true);
        expect(pickerBuilder.build).toHaveBeenCalled();
        expect(windowSizeService.isSm).toHaveBeenCalled();
        expect(windowSizeService.isXs).toHaveBeenCalled();
        expect(pickerBuilder.setSize).toHaveBeenCalledTimes(0);
      })
    )));

  it('should check for an auth token when opening the picker dialog',
    fakeAsync(inject([GDrivePickerService], (service: GDrivePickerService) => {
      // given

      window['gapi'] = {
        load(type: string, callback: Function): void {
          expect(type).toEqual('picker');
          callback();
        }
      };
      windowSizeService.isSm.and.returnValue(false);
      windowSizeService.isXs.and.returnValue(true);

      // when
      service.open();
      tick();

      // then
      expect(pickerBuilder.enableFeature).toHaveBeenCalledTimes(2);
      expect(pickerBuilder.enableFeature).toHaveBeenCalledWith('MULTISELECT_ENABLED');
      expect(pickerBuilder.setSize).toHaveBeenCalled();
    }))
  );

  it('should open picker and add all views',
    fakeAsync(inject([GDrivePickerService], (service: GDrivePickerService) => {
      // given
      window['gapi'] = {
        load(type: string, callback: Function): void {
          expect(type).toEqual('picker');
          callback();
        }
      };

      // when
      service.open({
        multipleSelect: false,
        view: 'DOCS' as any,
        uploadView: true,
        recentFilesView: true
      });
      tick();

      // then
      expect(pickerBuilder.addView).toHaveBeenCalledTimes(5);
      expect(pickerBuilder.addView.calls.argsFor(4)[0].type).toEqual('DocsUploadView');

      expect(docsView.setIncludeFolders).toHaveBeenCalledTimes(3);
      expect(docsView.setIncludeFolders).toHaveBeenCalledWith(true); // personal drive
      expect(docsView.setIncludeFolders).toHaveBeenCalledWith(true); // team drive
      expect(docsView.setIncludeFolders).toHaveBeenCalledWith(true); // shared with me drive

      expect(docsView.setSelectFolderEnabled).toHaveBeenCalledTimes(3);
      expect(docsView.setSelectFolderEnabled).toHaveBeenCalledWith(false); // personal drive
      expect(docsView.setSelectFolderEnabled).toHaveBeenCalledWith(false); // team drive
      expect(docsView.setSelectFolderEnabled).toHaveBeenCalledWith(false); // shared with me drive

      expect(docsView.setOwnedByMe).toHaveBeenCalledTimes(2);
      expect(docsView.setOwnedByMe).toHaveBeenCalledWith(true); // personal drive
      expect(docsView.setOwnedByMe).toHaveBeenCalledWith(false); // shared with me drive

      expect(docsView.setEnableTeamDrives).toHaveBeenCalledTimes(3);
      expect(docsView.setEnableTeamDrives).toHaveBeenCalledWith(false); // personal drive
      expect(docsView.setEnableTeamDrives).toHaveBeenCalledWith(true); // team drive
      expect(docsView.setEnableTeamDrives).toHaveBeenCalledWith(false); // shared with me drive
    }))
  );
});
