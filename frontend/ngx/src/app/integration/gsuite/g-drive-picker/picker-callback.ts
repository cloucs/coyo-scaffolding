/**
 * Model for carrying a selected element coming from the Google filepicker.
 */
export interface PickerCallback {
  id: string;
  url: string;
  displayName: string;
}
