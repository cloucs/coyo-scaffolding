import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {Observable} from 'rxjs';
import {flatMap, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SharePointApiService {
  static readonly TOKEN_ENDPOINT: string = '/web/sharepoint/token';

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Creates a GET request to the SharePoint API and adds the needed headers
   * @param sharePointUrl the url of the SharePoint target (e.g. https://your-key.sharepoint.com)
   * @param endpoint the endpoint of the GraphAPI (with leading slash)
   * @returns an Observable with the data as rows
   */
  get(sharePointUrl: string, endpoint: string): Observable<Row[]> {
    return this.getSharePointToken().pipe(flatMap(token => {
      const url = `${sharePointUrl}/_api${endpoint}`;
      const options = {headers: {Authorization: 'Bearer ' + token}};
      return this.httpClient
        .get<any>(url, options).pipe(
          map(response => _.get(response, 'PrimaryQueryResult.RelevantResults.Table.Rows', []))
        );
    }));
  }

  private getSharePointToken(): Observable<string> {
    return this.httpClient
      .get<{ token: string }>(SharePointApiService.TOKEN_ENDPOINT)
      .pipe(map(response => response.token));
  }
}

export interface Row {
  Cells: [{
    Key: string;
    Value: string;
  }];
}
