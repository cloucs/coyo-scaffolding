/**
 * Represents a SharePoint site
 */
export interface Site {
  id: string;
  displayName: string;
  lastModifiedDateTime: string;
  webUrl?: string;
}
