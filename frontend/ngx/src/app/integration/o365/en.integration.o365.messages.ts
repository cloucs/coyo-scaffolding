import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'INTEGRATION.O365.FILEPICKER.SEARCH_FOR_FILES' : 'Search in SharePoint',
    'RECENT_FOLDER.NAME' : 'Recent'
  }
};
