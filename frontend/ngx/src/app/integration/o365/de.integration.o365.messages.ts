import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'INTEGRATION.O365.FILEPICKER.SEARCH_FOR_FILES' : 'In SharePoint suchen',
    'RECENT_FOLDER.NAME' : 'Zuletzt bearbeitet'
  }
};
