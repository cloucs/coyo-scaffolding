import {fakeAsync} from '@angular/core/testing';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {Drive} from '@app/integration/o365/o365-api/domain/drive';
import {DriveFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/drive-filepicker-item';
import {DriveItemFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/drive-item-filepicker-item';
import {of} from 'rxjs';

describe('DriveFilepickerItem', () => {
  let drive: Drive;
  let fetchDriveItems: jasmine.Spy;
  let filePickerItem: FilepickerItem;

  beforeEach(() => {
    drive = {id: '1', name: 'drive', lastModifiedDateTime: ''};
    fetchDriveItems = jasmine.createSpy();
    filePickerItem = new DriveFilepickerItem(drive, fetchDriveItems);
  });

  it('should initialize correctly', () => {
    expect(filePickerItem.name).toBe(drive.name);
    expect(filePickerItem.isFolder).toBe(true);
  });

  it('should get folders and files as children', fakeAsync(() => {
    // given
    const driveItems: DriveItemFilepickerItem[] = [];
    fetchDriveItems.and.returnValue(of(driveItems));

    // when/then
    filePickerItem.getChildren().subscribe(
      items => {
        expect(fetchDriveItems).toHaveBeenCalledWith(drive.id);
        expect(items).toBe(driveItems);
      },
      error => fail()
    );
  }));
});
