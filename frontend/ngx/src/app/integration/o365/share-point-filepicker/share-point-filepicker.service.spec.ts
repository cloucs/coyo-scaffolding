import {TestBed} from '@angular/core/testing';
import {FilepickerService} from '@app/filepicker/filepicker.service';
import {O365ApiService} from '@app/integration/o365/o365-api/o365-api.service';
import {TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {SharePointFilepickerService} from './share-point-filepicker.service';
import SpyObj = jasmine.SpyObj;

describe('SharePointFilepickerService', () => {
  let service: SharePointFilepickerService;
  let o365ApiService: SpyObj<O365ApiService>;
  let filePickerService: SpyObj<FilepickerService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: FilepickerService,
          useValue: jasmine.createSpyObj('FilePickerService', ['openFilepicker'])
        },
        {
          provide: O365ApiService,
          useValue: jasmine.createSpyObj('O365ApiService', ['getSites', 'getRecentFiles', 'getDrivesBySiteId', 'getDriveItems'])
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('TranslateService', ['instant'])
        }
      ]
    });

    service = TestBed.get(SharePointFilepickerService);
    o365ApiService = TestBed.get(O365ApiService);
    filePickerService = TestBed.get(FilepickerService);
  });

  beforeEach(() => {
    o365ApiService.getRecentFiles.and.returnValue(of([]));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
