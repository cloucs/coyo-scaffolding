import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AccessToken} from '@app/integration/gsuite/google-api/access-token';
import {AuthService} from '@core/auth/auth.service';
import {SettingsService} from '@domain/settings/settings.service';
import * as _ from 'lodash';
import {concat, from, Observable, of} from 'rxjs';
import {catchError, every, map, shareReplay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IntegrationApiService {
  private TOKEN_ENDPOINT: string = '/web/sso/token';

  constructor(private settingsService: SettingsService,
              private http: HttpClient,
              private authService: AuthService) {
  }

  updateAndGetActiveState(integrationType: string): Observable<boolean> {
    return concat(
        this.isIntegrationSettingActiveFor(integrationType),
        this.hasUserPermissionToUseIntegration(integrationType),
        this.isUserLoggedInWithOauthAccount()
    ).pipe(
        every(bool => bool),
        catchError(() => of(false)),
        shareReplay(1)
    );
  }

  private isIntegrationSettingActiveFor(typeOfIntegration: string): Observable<boolean> {
    return from(this.settingsService.retrieveByKey('integrationType'))
        .pipe(map(integrationType => integrationType === typeOfIntegration));
  }

  private hasUserPermissionToUseIntegration(integrationType: string): Observable<boolean> {
    const permissionName = 'ACCESS_' + integrationType + '_INTEGRATION';
    return this.authService
        .getUser()
        .pipe(map(user => _.includes(user.globalPermissions, permissionName)));
  }

  private isUserLoggedInWithOauthAccount(): Observable<boolean> {
    return this.getToken()
        .pipe(
            map(() => true),
            catchError(() => of(false)));
  }

  /**
   * Fetches an OAuth token.
   *
   * @returns an observable of the access token
   */
  getToken(): Observable<AccessToken> {
    const httpOptions = {
      headers: new HttpHeaders({
        handleErrors: 'false'
      })
    };
    return this.http.get<AccessToken>(this.TOKEN_ENDPOINT, httpOptions);
  }

  isIntegrationApiActiveFor(integrationType: string): Observable<boolean> {
    return this.updateAndGetActiveState(integrationType);
  }
}
