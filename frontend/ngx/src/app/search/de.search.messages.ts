import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'MODULE.SEARCH.EXTERNAL.HEADLINE.RESULTS_ON': ' Treffer in ',
    'MODULE.SEARCH.EXTERNAL.HEADLINE.VIEW_ALL_ON.GOOGLE': 'Alle in Google Drive ansehen',
    'MODULE.SEARCH.EXTERNAL.HEADLINE.VIEW_ALL_ON.GOOGLE.ARIA': 'Alle Suchergebnisse in Google Drive ansehen',
    'MODULE.SEARCH.EXTERNAL.HEADLINE.WE_ALSO_FOUND': 'Es gibt',
    'MODULE.SEARCH.EXTERNAL.MODIFIED_DATE.ARIA': 'Zuletzt bearbeitet ',
    'MODULE.SEARCH.EXTERNAL.PROVIDER.GOOGLE': 'Google Drive'
  }
};
