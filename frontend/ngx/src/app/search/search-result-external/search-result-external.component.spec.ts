import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SearchResultExternalComponent} from './search-result-external.component';

describe('SearchResultExternalComponent', () => {
  let component: SearchResultExternalComponent;
  let fixture: ComponentFixture<SearchResultExternalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchResultExternalComponent ]
    })
    .overrideTemplate(SearchResultExternalComponent, '')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultExternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
