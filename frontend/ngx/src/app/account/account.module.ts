import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {messagesDe} from './de.account.messages';
import {messagesEn} from './en.account.messages';
import {IntegrationSettingsComponent} from './integration-settings/integration-settings.component';
import './integration-settings/integration-settings.component.downgrade';

/**
 * Module which contains user setting related components.
 */
@NgModule({
  imports: [
    CommonModule,
    CoyoFormsModule,
    TranslateModule
  ],
  declarations: [
    IntegrationSettingsComponent
  ],
  entryComponents: [
    IntegrationSettingsComponent
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn},
  ]
})
export class AccountModule {}
