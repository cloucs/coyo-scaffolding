import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.IMAGE.DESCRIPTION': 'Zeig ein einzelnes Bild an.',
    'WIDGET.IMAGE.NAME': 'Bild'
  }
};
