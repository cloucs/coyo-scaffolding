import {Widget} from '@domain/widget/widget';
import {ImageWidgetSettings} from '@widgets/image/image-widget-settings.model';

export interface ImageWidget extends Widget<ImageWidgetSettings> {
}
