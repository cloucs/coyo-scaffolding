import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of an iframe widget
 */
export interface IframeWidgetSettings extends WidgetSettings {
  url: string;
  height: number;
  scrolling: boolean;
}
