import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.BUTTON.NAME': 'Link Button',
    'WIDGET.BUTTON.DESCRIPTION': 'Ein Button, der Benutzer zu einer URL weiterleitet. Den Button kannst du nach deinen Wünschen designen.',
    'WIDGET.BUTTON.SETTINGS.TEXT': 'Titel',
    'WIDGET.BUTTON.SETTINGS.TARGET': 'Link in neuem Browser-Tab öffnen',
    'WIDGET.BUTTON.SETTINGS.STYLE': 'Stil',
    'WIDGET.BUTTON.SETTINGS.STYLE.DEFAULT': 'Standard',
    'WIDGET.BUTTON.SETTINGS.STYLE.PRIMARY': 'Primär',
    'WIDGET.BUTTON.SETTINGS.STYLE.SUCCESS': 'Erfolg',
    'WIDGET.BUTTON.SETTINGS.STYLE.INFO': 'Info',
    'WIDGET.BUTTON.SETTINGS.STYLE.WARNING': 'Warnung',
    'WIDGET.BUTTON.SETTINGS.STYLE.DANGER': 'Achtung',
    'WIDGET.BUTTON.SETTINGS.URL': 'URL',
    'WIDGET.BUTTON.SETTINGS.URL.PLACEHOLDER': 'https://'
  }
};
