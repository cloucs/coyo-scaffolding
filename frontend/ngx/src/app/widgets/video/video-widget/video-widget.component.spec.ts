import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {OembedService} from '@domain/oembed/oembed.service';
import {VideoWidget} from '../video-widget';
import {VideoWidgetComponent} from './video-widget.component';

describe('VideoWidgetComponent', () => {
  let component: VideoWidgetComponent;
  let fixture: ComponentFixture<VideoWidgetComponent>;
  let oembedService: jasmine.SpyObj<OembedService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoWidgetComponent ],
      providers: [
        {provide: OembedService, useValue: jasmine.createSpyObj('oembedService', ['createByUrl'])}
      ]
    }).overrideTemplate(VideoWidgetComponent, '')
    .compileComponents();

    oembedService = TestBed.get(OembedService);
    oembedService.createByUrl.and.returnValue('<iframe webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" ' +
    'src="//www.youtube-nocookie.com/embed/Bh4x2jCWsB4" width="450" height="252.00000000000003"></iframe>');
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {settings: {
      description: 'Coyo video',
      title: 'Coyo',
      _url: 'https://www.youtube.com/watch?v=Bh4x2jCWsB4',
      _backendData: {
        videoUrl: 'https://www.youtube.com/embed/Bh4x2jCWsB4?feature=oembed',
        ratio: 56.25
      }
    }} as VideoWidget;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given
    // when
    component.ngOnInit();

    // then
    expect(component.videoUrl).toEqual(component.widget.settings._backendData.videoUrl);
    expect(component.ratio).toEqual(component.widget.settings._backendData.ratio);
  });

  it('should init (create videoHtml)', () => {
    // given
    component.widget = {settings: {
      description: 'Coyo video',
      title: 'Coyo',
      _url: 'https://www.youtube.com/watch?v=Bh4x2jCWsB4'
    }} as VideoWidget;

    // when
    component.ngOnInit();

    // then
    expect(oembedService.createByUrl).toHaveBeenCalledWith(component.widget.settings._url, fixture.elementRef.nativeElement);
    expect(component.videoHtml).toEqual(oembedService.createByUrl(component.widget.settings._url, fixture.elementRef.nativeElement));
    expect(component.ratio).toBe(56.25);
  });

  it('should init (error)', () => {
    // given
    component.widget = {settings: {
      description: 'Coyo video',
      title: 'Coyo'
    }} as VideoWidget;
    oembedService.createByUrl.and.returnValue(undefined);

    // when
    component.ngOnInit();

    // then
    expect(oembedService.createByUrl).toHaveBeenCalledWith(undefined, fixture.elementRef.nativeElement);
    expect(component.error).toBeTruthy();
  });
});
