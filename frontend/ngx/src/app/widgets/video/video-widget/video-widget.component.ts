import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnChanges, OnInit} from '@angular/core';
import {OembedService} from '@domain/oembed/oembed.service';
import {WidgetComponent} from '@widgets/api/widget-component';
import {VideoWidget} from '../video-widget';

@Component({
  selector: 'coyo-video-widget',
  templateUrl: './video-widget.component.html',
  styleUrls: ['./video-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VideoWidgetComponent extends WidgetComponent<VideoWidget> implements OnInit, OnChanges {
  videoUrl: string;
  ratio: number;
  error: boolean;
  videoHtml: HTMLElement;

  constructor(cd: ChangeDetectorRef, private oembedService: OembedService,
              private elementRef: ElementRef<HTMLElement>) {
    super(cd);
   }

  ngOnInit(): void {
    if (this.widget.settings._backendData) {
      this.videoUrl = this.widget.settings._backendData.videoUrl;
      this.ratio = this.widget.settings._backendData.ratio;
    } else {
      const url = this.widget.settings._url;
      const container = this.elementRef.nativeElement;
      const videoHtml = this.oembedService.createByUrl(url, container);
      if (videoHtml) {
        this.videoHtml = videoHtml;
        this.ratio = 56.25;
      } else {
        this.error = true;
      }
    }
  }

  ngOnChanges(): void {
    this.videoUrl = this.widget.settings._backendData.videoUrl;
  }
}
