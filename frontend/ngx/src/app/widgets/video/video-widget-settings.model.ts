import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entitiy model for the settings of a video widget
 */
export interface VideoWidgetSettings extends WidgetSettings {
  _url: string;
  title?: string;
  description?: string;
  _backendData?: {
    videoUrl: string;
    ratio: number;
  };
}
