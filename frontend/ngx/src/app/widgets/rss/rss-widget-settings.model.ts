import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a RSS widget
 */
export interface RssWidgetSettings extends WidgetSettings {
  rssUrl: string;
  userName: string;
  _encrypted_password: string;
  maxCount: number;
  displayImage: boolean;
}
