import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {WidgetComponent} from '@widgets/api/widget-component';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {RssEntry} from '@widgets/rss/rss-entry.model';
import {RssWidget} from '@widgets/rss/rss-widget';
import {RssWidgetService} from '@widgets/rss/rss-widget.service';
import {Observable} from 'rxjs';

/**
 * This widget displays entries of a RSS feed.
 */
@Component({
  templateUrl: './rss-widget.component.html',
  styleUrls: ['./rss-widget.component.scss']
})
export class RssWidgetComponent extends WidgetComponent<RssWidget> implements OnInit {

  entries$: Observable<RssEntry[]>;

  constructor(private rssWidgetService: RssWidgetService,
              private widgetSettingsService: WidgetSettingsService,
              cd: ChangeDetectorRef) {
    super(cd);
  }

  ngOnInit(): void {
    this.loadRssEntries();

    this.widgetSettingsService.getChanges$(this.widget.id).subscribe(() => {
      this.loadRssEntries();
      super.detectChanges();
    });
  }

  /**
   * Retrieves the entries of a RSS feed.
   */
  loadRssEntries(): void {
    if (!this.widget.id || this.editMode) {
      this.entries$ = this.rssWidgetService.getEntriesForPreview(this.widget.settings.rssUrl,
        this.widget.settings.displayImage, this.widget.settings.maxCount, this.widget.settings.userName,
        this.widget.settings._encrypted_password);
    } else {
      this.entries$ = this.rssWidgetService.getEntries(this.widget.id);
    }
  }
}
