import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {RssEntry} from '@widgets/rss/rss-entry.model';
import {RssWidget} from '@widgets/rss/rss-widget';
import {RssWidgetService} from '@widgets/rss/rss-widget.service';
import {RssWidgetComponent} from '@widgets/rss/rss-widget/rss-widget.component';
import {defer, of} from 'rxjs';

describe('RssWidgetComponent', () => {

  const rssEntry: RssEntry = {
    title: 'title-1', description: 'description-1', publishedDate: new Date('2019-11-12T12:00:00'),
    feedUri: 'http://rssfeed.uri', imageUrl: 'http://some.site/image1.jpg'
  };
  const rssEntries = [rssEntry];

  let component: RssWidgetComponent;
  let fixture: ComponentFixture<RssWidgetComponent>;
  let rssWidgetService: jasmine.SpyObj<RssWidgetService>;
  let widgetSettingsService: jasmine.SpyObj<WidgetSettingsService>;

  let widget: RssWidget;

  beforeEach(async(() => {
    // noinspection JSIgnoredPromiseFromCall
    TestBed.configureTestingModule({
      declarations: [RssWidgetComponent],
      providers: [FormBuilder, {
        provide: RssWidgetService,
        useValue: jasmine.createSpyObj('rssWidgetService', ['getEntries', 'getEntriesForPreview'])
      }, {
        provide: WidgetSettingsService,
        useValue: jasmine.createSpyObj('widgetSettingsService', ['getChanges$'])
      }]
    }).overrideTemplate(RssWidgetComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    widget = {
      id: 'widget-id',
      settings: {
        rssUrl: 'https://some.site/rss/feed',
        userName: 'John Doe',
        _encrypted_password: 'secret',
        maxCount: 3,
        displayImage: true
      }
    };

    fixture = TestBed.createComponent(RssWidgetComponent);
    component = fixture.componentInstance;
    component.widget = widget;
    rssWidgetService = TestBed.get(RssWidgetService);
    widgetSettingsService = TestBed.get(WidgetSettingsService);
    widgetSettingsService.getChanges$.and.returnValue(of({}));
    rssWidgetService.getEntries.and.returnValue(defer(() => Promise.resolve(rssEntries)));
    fixture.detectChanges();
  });

  it('should initially load the RSS entries', async(async() => {
    expect(widgetSettingsService.getChanges$).toHaveBeenCalledWith('widget-id');
    expect(rssWidgetService.getEntries).toHaveBeenCalledWith('widget-id');
    await assertActualRssEntries();
  }));

  it('should load the preview RSS entries if no widget Id is given', async(async() => {
    // Given
    widget.id = undefined;
    rssWidgetService.getEntriesForPreview.and.returnValue(defer(() => Promise.resolve(rssEntries)));
    rssWidgetService.getEntries.calls.reset();

    // When
    component.loadRssEntries();

    // then
    await assertActualRssEntriesFromPreview();
  }));

  it('should load the preview RSS entries if in edit mode', async(async() => {
    // Given
    component.editMode = true;
    rssWidgetService.getEntriesForPreview.and.returnValue(defer(() => Promise.resolve(rssEntries)));
    rssWidgetService.getEntries.calls.reset();

    // When
    component.loadRssEntries();

    // then
    await assertActualRssEntriesFromPreview();
  }));

  async function assertActualRssEntriesFromPreview(): Promise<void> {
    expect(rssWidgetService.getEntriesForPreview).toHaveBeenCalledWith(
      'https://some.site/rss/feed', true, 3, 'John Doe', 'secret');
    expect(rssWidgetService.getEntries).not.toHaveBeenCalled();
    await assertActualRssEntries();
  }

  async function assertActualRssEntries(): Promise<void> {
    let actualRssEntries;
    component.entries$.subscribe(entries => {
      actualRssEntries = entries;
    });
    await fixture.whenStable();
    fixture.detectChanges();
    // noinspection JSUnusedAssignment
    expect(actualRssEntries).toBe(rssEntries);
  }
});
