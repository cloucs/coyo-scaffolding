export interface RssWidgetVerifyUrlResponse {
  validationMessage: string;
  valid: boolean;
}
