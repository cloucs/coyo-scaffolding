import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  ViewChild
} from '@angular/core';
import {Pageable} from '@domain/pagination/pageable';
import {WidgetSettingsComponent} from '@widgets/api/widget-settings-component';
import {BlogArticleWidget, SingleBlogArticle} from '@widgets/blog-article/blog-article-widget';
import {BlogArticleWidgetService} from '@widgets/blog-article/blog-article-widget.service';
import * as _ from 'lodash';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'coyo-blog-article-widget-settings',
  templateUrl: './blog-article-widget-settings.component.html',
  styleUrls: ['./../blog-article-widget/blog-article-widget.component.global.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BlogArticleWidgetSettingsComponent extends WidgetSettingsComponent<BlogArticleWidget> implements OnInit {

  @ViewChild('inputParent', {
    static: false,
    read: ElementRef
  }) inputParent: ElementRef<HTMLElement>;
  loading: boolean = false;
  articles: SingleBlogArticle[] = [];
  preFetchedArticlesSize: number = 20;
  term: string = '';
  termEvent: EventEmitter<string> = new EventEmitter<string>(true);
  private lastPageReached: boolean = false;
  private nextPage: number = 0;

  constructor(private blogArticleWidgetService: BlogArticleWidgetService,
              private cd: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this.loadNextPage();
  }

  updateArticle(article: SingleBlogArticle): void {
    this.widget.article = article;
    this.widget.settings._articleId = article.id;
    this.inputParent.nativeElement.focus();
  }

  onOpen(): void {
    if (this.nextPage === 0) {
      this.loadNextPage();
    }
  }

  onScroll(event$: { end: number }): void {
    if (this.loading || this.articles.length <= this.preFetchedArticlesSize || this.lastPageReached) {
      return;
    }

    if (event$.end + this.preFetchedArticlesSize >= this.articles.length) {
      this.loadNextPage();
    }
  }

  onScrollToEnd(): void {
    if (this.loading || !this.lastPageReached) {
      this.loadNextPage();
    }
  }

  search($event: { term: string, items: SingleBlogArticle[] }): void {
    if (_.isEmpty($event.term)) {
      this.clear(true);
    } else {
      this.termEvent.emit($event.term);
      this.termEvent.asObservable().pipe(debounceTime(300)).subscribe(value => {
        this.clear(true, value);
      });
    }
  }

  clear(withReload: boolean = false, term: string = ''): void {
    this.lastPageReached = false;
    this.term = term;
    this.nextPage = 0;
    this.articles = [];
    if (withReload) {
      this.loadNextPage();
    }
    this.cd.detectChanges();
  }

  private loadNextPage(): void {
    if ((this.loading || this.lastPageReached)) {
      return;
    }
    this.loadArticles(this.nextPage, 20);
  }

  private loadArticles(page: number, pageSize: number): void {
    this.loading = true;
    this.blogArticleWidgetService.getArticles(new Pageable(page, pageSize), this.term).subscribe(result => {
      if (this.nextPage > 0 && (_.isEmpty(result.content) || result.content.length < pageSize)) {
        this.lastPageReached = true;
      }
      this.nextPage++;
      this.articles = _.union(this.articles, result.content);
      this.loading = false;
      this.cd.detectChanges();
    });
  }
}
