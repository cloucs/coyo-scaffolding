import {AfterContentInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Target} from '@domain/sender/target';
import {TargetService} from '@domain/sender/target/target.service';
import {WidgetComponent} from '@widgets/api/widget-component';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {
  BlogArticleWidget,
  BlogArticleWidgetSettings,
  SingleBlogArticle
} from '@widgets/blog-article/blog-article-widget';
import {BlogArticleWidgetService} from '@widgets/blog-article/blog-article-widget.service';
import {flatMap} from 'rxjs/operators';

@Component({
  selector: 'coyo-blog-article-widget',
  templateUrl: './blog-article-widget.component.html',
  styleUrls: ['./blog-article-widget.component.global.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BlogArticleWidgetComponent extends WidgetComponent<BlogArticleWidget> implements OnInit, AfterContentInit {

  constructor(private blogArticleWidgetService: BlogArticleWidgetService,
              private targetService: TargetService, cd: ChangeDetectorRef,
              private widgetSettingsService: WidgetSettingsService) {
    super(cd);
  }

  ngOnInit(): void {
    this.blogArticleWidgetService.getArticleById(this.widget.settings._articleId)
        .subscribe((article: SingleBlogArticle) => {
          this.widget.article = article;
          super.detectChanges();
        });
  }

  ngAfterContentInit(): void {
    this.widgetSettingsService.getChanges$(this.widget.id || this.widget['tempId']).pipe(
        flatMap((settings: BlogArticleWidgetSettings) => this.blogArticleWidgetService.getArticleById(settings._articleId))
    ).subscribe((article: SingleBlogArticle) => {
      this.widget.article = article;
      super.detectChanges();
    });
  }

  isLoading(): boolean {
    return this.widget.article === undefined;
  }

  getLink(target: Target): string {
    return this.targetService.getLinkTo(target);
  }

}
