import {Target} from '@domain/sender/target';
import {Widget} from '@domain/widget/widget';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

export class SingleBlogArticle {
  appDisplayname: string;
  appName: string;
  appTarget: Target;
  articleTarget: Target;
  created: number;
  id: string;
  modified: number;
  published: number;
  senderName: string;
  senderTarget: Target;
  teaserImageWideFileId: string;
  teaserImageWideSenderId: string;
  teaserText: string;
  title: string;

}

export interface BlogArticleWidgetSettings extends WidgetSettings {
  _articleId: string;
}

export interface BlogArticleWidget extends Widget<BlogArticleWidgetSettings> {
  article: SingleBlogArticle;
}
