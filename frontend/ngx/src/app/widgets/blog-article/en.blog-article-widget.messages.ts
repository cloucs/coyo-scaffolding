import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGETS.BLOGARTICLE.DESCRIPTION': 'Displays a single blog article.',
    'WIDGETS.BLOGARTICLE.IN': 'in',
    'WIDGETS.BLOGARTICLE.NAME': 'Blog article',
    'WIDGETS.BLOGARTICLE.NO_ARTICLES': 'No articles found.',
    'WIDGETS.BLOGARTICLE.NOTHING.SELECTED': 'Please select blog article.',
    'WIDGETS.BLOGARTICLE.READMORE': 'Read more',
    'WIDGETS.BLOGARTICLE.SETTINGS.BLOG_ARTICLE': 'Blog article',
    'WIDGETS.BLOGARTICLE.SETTINGS.BLOG_ARTICLE.HELP': 'Select the blog article.',
    'WIDGETS.BLOGARTICLE.SETTINGS.PERMISSION_ERROR': 'You don\'t have sufficient permissions.'
  }
};
