import {Input, OnDestroy, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Modal} from '@domain/modal/modal';
import {Widget} from '@domain/widget/widget';
import {UIRouter} from '@uirouter/angular';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import * as _ from 'lodash';
import {BsModalRef} from 'ngx-bootstrap';
import {Subject} from 'rxjs';
import {WidgetConfig} from './widget-config';
import {WidgetSettingsContainerComponent} from './widget-settings-container/widget-settings-container.component';

/**
 * A Modal object for a widgets
 */
export class WidgetModal extends Modal<{ config: WidgetConfig<Widget<WidgetSettings>>; settings: any; }> implements OnDestroy {
  widgetForm: FormGroup;

  // legacy ng1
  ng1WidgetForm: any;
  widget: any;
  callbacks: {onBeforeSave: any} = {
    onBeforeSave: (): Promise<any> => Promise.resolve()
  };

  // ngx
  /**
   * The configuration of the widget
   */
  @Input() config: WidgetConfig<Widget<WidgetSettings>>;
  onSubmitSubject: Subject<any> = new Subject<any>();

  @ViewChild(WidgetSettingsContainerComponent, {static: false})
  settingsContainer: WidgetSettingsContainerComponent;

  constructor(public modal: BsModalRef, uiRouter: UIRouter) {
    super(modal, uiRouter);
    this.rebuildForm();
  }

  /**
   * Resets the form
   */
  rebuildForm(): void {
    this.widgetForm = new FormGroup({});
  }

  /**
   * Submits the form
   *
   * @param settings
   * The newly defined settings of the widget
   */
  submit(settings?: any): void {
    if (this.widgetForm.invalid || (this.ng1WidgetForm && this.ng1WidgetForm.$invalid)) {
      return;
    }
    const onBeforeSave = this.settingsContainer ? this.settingsContainer.widgetSettingsComponent.onBeforeSave(settings) : null;
    const callback = onBeforeSave ? onBeforeSave.toPromise() : this.callbacks.onBeforeSave();
    callback.then((updatedSettings: any) => {
      const newSettings = updatedSettings || settings;

      // ngx save callback
      this.onSubmitSubject.next(newSettings);

      // legacy ng1 save callback
      const allSettings = _.extend(this.widget.settings, newSettings);

      this.onClose.next({
        config: this.config,
        settings: allSettings
      });
      this.modal.hide();
    });
  }

  /**
   * Method called when the ng1 form is submitted
   *
   * @param form
   * The form data
   */
  onLegacyFormSet(form: any): void {
    this.ng1WidgetForm = form;
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.onSubmitSubject.complete();
  }
}
