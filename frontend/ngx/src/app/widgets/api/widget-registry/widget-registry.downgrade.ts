import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {WidgetRegistryService} from './widget-registry.service';

getAngularJSGlobal()
  .module('coyo.widgets.api')
  .factory('ngxWidgetRegistry', downgradeInjectable(WidgetRegistryService) as any);
