import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Ng1ScrollBehaviourService} from '@root/typings';
import {UIRouter} from '@uirouter/core';
import {NG1_SCROLL_BEHAVIOR_SERVICE} from '@upgrade/upgrade.module';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {of} from 'rxjs';
import {WidgetSettingsModalComponent} from './widget-settings-modal.component';

describe('WidgetSettingsModalComponent', () => {
  let component: WidgetSettingsModalComponent;
  let fixture: ComponentFixture<WidgetSettingsModalComponent>;
  let scrollBehaviourService: jasmine.SpyObj<Ng1ScrollBehaviourService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WidgetSettingsModalComponent],
      providers: [{
        provide: BsModalRef,
        useValue: jasmine.createSpyObj('BsModalRef', ['modal'])
      }, {
        provide: UIRouter, useValue: {globals: {start$: of()}}
      }, {
        provide: NG1_SCROLL_BEHAVIOR_SERVICE, useValue: jasmine.createSpyObj('scrollBehaviourService',
          ['enableBodyScrolling', 'disableBodyScrolling', 'disableBodyScrollingOnXsScreen'])
      }]
    }).overrideTemplate(WidgetSettingsModalComponent, '<div></div>')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetSettingsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    scrollBehaviourService = TestBed.get(NG1_SCROLL_BEHAVIOR_SERVICE);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
