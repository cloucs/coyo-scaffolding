import {Injectable, NgZone} from '@angular/core';
import {environment} from '@root/environments/environment';
import * as _ from 'lodash';
import {BsModalService} from 'ngx-bootstrap/modal';
import {first} from 'rxjs/operators';
import {WidgetRegistryService} from '../widget-registry/widget-registry.service';
import {WidgetSettingsModalComponent} from './widget-settings-modal.component';

/**
 * A service that opens a {@link WidgetSettingsModalComponent} modal to configure widgets.
 */
@Injectable()
export class WidgetSettingsModalService {

  constructor(private modalService: BsModalService,
              private widgetRegistry: WidgetRegistryService,
              private ngZone: NgZone) {
  }

  /**
   * Opens a {@link WidgetSettingsModalComponent} to select and configure widgets.
   *
   * @param widget the widget to open the settings for
   * @returns a promise containing the widget data
   */
  open(widget: any): Promise<any> {
    return this.ngZone.run(() => {
      const modal = this.modalService.show(WidgetSettingsModalComponent, {
        animated: environment.enableAnimations,
        class: 'modal-lg'
      });
      modal.content.config = this.widgetRegistry.get(widget.key);
      modal.content.widget = _.cloneDeep(widget);
      return modal.content.onClose.pipe(first()).toPromise();
    });
  }
}
