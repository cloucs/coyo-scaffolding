import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.SUBSCRIPTIONS.DESCRIPTION': 'Displays the current user\'s subscriptions to pages and workspaces.',
    'WIDGET.SUBSCRIPTIONS.EMPTY.PAGE': 'You are not subscribed to any pages.',
    'WIDGET.SUBSCRIPTIONS.EMPTY.WORKSPACE': 'You are not subscribed to any workspaces.',
    'WIDGET.SUBSCRIPTIONS.FAVORITE.SET': 'Add to favorites',
    'WIDGET.SUBSCRIPTIONS.FAVORITE.UNSET': 'Remove from favorites',
    'WIDGET.SUBSCRIPTIONS.MORE.PAGE': 'Show more',
    'WIDGET.SUBSCRIPTIONS.MORE.PAGE.ARIA': 'Show more pages',
    'WIDGET.SUBSCRIPTIONS.MORE.WORKSPACE': 'Show more',
    'WIDGET.SUBSCRIPTIONS.MORE.WORKSPACE.ARIA': 'Show more workspaces',
    'WIDGET.SUBSCRIPTIONS.NAME': 'Subscriptions',
    'WIDGET.SUBSCRIPTIONS.PAGE': 'Pages',
    'WIDGET.SUBSCRIPTIONS.PAGE.ARIA': 'Subscribed pages',
    'WIDGET.SUBSCRIPTIONS.WORKSPACE': 'Workspaces',
    'WIDGET.SUBSCRIPTIONS.WORKSPACE.ARIA': 'Subscribed workspaces'
  }
};
