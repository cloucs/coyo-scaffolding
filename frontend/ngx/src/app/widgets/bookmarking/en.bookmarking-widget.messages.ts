import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGETS.BOOKMARKING.ADD_BOOKMARK': 'Add bookmark',
    'WIDGETS.BOOKMARKING.DESCRIPTION': 'Displays a list of bookmarks.',
    'WIDGETS.BOOKMARKING.NAME': 'Bookmarks',
    'WIDGETS.BOOKMARKING.NO_BOOKMARKS': 'Currently there are no bookmarks',
    'WIDGETS.BOOKMARKING.TITLE.PLACEHOLDER': 'Enter a label',
    'WIDGETS.BOOKMARKING.TITLE.ARIA': 'Bookmark label',
    'WIDGETS.BOOKMARKING.URL.ARIA': 'Bookmark URL',
    'WIDGETS.BOOKMARKING.URL.INVALID': 'The entered URL is invalid',
    'WIDGETS.BOOKMARKING.EDIT.TOGGLE_FROM.URL': 'Switch to bookmark label',
    'WIDGETS.BOOKMARKING.EDIT.TOGGLE_FROM.TITLE': 'Switch to bookmark URL',
    'WIDGETS.BOOKMARKING.EDIT.NEXT_FROM.URL': 'Proceed to input of bookmark URL',
    'WIDGETS.BOOKMARKING.EDIT.NEXT_FROM.TITLE': 'Add bookmark',
    'WIDGETS.BOOKMARKING.EDIT.REMOVE': 'Delete bookmark',
    'WIDGETS.BOOKMARKING.EDIT.DRAG_REORDER': 'Re-order (keep mouse-button pressed and drag to new position)'
  }
};
