import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.HEADLINE.DESCRIPTION': 'Displays a single headline which size is customizable.',
    'WIDGET.HEADLINE.NAME': 'Headline',
    'WIDGET.HEADLINE.PLACEHOLDER': 'Headline...'
  }
};
