/**
 * A size option of a headline widget
 */
export interface SizeOption {
  size: string;
  previewClass: string;
}
