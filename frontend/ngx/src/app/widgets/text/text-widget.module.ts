import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {HashtagModule} from '@shared/hashtags/hashtag.module';
import {MarkdownModule} from '@shared/markdown/markdown.module';
import {WIDGET_CONFIGS} from '@widgets/api/widget-config';
import {TEXT_WIDGET} from '@widgets/text/text-widget-config';
import {AutosizeModule} from 'ngx-autosize';
import {messagesDe} from './de.text-widget.messages';
import {messagesEn} from './en.text-widget.messages';
import {TextWidgetComponent} from './text-widget/text-widget.component';

/**
 * Module providing the text widget
 */
@NgModule({
  imports: [
    AutosizeModule,
    CoyoCommonsModule,
    CoyoFormsModule,
    HashtagModule,
    MarkdownModule,
    TranslateModule
  ],
  declarations: [
    TextWidgetComponent
  ],
  entryComponents: [
    TextWidgetComponent
  ],
  providers: [
    {provide: WIDGET_CONFIGS, useValue: TEXT_WIDGET, multi: true},
    {provide: 'messages', useValue: messagesDe, multi: true},
    {provide: 'messages', useValue: messagesEn, multi: true}
  ]
})

export class TextWidgetModule {}
