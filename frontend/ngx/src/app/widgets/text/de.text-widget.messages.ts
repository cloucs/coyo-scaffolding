import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.TEXT.DESCRIPTION': 'Zeigt einen Text mit optionalem Titel an.',
    'WIDGET.TEXT.NAME': 'Text',
    'WIDGET.TEXT.PLACEHOLDER': 'Gib hier den Text ein…'
  }
};
