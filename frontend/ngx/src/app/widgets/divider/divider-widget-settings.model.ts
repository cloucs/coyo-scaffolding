import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a divider widget
 */
export interface DividerWidgetSettings extends WidgetSettings {
  text: string;
}
