import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.DIVIDER.DESCRIPTION': 'Zeigt eine einfache Trennlinie an',
    'WIDGET.DIVIDER.HELP.TEXT': 'Gib der Trennlinie einen Titel. Falls es keinen Titel geben soll, lasse das einfach Feld leer.',
    'WIDGET.DIVIDER.NAME': 'Trennlinie',
    'WIDGET.DIVIDER.SETTINGS.TEXT': 'Title'
  }
};
