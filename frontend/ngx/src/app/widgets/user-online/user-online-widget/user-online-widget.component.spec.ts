import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserService} from '@domain/user/user.service';
import {of} from 'rxjs';
import {UserOnlineWidgetComponent} from './user-online-widget.component';

describe('UserOnlineWidgetComponent', () => {
  let component: UserOnlineWidgetComponent;
  let fixture: ComponentFixture<UserOnlineWidgetComponent>;
  let userService: jasmine.SpyObj<UserService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserOnlineWidgetComponent],
      providers: [{
        provide: UserService,
        useValue: jasmine.createSpyObj('userService', ['getUserOnlineCount'])
      }]
    }).overrideTemplate(UserOnlineWidgetComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserOnlineWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    userService = TestBed.get(UserService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init the user online count', () => {
    // given
    const count$ = of(1);
    userService.getUserOnlineCount.and.returnValue(count$);

    // when
    component.ngOnInit();

    // then
    expect(component.count$).toEqual(count$);
    expect(userService.getUserOnlineCount).toHaveBeenCalled();
  });
});
