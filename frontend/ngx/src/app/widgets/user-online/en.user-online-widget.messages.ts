import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.USER_ONLINE.DESCRIPTION': 'Displays the number of all currently online users.',
    'WIDGET.USER_ONLINE.NAME': 'Online users',
    'WIDGET.USER_ONLINE.USERS': '{count, plural, =1{User} other{Users}}',
    'WIDGET.USER_ONLINE.ONLINE': 'Online'
  }
};
