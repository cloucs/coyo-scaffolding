import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.RTE.NAME': 'Rich-Text-Editor',
    'WIDGET.RTE.DESCRIPTION': 'Zeigt Rich-Text mit einem optionalen Titel.'
  }
};
