import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.RTE.NAME': 'Rich text editor',
    'WIDGET.RTE.DESCRIPTION': 'Displays rich text with an optional title.'
  }
};
