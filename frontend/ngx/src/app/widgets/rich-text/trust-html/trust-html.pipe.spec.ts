import { TrustHtmlPipe } from './trust-html.pipe';

describe('TrustHtmlPipe', () => {
  it('should trust passed html', () => {
    const sanitizerMock = jasmine.createSpyObj('sanitizier', ['bypassSecurityTrustHtml']);
    const pipe = new TrustHtmlPipe(sanitizerMock);
    const value = '<p>any html</p>';

    // when
    pipe.transform(value);

    expect(sanitizerMock.bypassSecurityTrustHtml).toHaveBeenCalledWith(value);
  });
});
