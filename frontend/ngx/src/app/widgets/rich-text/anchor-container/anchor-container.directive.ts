import {Directive, HostListener, Inject} from '@angular/core';
import {WINDOW} from '@root/injection-tokens';

@Directive({
  selector: '[coyoAnchorContainer]'
})
export class AnchorContainerDirective {

  constructor(@Inject(WINDOW) private window: Window) { }

  @HostListener('click', ['$event']) onClick($event: MouseEvent): void {
    const target = $event.target as HTMLAnchorElement;
    if (target.getAttribute('coyo-anchor') !== null) {
      // reset to empty hash before setting it to the real value so scrolling works multiple times instead of just once
      this.window.location.hash = '';
      this.window.location.hash = target.hash;
    }
  }
}
