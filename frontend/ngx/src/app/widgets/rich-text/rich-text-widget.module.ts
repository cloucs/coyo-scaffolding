import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {HashtagModule} from '@shared/hashtags/hashtag.module';
import {RteModule} from '@shared/rte/rte.module';
import {WIDGET_CONFIGS} from '@widgets/api/widget-config';
import {messagesDe} from '@widgets/rich-text/de.rich-text-widget.messages';
import {messagesEn} from '@widgets/rich-text/en.rich-text-widget.messages';
import {RICH_TEXT_WIDGET} from '@widgets/rich-text/rich-text-widget-config';
import {AnchorContainerDirective} from './anchor-container/anchor-container.directive';
import {RichTextWidgetComponent} from './rich-text-widget/rich-text-widget.component';
import {TrustHtmlPipe} from './trust-html/trust-html.pipe';
import {YoutubeNoCookiePipe} from './youtube-no-cookie/youtube-no-cookie.pipe';

@NgModule({
  declarations: [RichTextWidgetComponent, YoutubeNoCookiePipe, TrustHtmlPipe, AnchorContainerDirective],
  imports: [
    CoyoCommonsModule,
    HashtagModule,
    RteModule
  ],
  providers: [
    {provide: WIDGET_CONFIGS, useValue: RICH_TEXT_WIDGET, multi: true},
    {provide: 'messages', useValue: messagesDe, multi: true},
    {provide: 'messages', useValue: messagesEn, multi: true}
  ],
  entryComponents: [RichTextWidgetComponent]
})
export class RichTextWidgetModule {
}
