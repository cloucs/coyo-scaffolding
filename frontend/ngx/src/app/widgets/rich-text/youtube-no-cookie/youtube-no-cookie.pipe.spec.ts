import {YoutubeNoCookiePipe} from './youtube-no-cookie.pipe';

describe('YoutubeNoCookiePipe', () => {
  it('should transform youtube url to no cookie url', () => {
    const pipe = new YoutubeNoCookiePipe();
    const originalUrl = 'https://www.youtube.com/embed/1hwuWmMNT4c';
    const expectedUrl = 'https://www.youtube-nocookie.com/embed/1hwuWmMNT4c';

    // when
    const transformedUrl = pipe.transform(originalUrl);

    // then
    expect(transformedUrl).toEqual(expectedUrl);
  });

  it('should not transform no-youtube url to another url', () => {
    const pipe = new YoutubeNoCookiePipe();
    const originalUrl = 'https://core.gocoyo.com/pages/objectives-and-key-results/apps/content/welcome';
    const expectedUrl = 'https://core.gocoyo.com/pages/objectives-and-key-results/apps/content/welcome';

    // when
    const transformedUrl = pipe.transform(originalUrl);

    // then
    expect(transformedUrl).toEqual(expectedUrl);
  });
});
