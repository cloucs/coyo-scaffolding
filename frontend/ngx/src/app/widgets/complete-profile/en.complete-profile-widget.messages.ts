import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.COMPLETE_PROFILE.DESCRIPTION': 'Displays a todo list for completing the user\'s profile',
    'WIDGET.COMPLETE_PROFILE.NAME': 'Complete profile',
    'WIDGET.COMPLETE_PROFILE.TITLE': 'Complete your profile',
    'WIDGET.COMPLETE_PROFILE.AVATAR': 'Upload a profile picture',
    'WIDGET.COMPLETE_PROFILE.COVER': 'Upload a cover picture',
    'WIDGET.COMPLETE_PROFILE.PROFILE_FIELDS': 'Fill in your profile details',
    'WIDGET.COMPLETE_PROFILE.CREATED_POST': 'Post something on your wall',
    'WIDGET.COMPLETE_PROFILE.FOLLOWING_USER': 'Follow a user',
    'WIDGET.COMPLETE_PROFILE.PAGE_MEMBER': 'Follow a page'
  }
};
