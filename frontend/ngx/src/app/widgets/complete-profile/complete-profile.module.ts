import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {WIDGET_CONFIGS} from '@widgets/api/widget-config';
import {COMPLETE_PROFILE_WIDGET} from '@widgets/complete-profile/complete-profile-widget-config';
import {CompleteProfileWidgetComponent} from './complete-profile-widget/complete-profile-widget.component';
import {messagesDe} from './de.complete-profile-widget.messages';
import {messagesEn} from './en.complete-profile-widget.messages';

/**
 * Module providing the complete profile widget.
 */
@NgModule({
  imports: [
    CoyoCommonsModule
  ],
  declarations: [
    CompleteProfileWidgetComponent
  ],
  entryComponents: [
    CompleteProfileWidgetComponent
  ],
  providers: [
    {provide: WIDGET_CONFIGS, useValue: COMPLETE_PROFILE_WIDGET, multi: true},
    {provide: 'messages', useValue: messagesDe, multi: true},
    {provide: 'messages', useValue: messagesEn, multi: true}
  ]
})
export class CompleteProfileModule {
}
