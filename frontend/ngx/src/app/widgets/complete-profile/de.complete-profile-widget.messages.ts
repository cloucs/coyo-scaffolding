import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.COMPLETE_PROFILE.DESCRIPTION': 'Zeigt eine ToDo-Liste zum Vervollständigen des Benutzerprofils an',
    'WIDGET.COMPLETE_PROFILE.NAME': 'Profil vervollständigen',
    'WIDGET.COMPLETE_PROFILE.TITLE': 'Vervollständige dein Profil',
    'WIDGET.COMPLETE_PROFILE.AVATAR': 'Lade ein Profilbild hoch',
    'WIDGET.COMPLETE_PROFILE.COVER': 'Lade ein Titelbild hoch',
    'WIDGET.COMPLETE_PROFILE.PROFILE_FIELDS': 'Gib weitere Informationen zu dir an',
    'WIDGET.COMPLETE_PROFILE.CREATED_POST': 'Schreibe etwas auf die Timeline',
    'WIDGET.COMPLETE_PROFILE.FOLLOWING_USER': 'Folge einem Kollegen',
    'WIDGET.COMPLETE_PROFILE.PAGE_MEMBER': 'Folge einer Seite'
  }
};
