import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {User} from '@domain/user/user';
import {WidgetVisibilityService} from '@widgets/api/widget-visibility/widget-visibility.service';
import {CompleteProfileWidget} from '@widgets/complete-profile/complete-profile-widget';
import {of} from 'rxjs';
import {CompleteProfileWidgetComponent} from './complete-profile-widget.component';

describe('CompleteProfileWidgetComponent', () => {
  let component: CompleteProfileWidgetComponent;
  let fixture: ComponentFixture<CompleteProfileWidgetComponent>;
  let httpTestingController: HttpTestingController;
  let authService: jasmine.SpyObj<AuthService>;
  let widgetVisibilityService: jasmine.SpyObj<WidgetVisibilityService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CompleteProfileWidgetComponent],
      providers: [{
        provide: AuthService,
        useValue: jasmine.createSpyObj('authService', ['getUser$'])
      }, {
        provide: WidgetVisibilityService,
        useValue: jasmine.createSpyObj('widgetVisibilityService', ['setHidden'])
      }]
    }).overrideTemplate(CompleteProfileWidgetComponent, '')
      .compileComponents();

    httpTestingController = TestBed.get(HttpTestingController);
    authService = TestBed.get(AuthService);
    authService.getUser$.and.returnValue(of({slug: 'user-slug'} as User));
    widgetVisibilityService = TestBed.get(WidgetVisibilityService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteProfileWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {id: 'widget-id'} as CompleteProfileWidget;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', done => {
    // when
    fixture.detectChanges();

    // then
    component.fields$.subscribe(fields => {
      expect(fields.length).toEqual(6);
      expect(fields[0]).toEqual({
        value: true,
        label: 'WIDGET.COMPLETE_PROFILE.PROFILE_FIELDS',
        state: 'main.profile.info',
        params: {userId: 'user-slug'}
      });
      expect(fields[1]).toEqual({
        value: false,
        label: 'WIDGET.COMPLETE_PROFILE.AVATAR',
        state: 'main.profile.activity',
        params: {userId: 'user-slug'}
      });
      expect(fields[2]).toEqual({
        value: true,
        label: 'WIDGET.COMPLETE_PROFILE.COVER',
        state: 'main.profile.activity',
        params: {userId: 'user-slug'}
      });
      expect(fields[3]).toEqual({
        value: false,
        label: 'WIDGET.COMPLETE_PROFILE.FOLLOWING_USER',
        state: 'main.colleagues'
      });
      expect(fields[4]).toEqual({
        value: true,
        label: 'WIDGET.COMPLETE_PROFILE.PAGE_MEMBER',
        state: 'main.page'
      });
      expect(fields[5]).toEqual({
        value: false,
        label: 'WIDGET.COMPLETE_PROFILE.CREATED_POST',
        state: 'main.profile.activity',
        params: {userId: 'user-slug'}
      });
      expect(widgetVisibilityService.setHidden).toHaveBeenCalled();
      done();
    });

    const req = httpTestingController.expectOne(request => request.url === '/web/widgets/complete-profile');
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush({
      profileFields: true,
      avatar: false,
      cover: true,
      followingUser: false,
      pageMember: true,
      createdPost: false
    });
  });
});
