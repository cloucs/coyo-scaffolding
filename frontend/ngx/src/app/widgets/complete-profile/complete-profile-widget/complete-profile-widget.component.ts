import {HttpClient} from '@angular/common/http';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {AuthService} from '@core/auth/auth.service';
import {User} from '@domain/user/user';
import {WidgetComponent} from '@widgets/api/widget-component';
import {WidgetVisibilityService} from '@widgets/api/widget-visibility/widget-visibility.service';
import {CompleteProfileWidget} from '@widgets/complete-profile/complete-profile-widget';
import * as _ from 'lodash';
import {combineLatest, Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';

/**
 * The profile information stored in the backend.
 */
interface ProfileInfo {
  profileFields: boolean;
  avatar: boolean;
  cover: boolean;
  followingUser: boolean;
  pageMember: boolean;
  createdPost: boolean;
}

/**
 * A single field of the complete profile widget.
 */
interface ProfileInfoField {
  value: boolean;
  label: string;
  state: string;
  params?: { [key: string]: any; };
}

/**
 * Renders the widget to display user profile information
 * which should be completed by the user.
 */
@Component({
  selector: 'coyo-complete-profile-widget',
  templateUrl: './complete-profile-widget.component.html',
  styleUrls: ['./complete-profile-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompleteProfileWidgetComponent extends WidgetComponent<CompleteProfileWidget> implements OnInit {

  fields$: Observable<ProfileInfoField[]>;

  constructor(private http: HttpClient,
              private authService: AuthService,
              private widgetVisibilityService: WidgetVisibilityService,
              @Inject(ChangeDetectorRef) cd: ChangeDetectorRef) {
    super(cd);
  }

  ngOnInit(): void {
    this.fields$ = combineLatest([
      this.authService.getUser$(),
      this.http.get('/web/widgets/complete-profile')
    ]).pipe(tap(([user, info]: [User, ProfileInfo]) =>
      this.widgetVisibilityService.setHidden(this.widget.id, _.every(_.values(info)))
    )).pipe(map(([user, info]: [User, ProfileInfo]) => [{
      value: info.profileFields,
      label: 'WIDGET.COMPLETE_PROFILE.PROFILE_FIELDS',
      state: 'main.profile.info',
      params: {userId: user.slug}
    }, {
      value: info.avatar,
      label: 'WIDGET.COMPLETE_PROFILE.AVATAR',
      state: 'main.profile.activity',
      params: {userId: user.slug}
    }, {
      value: info.cover,
      label: 'WIDGET.COMPLETE_PROFILE.COVER',
      state: 'main.profile.activity',
      params: {userId: user.slug}
    }, {
      value: info.followingUser,
      label: 'WIDGET.COMPLETE_PROFILE.FOLLOWING_USER',
      state: 'main.colleagues'
    }, {
      value: info.pageMember,
      label: 'WIDGET.COMPLETE_PROFILE.PAGE_MEMBER',
      state: 'main.page'
    }, {
      value: info.createdPost,
      label: 'WIDGET.COMPLETE_PROFILE.CREATED_POST',
      state: 'main.profile.activity',
      params: {userId: user.slug}
    }]));
  }

  get title(): string {
    return _.get(this.widget, 'settings._titles[0]');
  }
}
