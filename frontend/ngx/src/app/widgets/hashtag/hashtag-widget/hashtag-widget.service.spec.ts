import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {Hashtag} from '../hashtag';
import {HashtagWidgetService} from './hashtag-widget.service';

describe('HashtagWidgetService', () => {
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HashtagWidgetService]
    });
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should be created', inject([HashtagWidgetService], (service: HashtagWidgetService) => {
    expect(service).toBeTruthy();
  }));

  it('should sort hashtags by count and calculate the correct opacity and font weight', inject([HashtagWidgetService], (service: HashtagWidgetService) => {
    // given
    const obj1: Hashtag = {tag: '#hashtag1', count: 4, opacity: 0.5714285714285714, fontWeight: 628.5714285714286};
    const obj2: Hashtag = {tag: '#hashtag2', count: 2, opacity: 0.35714285714285715, fontWeight: 542.8571428571429};
    const obj3: Hashtag = {tag: '#hashtag3', count: 1, opacity: 0.4, fontWeight: 560};
    const obj4: Hashtag = {tag: '#hashtag4', count: 8, opacity: 1, fontWeight: 800};
    const obj5: Hashtag = {tag: '#hashtag5', count: 7, opacity: 0.8928571428571428, fontWeight: 757.1428571428571};

    // when
    service.getTrendingHashtags('ONE_MONTH').subscribe(res => expect(res).toEqual([obj4, obj5, obj1, obj2, obj3]));

    // then
    const request = httpTestingController.expectOne(req => req.method === 'GET');

    // finally
    request.flush({'#hashtag3': 1, '#hashtag2': 2, '#hashtag4': 8, '#hashtag5': 7, '#hashtag1': 4});
    httpTestingController.verify();
  }));
});
