import {Widget} from '@domain/widget/widget';
import {HashtagWidgetSettings} from '@widgets/hashtag/hashtag-widget-settings.model';

export interface HashtagWidget extends Widget<HashtagWidgetSettings> {
}
