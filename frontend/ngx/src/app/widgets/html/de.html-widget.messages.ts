import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.HTML.DESCRIPTION': 'Stellt beliebigen HTML-Code dar.',
    'WIDGET.HTML.NAME': 'HTML',
    'WIDGET.HTML.SETTINGS.TEXT': 'HTML',
    'WIDGET.HTML.SETTINGS.TEXT.HELP': 'Gib beliebigen HTML-Code ein, der von dem Widget interpretiert und angezeigt werden soll.'
  }
};
