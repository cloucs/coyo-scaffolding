import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a html widget
 */
export interface HtmlWidgetSettings extends WidgetSettings {
  html_content: string;
}
