import {Pipe, PipeTransform} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Page} from '@test/util/page';
import * as _ from 'lodash';
import {HtmlWidgetComponent} from './html-widget.component';

class ComponentPage extends Page<HtmlWidgetComponent> {
  get content(): HTMLElement {
    return this.query<HTMLElement>('.html-widget');
  }
}

@Pipe({name: 'hashtags'})
class HashtagsPipeMock implements PipeTransform {
  transform = (value: string, ...args: any[]) =>
    _.isString(value) ? value.replace(/#[a-z]*/g, 'HASHTAG') : value;
}

@Pipe({name: 'mention'})
class MentionPipeMock implements PipeTransform {
  transform = (value: string, ...args: any[]) =>
    _.isString(value) ? value.replace(/@[a-z]*/g, 'YOU') : value;
}

@Pipe({name: 'trustHtml'})
class TrustHtmlPipeMock implements PipeTransform {
  transform = (value: string, ...args: any[]) =>
    _.isString(value) ? ('trusted(' + value + ')') : value;
}

describe('HtmlWidgetComponent', () => {
  let component: HtmlWidgetComponent;
  let fixture: ComponentFixture<HtmlWidgetComponent>;
  let page: ComponentPage;

  let widget: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HtmlWidgetComponent, HashtagsPipeMock, MentionPipeMock, TrustHtmlPipeMock]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlWidgetComponent);
    page = new ComponentPage(fixture);
    component = fixture.componentInstance;
    widget = {settings: {}};
    component.widget = widget;
    fixture.detectChanges();
  });

  it('should use the configured HTML content', () => {
    widget.settings['html_content'] = 'Hello @ianbold HTML Content #great for the win';
    fixture.detectChanges();

    expect(page.content.textContent).toContain('trusted(Hello YOU HTML Content HASHTAG for the win)');
  });
});
