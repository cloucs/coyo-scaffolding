import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.USERPROFILE.DESCRIPTION': 'A widget to display a summary of a user\'s profile fields.',
    'WIDGET.USERPROFILE.NAME': 'User profile',
    'WIDGET.USERPROFILE.SETTINGS.HELP': 'Please select a user to be displayed by the widget.',
    'WIDGET.USERPROFILE.SHOW.INFO': 'Info',
    'WIDGET.USERPROFILE.SHOW.INFO.LABEL': 'Show contact information',
    'WIDGET.USERPROFILE.USER': 'User',
    'WIDGET.USERPROFILE.USER.NOTFOUND': 'The previously selected user can not be displayed.'
  }
};
