import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {SenderUIModule} from '@shared/sender-ui/sender-ui.module';
import {WIDGET_CONFIGS} from '@widgets/api/widget-config';
import {messagesDe} from '@widgets/user-profile/de.user-profile-widget.messages';
import {messagesEn} from '@widgets/user-profile/en.user-profile-widget.messages';
import {USER_PROFILE_WIDGET} from '@widgets/user-profile/user-profile-widget-config';
import {CleanPhoneNumberPipe} from './clean-phone-number/clean-phone-number.pipe';
import {SelectUserIdFormControlComponent} from './select-user-id-form-control/select-user-id-form-control.component';
import {UserProfileWidgetSettingsComponent} from './user-profile-widget-settings/user-profile-widget-settings.component';
import {UserProfileWidgetComponent} from './user-profile-widget/user-profile-widget.component';

@NgModule({
  declarations: [UserProfileWidgetComponent, UserProfileWidgetSettingsComponent, SelectUserIdFormControlComponent, CleanPhoneNumberPipe],
  entryComponents: [UserProfileWidgetComponent, UserProfileWidgetSettingsComponent],
  imports: [
    CommonModule,
    CoyoCommonsModule,
    ReactiveFormsModule,
    SenderUIModule
  ],
  providers: [
    {provide: WIDGET_CONFIGS, useValue: USER_PROFILE_WIDGET, multi: true},
    {provide: 'messages', useValue: messagesDe, multi: true},
    {provide: 'messages', useValue: messagesEn, multi: true}
  ]
})
export class UserProfileWidgetModule {
}
