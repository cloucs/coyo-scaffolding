import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.USERPROFILE.DESCRIPTION': 'Zeigt das Profil eines Benutzers an.',
    'WIDGET.USERPROFILE.NAME': 'Benutzerprofil',
    'WIDGET.USERPROFILE.SETTINGS.HELP': 'Bitte wähle einen Benutzer aus, dessen Profil angezeigt werden soll.',
    'WIDGET.USERPROFILE.SHOW.INFO': 'Info',
    'WIDGET.USERPROFILE.SHOW.INFO.LABEL': 'Zeige Kontaktinformationen',
    'WIDGET.USERPROFILE.USER': 'Benutzer',
    'WIDGET.USERPROFILE.USER.NOTFOUND': 'Der ausgewählte Benutzer kann nicht angezeigt werden.'
  }
};
