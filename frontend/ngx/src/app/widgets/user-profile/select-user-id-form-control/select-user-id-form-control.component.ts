import {ChangeDetectionStrategy, Component, forwardRef, OnDestroy, OnInit} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from '@angular/forms';
import {UrlService} from '@core/http/url/url.service';
import {SenderService} from '@domain/sender/sender/sender.service';
import {SenderParameter} from '@shared/sender-ui/select-sender/sender-parameter';
import {Subscription} from 'rxjs';

/**
 * Component that allows to select a user and has the user id as the form field value.
 */
@Component({
  selector: 'coyo-select-user-id-form-control',
  templateUrl: './select-user-id-form-control.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SelectUserIdFormControlComponent),
    multi: true
  }]
})
export class SelectUserIdFormControlComponent implements OnInit, OnDestroy, ControlValueAccessor {

  selectControl: FormControl;

  private onChangeFn: any;
  private onTouchedFn: any;
  private valueChangesSubscription: Subscription;

  readonly senderSelectOptions: SenderParameter = {
    filters: this.urlService.toUrlParamString('type', ['user']),
    findOnlyManagedSenders: false
  };

  constructor(private urlService: UrlService, private senderService: SenderService) {
  }

  ngOnInit(): void {
    this.selectControl = new FormControl();
    this.valueChangesSubscription = this.selectControl.valueChanges.subscribe($event => {
      if (this.onTouchedFn) {
        this.onTouchedFn();
      }
      if (this.onChangeFn) {
        this.onChangeFn($event ? $event.id : $event);
      }
    });
  }

  ngOnDestroy(): void {
    this.valueChangesSubscription.unsubscribe();
  }

  registerOnChange(fn: any): void {
    this.onChangeFn = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedFn = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(obj: string): void {
    if (obj) {
      this.senderService.get(obj).subscribe(sender => {
        this.selectControl.patchValue(sender);
      });
    } else {
      this.selectControl.reset();
    }
  }
}
