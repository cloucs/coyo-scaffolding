import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import {CalloutWidgetSettingsComponent} from './callout-widget-settings.component';

describe('CalloutWidgetSettingsComponent', () => {
  let component: CalloutWidgetSettingsComponent;
  let fixture: ComponentFixture<CalloutWidgetSettingsComponent>;

  let widget: any;
  let parentForm: FormGroup;
  const onSubmit: Subject<any> = new Subject<any>();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [FormBuilder],
      declarations: [CalloutWidgetSettingsComponent]
    }).overrideTemplate(CalloutWidgetSettingsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    widget = {settings: {}};
    parentForm = new FormGroup({});
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalloutWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = widget;
    component.parentForm = parentForm;
    component.onSubmit = onSubmit;
    fixture.detectChanges();
  });

  it('should validate settings', () => {
    component.parentForm.patchValue({
      md_text: 'Callout'
    });

    expect(component.parentForm.valid).toBeTruthy();
    expect(component.parentForm.get('_callout').value).toEqual({alertClass: 'alert-success'});
  });
});
