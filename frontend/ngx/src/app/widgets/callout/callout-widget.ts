import {Widget} from '@domain/widget/widget';
import {CalloutWidgetSettings} from '@widgets/callout/callout-widget-settings.model';

export interface CalloutWidget extends Widget<CalloutWidgetSettings> {
}
