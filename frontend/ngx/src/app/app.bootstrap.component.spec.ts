import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {AuthService} from '@core/auth/auth.service';
import {ThemeService} from '@core/theme/theme.service';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {User} from '@domain/user/user';
import {UIRouter, UrlService} from '@uirouter/core';
import {NG1_MOBILE_EVENTS_SERVICE} from '@upgrade/upgrade.module';
import {NGXLogger} from 'ngx-logger';
import {NgxPermissionsService} from 'ngx-permissions';
import {Observable, of, Subscription, throwError} from 'rxjs';
import {Ng1MobileEventsService} from 'typings';
import {BootstrapComponent} from './app.bootstrap.component';

describe('BootstrapComponent', () => {
  let fixture: ComponentFixture<BootstrapComponent>;
  let windowResizeService: jasmine.SpyObj<WindowSizeService>;
  let uiRouter: jasmine.SpyObj<UIRouter>;
  let urlService: jasmine.SpyObj<UrlService>;
  let themeService: jasmine.SpyObj<ThemeService>;
  let authService: jasmine.SpyObj<AuthService>;
  let permissionsService: jasmine.SpyObj<NgxPermissionsService>;
  let googleApiService: jasmine.SpyObj<GoogleApiService>;
  let mobileEventsService: jasmine.SpyObj<Ng1MobileEventsService>;
  let logger: jasmine.SpyObj<NGXLogger>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BootstrapComponent],
      providers: [{
        provide: UrlService,
        useValue: jasmine.createSpyObj('urlService', ['listen', 'sync'])
      }, {
        provide: UIRouter,
        useValue: jasmine.createSpyObj('uiRouter', ['plugin'])
      }, {
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['updateWindow'])
      }, {
        provide: ThemeService,
        useValue: jasmine.createSpyObj('themeService', ['applyTheme'])
      }, {
        provide: Window,
        useValue: window
      }, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('AuthService', ['getUser$'])
      }, {
        provide: NgxPermissionsService,
        useValue: jasmine.createSpyObj('NgxPermissionsService', ['loadPermissions', 'flushPermissions'])
      }, {
        provide: GoogleApiService,
        useValue: jasmine.createSpyObj('GoogleApiService', ['initGoogleApi'])
      }, {
        provide: NG1_MOBILE_EVENTS_SERVICE,
        useValue: jasmine.createSpyObj('mobileEventsService', ['propagate'])
      }, {
        provide: NGXLogger,
        useValue: jasmine.createSpyObj('NGXLogger', ['info'])
      }]
    }).compileComponents();

    urlService = TestBed.get(UrlService);
    uiRouter = TestBed.get(UIRouter);
    windowResizeService = TestBed.get(WindowSizeService);
    themeService = TestBed.get(ThemeService);
    authService = TestBed.get(AuthService);
    permissionsService = TestBed.get(NgxPermissionsService);
    googleApiService = TestBed.get(GoogleApiService);
    mobileEventsService = TestBed.get(NG1_MOBILE_EVENTS_SERVICE);
    logger = TestBed.get(NGXLogger);
    authService.getUser$.and.returnValue(of({} as User));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BootstrapComponent);
  });

  it('should initialize global permissions', () => {
    // given
    const permissions = ['PERMISSION', 'OTHER_PERMISSION'];
    authService.getUser$.and.returnValue(of({
      globalPermissions: permissions
    } as User));

    // when
    fixture.detectChanges();

    // then
    expect(authService.getUser$).toHaveBeenCalled();
    expect(permissionsService.loadPermissions).toHaveBeenCalledWith(permissions);
  });

  it('should unsubscribe onDestroy', () => {
    // given
    const subscription: jasmine.SpyObj<Subscription> = jasmine.createSpyObj('subscription', ['unsubscribe']);
    const observable: jasmine.SpyObj<Observable<User>> = jasmine.createSpyObj('observable', ['subscribe']);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    observable.subscribe.and.returnValue(subscription);
    authService.getUser$.and.returnValue(observable);

    // when
    fixture.detectChanges();
    fixture.destroy();

    // then
    expect(subscription.unsubscribe).toHaveBeenCalled();
  });

  it('should have initialized G-Suite', () => {
    // when
    fixture.detectChanges();

    // then
    expect(googleApiService.initGoogleApi).toHaveBeenCalled();
  });

  it('should initialize router', () => {
    // when
    fixture.detectChanges();

    // then
    expect(uiRouter.plugin).toHaveBeenCalled();
    expect(urlService.sync).toHaveBeenCalled();
    expect(urlService.listen).toHaveBeenCalled();
    expect(themeService.applyTheme).toHaveBeenCalled();
  });

  it('should propagate onContextLoaded event on initialization', () => {
    // when
    fixture.detectChanges();

    // then
    expect(mobileEventsService.propagate).toHaveBeenCalledWith('onContextLoaded');
  });

  it('should update window size when resizing window', () => {
    // given
    fixture.detectChanges();

    // when
    const window = TestBed.get(Window);
    window.dispatchEvent(new Event('resize'));

    // then
    expect(windowResizeService.updateWindow).toHaveBeenCalled();
  });

  it('should fail silently withour error', () => {
    // given
    authService.getUser$.and.returnValue(throwError('please fail'));

    // when
    fixture.detectChanges();

    // then
    expect(logger.info).toHaveBeenCalledWith('please fail');
  });
});
