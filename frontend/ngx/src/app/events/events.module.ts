import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {EventAvatarComponent} from '@app/events/event-avatar/event-avatar.component';
import {EventCreateMenuComponent} from '@app/events/event-create-menu/event-create-menu.component';
import {EventCreateComponent} from '@app/events/event-create/event-create.component';
import {EventDateOverlayComponent} from '@app/events/event-date-overlay/event-date-overlay.component';
import {EventParticipationComponent} from '@app/events/event-participation/event-participation.component';
import {EventSettingsComponent} from '@app/events/event-settings/event-settings.component';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {ContextMenuModule} from '@shared/context-menu/context-menu.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {RteModule} from '@shared/rte/rte.module';
import {SenderUIModule} from '@shared/sender-ui/sender-ui.module';
import {UpgradeModule} from '@upgrade/upgrade.module';
import {MomentModule} from 'ngx-moment';
import {messagesDe} from './de.events.messages';
import {messagesEn} from './en.events.messages';
import './event-avatar/event-avatar.component.downgrade';
import './event-create-menu/event-create-menu.component.downgrade';
import './event-create/event-create.component.downgrade';
import './event-participation/event-participation.component.downgrade';
import './event-settings/event-settings.component.downgrade';

/**
 * Event feature module
 */
@NgModule({
  imports: [
    ContextMenuModule,
    CoyoCommonsModule,
    CoyoFormsModule,
    MomentModule,
    ReactiveFormsModule,
    RteModule,
    SenderUIModule,
    UpgradeModule
  ],
  declarations: [
    EventAvatarComponent,
    EventCreateComponent,
    EventCreateMenuComponent,
    EventDateOverlayComponent,
    EventParticipationComponent,
    EventSettingsComponent
  ],
  exports: [
    EventAvatarComponent,
    EventCreateComponent,
    EventParticipationComponent,
    EventSettingsComponent
  ],
  entryComponents: [
    EventAvatarComponent,
    EventCreateComponent,
    EventCreateMenuComponent,
    EventParticipationComponent,
    EventSettingsComponent
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn}
  ],
})
export class EventsModule {
}
