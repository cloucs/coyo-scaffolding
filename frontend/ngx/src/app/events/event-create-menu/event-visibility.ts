export interface EventVisibility {
  name: string;
  icon: string;
}
