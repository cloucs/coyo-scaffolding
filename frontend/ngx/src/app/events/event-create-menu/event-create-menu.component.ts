import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {EventVisibility} from '@app/events/event-create-menu/event-visibility';
import {NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';

@Component({
  selector: 'coyo-event-create-menu',
  templateUrl: './event-create-menu.component.html',
  styleUrls: ['./event-create-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventCreateMenuComponent {

  readonly eventVisibilities: EventVisibility[] = [{name: 'PUBLIC', icon: 'globe'}, {name: 'PRIVATE', icon: 'lock'}];

  constructor(@Inject(NG1_STATE_SERVICE) private stateService: IStateService) { }

  /**
   * Called after a button was clicked, representing the event visibility.
   * @param visibility the event visibility object
   */
  redirectToEventCreate(visibility: EventVisibility): void {
    this.stateService.go('main.event.create', {public: visibility.name === 'PUBLIC'});
  }
}
