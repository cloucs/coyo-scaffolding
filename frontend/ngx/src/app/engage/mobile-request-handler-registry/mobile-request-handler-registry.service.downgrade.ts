import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {MobileRequestHandlerRegistryService} from '@app/engage/mobile-request-handler-registry/mobile-request-handler-registry.service';

getAngularJSGlobal()
  .module('commons.mobile')
  .factory('mobileRequestHandlerRegistry', downgradeInjectable(MobileRequestHandlerRegistryService));
