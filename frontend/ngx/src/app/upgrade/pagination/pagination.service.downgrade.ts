import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {PaginationService} from './pagination.service';

getAngularJSGlobal()
  .module('commons.ui')
  .factory('ngxPaginationService', downgradeInjectable(PaginationService) as any);
