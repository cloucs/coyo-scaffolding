import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LaunchpadAppsComponent} from './launchpad-apps.component';

describe('LaunchpadAppsComponent', () => {
  let component: LaunchpadAppsComponent;
  let fixture: ComponentFixture<LaunchpadAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LaunchpadAppsComponent]
    }).overrideTemplate(LaunchpadAppsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchpadAppsComponent);
    component = fixture.componentInstance;
    component.apps = [
      {name: 'Outlook', icon: 'o365-outlook', url: 'https://outlook.office365.com'},
      {name: 'Word', icon: 'o365-word', url: 'https://www.office.com/launch/word'},
      {name: 'Excel', icon: 'o365-excel', url: 'https://www.office.com/launch/excel'},
      {name: 'PowerPoint', icon: 'o365-powerpoint', url: 'https://www.office.com/launch/powerpoint'},
      {name: 'Teams', icon: 'o365-teams', url: 'https://teams.microsoft.com'},
      {name: 'Planner', icon: 'o365-planner', url: 'https://tasks.office.com'},
      {name: 'SharePoint', icon: 'o365-sharepoint', url: ''},
      {name: 'OneNote', icon: 'o365-one-note', url: 'https://www.office.com/launch/onenote'},
      {name: 'OneDrive', icon: 'o365-one-drive', url: 'https://portal.office.com/onedrive'}
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate the number of apps to show on available width', () => {
    // given
    let called = false;
    fixture.elementRef.nativeElement.style.width = '400px';
    const subscription = component.visibleApps$.subscribe(apps => {
      expect(apps.columns).toBe(3);
      expect(apps.apps[0].length).toBe(3);
      called = true;
    });
    // when
    component.ngAfterViewInit();
    // then
    expect(called).toBeTruthy('Subscription not called');
    subscription.unsubscribe();
  });
});
