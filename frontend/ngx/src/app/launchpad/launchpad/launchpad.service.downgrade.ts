import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {LaunchpadService} from './launchpad.service';

getAngularJSGlobal()
  .module('coyo.launchpad')
  .factory('coyoLaunchpad', downgradeInjectable(LaunchpadService) as any);
