import {LaunchpadCategory} from '@domain/launchpad/launchpad-category';
import {LaunchpadCategoryColorPipe} from './launchpad-category-color.pipe';

describe('LaunchpadCategoryColorPipe', () => {
  let pipe: LaunchpadCategoryColorPipe;

  beforeEach(() => {
    pipe = new LaunchpadCategoryColorPipe();
  });

  it('should transform null', () => {
    // when
    const result = pipe.transform(null);

    // then
    expect(pipe.COLORS).toContain(result);
  });

  it('should transform a link', () => {
    // when
    const result = pipe.transform({
      id: 'someId'
    } as LaunchpadCategory);

    // then
    expect(pipe.COLORS).toContain(result);
  });
});
