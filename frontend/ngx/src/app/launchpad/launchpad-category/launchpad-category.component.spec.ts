import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LaunchpadCategory} from '@domain/launchpad/launchpad-category';
import {LaunchpadLink} from '@domain/launchpad/launchpad-link';
import {LaunchpadCategoryComponent} from './launchpad-category.component';

describe('LaunchpadCategoryComponent', () => {
  let component: LaunchpadCategoryComponent;
  let fixture: ComponentFixture<LaunchpadCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LaunchpadCategoryComponent]
    }).overrideTemplate(LaunchpadCategoryComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchpadCategoryComponent);
    component = fixture.componentInstance;
    component.category = {
      id: 'categoryId',
      links: [{id: 'linkId'} as LaunchpadLink]
    } as LaunchpadCategory;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
