import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'LAUNCHPAD.HEADLINE': 'Launchpad',
    'LAUNCHPAD.SETTINGS.LAYOUT': 'Launchpad Einstellungen',
    'LAUNCHPAD.SETTINGS.LAYOUT.USE_NO_COLUMNS': 'Listenansicht',
    'LAUNCHPAD.SETTINGS.LAYOUT.USE_COLUMNS': 'Rasteransicht',
    'LAUNCHPAD.SETTINGS.LAYOUT.USE_NO_CONDENSED': 'Kompakte Darstellung',
    'LAUNCHPAD.SETTINGS.LAYOUT.USE_CONDENSED': 'Kompakte Darstellung',
    'LAUNCHPAD.CATEGORY.ADD_LINK': 'Link hinzufügen',
    'LAUNCHPAD.CATEGORY.EMPTY.HINT': 'Hinweis:',
    'LAUNCHPAD.CATEGORY.EMPTY.MESSAGE': 'Benutzer können diese Kategorie nicht sehen, bis Links hinzugefügt werden.',
    'LAUNCHPAD.CATEGORY.PERSONAL': 'Deine Lesezeichen',
    'LAUNCHPAD.MANAGE.CATEGORY': 'Kategorie',
    'LAUNCHPAD.MANAGE.CATEGORY.ARIA': 'Kategorie des neuen Links',
    'LAUNCHPAD.MANAGE.CATEGORY.PLACEHOLDER': 'Kategorie auswählen',
    'LAUNCHPAD.MANAGE.DISCARD': 'Abbrechen',
    'LAUNCHPAD.MANAGE.HEADLINE': 'Link Details',
    'LAUNCHPAD.MANAGE.ICON.UPLOAD': 'Hochladen',
    'LAUNCHPAD.MANAGE.ICON.REMOVE': 'Entfernen',
    'LAUNCHPAD.MANAGE.SAVE': 'Link speichern',
    'LAUNCHPAD.MANAGE.NAME': 'Titel',
    'LAUNCHPAD.MANAGE.NAME.ARIA': 'Titel des neuen Links',
    'LAUNCHPAD.MANAGE.NAME.PLACEHOLDER': '',
    'LAUNCHPAD.MANAGE.URL': 'URL',
    'LAUNCHPAD.MANAGE.URL.ARIA': 'URL des neuen Links',
    'LAUNCHPAD.MANAGE.AUTOSUGGEST': 'Bild und Titel von URL abrufen',
    'LAUNCHPAD.MANAGE.SHOW_ALL': 'Alle anzeigen',
    'LAUNCHPAD.MANAGE.SHOW_LESS': 'Weniger anzeigen'
  }
};
