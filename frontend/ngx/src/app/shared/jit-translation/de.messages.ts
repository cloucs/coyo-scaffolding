import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'TRANSLATIONS.JIT.TRANSLATION.SHOW': 'Übersetzen',
    'TRANSLATIONS.JIT.TRANSLATION.HIDE': 'Ausblenden',
    'TRANSLATIONS.JIT.ERROR.NOTAVAILABLE': 'Übersetzungen momentant nicht verfügbar'
  }
};
