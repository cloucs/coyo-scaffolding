import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {JitTranslationSettingsService} from '@app/admin/settings/jit-translation-settings/jit-translation-settings.service';
import {AuthService} from '@core/auth/auth.service';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {User} from '@domain/user/user';
import {of, throwError} from 'rxjs';
import {JitTranslationService} from '../jit-translation/jit-translation.service';
import {JitTranslationToggleComponent} from './jit-translation-toggle.component';

describe('JitTranslationToggleComponent', () => {
  let component: JitTranslationToggleComponent;
  let fixture: ComponentFixture<JitTranslationToggleComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let translationService: jasmine.SpyObj<JitTranslationService>;
  let jitSettingsService: jasmine.SpyObj<JitTranslationSettingsService>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [JitTranslationToggleComponent],
      providers: [{
        provide: AuthService,
        useValue: jasmine.createSpyObj('AuthService', ['getUser$'])
      }, {
        provide: JitTranslationService,
        useValue: jasmine.createSpyObj('JitTranslationService', ['getLanguage', 'getTranslation'])
      }, {
        provide: JitTranslationSettingsService,
        useValue: jasmine.createSpyObj('JitTranslationSettingsService', ['getActiveSettings'])
      }, {
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('WindowSizeService', ['observeScreenChange'])
      }]
    }).overrideTemplate(JitTranslationToggleComponent, '')
      .compileComponents();

    authService = TestBed.get(AuthService);
    translationService = TestBed.get(JitTranslationService);
    jitSettingsService =  TestBed.get(JitTranslationSettingsService);
    windowSizeService = TestBed.get(WindowSizeService);

    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.MD));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JitTranslationToggleComponent);
    component = fixture.componentInstance;
  });

  it('should get the language of a text on init', () => {
    // given
    const user = {
      language: 'en'
    } as User;
    const language = 'DE';
    const text = 'Hallo Welt!';
    const active = {
      activeLanguages: ['DE', 'EN'] as string[]
    };
    component.originalText = text;
    component.field = 'message';
    component.id = 'timeline-item-id';
    translationService.getLanguage.and.returnValue(of({language}));
    authService.getUser$.and.returnValue(of(user));
    jitSettingsService.getActiveSettings.and.returnValue(of(active));

    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe(result => {
      expect(result.userLanguage).toBe('en');
      expect(result.id).toBe(component.id);
      expect(result.showButton).toBe(true);
    });
  });

  it('should retry to get the language in case of NOT_PROCESSED up to three times', fakeAsync(() => {
    // given
    const user = {
      language: 'en'
    } as User;
    const text = 'Hallo Welt!';
    const active = {
      activeLanguages: ['DE', 'EN']
    };
    component.originalText = text;
    component.field = 'message';
    component.id = 'timeline-item-id';
    translationService.getLanguage.and.returnValues(throwError('NOT_PROCESSED'),
      throwError('NOT_PROCESSED'),
      throwError('NOT_PROCESSED'),
      of({language: 'DE'}));
    authService.getUser$.and.returnValue(of(user));
    jitSettingsService.getActiveSettings.and.returnValue(of(active));

    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe(result => {
      expect(result.showButton).toBe(true);
      expect(result.userLanguage).toBe('en');
      expect(result.id).toBe(component.id);
    });

    tick(10000);
    expect(translationService.getLanguage).toHaveBeenCalledTimes(2);
    tick(30000);
    expect(translationService.getLanguage).toHaveBeenCalledTimes(3);
    tick(50000);
    expect(translationService.getLanguage).toHaveBeenCalledTimes(4);
  }));

  it('should retry to get the language in case of NOT_PROCESSED up to three times and fail then', fakeAsync(() => {
    // given
    const user = {
      language: 'en'
    } as User;
    const text = 'Hallo Welt!';
    const active = {
      activeLanguages: ['DE', 'EN']
    };
    component.originalText = text;
    component.field = 'message';
    component.id = 'timeline-item-id';
    translationService.getLanguage.and.returnValues(throwError('NOT_PROCESSED'),
      throwError('NOT_PROCESSED'),
      throwError('NOT_PROCESSED'),
      throwError('NOT_PROCESSED'));
    authService.getUser$.and.returnValue(of(user));
    jitSettingsService.getActiveSettings.and.returnValue(of(active));

    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe(() => {
    }, error => {
      expect(error).toBe('NOT_PROCESSED');
      return true;
    });

    tick(10000);
    expect(translationService.getLanguage).toHaveBeenCalledTimes(2);
    tick(30000);
    expect(translationService.getLanguage).toHaveBeenCalledTimes(3);
    tick(50000);
    expect(translationService.getLanguage).toHaveBeenCalledTimes(4);
  }));

  it('should not toggle if already loading', () => {
    // given
    component.isLoading = true;
    component.showTranslation = true;
    spyOn(component.translated, 'emit');

    // when
    component.toggle();

    // then
    expect(component.translated.emit).toHaveBeenCalledTimes(0);
  });

  it('should toggle from hide translation to show translation', () => {
    // given
    component.state$ = of({
      userLanguage: 'en',
      showButton: true,
      id: '',
      active: true
    });
    component.isLoading = false;
    component.showTranslation = false;
    translationService.getTranslation.and.returnValue(of('Translation'));

    // when
    component.toggle();

    // then
    expect(component.showTranslation).toBe(true);
  });

  it('should emit null if disabling show translated', () => {
    component.isLoading = false;
    component.showTranslation = true;
    spyOn(component.translated, 'emit');

    // when
    component.toggle();

    // then
    expect(component.showTranslation).toBe(false);
    expect(component.translated.emit).toHaveBeenCalledTimes(1);
    expect(component.translated.emit).toHaveBeenCalledWith(null);
  });

  it('should use previous translation if available', () => {
    component.isLoading = false;
    component.showTranslation = false;
    component['translation'] = 'Hallo Übersetzung';
    spyOn(component.translated, 'emit');

    // when
    component.toggle();

    // then
    expect(component.showTranslation).toBe(true);
    expect(component.translated.emit).toHaveBeenCalledTimes(1);
    expect(component.translated.emit).toHaveBeenCalledWith('Hallo Übersetzung');
  });

  it('should not show the button if target language is not supported', () => {
    // given
    const user = {
      language: 'en'
    } as User;
    const language = 'FR';
    const active = {
      activeLanguages: ['DE', 'EN'] as string[]
    };
    translationService.getLanguage.and.returnValue(of({language}));
    authService.getUser$.and.returnValue(of(user));
    jitSettingsService.getActiveSettings.and.returnValue(of(active));

    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe(result => {
      expect(result.showButton).toBe(false);
    });
  });

  it('should not show the button if user language is not supported', () => {
    // given
    const user = {
      language: 'fr'
    } as User;
    const language = 'DE';
    const active = {
      activeLanguages: ['DE', 'EN'] as string[]
    };
    translationService.getLanguage.and.returnValue(of({language}));
    authService.getUser$.and.returnValue(of(user));
    jitSettingsService.getActiveSettings.and.returnValue(of(active));

    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe(result => {
      expect(result.showButton).toBe(false);
    });
  });

  it('should not request languages if translation is not active', () => {
    // given
    jitSettingsService.getActiveSettings.and.returnValue(of({activeLanguages: []}));

    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe(result => {
      expect(result.showButton).toBe(false);
    });
    expect(authService.getUser$).toHaveBeenCalledTimes(0);
    expect(translationService.getLanguage).toHaveBeenCalledTimes(0);
  });
});
