import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {ShareNavBarButtonComponent} from '@shared/social/shares/share-nav-bar-button/share-nav-bar-button.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoShareNavBarButton', downgradeComponent({
    component: ShareNavBarButtonComponent
  }));
