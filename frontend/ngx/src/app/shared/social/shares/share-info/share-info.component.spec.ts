import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {Share} from '@domain/share/share';
import {ShareService} from '@domain/share/share.service';
import {Shareable} from '@domain/share/shareable';
import {SharesModalResults} from '@shared/social/shares/shares-modal/shares-modal-results';
import {SharesModalComponent} from '@shared/social/shares/shares-modal/shares-modal.component';
import {of, Subject} from 'rxjs';
import {ShareInfoComponent} from './share-info.component';

describe('ShareInfoComponent', () => {
  let component: ShareInfoComponent;
  let fixture: ComponentFixture<ShareInfoComponent>;
  let dialog: jasmine.SpyObj<MatDialog>;
  let shareService: jasmine.SpyObj<ShareService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShareInfoComponent],
      providers: [{
        provide: MatDialog,
        useValue: jasmine.createSpyObj('dialog', ['open'])
      }, {
        provide: ShareService,
        useValue: jasmine.createSpyObj('shareService', ['getShares'])
      }]
    }).overrideTemplate(ShareInfoComponent, '')
      .compileComponents();

    dialog = TestBed.get(MatDialog);
    dialog.open.and.returnValue({afterClosed: () => of({})});
    shareService = TestBed.get(ShareService);
    shareService.getShares.and.returnValue(of([{} as Share]));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Shares modal', () => {

    it('should open', () => {
      // given
      component.target = {
        typeName: 'timeline-item',
        id: 'id'
      } as Shareable;

      // when
      component.openSharesModal();

      // then
      expect(dialog.open).toHaveBeenCalled();
    });

    it('should open with initial state', () => {
      // given
      component.target = {
        typeName: 'timeline-item',
        id: 'id',
        itemType: 'item-type'
      } as Shareable;

      // when
      component.openSharesModal();

      // then
      expect(shareService.getShares).toHaveBeenCalled();
      expect(dialog.open).toHaveBeenCalledWith(SharesModalComponent, {
        data: {
          shares: [{} as Share],
          itemType: 'item-type'
        }
      });
    });

    it('should open and process result on close', () => {
      // given
      component.target = {
        typeName: 'timeline-item',
        id: 'id'
      } as Shareable;

      spyOn(component.sharesDeleted, 'emit');
      const modalResult: SharesModalResults = {
        deletedShares: [{id: 'id-2'}] as Share[]
      };
      const closeSubject = new Subject<SharesModalResults>();
      dialog.open.and.returnValue({afterClosed: () => closeSubject});

      // when
      component.openSharesModal();

      closeSubject.next(modalResult);

      // then
      expect(component.sharesDeleted.emit).toHaveBeenCalledWith(modalResult.deletedShares);
    });
  });
});
