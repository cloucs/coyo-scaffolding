import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {StickyExpiryPeriod} from '@app/timeline/timeline-form/sticky-btn/sticky-expiry-period';
import {StickyExpiryPeriods} from '@app/timeline/timeline-form/sticky-btn/sticky-expiry-periods';
import {UrlService} from '@core/http/url/url.service';
import {Sender} from '@domain/sender/sender';
import {ShareService} from '@domain/share/share.service';
import {Ng1NotificationService} from '@root/typings';
import {SenderParameter} from '@shared/sender-ui/select-sender/sender-parameter';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {distinctUntilChanged, filter} from 'rxjs/operators';
import {ShareModalData} from './share-modal-data';

/**
 * Renders the share modal view.
 */
@Component({
  selector: 'coyo-share-modal',
  templateUrl: './share-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareModalComponent implements OnInit {

  readonly stickyExpiryOptions: StickyExpiryPeriod[] = StickyExpiryPeriods.all;

  readonly shareWithConfig: SenderParameter = {
    filters: this.urlService.toUrlParamString('type', ['user', 'page', 'workspace']),
    findSharingRecipients: true
  };

  readonly shareAsConfig: SenderParameter = {
    filters: this.urlService.toUrlParamString('type', ['user', 'page', 'workspace', 'event']),
    findOnlyManagedSenders: true
  };

  shareForm: FormGroup;

  showExtended: boolean = false;

  constructor(private shareService: ShareService,
              private urlService: UrlService,
              private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<ShareModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ShareModalData,
              @Inject(NG1_NOTIFICATION_SERVICE) private notificationService: Ng1NotificationService) {
  }

  ngOnInit(): void {
    this.shareForm = this.formBuilder.group({
      shareWith: [null, Validators.required],
      message: [null],
      shareAs: [this.data.initialSender, Validators.required],
      stickyExpiry: StickyExpiryPeriods.none
    });
    this.initValueChangeValidation();
  }

  /**
   * Will be called on a form submit, save the share form data and pass the response to the parent component.
   */
  onSubmit(): void {
    this.shareForm.disable();
    const shareData = this.shareForm.getRawValue();
    const share: any = {
      itemId: this.data.targetId,
      recipientId: shareData.shareWith.id,
      data: {message: shareData.message},
      authorId: shareData.shareAs.id,
      stickyExpiry: this.getDurationDate(shareData.stickyExpiry.expiry)
    };

    this.shareService.post(share, {path: '/' + this.data.typeName})
      .subscribe(createdShare => this.dialogRef.close(createdShare), () => {
        this.notificationService.error('ERRORS.STATUSCODE.NOT_FOUND.DEFAULT');
        this.shareForm.enable();
      });
  }

  private initValueChangeValidation(): void {
    this.shareForm.controls.shareWith.valueChanges
      .pipe(distinctUntilChanged())
      .pipe(filter((shareWithData: Sender) => !!shareWithData))
      .subscribe((shareWithData: Sender) => {
        if (shareWithData.typeName === 'user') {
          this.shareForm.get('stickyExpiry').disable();
        } else {
          this.shareForm.get('stickyExpiry').enable();
        }
      });
  }

  private getDurationDate(duration: number): number {
    return duration ? new Date().getTime() + duration : null;
  }
}
