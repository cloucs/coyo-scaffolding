import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'COMMENTS.ACTIONS.UPDATE': 'Anpassen',
    'COMMENTS.ACTIONS.CANCEL': 'Abbrechen',
    'COMMENTS.ACTIONS.SEND': 'Speichern',
    'COMMENTS.ACTIONS.TRY_AGAIN': 'es erneut zu probieren',
    'COMMENTS.ACTIONS.PRESS_ESCAPE': 'Drücke **ESC** zum',
    'COMMENTS.ACTIONS.PRESS_ENTER': 'Drücke **ENTER** zum',
    'COMMENTS.ACTIONS.ERROR': 'Kommentar kann nicht mehr editiert werden.',
    'COMMENTS.FORM.STATUS.SUBMITTING': 'Sende…',
    'COMMENTS.FORM.PLACEHOLDER': 'Schreibe einen Kommentar',
    'COMMENTS.FORM.PLACEHOLDER.SHORT': 'Kommentiere…',
    'COMMENTS.LOAD_MORE': 'Weitere Kommentare anzeigen',
    'COMMENTS.EDITED': 'Editiert',
    'COMMENTS.NEW': 'Neu'
  }
};
