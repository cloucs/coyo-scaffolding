import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'COMMONS.SHARES.MESSAGE.HELP': 'Die Nachricht für deinen Beitrag',
    'COMMONS.SHARES.MESSAGE.LABEL': 'Nachricht',
    'COMMONS.SHARES.PLACEHOLDER': 'Ziel auswählen',
    'COMMONS.SHARES.SUBMIT': 'Teilen',
    'COMMONS.SHARES.SUCCESS': 'Erfolgreich geteilt',
    'COMMONS.SHARES.TIMELINE.HELP': 'Wähle aus, mit wem du dieses Element teilen möchtest',
    'COMMONS.SHARES.TIMELINE.LABEL': 'Teilen mit',
    'COMMONS.SHARES.EXTENDED_OPTIONS': 'Erweiterte Optionen',
    'COMMONS.SHARES.FROM.HELP': 'Wähle aus, in wessen Namen du dieses Element teilen möchtest',
    'COMMONS.SHARES.FROM.LABEL': 'Teilen als',
    'COMMONS.SHARES.STICKY.EXPIRY.HELP': 'Gibt an, wie lang der Beitrag sticky bleibt.',
    'COMMONS.SHARES.STICKY.EXPIRY.LABEL': 'Sticky Dauer',
    'COMMONS.SHARES.CREATE.TITLE': 'Teilen',
    'COMMONS.SHARES.LIST.TITLE': 'Teilen',
    'COMMONS.SHARES.WARNING.PRIVATE': 'Du teilst einen nicht öffentlichen Beitrag, bist du dir sicher?',
    'EVENT.SHARE': 'Event teilen',
    'MODULE.TIMELINE.TOOLTIPS.SUBSCRIBE': 'Zum Abonnieren klicken',
    'MODULE.TIMELINE.TOOLTIPS.UNSUBSCRIBE': 'Klicken um nicht mehr zu abonnieren',
    'MODULE.SOCIAL.LIKE.TEXT': '{isLiked, select, true{Dir{othersCount, plural, offset:1 =0{} =1{ und {latest}} ' +
      '=2{, {latest} und einem anderen} other{, {latest} und # andere}}} ' +
      'other{{othersCount, plural, offset:1 =0{} =1{{latest}} =2{{latest} und einem anderen} other{{latest} und # andere}}}}',
    'PAGE.SHARE': 'Seite teilen',
    'SHARE_BUTTON.SHARE_COUNT.LABEL': '{count} mal geteilt',
    'SHARE_BUTTON.SHARE_MODAL.OPEN.ARIA': 'Inhalt teilen',
    'SHARE_BUTTON.SHARES_MODAL.OPEN.ARIA': '{count, plural, one{Ein geteielter Inhalt. Zeige Teilung.} other{# geteielte Inhalte. Zeige alle Teilungen.}}',
    'WORKSPACE.SHARE': 'Workspace teilen'
  }
};
