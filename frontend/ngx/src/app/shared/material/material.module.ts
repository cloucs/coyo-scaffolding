import {APP_INITIALIZER, NgModule} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule, MatIconRegistry} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTooltipModule} from '@angular/material/tooltip';
import {DomSanitizer} from '@angular/platform-browser';
import {MaterialModule as MaterialUiModule} from '@coyo/ui';

export function registerIcons(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer): Function {
  return () => iconRegistry.addSvgIconSet(sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/sprite.defs.svg'));
}

/**
 * Module exporting used angular modules and directives.
 */
@NgModule({
  imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule,
    MatMenuModule,
    MatNativeDateModule,
    MatTooltipModule,
    MaterialUiModule
  ],
  exports: [
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule,
    MatMenuModule,
    MatNativeDateModule,
    MatTooltipModule,
    MaterialUiModule
  ],
  providers: [{
    provide: APP_INITIALIZER, useFactory: registerIcons,
    deps: [MatIconRegistry, DomSanitizer],
    multi: true
  }]
})
export class MaterialModule {
}
