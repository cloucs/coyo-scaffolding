import {SimpleChange} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {GSUITE, LOCAL_FILE_LIBRARY} from '@domain/attachment/storage';
import {FilePreview} from '@domain/preview/file-preview';
import {of} from 'rxjs';
import {NoPreviewAvailableComponent} from './no-preview-available.component';

describe('NoPreviewAvailableComponent', () => {
  let component: NoPreviewAvailableComponent;
  let fixture: ComponentFixture<NoPreviewAvailableComponent>;
  let googleApiService: jasmine.SpyObj<GoogleApiService>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NoPreviewAvailableComponent],
      providers: [{
        provide: GoogleApiService,
        useValue: jasmine.createSpyObj('googleApiService', ['isGoogleApiActive', 'getFileMetadata'])
      }, {
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['observeScreenChange'])
      }]
    }).overrideTemplate(NoPreviewAvailableComponent, '<div></div>').compileComponents();

    googleApiService = TestBed.get(GoogleApiService);
    windowSizeService = TestBed.get(WindowSizeService);
  }));

  beforeEach(() => {
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.LG));
    fixture = TestBed.createComponent(NoPreviewAvailableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should init large window size', () => {
    // given
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.LG));

    // when
    component.ngOnInit();

    // then
    component.isDesktop$.subscribe(isLargeScreen => expect(isLargeScreen).toBeTruthy());
  });

  it('should init none large window size', () => {
    // given
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.XS));

    // when
    component.ngOnInit();

    // then
    component.isDesktop$.subscribe(isLargeScreen => expect(isLargeScreen).toBeFalsy());
  });

  it('should init properties correctly for google file', () => {
    // given
    const file = {fileId: 'file-id', storage: GSUITE} as FilePreview;
    component.file = file;
    const googleFileData = {webContentLink: 'http://content.google.com/filelink'};
    googleApiService.isGoogleApiActive.and.returnValue(of(true));
    googleApiService.getFileMetadata.and.callFake(() => {
      component.googleFileData = googleFileData;
      return Promise.resolve();
    });
    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });

    // then
    expect(component.isGSuiteFile).toBeTruthy();
    expect(component.gSuiteActive).toBeTruthy();
    expect(component.googleFileData).toEqual(googleFileData);
  });

  it('should init properties correctly for none G Suite file', () => {
    // given
    const file = {fileId: 'file-id', storage: LOCAL_FILE_LIBRARY} as FilePreview;
    component.file = file;

    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });

    // then
    expect(googleApiService.isGoogleApiActive).not.toHaveBeenCalled();
    expect(googleApiService.getFileMetadata).not.toHaveBeenCalled();
    expect(component.isGSuiteFile).toBeFalsy();
    expect(component.gSuiteActive).toBeFalsy();
  });

  it('should init properties correctly when google api is not active and / or meta data not available', () => {
    // given
    const file = {fileId: 'file-id', storage: GSUITE} as FilePreview;
    component.file = file;
    googleApiService.isGoogleApiActive.and.returnValue(of(false));
    googleApiService.getFileMetadata.and.returnValue(Promise.reject('any error'));

    // when
    component.ngOnChanges({
      file: new SimpleChange(null, component.file, true)
    });

    // then
    expect(googleApiService.isGoogleApiActive).toHaveBeenCalled();
    expect(googleApiService.getFileMetadata).toHaveBeenCalled();
    expect(component.isGSuiteFile).toBeTruthy();
    expect(component.gSuiteActive).toBeFalsy();
  });
});
