import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {
  FilePreviewAnimationComponent,
  LOTTIE
} from '@shared/preview/file-preview/animation/file-preview-animation/file-preview-animation.component';
import Lottie from 'lottie-web/build/player/lottie_light';

/**
 * A lazy loaded module that contains the animation component for file preview generation.
 */
@NgModule({
  imports: [TranslateModule],
  declarations: [
    FilePreviewAnimationComponent
  ],
  exports: [
    FilePreviewAnimationComponent
  ],
  entryComponents: [
    FilePreviewAnimationComponent
  ],
  providers: [
    {provide: LOTTIE, useValue: Lottie}
  ]
})
export class FilePreviewAnimationLazyModule {
}
