import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ImageLoaderService} from '@core/image/image-loader.service';
import {ImageContainerComponent} from '@shared/preview/file-preview/image-container/image-container.component';
import {of} from 'rxjs';
import {Vec2} from './vec2';

describe('ImageContainer', () => {
  let component: ImageContainerComponent;
  let fixture: ComponentFixture<ImageContainerComponent>;
  const mockImage = {width: 50, height: 30};
  const imageLoaderService = jasmine.createSpyObj(['loadImage']);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ImageContainerComponent],
      providers: [
        {
          provide: ImageLoaderService,
          useValue: imageLoaderService
        }
      ]
    })
      .compileComponents();
    imageLoaderService.loadImage.and.callFake(() => of(mockImage));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOnProperty(component, 'canvasSize', 'get').and.returnValue(new Vec2(100, 80));
    component.updateImg(false);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should correctly initialize with an image', () => {
    expect(component.imageSize.x).toEqual(mockImage.width);
    expect(component.imageSize.y).toEqual(mockImage.height);
    expect(component.location).toEqual(new Vec2(0, 0));
  });

  it('should correctly convert screen coordinates into image coordinates', () => {
    component.zoom = 1;
    expect(component.screen2ImageLocation(new Vec2(26, 71))).toEqual(new Vec2(1, 46));

    component.zoom = 0.5;
    expect(component.screen2ImageLocation(new Vec2(67, 33))).toEqual(new Vec2(59, 1));

    component.zoom = 1;
    component.location = new Vec2(4, 6);
    expect(component.screen2ImageLocation(new Vec2(26, 71))).toEqual(new Vec2(-3, 40));

    component.zoom = 2;
    expect(component.screen2ImageLocation(new Vec2(67, 33))).toEqual(new Vec2(29.5, 5.5));
  });

  it('should correctly convert image coordinates into screen coordinates', () => {

    component.zoom = 1;
    expect(component.image2ScreenLocation(new Vec2(1, 46))).toEqual(new Vec2(26, 71));

    component.zoom = 0.5;
    expect(component.image2ScreenLocation(new Vec2(59, 1))).toEqual(new Vec2(67, 33));

    component.zoom = 1;
    component.location = new Vec2(4, 6);
    expect(component.image2ScreenLocation(new Vec2(-3, 40))).toEqual(new Vec2(26, 71));

    component.zoom = 2;
    expect(component.image2ScreenLocation(new Vec2(29.5, 5.5))).toEqual(new Vec2(67, 33));
  });

  it('should correctly set the velocity', () => {
    component.location = new Vec2(10, 5);
    component.setLocation(new Vec2(30, 0));
    expect(component.velocity).toEqual(new Vec2(20, -5));
  });
});
