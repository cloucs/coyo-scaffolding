import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ExternalIconComponent} from './external-icon.component';

describe('ExternalIconComponent', () => {
  let component: ExternalIconComponent;
  let fixture: ComponentFixture<ExternalIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalIconComponent ]
    })
      .overrideTemplate(ExternalIconComponent, '<div></div>')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
