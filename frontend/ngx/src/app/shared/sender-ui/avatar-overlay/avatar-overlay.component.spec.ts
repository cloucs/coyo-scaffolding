import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AvatarOverlayComponent} from '@shared/sender-ui/avatar-overlay/avatar-overlay.component';

describe('AvatarImageComponent', () => {
  let component: AvatarOverlayComponent;
  let fixture: ComponentFixture<AvatarOverlayComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AvatarOverlayComponent],
      providers: []
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarOverlayComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
