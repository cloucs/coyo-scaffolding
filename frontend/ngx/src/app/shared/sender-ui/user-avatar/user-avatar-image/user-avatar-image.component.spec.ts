import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {Target} from '@domain/sender/target';
import {User} from '@domain/user/user';
import {UserService} from '@domain/user/user.service';
import {Ng1MessagingService} from '@root/typings';
import {NG1_MESSAGING_SERVICE} from '@upgrade/upgrade.module';
import {of} from 'rxjs';
import {UserAvatarImageComponent} from './user-avatar-image.component';

describe('UserAvatarImageComponent', () => {
  let component: UserAvatarImageComponent;
  let fixture: ComponentFixture<UserAvatarImageComponent>;
  let userService: jasmine.SpyObj<UserService>;
  let authService: jasmine.SpyObj<AuthService>;
  let messagingService: jasmine.SpyObj<Ng1MessagingService>;

  const user = {
    id: 'user-id',
    globalPermissions: ['ACCESS_OWN_USER_PROFILE'],
    target: {
      name: 'user',
      params: {
        id: 'user-id',
        slug: 'bob-ross'
      }
    } as Target
  } as User;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAvatarImageComponent ],
      providers: [
        {provide: UserService, useValue: jasmine.createSpyObj('userService', ['getPresenceStatus$'])},
        {provide: AuthService, useValue: jasmine.createSpyObj('authService', ['getUser'])},
        {provide: NG1_MESSAGING_SERVICE, useValue: jasmine.createSpyObj('messagingService', ['start'])}
      ]
    }).overrideTemplate(UserAvatarImageComponent, '')
    .compileComponents();

    userService = TestBed.get(UserService);
    userService.getPresenceStatus$.and.returnValue(of({state: 'ONLINE'}));
    authService = TestBed.get(AuthService);
    authService.getUser.and.returnValue(of(user));
    messagingService = TestBed.get(NG1_MESSAGING_SERVICE);
    messagingService.start.and.returnValue('');
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAvatarImageComponent);
    component = fixture.componentInstance;
    component.user = user;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should externalFlag be defined', () => {
    // given
    component.showExternalFlag = true;
    // when
    component.ngOnInit();
    // then
    expect(component.showExternalFlag).toBeDefined();
  });

  it('should call errorHandler', () => {
    // when
    component.errorHandler();
    // then
    expect(component.loadError).toBe(true);
  });

  it('should start conversation', () => {
    // given
    const event = jasmine.createSpyObj('event', ['stopPropagation']);
    const partnerId = '12345';
    // when
    component.startConversation(event, partnerId);
    // then
    expect(event.stopPropagation).toHaveBeenCalled();
    expect(messagingService.start).toHaveBeenCalled();
    expect(messagingService.start).toHaveBeenCalledWith(partnerId);
  });

  it('should set presenceStatus access own profile', () => {
    // given
    component.showOnlineStatus = true;
    // when
    component.ngOnInit();
    // then
    expect(authService.getUser).toHaveBeenCalled();
    component.presenceStatus$.subscribe(status => expect(status.state).toBe('ONLINE'));
  });

  it('should set presenceStatus (accessing other profile)', () => {
    // given
    component.showOnlineStatus = true;
    const currentUser = {id: '12345'} as User;
    authService.getUser.and.returnValue(of(currentUser));
    // when
    component.ngOnInit();
    // then
    expect(authService.getUser).toHaveBeenCalled();
    component.presenceStatus$.subscribe(status => expect(status.state).toBe('ONLINE'));
  });
});
