import {TestBed} from '@angular/core/testing';
import {NG1_SUBLINE_SERVICE} from '@upgrade/upgrade.module';
import {SublineService} from './subline.service';

describe('SublineService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    providers: [SublineService, {
      provide: NG1_SUBLINE_SERVICE,
      useValue: jasmine.createSpyObj('sublineService', ['getSublineForUser', 'getSublineForSender'])
    }]
  }));

  it('should be created', () => {
    const service: SublineService = TestBed.get(SublineService);
    expect(service).toBeTruthy();
  });

});
