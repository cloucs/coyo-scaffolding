/**
 * Optional parameter for the sender endpoint.
 */
export interface SenderParameter {
  findOnlyManagedSenders?: boolean;
  findSharingRecipients?: boolean;
  filters?: string; // url param format e.g.: "type=user&type=page&type=workspace&type=event"
}
