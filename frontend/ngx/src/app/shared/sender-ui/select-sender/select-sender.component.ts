import {HttpParams} from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Output,
  Provider
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {ScrollPositionEvent} from '@shared/sender-ui/select-sender/scroll-position-event';
import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, finalize, map, startWith, switchMap, tap} from 'rxjs/operators';
import {SenderParameter} from './sender-parameter';

const selectSenderValueProvider: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SelectSenderComponent), // tslint:disable-line:no-use-before-declare
  multi: true
};

/**
 * Component to search and select a sender.
 */
@Component({
  selector: 'coyo-select-sender',
  templateUrl: './select-sender.component.html',
  providers: [selectSenderValueProvider],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectSenderComponent implements OnInit, ControlValueAccessor {

  private static readonly scrollOffsetTrigger: number = 5;
  private static readonly debounceTime: number = 250;

  /**
   * Placeholder for the select input field.
   */
  @Input() placeholder: string;

  /**
   * Additional http params.
   */
  @Input() parameters: SenderParameter;

  /**
   * Flag to disable the select.
   */
  @Input() disabled?: boolean = false;

  /**
   * Selected sender.
   */
  @Input() ngModel?: Sender;

  /**
   * Selected sender.
   */
  @Output() ngModelChange: EventEmitter<Sender> = new EventEmitter<Sender>();

  senders$: Observable<Sender[]>;
  senderFilterInput$: Subject<string> = new Subject<string>();
  pageNumber: Subject<number> = new Subject();
  loading: boolean = false;

  private page: Page<Sender> = {} as Page<Sender>;
  private searchTerm: string = '';
  private path: string;
  private senders: Sender[] = [];
  private onChangeFn: any;

  constructor(private senderService: SenderService,
              private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.page.number = 0;
    this.page.size = 10;
    this.page.totalPages = 0;
    this.page.totalElements = 0;
    this.setUrlPath();
    this.initializeSenders();
    this.initializeSenderFilter();
  }

  /* tslint:disable-next-line:completed-docs */
  registerOnChange(fn: any): void {
    this.onChangeFn = fn;
  }

  /* tslint:disable-next-line:completed-docs */
  registerOnTouched(fn: any): void {
  }

  /* tslint:disable-next-line:completed-docs */
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  /* tslint:disable-next-line:completed-docs */
  writeValue(value: any): void {
    this.ngModel = value;
  }

  /**
   * Will be called when the ng-select triggers a scroll event.
   *
   * @param $event The event.
   */
  onScroll($event: ScrollPositionEvent): void {
    if (this.loading) {
      return;
    }
    let elements = (this.page.number + 1) * this.page.size;
    if (this.page.last) {
      elements -= this.page.size;
      elements += this.page.numberOfElements;
    }
    if ($event.end + SelectSenderComponent.scrollOffsetTrigger > elements && this.page.number + 1 < this.page.totalPages) {
      this.pageNumber.next(++this.page.number);
    }
  }

  /**
   * Is executed when the ng-select triggers a change event.
   *
   * @param selectedSender the selected sender.
   */
  onChange(selectedSender: Sender): void {
    const sender = selectedSender ? selectedSender : null;
    if (this.onChangeFn) {
      this.onChangeFn(sender);
    }
    this.ngModelChange.emit(sender);
  }

  private setUrlPath(): void {
    if (this.parameters) {
      if (this.parameters.findOnlyManagedSenders) {
        this.path = '/search/managed';
      } else if (this.parameters.findSharingRecipients) {
        this.path = '/search/sharing-recipients';
      } else {
        this.path = '/search';
      }
    }
  }

  private initializeSenders(): void {
    this.senders$ = this.pageNumber.asObservable()
      .pipe(
        startWith(0),
        switchMap(pageNumber => this.getSenders(pageNumber)),
        map(content => this.senders = [...this.senders, ...content])
      );
  }

  private initializeSenderFilter(): void {
    this.senderFilterInput$
      .pipe(
        debounceTime(SelectSenderComponent.debounceTime),
        distinctUntilChanged()
      )
      .subscribe(term => {
        this.searchTerm = term ? term : '';
        this.senders = [];
        this.pageNumber.next(0);
      });
  }

  private getSenders(pageNumber: number): Observable<Sender[]> {
    this.loading = true;
    const params = new HttpParams({fromObject: this.parameters as any})
      .set('term', this.searchTerm);
    return this.senderService
      .getPage(new Pageable(pageNumber, this.page.size), {params, path: this.path})
      .pipe(
        tap(response => this.page = response),
        map(response => response.content),
        finalize(() => {
          this.loading = false;
          this.changeDetectorRef.markForCheck();
        })
      );
  }
}
