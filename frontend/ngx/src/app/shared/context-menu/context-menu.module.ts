import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {ContextMenuComponent} from '@shared/context-menu/context-menu.component';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {messagesDe} from './de.context-menu.messages';
import {messagesEn} from './en.context-menu.messages';

/**
 * Module exporting the context menu component.
 */
@NgModule({
  imports: [
    BsDropdownModule,
    CoyoCommonsModule,
    TooltipModule,
    TranslateModule
  ],
  declarations: [
    ContextMenuComponent
  ],
  exports: [
    ContextMenuComponent
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn}
  ],
})
export class ContextMenuModule {}
