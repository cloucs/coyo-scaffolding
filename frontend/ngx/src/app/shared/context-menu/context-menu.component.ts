import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Inject,
  Input,
  OnDestroy,
  ViewChild
} from '@angular/core';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {Ng1ScrollBehaviourService} from '@root/typings';
import {NG1_SCROLL_BEHAVIOR_SERVICE} from '@upgrade/upgrade.module';
import {BsDropdownDirective} from 'ngx-bootstrap/dropdown';
import {Observable, Subscription} from 'rxjs';
import {map, take} from 'rxjs/operators';

/**
 * Component creates a context menu as dropdown on desktop or overlay on mobile.
 *
 * List item content will be rendered into the dropdown/overlay.
 *
 * @example
 * ```html
 * <coyo-context-menu headline="Greetings">
 *   <li><a href="#" role="menuitem">Hello World</a></li>
 * </coyo-context-menu>
 * ```
 */
@Component({
  selector: 'coyo-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContextMenuComponent implements AfterViewInit, OnDestroy {
  private isOpenSubscription: Subscription;

  isOpen: boolean;
  isFocused: boolean;

  /**
   * The active state of the component, i.e. if the component has either focus or is open.
   */
  @HostBinding('class.active') get isActive(): boolean {
    return this.isOpen || this.isFocused;
  }

  /**
   * The headline message rendered as first and not selectable element in the dropdown.
   */
  @Input() headline: string;

  /**
   * The headline message for screen readers.
   */
  @Input() headlineAria?: string;

  /**
   * The tooltip for the context menu toggle.
   */
  @Input() tooltipText: string;

  /**
   * The icon for the context menu toggle.
   */
  @Input() icon: string = 'more-horizontal';

  /**
   * Flag to add the active state to the context menu. The trigger is then rendered in link-color.
   */
    // tslint:disable-next-line:no-input-rename
  @Input('active') btnActive: boolean = false;

  /**
   * Callback function called when the context menu is opened.
   */
  @Input() onOpen: () => Observable<any>;

  /**
   * Flag to show an empty message. As the context menu is relying on the ng-content mechanism
   * this flag is needed as an input to decide when to show the 'no actions' list item.
   */
  @Input() empty: boolean;

  /**
   * Flag to show a loading spinner.
   */
  @Input() loading: boolean;

  /**
   * The vertical placement of the context menu.
   */
  @Input() verticalPlacement: 'top' | 'bottom' = 'bottom';

  /**
   * The horizontal placement of the context menu.
   */
  @Input() horizontalPlacement: 'left' | 'right' = 'right';

  @ViewChild('dropdown', {
    static: true
  }) dropdown: BsDropdownDirective;

  screenSizeXs$: Observable<boolean>;

  constructor(private cd: ChangeDetectorRef,
              private windowSizeService: WindowSizeService,
              @Inject(NG1_SCROLL_BEHAVIOR_SERVICE) private scrollBehaviourService: Ng1ScrollBehaviourService) {
    this.screenSizeXs$ = this.windowSizeService.observeScreenChange()
      .pipe(map(screenSize => screenSize === ScreenSize.XS));
  }

  ngAfterViewInit(): void {
    this.isOpenSubscription = this.dropdown.isOpenChange.subscribe((isOpen: boolean) => {
      this.isOpen = isOpen;
      this.cd.markForCheck();
      if (this.windowSizeService.isSm() || this.windowSizeService.isXs()) {
        isOpen
          ? this.scrollBehaviourService.disableBodyScrolling()
          : this.scrollBehaviourService.enableBodyScrolling();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.isOpenSubscription && !this.isOpenSubscription.closed) {
      this.isOpenSubscription.unsubscribe();
    }
  }

  onFocusChanged(isFocused: boolean): void {
    this.isFocused = isFocused;
  }

  onShown(): void {
    if (this.onOpen) {
      this.onOpen().pipe(take(1)).subscribe();
    }
  }
}
