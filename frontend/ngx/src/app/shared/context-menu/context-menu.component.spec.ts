import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {NG1_SCROLL_BEHAVIOR_SERVICE} from '@upgrade/upgrade.module';
import {of} from 'rxjs';
import {ContextMenuComponent} from './context-menu.component';

describe('ContextMenuComponent', () => {
  let component: ContextMenuComponent;
  let fixture: ComponentFixture<ContextMenuComponent>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContextMenuComponent],
      providers: [{
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['observeScreenChange'])
      }, {
        provide: NG1_SCROLL_BEHAVIOR_SERVICE,
        useValue: jasmine.createSpyObj('scrollBehaviourService', ['enableBodyScrolling', 'disableBodyScrolling'])
      }]
    }).overrideTemplate(ContextMenuComponent, '')
      .compileComponents();

    windowSizeService = TestBed.get(WindowSizeService);
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.LG));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextMenuComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(windowSizeService.observeScreenChange).toHaveBeenCalled();
  });

  it('should call the onOpen callback', fakeAsync(() => {
    // given
    const spy = jasmine.createSpy('onOpenCallback');
    component.onOpen = spy;
    spy.and.returnValue(of({}));

    // when
    component.onShown();
    tick();

    // then
    expect(spy).toHaveBeenCalled();
  }));
});
