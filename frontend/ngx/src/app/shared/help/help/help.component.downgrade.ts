import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {HelpComponent} from './help.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoHelp', downgradeComponent({
    component: HelpComponent,
    propagateDigest: false
  }));
