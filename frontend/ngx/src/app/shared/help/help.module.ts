import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {HashtagModule} from '@shared/hashtags/hashtag.module';
import {MarkdownModule} from '@shared/markdown/markdown.module';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {HelpModalComponent} from './help-modal/help-modal.component';
import {HelpComponent} from './help/help.component';
import './help/help.component.downgrade';

/**
 * Module for the help component
 */
@NgModule({
  imports: [
    CoyoCommonsModule,
    HashtagModule,
    MarkdownModule,
    TooltipModule
  ],
  declarations: [
    HelpComponent,
    HelpModalComponent
  ],
  exports: [
    HelpComponent
  ],
  entryComponents: [
    HelpComponent,
    HelpModalComponent
  ]
})
export class HelpModule {}
