import {CoyoFormsModule} from './forms.module';

describe('CoyoFormsModule', () => {
  let coyoFormsModule: CoyoFormsModule;

  beforeEach(() => {
    coyoFormsModule = new CoyoFormsModule();
  });

  it('should create an instance', () => {
    expect(coyoFormsModule).toBeTruthy();
  });
});
