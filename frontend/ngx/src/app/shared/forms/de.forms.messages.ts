import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'CHRONO_UNIT.YEARS': 'Jahre',
    'CHRONO_UNIT.MONTHS': 'Monate',
    'CHRONO_UNIT.DAYS': 'Tage',
    'CHRONO_UNIT.HOURS': 'Stunden',
    'CHRONO_UNIT.MINUTES': 'Minuten',
    'CHRONO_UNIT.SECONDS': 'Sekunden',
    'CHRONO_UNIT.MILLISECONDS': 'Millisekunden',
    'VALIDATION.ERROR.REQUIRED': 'Dieses Feld muss ausgefüllt sein',
    'VALIDATION.ERROR.PATTERN': 'Diese Eingabe ist ungültig'
  }
};
