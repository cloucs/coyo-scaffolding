import {DurationFormatter, DurationUnit} from './duration';
import {DurationPickerMode, DurationRangeChecker} from './duration-range-checker';

describe('Duration', () => {

  it('should evaluate unit maximum values for time in LENIENT mode', () => {
    // given
    const value = DurationFormatter.parseIso8601('PT2H3M4S');
    const max = DurationFormatter.parseIso8601('PT9H8M7S');
    const uut = new DurationRangeChecker();

    // when
    uut.mode = DurationPickerMode.LENIENT;
    uut.value = value;
    uut.max = max;

    // then
    expect(uut.getMaxUnit(DurationUnit.DAYS)).toEqual(0);
    expect(uut.getMaxUnit(DurationUnit.HOURS)).toEqual(9); // PT9H3M4S -> PT9H3M4S
    expect(uut.getMaxUnit(DurationUnit.MINUTES)).toEqual(428); // PT2H428M4S -> PT9H8M4S
    expect(uut.getMaxUnit(DurationUnit.SECONDS)).toEqual(25507); // PT2H3M25507S -> PT9H8M7S
  });

  it('should limit units for time in NORMALIZE mode', () => {
    // given
    const value = DurationFormatter.parseIso8601('PT2H3M4S');
    const max = DurationFormatter.parseIso8601('PT9H8M7S');
    const uut = new DurationRangeChecker();

    // when
    uut.mode = DurationPickerMode.NORMALIZE;
    uut.value = value;
    uut.max = max;

    // then
    expect(uut.getMaxUnit(DurationUnit.DAYS)).toEqual(0);
    expect(uut.getMaxUnit(DurationUnit.HOURS)).toEqual(9);
    expect(uut.getMaxUnit(DurationUnit.MINUTES)).toEqual(60);
    expect(uut.getMaxUnit(DurationUnit.SECONDS)).toEqual(60);
  });

  it('should limit units for time in STRICT mode', () => {
    // given
    const value = DurationFormatter.parseIso8601('PT2H3M4S');
    const max = DurationFormatter.parseIso8601('PT9H8M7S');
    const uut = new DurationRangeChecker();

    // when
    uut.mode = DurationPickerMode.STRICT;
    uut.value = value;
    uut.max = max;

    // then
    expect(uut.getMaxUnit(DurationUnit.DAYS)).toEqual(0);
    expect(uut.getMaxUnit(DurationUnit.HOURS)).toEqual(9);
    expect(uut.getMaxUnit(DurationUnit.MINUTES)).toEqual(59);
    expect(uut.getMaxUnit(DurationUnit.SECONDS)).toEqual(59);
  });

  it('should evaluate unit maximum values for time with days', () => {
    // given
    const value = DurationFormatter.parseIso8601('P1DT2H3M4S');
    const max = DurationFormatter.parseIso8601('P2DT9H8M7S');
    const uut = new DurationRangeChecker();

    // when
    uut.value = value;
    uut.max = max;

    // then
    expect(uut.getMaxUnit(DurationUnit.DAYS)).toEqual(2);
    expect(uut.getMaxUnit(DurationUnit.HOURS)).toEqual(33); // P1T33H3M4S -> P2DT9H3M4S
    expect(uut.getMaxUnit(DurationUnit.MINUTES)).toEqual(1868); // P1DT2H1868M4S -> P2DT9H8M4S
    expect(uut.getMaxUnit(DurationUnit.SECONDS)).toEqual(111907); // P1DT2H3M111907S -> P2DT9H8M7S
  });
});
