import {AbstractControl, FormGroup} from '@angular/forms';
import {of} from 'rxjs';
import {CoyoValidators} from './validators';

describe('CoyoValidators', () => {

  describe('notBlank', () => {

    it('validate a valid input', () => {
      // given
      const control = {value: 'Valid text'} as AbstractControl;

      // when
      const result = CoyoValidators.notBlank(control);

      // then
      expect(result).toBeNull();
    });

    it('validate an invalid input (null)', () => {
      // given
      const control = {value: null} as AbstractControl;

      // when
      const result = CoyoValidators.notBlank(control);

      // then
      expect(result).toEqual({notBlank: true});
    });

    it('validate an invalid input (blank)', () => {
      // given
      const control = {value: '   '} as AbstractControl;

      // when
      const result = CoyoValidators.notBlank(control);

      // then
      expect(result).toEqual({notBlank: true});
    });
  });

  describe('options', () => {

    it('validate a valid input', () => {
      // given
      const control = {value: 'Apple'} as AbstractControl;

      // when
      const result = CoyoValidators.options('Apple', 'Banana', 'Orange')(control);

      // then
      expect(result).toBeNull();
    });

    it('validate an invalid input', () => {
      // given
      const control = {value: 'Peach'} as AbstractControl;

      // when
      const result = CoyoValidators.options('Apple', 'Banana', 'Orange')(control);

      // then
      expect(result).toEqual({options: {options: ['Apple', 'Banana', 'Orange'], actual: 'Peach'}});
    });
  });

  describe('url', () => {

    it('should validate empty urls', () => {
      assertIsValidUrl(null, 'null url');
      assertIsValidUrl('', 'empty url');
    });

    it('should validate urls', () => {
      assertIsValidUrl('https://www.demourl.com', 'regular URL without path');
      assertIsValidUrl('https://demourl.com/rss.url', 'regular URL with path');
      assertIsValidUrl('https://www.demourl.com?search#anchor', 'regular URL with search params and anchor');
      assertIsValidUrl('https://ww3.2-5.com', 'digits and dashes in host name');
      assertIsValidUrl('https://someserver', 'single host name without TLD');
      assertIsValidUrl('https://name:password@demourl.com', 'with userinfo');
      assertIsValidUrl('http://rs0210/approot/personal/Rueckkehrgespraech_Offen.htm', 'internal domain-1');
      assertIsValidUrl('http://rs0210/approot/einkauf/einkauf_listen_neu.htm', 'internal domain-2');
      assertIsValidUrl('http://rs0199/rueckkehr', 'internal domain-3');
    });

    it('should validate urls as server, not path', () => {
      // these cases have a debatable internal behaviour, but we had the requirements. results are the same though
      assertIsValidUrl('lokalhorst', 'unprefixed single host name');
      assertIsValidUrl('lokalhorst.local/path', 'unprefixed FQDN');
      assertIsValidUrl('lokalhorst/path', 'unprefixed FQDN with path');
    });

    it('validate invalid urls', () => {
      assertIsInvalidUrl('/absolute/path', 'absolute path only');
      assertIsInvalidUrl('//lokalhorst/path', 'with host prefix');
      assertIsInvalidUrl('https://$@!@#&^$$^$#$@#$%#$^$%&^%$@#$???.com', 'some monkey sat on the keyboard');
    });

    function assertIsValidUrl(url?: string, description?: string): void {
      const control = {value: url} as AbstractControl;

      const result = CoyoValidators.url(control);

      expect(result).toBeNull('Test case: ' + description);
    }

    function assertIsInvalidUrl(url?: string, description?: string): void {
      const control = {value: url} as AbstractControl;

      const result = CoyoValidators.url(control);

      expect(result).toBeTruthy('Test case: ' + description + ': result was undefined or null');
      if (!!result) {
        expect(result.pattern).toBeTruthy('Test case: ' + description + ': no "pattern" property found');
      }
    }
  });

  describe('requiredIfTrue', () => {

    it('validate an valid input', () => {
      // given
      const requiredTrueControlName = 'requiredNewControl';
      const requiredControlName = 'requiredControl';
      const form = createFormGroup(true, 'test');

      // when
      const validatorFn = CoyoValidators.requiredIfTrue(requiredTrueControlName, requiredControlName, control => control.value);
      const result = validatorFn(form);

      // then
      expect(result).toBeFalsy();
    });

    it('validate an invalid input', () => {
      // given
      const requiredTrueControlName = 'requiredNewControl';
      const requiredControlName = 'requiredControl';
      const form = createFormGroup(true, '');

      // when
      const validatorFn = CoyoValidators.requiredIfTrue(requiredTrueControlName, requiredControlName, control => control.value);
      const result = validatorFn(form);

      // then
      expect(result[requiredControlName].notBlank).toBeTruthy();
    });

    it('validate valid input when requiredControl is not necessary', () => {
      // given
      const requiredTrueControlName = 'requiredNewControl';
      const requiredControlName = 'requiredControl';
      const form = createFormGroup(false, '');

      // when
      const validatorFn = CoyoValidators.requiredIfTrue(requiredTrueControlName, requiredControlName, control => control.value);
      const result = validatorFn(form);

      // then
      expect(result).toBeFalsy();
    });

    it('validates api key positive', () => {
      // given
      const settingsService = jasmine.createSpyObj('settingsService', ['validateApiKey']);
      settingsService.validateApiKey.and.returnValue(of(true));
      const formControl = {
        parent: {
          get: jasmine.createSpy()
        }
      };
      const providerFormControl = {value: 'DEEP'};

      formControl.parent.get.and.returnValue(providerFormControl);

      // when
      const validatorFn = CoyoValidators.createApiKeyValidator(settingsService);
      validatorFn(formControl as any as AbstractControl).subscribe(value => {
        expect(value).toBeNull();
      });
    });

    it('validates api key negative', () => {
      // given
      const settingsService = jasmine.createSpyObj('settingsService', ['validateApiKey']);
      settingsService.validateApiKey.and.returnValue(of(false));
      const formControl = {
        parent: {
          get: jasmine.createSpy()
        }
      };
      const providerFormControl = {value: 'DEEP'};

      formControl.parent.get.and.returnValue(providerFormControl);

      // when
      const validatorFn = CoyoValidators.createApiKeyValidator(settingsService);
      validatorFn(formControl as any as AbstractControl).subscribe(value => {
        expect(value).toEqual({invalidKey: true});
      });
    });

    function createFormGroup(trueControlValue: boolean, requiredControlValue: string): FormGroup {
      const requiredTrueControl = {value: trueControlValue} as AbstractControl;
      const requiredControl = {value: requiredControlValue} as AbstractControl;
      return {
        get(name: string): AbstractControl {
          if (name === 'requiredNewControl') {
            return requiredTrueControl;
          } else {
            return requiredControl;
          }
        }
      } as FormGroup;
    }
  });
});
