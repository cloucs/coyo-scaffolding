import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'CHRONO_UNIT.YEARS': 'years',
    'CHRONO_UNIT.MONTHS': 'months',
    'CHRONO_UNIT.DAYS': 'days',
    'CHRONO_UNIT.HOURS': 'hours',
    'CHRONO_UNIT.MINUTES': 'minutes',
    'CHRONO_UNIT.SECONDS': 'seconds',
    'CHRONO_UNIT.MILLISECONDS': 'milliseconds',
    'VALIDATION.ERROR.REQUIRED': 'This field is required',
    'VALIDATION.ERROR.PATTERN': 'The pattern is wrong'
  }
};
