import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {TimeAgoComponent} from './time-ago.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoTimeAgo', downgradeComponent({
    /* tslint:disable-next-line:deprecation */
    component: TimeAgoComponent
  }));
