import {Inject, Injectable} from '@angular/core';
import {WINDOW} from '@root/injection-tokens';
import * as _ from 'lodash';
import {markdown} from './markdown';

/**
 * Service to handle markdown flavoured texts.
 */
@Injectable({
  providedIn: 'root'
})
export class MarkdownService {

  private removeMd: any = require('remove-markdown');

  constructor(@Inject(WINDOW) private window: Window) {
  }

  /**
   * Transforms text containing markdown to HTML.
   *
   * @param text the markdown-flavoured source text
   * @param minimal a minimal flag to only use a small subset of markdown directives
   *
   * @returns the HTML-enriched result
   */
  markdown(text: string, minimal: boolean = true): string {
    return this.isValid(text) ? markdown(text, minimal, this.window).replace(/\n$/g, '') : text;
  }

  /**
   * Strips all markdown directives from the given text.
   *
   * @param text the markdown-flavoured source text
   *
   * @returns the plain result
   */
  strip(text: string): string {
    return this.isValid(text) ? this.removeMd(text) : text;
  }

  private isValid(text: string): boolean {
    return _.isString(text) && !_.isEmpty(text);
  }
}
