import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {IconComponent} from './icon.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoIcon', downgradeComponent({component: IconComponent}));
