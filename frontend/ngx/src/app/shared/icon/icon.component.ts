import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

/**
 * Renders a COYO SVG icon.
 */
@Component({
  selector: 'coyo-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent {

  /**
   * The name of the icon as registered in the SVG registry.
   */
  @Input() name: string;

  /**
   * An optional ARIA label. The icon will be ARIA hidden without a label.
   */
  @Input() label: string;

  constructor() {
  }
}
