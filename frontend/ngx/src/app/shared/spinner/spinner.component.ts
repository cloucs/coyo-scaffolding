import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

/**
 * Shows a spinner.
 */
@Component({
  selector: 'coyo-spinner',
  templateUrl: './spinner.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerComponent {

  /**
   * The size of the spinner. Default is 'md'.
   *
   */
  @Input() size: 'xs' | 'sm' | 'md' | 'lg' | 'xl' = 'md';

  /**
   * Flag if the spinners colors should be inverted to use it on dark backgrounds.
   */
  @Input() inverted: boolean = false;

  constructor() {}
}
