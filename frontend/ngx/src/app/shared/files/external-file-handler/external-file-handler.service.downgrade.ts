import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {ExternalFileHandlerService} from './external-file-handler.service';

getAngularJSGlobal()
  .module('commons.ui')
  .factory('externalFileHandlerService', downgradeInjectable(ExternalFileHandlerService) as any);
