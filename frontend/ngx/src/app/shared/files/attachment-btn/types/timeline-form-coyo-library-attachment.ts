/**
 * Represents a timeline form coyo file library attachment.
 */
export interface TimelineFormCoyoLibraryAttachment {
  fileId: string;
  senderId: string;
  name: string;
  displayName: string;
  storage: string;
}
