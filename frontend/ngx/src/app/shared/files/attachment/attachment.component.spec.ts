import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import * as _ from 'lodash';
import {FileItem, FileUploader} from 'ng2-file-upload';
import {AttachmentComponent} from './attachment.component';

describe('AttachmentComponent', () => {
  let component: AttachmentComponent;
  let fixture: ComponentFixture<AttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AttachmentComponent]
    }).overrideTemplate(AttachmentComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given
    component.fileItem = new FileItem(
      new FileUploader({}),
      new File([''], 'filename', {type: 'text/html'}),
      null
    );

    // when
    fixture.detectChanges();

    // then
    let called = false;
    component.state$.subscribe(value => {
      expect(value.progress).toEqual(0);
      expect(value.completed).toEqual(false);
      called = true;
    });
    expect(component.fileItem.onProgress).toBeDefined();
    expect(component.fileItem.onSuccess).toBeDefined();
    expect(called).toBeTruthy();
  });

  it('should emit remove event for fie item', () => {
    // given
    component.fileItem = {file: {name: 'file.png'}} as FileItem;
    spyOn(component.removedFileItem, 'emit');

    // when
    component.removeFile(component.fileItem);

    // then
    expect(component.removedFileItem.emit).toHaveBeenCalledWith(component.fileItem);
  });

  it('should recognize protected files', () => {
    // given
    component.fileItem = {file: {name: 'file.png'}} as FileItem;
    _.set(component.fileItem, 'visibility', 'PRIVATE');

    // when
    const protectedValue = component.isFileProtected();

    // then
    expect(protectedValue).toBe(true);
  });

  it('should recognize public files', () => {
    // given
    component.fileItem = {file: {name: 'file.png'}} as FileItem;
    _.set(component.fileItem, 'visibility', 'PUBLIC');

    // when
    const protectedValue = component.isFileProtected();

    // then
    expect(protectedValue).toBe(false);
  });

  it('should consider other files public', () => {
    // given
    component.fileItem = {file: {name: 'file.png'}} as FileItem;

    // when
    const protectedValue = component.isFileProtected();

    // then
    expect(protectedValue).toBe(false);
  });
});
