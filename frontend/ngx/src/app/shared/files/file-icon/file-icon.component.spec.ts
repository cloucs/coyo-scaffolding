import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {File} from '@domain/file/file';
import {IconService} from '@domain/icon/icon.service';
import {FileIconComponent} from './file-icon.component';

describe('FileIconComponent', () => {
  let component: FileIconComponent;
  let fixture: ComponentFixture<FileIconComponent>;
  let iconServiceMock: jasmine.SpyObj<IconService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileIconComponent ],
      providers: [
        {provide: IconService, useValue: jasmine.createSpyObj('iconService', ['getFileIcons'])}
      ]
    }).overrideTemplate(FileIconComponent, '<div></div>')
    .compileComponents();

    iconServiceMock = TestBed.get(IconService);
    iconServiceMock.getFileIcons.and.returnValue(['zmdi-coyo zmdi-coyo-image']);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get icons on changes', () => {
    // given
    component.file = {contentType: 'image/png'} as File;
    // when
    component.ngOnChanges();
    // then
    expect(component.icons).toEqual(['zmdi-coyo zmdi-coyo-image']);
  });
});
