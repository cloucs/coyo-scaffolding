import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GSUITE} from '@domain/attachment/storage';
import {IconService} from '@domain/icon/icon.service';
import {FileIconByMimeTypeComponent} from '@shared/files/file-icon/file-icon-by-mime-type.component';

describe('FileIconByMimeTypeComponent', () => {
  let component: FileIconByMimeTypeComponent;
  let fixture: ComponentFixture<FileIconByMimeTypeComponent>;
  let iconServiceMock: jasmine.SpyObj<IconService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileIconByMimeTypeComponent],
      providers: [
        {provide: IconService, useValue: jasmine.createSpyObj('iconService', ['getFileIconByMimeType'])}
      ]
    }).overrideTemplate(FileIconByMimeTypeComponent, '<div></div>')
      .compileComponents();

    iconServiceMock = TestBed.get(IconService);
    iconServiceMock.getFileIconByMimeType.and.returnValue('zmdi-coyo zmdi-coyo-image');
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileIconByMimeTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get icon on changes', () => {
    // given
    component.mimeType = 'image/png';
    component.storage = GSUITE;
    // when
    component.ngOnChanges();
    // then
    expect(component.icon).toEqual('zmdi-coyo zmdi-coyo-image');
    expect(iconServiceMock.getFileIconByMimeType).toHaveBeenCalledWith(component.mimeType, component.storage);
  });
});
