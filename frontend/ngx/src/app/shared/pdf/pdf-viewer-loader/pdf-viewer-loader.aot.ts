import {ComponentFactory, Injectable, Injector, NgModuleFactory} from '@angular/core';
import {PdfLazyModule} from '@shared/pdf/pdf-lazy.module';
import {PdfViewerComponent} from '@shared/pdf/pdf-viewer/pdf-viewer.component';

@Injectable({
  providedIn: 'root'
})
export class PdfViewerLoader {
  constructor(private injector: Injector) {}

  load(): Promise<ComponentFactory<PdfViewerComponent>> {
    // @ts-ignore
    return import('../pdf-lazy.module.ngfactory')
      .then(module => {
        const moduleRef = (module.PdfLazyModuleNgFactory as NgModuleFactory<PdfLazyModule>).create(this.injector);
        const factory = moduleRef.componentFactoryResolver.resolveComponentFactory(PdfViewerComponent);
        return factory;
      }) as Promise<PdfViewerComponent>;
  }
}
