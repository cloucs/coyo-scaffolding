import {EventEmitter, SimpleChange} from '@angular/core';
import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {PdfViewerLoaderComponent} from '@shared/pdf/pdf-viewer-loader/pdf-viewer-loader.component';
import {PdfViewerComponent} from '../pdf-viewer/pdf-viewer.component';

describe('PdfViewerLoaderComponent', () => {
  let component: PdfViewerLoaderComponent;
  let fixture: ComponentFixture<PdfViewerLoaderComponent>;
  let pdfViewerComponent: jasmine.SpyObj<PdfViewerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PdfViewerLoaderComponent],
      providers: []
    }).compileComponents();
    pdfViewerComponent = jasmine.createSpyObj('PdfViewerComponent', ['ngOnChanges']);
  });

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(PdfViewerLoaderComponent);
    component = fixture.componentInstance;

    // @ts-ignore
    pdfViewerComponent.loadingCompleted = new EventEmitter<void>();
    // @ts-ignore
    pdfViewerComponent.loadingFailed = new EventEmitter<void>();

    // @ts-ignore
    spyOn(component, 'createPdfViewerComponent').and.returnValue(Promise.resolve(pdfViewerComponent));

    component.ngOnInit();
    tick();
  }));

  it('should call ngOnChanges of PdfViewerComponent with same changes', () => {
    // given
    const changes = {
      src: new SimpleChange('oldSrc', 'currentSrc', false)
    };

    // when
    component.ngOnChanges(changes);

    // then
    expect(pdfViewerComponent.ngOnChanges).toHaveBeenCalledWith(changes);
  });

  it('should set inputs/outputs of PdfViewerComponent', () => {
    // given
    const changes = {
      src: new SimpleChange('oldSrc', 'currentSrc', false),
      showToolbar: new SimpleChange(false, true, false),
      loadingCompleted: new SimpleChange(new EventEmitter<void>(), new EventEmitter<void>(), false)
    };

    // when
    component.ngOnChanges(changes);

    // then
    expect(pdfViewerComponent.src).toBe(changes.src.currentValue);
    expect(pdfViewerComponent.showToolbar).toBe(changes.showToolbar.currentValue);
    expect(pdfViewerComponent.loadingCompleted).toBe(changes.loadingCompleted.currentValue);
  });

});
