import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'PDFVIEWER.OF': 'von',
    'PDFVIEWER.AUTOZOOM': 'Automatischer Zoom',
    'PDFVIEWER.ORIGINALSIZE': 'Originalgröße'
  }
};
