import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {messagesDe} from '@shared/pdf/de.pdf.messages';
import {messagesEn} from '@shared/pdf/en.pdf.messages';
import {PdfViewerLoaderComponent} from '@shared/pdf/pdf-viewer-loader/pdf-viewer-loader.component';

/**
 * Module that lazy loads the pdf viewer module.
 */
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PdfViewerLoaderComponent
  ],
  exports: [
    PdfViewerLoaderComponent
  ],
  entryComponents: [
    PdfViewerLoaderComponent
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn}
  ]
})
export class PdfModule {
}
