import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {GDrivePickerService} from '@app/integration/gsuite/g-drive-picker/g-drive-picker.service';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {UrlService} from '@core/http/url/url.service';
import {ModalService} from '@domain/modal/modal/modal.service';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {ImageFromGSuitePlugin} from '@shared/rte/image-plugin/image-from-g-suite-plugin';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';
import {NG1_COYO_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import * as _ from 'lodash';

describe('ImageFromGSuitePlugin', () => {
  let imageFromGSuitePlugin: ImageFromGSuitePlugin;
  let gDrivePickerService: jasmine.SpyObj<GDrivePickerService>;
  let froala: jasmine.SpyObj<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImageFromGSuitePlugin, {
        provide: FROALA_EDITOR,
        useValue: jasmine.createSpyObj('froala', ['RegisterCommand'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }, {
        provide: GDrivePickerService,
        useValue: jasmine.createSpyObj('gDrivePickerService', ['open'])
      }, {
        provide: GoogleApiService,
        useValue: jasmine.createSpyObj('googleApiService', ['getFileMetadata', 'isFilePublicVisible'])
      }, {
        provide: NG1_COYO_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('coyoNotification', ['error'])
      }, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('urlService', ['getBackendUrl'])
      }, {
        provide: ModalService,
        useValue: jasmine.createSpyObj('modalService', ['openConfirm'])
      }],
      imports: [HttpClientTestingModule]
    });

    imageFromGSuitePlugin = TestBed.get(ImageFromGSuitePlugin);
    gDrivePickerService = TestBed.get(GDrivePickerService);
    froala = TestBed.get(FROALA_EDITOR);
  });

  it('should be created', () => {
    expect(imageFromGSuitePlugin).toBeTruthy();
  });

  it('should not initialize file GSuite command without permissions', () => {
    // when
    imageFromGSuitePlugin.initialize({canAccessGoogle: false} as RteSettings);

    // then
    expect(froala.RegisterCommand).not.toHaveBeenCalled();
  });

  it('should initialize file GSuite command', () => {
    // when
    imageFromGSuitePlugin.initialize({canAccessGoogle: true} as RteSettings);

    // then
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertImageFromGSuite', jasmine.any(Object));
  });

  it('should open GSuite picker', () => {
    // given
    const editor = {
      id: 'id',
      selection: jasmine.createSpyObj('selection', ['text', 'save', 'restore']),
      $oel: jasmine.createSpyObj('$oel', ['find']),
      image: jasmine.createSpyObj('image', ['get', 'insert']),
      html: jasmine.createSpyObj('html', ['insert']),
      undo: jasmine.createSpyObj('undo', ['saveStep'])
    };
    editor.$oel.find.and.returnValue({
      scrollTop: () => {
      },
      remove: () => {}
    });
    gDrivePickerService.open.and.returnValue(Promise.resolve());

    // when
    imageFromGSuitePlugin.initialize({canAccessGoogle: true} as RteSettings);
    callCallbackOfCommand('coyoInsertImageFromGSuite', editor);

    // then
    expect(gDrivePickerService.open).toHaveBeenCalled();
  });

  function callCallbackOfCommand(commandKey: string, editor: any): void {
    const calls = froala.RegisterCommand.calls.all();
    const command = _.find(calls, (call: jasmine.CallInfo): boolean => call.args[0] === commandKey);
    command.args[1].callback.apply(editor);
  }
});
