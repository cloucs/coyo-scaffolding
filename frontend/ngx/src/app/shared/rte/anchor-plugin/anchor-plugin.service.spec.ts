import {DOCUMENT} from '@angular/common';
import {inject, TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';
import * as _ from 'lodash';
import {AnchorPluginService} from './anchor-plugin.service';

describe('AnchorPluginService', () => {
  let document: jasmine.SpyObj<Document>;
  let froala: jasmine.SpyObj<any>;

  beforeEach(() => {
    const elementMock = jasmine.createSpyObj<Document>('element', ['querySelectorAll']);
    elementMock.querySelectorAll.and.returnValue([]);
    const documentMock = jasmine.createSpyObj('document', ['createElement']);
    documentMock.createElement.and.returnValue(elementMock);
    const froalaMock = jasmine.createSpyObj('froala', ['RegisterCommand', 'DefineIcon']);
    froalaMock.PLUGINS = {};
    froalaMock.POPUP_TEMPLATES = {};
    froalaMock.START_MARKER = '';
    froalaMock.END_MARKER = '';

    TestBed.configureTestingModule({
      providers: [AnchorPluginService, {
        provide: DOCUMENT,
        useValue: documentMock
      }, {
        provide: FROALA_EDITOR,
        useValue: froalaMock
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }]
    });

    document = TestBed.get(DOCUMENT);
    froala = TestBed.get(FROALA_EDITOR);
  });

  it('should be created', inject([AnchorPluginService], (service: AnchorPluginService) => {
    expect(service).toBeTruthy();
  }));

  it('should register icons', inject([AnchorPluginService], (service: AnchorPluginService) => {
    // given
    const icons = [
      'coyoAnchorIcon',
      'coyoAnchorRemoveIcon'];

    // when
    service.initialize({} as RteSettings);

    // then
    icons.forEach(icon => expect(froala.DefineIcon).toHaveBeenCalledWith(icon, jasmine.any(Object)));
  }));

  it('should register commands', inject([AnchorPluginService], (service: AnchorPluginService) => {
    // when
    service.initialize({} as RteSettings);

    // then
    expect(froala.PLUGINS.coyoAnchorPlugin()).toBeTruthy();
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertAnchor', jasmine.any(Object));
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertAnchorImage', jasmine.any(Object));
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoEditAnchor', jasmine.any(Object));
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoRemoveAnchor', jasmine.any(Object));
  }));

  it('should insert anchor', inject([AnchorPluginService], (service: AnchorPluginService): void => {
    // given
    const editor = {
      image: jasmine.createSpyObj('image', ['get']),
      popups: jasmine.createSpyObj('froalaPopups', ['get', 'hide']),
      selection: jasmine.createSpyObj('froalaSelection', ['get', 'text', 'save', 'remove', 'restore']),
      html: jasmine.createSpyObj('html', ['get', 'getSelected', 'insert']),
      clean: jasmine.createSpyObj('clean', ['html'])
    };
    editor['selection'].get.and.returnValue({anchorNode: {}});
    editor['selection'].text.and.returnValue('');
    const popup = jasmine.createSpyObj('popup', ['find']);
    const inputs = jasmine.createSpyObj('inputs', ['filter']);
    const nameInput = jasmine.createSpyObj('nameInput', ['val']);
    const textInput = jasmine.createSpyObj('textInput', ['val']);
    editor['popups'].get.and.returnValue(popup);
    popup['find'].and.returnValue(inputs);
    inputs.filter.and.callFake((filter: string) => filter.includes('anchorName') ? nameInput : textInput);
    nameInput.val.and.returnValue('linkName');
    textInput.val.and.returnValue('Link Text');

    // when
    service.initialize({} as RteSettings);
    callCallbackOfCommand('coyoInsertAnchor', ['coyoInsertAnchorSubmit'], editor);

    // then
    expect(editor['popups'].hide).toHaveBeenCalled();
    expect(editor['html'].insert).toHaveBeenCalledWith('<a id="linkname" name="linkname" class="fr-anchor">Link Text</a>');
  }));

  it('should insert anchor link', inject([AnchorPluginService], (service: AnchorPluginService) => {
    // given
    const editor = {
      popups: jasmine.createSpyObj('froalaPopups', ['get', 'hide']),
      selection: jasmine.createSpyObj('froalaSelection', ['get', 'element', 'text', 'save', 'remove', 'restore']),
      html: jasmine.createSpyObj('html', ['getSelected', 'insert']),
      clean: jasmine.createSpyObj('clean', ['html'])
    };
    editor['selection'].get.and.returnValue({anchorNode: {}});
    editor['selection'].element.and.returnValue(jasmine.createSpyObj('element', ['setAttribute']));
    editor['selection'].text.and.returnValue('');
    const popup = jasmine.createSpyObj('popup', ['find']);
    const nameInput = jasmine.createSpyObj('nameInput', ['val']);
    const textInput = jasmine.createSpyObj('textInput', ['val']);
    editor['popups'].get.and.returnValue(popup);
    popup['find'].and.callFake((filter: string) => filter.includes('select') ? nameInput : textInput);
    nameInput.val.and.returnValue('linkName');
    textInput.val.and.returnValue('Link Text');

    service.initialize({} as RteSettings);
    callCallbackOfCommand('coyoInsertAnchor', ['coyoInsertAnchorLinkSubmit'], editor);

    // then
    expect(editor['popups'].hide).toHaveBeenCalled();
    expect(editor['html'].insert).toHaveBeenCalledWith('<a href="#linkname">Link Text</a>');
  }));

  it('should edit anchor', inject([AnchorPluginService], (service: AnchorPluginService) => {
    // given
    const editor = {
      html: jasmine.createSpyObj('html', ['get']),
      popups: jasmine.createSpyObj('froalaPopups', ['get', 'hide']),
      selection: jasmine.createSpyObj('selection', ['get', 'element'])
    };
    const anchor = jasmine.createSpyObj('anchor', ['id', 'setAttribute']);
    const popup = jasmine.createSpyObj('popup', ['find']);
    const textInputs = jasmine.createSpyObj('textInputs', ['filter']);
    const textValue = jasmine.createSpyObj('value', ['val']);
    editor['html'].get.and.returnValue('');
    editor['popups'].get.and.returnValue(popup);
    popup.find.and.returnValue(textInputs);
    textInputs.filter.and.returnValue(textValue);
    textValue.val.and.returnValue('an anchor');
    editor['selection'].get.and.returnValue({});
    editor['selection'].element.and.returnValue(anchor);
    anchor.id.and.returnValue('an-anchor2');
    anchor.classList = jasmine.createSpyObj('anchorClassMock', ['contains']);
    anchor.classList.contains.and.returnValue(true);

    // when
    service.initialize({} as RteSettings);
    callCallbackOfCommand('coyoEditAnchor', ['coyoEditAnchorSubmit'], editor);

    // then
    expect(editor['popups'].hide).toHaveBeenCalled();
    expect(anchor.setAttribute).toHaveBeenCalledWith('id', 'an-anchor');
  }));

  it('should remove anchor when editing with empty input field',
    inject([AnchorPluginService], (service: AnchorPluginService) => {
      // given
      const editor = {
        html: jasmine.createSpyObj('html', ['get']),
        link: jasmine.createSpyObj('link', ['remove']),
        popups: jasmine.createSpyObj('froalaPopups', ['get', 'hide']),
        selection: jasmine.createSpyObj('selection', ['get', 'element'])
      };
      const anchor = jasmine.createSpyObj('anchor', ['id']);
      const popup = jasmine.createSpyObj('popup', ['find']);
      const textInputs = jasmine.createSpyObj('textInputs', ['filter']);
      const textValue = jasmine.createSpyObj('value', ['val']);
      editor['html'].get.and.returnValue('');
      editor['popups'].get.and.returnValue(popup);
      popup.find.and.returnValue(textInputs);
      textInputs.filter.and.returnValue(textValue);
      textValue.val.and.returnValue('');
      editor['selection'].get.and.returnValue({});
      editor['selection'].element.and.returnValue(anchor);
      anchor.id.and.returnValue('an-anchor2');
      anchor.classList = jasmine.createSpyObj('anchorClassMock', ['contains']);
      anchor.classList.contains.and.returnValue(true);

      // when
      service.initialize({} as RteSettings);
      callCallbackOfCommand('coyoEditAnchor', ['coyoEditAnchorSubmit'], editor);

      // then
      expect(editor['popups'].hide).toHaveBeenCalled();
      expect(editor['link'].remove).toHaveBeenCalled();
    }));

  function callCallbackOfCommand(commandKey: string, params: string[], editor: any): void {
    const calls = froala.RegisterCommand.calls.all();
    const command = _.find(calls, (call: jasmine.CallInfo): boolean => call.args[0] === commandKey);
    params.unshift(commandKey);
    command.args[1].callback.apply(editor, params);
  }
});
