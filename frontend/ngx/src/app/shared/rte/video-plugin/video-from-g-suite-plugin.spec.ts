import {DOCUMENT} from '@angular/common';
import {TestBed} from '@angular/core/testing';
import {GDrivePickerService} from '@app/integration/gsuite/g-drive-picker/g-drive-picker.service';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {ModalService} from '@domain/modal/modal/modal.service';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';
import {VideoFromGSuitePlugin} from '@shared/rte/video-plugin/video-from-g-suite-plugin';
import {NG1_COYO_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import * as _ from 'lodash';

describe('VideoFromGSuitePlugin', () => {
  let videoFromGSuitePlugin: VideoFromGSuitePlugin;
  let gDrivePickerService: jasmine.SpyObj<GDrivePickerService>;
  let document: jasmine.SpyObj<Document>;
  let froala: jasmine.SpyObj<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VideoFromGSuitePlugin, {
        provide: DOCUMENT,
        useValue: jasmine.createSpyObj('Document', ['getElementById'])
      }, {
        provide: FROALA_EDITOR,
        useValue: jasmine.createSpyObj('froala', ['RegisterCommand'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }, {
        provide: GDrivePickerService,
        useValue: jasmine.createSpyObj('gDrivePickerService', ['open'])
      }, {
        provide: GoogleApiService,
        useValue: jasmine.createSpyObj('googleApiService', ['getFileMetadata', 'isFilePublicVisible'])
      }, {
        provide: NG1_COYO_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('coyoNotification', ['error'])
      }, {
        provide: ModalService,
        useValue: jasmine.createSpyObj('modalService', ['openConfirm'])
      }]
    });

    videoFromGSuitePlugin = TestBed.get(VideoFromGSuitePlugin);
    gDrivePickerService = TestBed.get(GDrivePickerService);
    document = TestBed.get(DOCUMENT);
    froala = TestBed.get(FROALA_EDITOR);

    document.getElementById.and.returnValue({
      parentNode: jasmine.createSpyObj('Node', ['removeChild'])
    });
  });

  it('should be created', () => {
    expect(videoFromGSuitePlugin).toBeTruthy();
  });

  it('should not initialize video GSuite command without permissions', () => {
    // when
    videoFromGSuitePlugin.initialize({canAccessGoogle: false} as RteSettings);

    // then
    expect(froala.RegisterCommand).not.toHaveBeenCalled();
  });

  it('should initialize video GSuite command', () => {
    // when
    videoFromGSuitePlugin.initialize({canAccessGoogle: true} as RteSettings);

    // then
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertVideoFromGSuite', jasmine.any(Object));
  });

  it('should open GSuite picker', () => {
    // given
    const editor = {
      id: '',
      $oel: jasmine.createSpyObj('$oel', ['find']),
      video: jasmine.createSpyObj('video', ['insert']),
      html: jasmine.createSpyObj('html', ['insert']),
      undo: jasmine.createSpyObj('undo', ['saveStep'])
    };
    editor.$oel.find.and.returnValue({
      scrollTop: () => {}
    });
    gDrivePickerService.open.and.returnValue(Promise.resolve());

    // when
    videoFromGSuitePlugin.initialize({canAccessGoogle: true} as RteSettings);
    callCallbackOfCommand('coyoInsertVideoFromGSuite', editor);

    // then
    expect(gDrivePickerService.open).toHaveBeenCalled();
  });

  function callCallbackOfCommand(commandKey: string, editor: any): void {
    const calls = froala.RegisterCommand.calls.all();
    const command = _.find(calls, (call: jasmine.CallInfo): boolean => call.args[0] === commandKey);
    command.args[1].callback.apply(editor);
  }
});
