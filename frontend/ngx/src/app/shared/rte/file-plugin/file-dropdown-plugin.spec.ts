import {TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {FileDropdownPlugin} from '@shared/rte/file-plugin/file-dropdown-plugin';
import {FileFromFileLibraryPlugin} from '@shared/rte/file-plugin/file-from-file-library-plugin';
import {FileFromGSuitePlugin} from '@shared/rte/file-plugin/file-from-g-suite-plugin';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';

describe('FileDropdownPlugin', () => {
  let fileDropdownPlugin: FileDropdownPlugin;
  let froala: jasmine.SpyObj<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileDropdownPlugin, {
        provide: FROALA_EDITOR,
        useValue: jasmine.createSpyObj('froala', ['RegisterCommand'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }]
    });

    fileDropdownPlugin = TestBed.get(FileDropdownPlugin);
    froala = TestBed.get(FROALA_EDITOR);
  });

  it('should be created', () => {
    expect(fileDropdownPlugin).toBeTruthy();
  });

  it('should initialize file button command', () => {
    // given
    froala.COMMANDS = {};
    froala.COMMANDS[FileFromFileLibraryPlugin.KEY] = {title: 'COYO'};

    // when
    fileDropdownPlugin.initialize({} as RteSettings);

    // then
    expect(froala.RegisterCommand).not.toHaveBeenCalledWith();
    expect(froala.COMMANDS['coyoInsertFile']).toEqual(jasmine.objectContaining({
      title: 'COYO'
    }));
  });

  it('should initialize file dropdown command', () => {
    // given
    froala.COMMANDS = {};
    froala.COMMANDS[FileFromFileLibraryPlugin.KEY] = {title: 'COYO'};
    froala.COMMANDS[FileFromGSuitePlugin.KEY] = {title: 'Google'};

    // when
    fileDropdownPlugin.initialize({} as RteSettings);

    // then
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertFile', jasmine.objectContaining({
      type: 'dropdown',
      options: {
        coyoInsertFileFromFileLibrary: 'COYO',
        coyoInsertFileFromGSuite: 'Google'
      }
    }));
  });
});
