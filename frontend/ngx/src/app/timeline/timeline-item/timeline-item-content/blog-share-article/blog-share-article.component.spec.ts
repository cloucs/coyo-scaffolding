import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {TargetService} from '@domain/sender/target/target.service';
import {TimelineItemData} from '@domain/timeline-item/timeline-item-data';
import {TimelineItemService} from '@domain/timeline-item/timeline-item.service';
import {of} from 'rxjs';
import {BlogShareArticleComponent} from './blog-share-article.component';

describe('BlogShareArticleComponent', () => {
  let component: BlogShareArticleComponent;
  let fixture: ComponentFixture<BlogShareArticleComponent>;

  let windowSizeService: jasmine.SpyObj<WindowSizeService>;
  let targetService: jasmine.SpyObj<TargetService>;
  let timelineItemService: jasmine.SpyObj<TimelineItemService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlogShareArticleComponent],
      providers: [{
        provide: TimelineItemService,
        useValue: jasmine.createSpyObj('timelineItemService', ['getAuthorIcon'])
      }, {
        provide: TargetService,
        useValue: jasmine.createSpyObj('targetService', ['getLinkTo'])
      }, {
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['isXs'])
      }]
    }).overrideTemplate(BlogShareArticleComponent, '')
      .compileComponents();

    windowSizeService = TestBed.get(WindowSizeService);
    targetService = TestBed.get(TargetService);
    timelineItemService = TestBed.get(TimelineItemService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogShareArticleComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    const link = '/link/to/article';
    const icon = 'zmdi-icon';
    component.item = {
      data: {
        article: {
          author: {
            id: 'some-id'
          },
          articleTarget: {
            name: 'target'
          }
        }
      } as TimelineItemData
    } as any;

    windowSizeService.isXs.and.returnValue(of(true));
    targetService.getLinkTo.and.returnValue(link);
    timelineItemService.getAuthorIcon.and.returnValue(icon);

    fixture.detectChanges();

    expect(component.articleLink).toBe(link);
    expect(component.authorIcon).toBe(icon);
    expect(component.author).toBe(component.article.author);
  });
});
