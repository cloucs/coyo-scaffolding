import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {Sender} from '@domain/sender/sender';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {of} from 'rxjs';
import {SenderShareItemComponent} from './sender-share-item.component';

describe('SenderShareItemComponent', () => {
  let component: SenderShareItemComponent;
  let fixture: ComponentFixture<SenderShareItemComponent>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;

  const sender = {} as Sender;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SenderShareItemComponent],
      providers: [{
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['observeScreenChange'])
      }]
    }).overrideTemplate(SenderShareItemComponent, '')
      .compileComponents();
    windowSizeService = TestBed.get(WindowSizeService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderShareItemComponent);
    component = fixture.componentInstance;
    component.item = {
      itemType: 'page',
      data: {
        page: sender
      } as any
    } as TimelineItem;
  });

  it('should be md sized avatar on XS screen size', async(() => {
    expect(component).toBeTruthy();
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.XS));
    fixture.detectChanges(); // call ngOnInit
    component.avatarSize$.subscribe(size => expect(size).toBe('md'));
  }));

  it('should be xl sized avatar on a screen bigger than XS', async(() => {
    expect(component).toBeTruthy();
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.MD));
    fixture.detectChanges(); // call ngOnInit
    component.avatarSize$.subscribe(size => expect(size).toBe('xl'));
  }));
});
