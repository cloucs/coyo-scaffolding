import {Messages} from '@core/i18n/messages';

/* tslint:disable no-trailing-whitespace */
export const messagesFormDe: Messages = {
  lang: 'de',
  messages: {
    'MODULE.TIMELINE.FORM.HELP.MODAL': `
Abhängig von deinen Berechtigungen kannst du die folgenden Einstellungen beim Erstellen eines neuen Beitrags vornehmen.

### Dateien anhängen

Dateien können hochgeladen oder aus der Dokumentenbibliothek verlinkt werden. Gängige Bildformate und PDFs werden als 
Vorschaubilder innerhalb des Beitrags angezeigt.

### Beitrag als Sticky markieren

Wenn du einen Beitrag als Sticky markierst wird er anderen Nutzern oben in ihrer Timeline angezeigt bis die eingestellte
 Zeit abgelaufen ist oder sie ihn als gelesen markieren.

### Beitrag sperren

Wenn du einen Beitrag sperrst kann er von anderen Nutzern weder kommentiert noch mit gefällt mir makiert werden.

### Markdown

Im folgenden Text lernst du wie du die **Markdown**-Syntax anwendest, um Texte zu formatieren und strukturieren. 
Du kannst Marktdown an vielen Stellen in COYO, z.B. in der Timeline oder in Kommentaren verwenden.

#### Absätze und Zeilenumbrüche

Du kannst einen neuen Absatz erzeugen, indem du eine leere Zeile zwischen zwei Textteilen lässt.

#### Formatierung von Text

Du kannst Inhalte mit fetter, kursiver oder durchgestrichener Schrift betonen.

| Syntax | Beispiel | Ausgabe |
| ------------- | ------------- | ------------- |
| \`** **\` oder \`__ __\` | \`**fett**\` | **fett** |
| \`* *\` oder \`_ _\` | \`*kursiv*\` | *kursiv* |
| \`~~ ~~\` | \`~~durchgestrichen~~\` | ~~durchgestrichen~~ |
| \`** **\` oder \`_ _\` | \`**Absolut _super_ wichtig**\` | **Absolut _super_ wichtig**  |

#### Listen

Du kannst eine Liste erstellen indem du vor eine oder mehrere Zeilen Text ein \`-\` oder ein \`*\` setzt.

    - George Washington
    - John Adams
    - Thomas Jefferson

- George Washington
- John Adams
- Thomas Jefferson

#### Texte zitieren

Du kannst einen Text zitieren, din dem du ein \`>\` einfügst.

    Mit den Worten von Abraham Lincoln:

    > Pardon my French

Mit den Worten von Abraham Lincoln:

> Pardon my French

#### Ausschalten der Markdown Formatierung

Um die Markdown Formatierung zu ignorieren kannst du folgendes Zeichen \`\\\` vor die Markdown Zeichen setzen.

    Lass uns \\*unser-neues-Projekt\\* umbenennen zu \\*unserem-alten-Projekt\\*.

Lass uns \\*unser-neues-Projekt\\* umbenennen zu \\*unserem-alten-Projekt\\*.

#### Hervorheben

Du kannst Text in einem Satz hervorheben indem du ihn mit einfachen Hochkommata umgibst. Der Text innerhalb der 
Hochkommata wird nicht formatiert.

    Dieses  \`Wort\` soll hervorgehoben werden.

Dieses  \`Wort\` soll hervorgehoben werden.

Um Code oder Text in einem eigenen, abgegrenzten Textblock zu formatieren benutze drei Hochkommata.

    \`\`\`
    console.log('Hello World');
    \`\`\`

\`\`\`
console.log('Hello World');
\`\`\`
    `
  }
};
