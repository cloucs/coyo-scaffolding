import {HTTP_INTERCEPTORS, HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {fakeAsync, TestBed} from '@angular/core/testing';
import {EtagCacheService} from '../etag-cache/etag-cache.service';
import {UrlService} from '../url/url.service';
import {EtagInterceptor} from './etag-interceptor';

describe('EtagInterceptor', () => {
  let http: HttpClient;
  let mock: HttpTestingController;
  let etagCacheService: jasmine.SpyObj<EtagCacheService>;

  beforeEach(() => {
    const mockedUrlService = jasmine.createSpyObj('urlService', ['getBackendUrl', 'isBackendUrlSet']);
    mockedUrlService.getBackendUrl.and.returnValue('http://host/');
    mockedUrlService.isBackendUrlSet.and.returnValue(true);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{
        provide: UrlService, useValue: mockedUrlService
      }, {
        provide: HTTP_INTERCEPTORS, useClass: EtagInterceptor, multi: true
      }, {
        provide: EtagCacheService, useValue: jasmine.createSpyObj('EtagCacheService',
          ['buildKey', 'isCached', 'isBulkCached', 'getEntry', 'putEntry', 'getBulkEntry', 'putBulkEntry'])
      }]
    });

    http = TestBed.get(HttpClient);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    mock = TestBed.get(HttpTestingController);
    etagCacheService = TestBed.get(EtagCacheService);
  });

  afterEach(() => {
    mock.verify();
  });

  describe('reqular requests', () => {

    it('should be enriched with none-cache header to prevent the browser to apply automatically etag caching', () => {
      // given
      const url = 'http://host/path';

      // when
      http.get(url).subscribe(() => {
      });

      // then
      const req = mock.expectOne(request => request.method === 'GET' && request.url === url);
      expect(req.request.headers.get('Cache-Control')).toEqual('no-cache, no-store');
      expect(req.request.headers.get('Pragma')).toEqual('no-cache');
      expect(req.request.headers.get('If-Modified-Since')).toEqual('0');
    });

    it('should pass request when no caching is active', () => {
      // given
      const url = 'http://host/path';
      etagCacheService.buildKey.and.returnValue(url);
      etagCacheService.isCached.and.returnValue(true);
      etagCacheService.getEntry.and.returnValue({etag: 'etag'});

      // when
      http.get(url, {headers: {etagEnabled: 'false'}}).subscribe(() => {
      });

      // then
      const req = mock.expectOne(request => request.method === 'GET' && request.url === url);
      expect(req.request.headers.has('If-None-Match')).toEqual(false);
      expect(etagCacheService.getEntry).not.toHaveBeenCalled();
      req.event(new HttpResponse({
        status: 200,
        statusText: 'OK'
      }));
      expect(etagCacheService.putEntry).not.toHaveBeenCalled();
    });

    it('should store response in cache when ETag header is present', () => {
      // given
      const url = 'http://host/path';
      etagCacheService.buildKey.and.returnValue(url);
      etagCacheService.isCached.and.returnValue(false);

      // when
      http.get(url).subscribe(() => {
      });

      // then
      const req = mock.expectOne(request => request.method === 'GET' && request.url === url);
      expect(req.request.headers.has('If-None-Match')).toEqual(false);
      req.event(new HttpResponse({
        status: 200,
        statusText: 'OK',
        headers: new HttpHeaders({Etag: '"etag"'}),
        body: {value: 'value'}
      }));
      expect(etagCacheService.putEntry).toHaveBeenCalledWith(url, 'etag', 200, {value: 'value'});
    });

    it('should store response in cache when weak ETag header is present', () => {
      // given
      const url = 'http://host/path';
      etagCacheService.buildKey.and.returnValue(url);
      etagCacheService.isCached.and.returnValue(false);

      // when
      http.get(url).subscribe(() => {
      });

      // then
      const req = mock.expectOne(request => request.method === 'GET' && request.url === url);
      expect(req.request.headers.has('If-None-Match')).toEqual(false);
      req.event(new HttpResponse({
        status: 200,
        statusText: 'OK',
        headers: new HttpHeaders({Etag: 'W/"etag"'}),
        body: {value: 'value'}
      }));
      expect(etagCacheService.putEntry).toHaveBeenCalledWith(url, 'etag', 200, {value: 'value'});
    });

    it('should send etag header when url is cached and return request result if backend does not know etag', () => {
      // given
      const url = 'http://host/path';
      etagCacheService.buildKey.and.returnValue(url);
      etagCacheService.isCached.and.returnValue(true);
      etagCacheService.getEntry.and.returnValue({etag: 'etag', body: {value: 'cached'}});

      // when
      http.get(url).subscribe(data => {
        expect(data).toEqual({value: 'value'});
      });

      // then
      const req = mock.expectOne(request => request.method === 'GET' && request.url === url);
      expect(req.request.headers.get('If-None-Match')).toEqual('"etag"');
      req.event(new HttpResponse({
        status: 200,
        statusText: 'OK',
        headers: new HttpHeaders({Etag: '"etag"'}),
        body: {value: 'value'}
      }));
    });

    it('should use value from cache on etag match', fakeAsync(() => {
      // given
      const url = 'http://host/path';
      etagCacheService.buildKey.and.returnValue(url);
      etagCacheService.isCached.and.returnValue(true);
      etagCacheService.getEntry.and.returnValue({etag: 'etag', body: {value: 'cached'}});

      // when
      http.get(url).subscribe(data => {
        expect(data).toEqual({value: 'cached'});
      });

      // then
      const req = mock.expectOne(request => request.method === 'GET' && request.url === url);
      expect(req.request.headers.has('If-None-Match')).toEqual(true);
      req.flush(null, new HttpErrorResponse({
        status: 304,
        statusText: 'Not Modified',
        headers: new HttpHeaders({Etag: '"etag"'})
      }));
    }));
  });

  describe('bulk requests', () => {

    it('should store bulk response in cache when bulk ETag header is present', () => {
      // given
      const url = 'http://host/path';
      etagCacheService.buildKey.and.returnValue(url);
      etagCacheService.isBulkCached.and.returnValue(false);

      // when
      http.get(url, {
        headers: {etagBulkId: 'ids'},
        params: {ids: ['id1', 'id2']}
      }).subscribe(data => expect(data).toEqual({id1: {value: 'value1'}, id2: {value: 'value2'}}));

      // then
      const req = mock.expectOne(request => request.method === 'GET' && request.url === url);
      expect(req.request.headers.has('X-Bulk-If-None-Match')).toEqual(false);
      req.event(new HttpResponse({
        status: 200,
        statusText: 'OK',
        headers: new HttpHeaders({
          'X-Bulk-ETag': '{"id1":"etag1","id2":"etag2"}',
          'X-Bulk-Status': '{"id1":200,"id2":200}'
        }),
        body: {id1: {value: 'value1'}, id2: {value: 'value2'}}
      }));
      expect(etagCacheService.putBulkEntry).toHaveBeenCalledWith(url, 'id1', 'etag1', 200, {value: 'value1'});
      expect(etagCacheService.putBulkEntry).toHaveBeenCalledWith(url, 'id2', 'etag2', 200, {value: 'value2'});
    });

    it('should send etag header when url is cached and return request result if backend does not know etag', () => {
      // given
      const url = 'http://host/path';
      etagCacheService.buildKey.and.returnValue(url);
      etagCacheService.isBulkCached.and.returnValue(true);
      etagCacheService.getBulkEntry.and.callFake((_key: string, id: string) => id === 'id1'
        ? {etag: 'etag1', body: {value: 'cached1'}}
        : {etag: 'etag2', body: {value: 'cached2'}});

      // when
      http.get(url, {
        headers: {etagBulkId: 'ids'},
        params: {ids: ['id1', 'id2']}
      }).subscribe(data => expect(data).toEqual({id1: {value: 'value1'}, id2: {value: 'value2'}}));

      // then
      const req = mock.expectOne(request => request.method === 'GET' && request.url === url);
      expect(req.request.headers.get('X-Bulk-If-None-Match')).toEqual('{"id1":"etag1","id2":"etag2"}');
      req.event(new HttpResponse({
        status: 200,
        statusText: 'OK',
        headers: new HttpHeaders({
          'X-Bulk-ETag': '{"id1":"etag1","id2":"etag2"}',
          'X-Bulk-Status': '{"id1":200,"id2":200}'
        }),
        body: {id1: {value: 'value1'}, id2: {value: 'value2'}}
      }));
    });

    it('should use value from cache on where bulk etag matches', () => {
      // given
      const url = 'http://host/path';
      etagCacheService.buildKey.and.returnValue(url);
      etagCacheService.isBulkCached.and.returnValue(true);
      etagCacheService.getBulkEntry.and.callFake((_key: string, id: string) => id === 'id1'
        ? {etag: 'etag1', body: {value: 'cached1'}}
        : {etag: 'etag2', body: {value: 'cached2'}});

      // when
      http.get(url, {
        headers: {etagBulkId: 'ids'},
        params: {ids: ['id1', 'id2']}
      }).subscribe(data => expect(data).toEqual({id1: {value: 'cached1'}, id2: {value: 'value2'}}));

      // then
      const req = mock.expectOne(request => request.method === 'GET' && request.url === url);
      expect(req.request.headers.get('X-Bulk-If-None-Match')).toEqual('{"id1":"etag1","id2":"etag2"}');
      req.event(new HttpResponse({
        status: 200,
        statusText: 'OK',
        headers: new HttpHeaders({
          'X-Bulk-ETag': '{"id1":"etag1","id2":"etag2"}',
          'X-Bulk-Status': '{"id1":304,"id2":200}'
        }),
        body: {id2: {value: 'value2'}}
      }));
    });
  });
});
