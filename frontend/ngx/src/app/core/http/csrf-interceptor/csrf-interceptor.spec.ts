import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {TranslateService} from '@ngx-translate/core';
import {Ng1CsrfService, Ng1ErrorService} from '@root/typings';
import {NG1_CSRF_SERVICE, NG1_ERROR_SERVICE} from '@upgrade/upgrade.module';
import {NGXLogger} from 'ngx-logger';
import {of} from 'rxjs';
import {CsrfInterceptor} from './csrf-interceptor';

describe('CsrfInterceptor', () => {
  let mockedTranslateService: jasmine.SpyObj<TranslateService>;
  let mockedLogService: jasmine.SpyObj<NGXLogger>;
  let mockedUrlService: jasmine.SpyObj<UrlService>;
  let mockedCsrfService: jasmine.SpyObj<Ng1CsrfService>;
  let mockedErrorService: Ng1ErrorService;

  beforeEach(() => {
    mockedTranslateService = jasmine.createSpyObj('translateService', ['get']);
    mockedLogService = jasmine.createSpyObj('logService', ['error']);
    mockedUrlService = jasmine.createSpyObj('UrlService', ['isConfigurable']);
    mockedCsrfService = jasmine.createSpyObj('csrfService', ['getToken']);
    mockedErrorService = jasmine.createSpyObj('errorService', ['showErrorPage']);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {provide: TranslateService, useValue: mockedTranslateService},
        {provide: NGXLogger, useValue: mockedLogService},
        {provide: UrlService, useValue: mockedUrlService},
        {provide: NG1_CSRF_SERVICE, useValue: mockedCsrfService},
        {provide: NG1_ERROR_SERVICE, useValue: mockedErrorService},
        {provide: HTTP_INTERCEPTORS, useClass: CsrfInterceptor, multi: true}
      ]
    });
  });

  it('should add csrf header to all non GET requests', fakeAsync(inject([HttpClient, HttpTestingController],
    (http: HttpClient, mock: HttpTestingController) => {
    // given
    mockedCsrfService.getToken.and.returnValue(of('test').toPromise());

    // when
    http.post('/web', {}).subscribe();
    http.put('/web', {}).subscribe();
    http.delete('/web', {}).subscribe();

    // then
    tick();
    mock.expectOne(req => req.method === 'POST' && req.headers.get('X-CSRF-TOKEN') === 'test', 'Add csrf header to POST');
    mock.expectOne(req => req.method === 'PUT' && req.headers.get('X-CSRF-TOKEN') === 'test', 'Add csrf header to PUT');
    mock.expectOne(req => req.method === 'DELETE' && req.headers.get('X-CSRF-TOKEN') === 'test', 'Add csrf header to DELETE');
    mock.verify();
  })));

  it('should show error page if no csrf token is available',
    fakeAsync(inject([HttpClient, HttpTestingController], (http: HttpClient, mock: HttpTestingController) => {
      // given
      mockedCsrfService.getToken.and.returnValue(Promise.reject('error'));
      mockedTranslateService.get.and.returnValue(of('test'));

      // when
      http.post('/web', {}).subscribe(() => {}, error => expect(error).toBeTruthy());

      // then
      tick();
      expect(mockedLogService.error).toHaveBeenCalled();
      expect(mockedErrorService.showErrorPage).toHaveBeenCalledWith('test', null, ['RETRY']);
      mock.verify();
    })));

  it('should not add csrf header to GET requests', inject([HttpClient, HttpTestingController],
    (http: HttpClient, mock: HttpTestingController) => {
    // when
    http.get('/web', {}).subscribe();

    // then
    mock.expectOne(req => req.method === 'GET' && !req.headers.has('X-CSRF-TOKEN'),
       'Not add csrf header to GET');
    mock.verify();
  }));
});
