import {inject, TestBed} from '@angular/core/testing';
import {WINDOW} from '@root/injection-tokens';
import {EtagCacheService} from './etag-cache.service';

describe('EtagCacheService', () => {
  let window: { sessionStorage: jasmine.SpyObj<Storage> };
  let sessionStorage = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EtagCacheService, {
        provide: WINDOW, useValue: {sessionStorage: jasmine.createSpyObj('sessionStorage', ['getItem', 'setItem'])}
      }]
    });

    sessionStorage = {};
    window = TestBed.get(WINDOW);
    window.sessionStorage.getItem.and.callFake((key: string) => sessionStorage[key]);
    window.sessionStorage.setItem.and.callFake((key: string, value: string) => sessionStorage[key] = value);
  });

  it('should be created', () => {
    // when
    const service = TestBed.get(EtagCacheService);

    // then
    expect(service).toBeTruthy();
  });

  describe('buildKey', () => {

    it('should return URL when no parameters are present', inject([EtagCacheService], (service: EtagCacheService) => {
      // given
      const url = '/web/foo?p1=v1&p2=v2';

      // when
      const result = service.buildKey(url);

      // then
      expect(result).toBe('/web/foo?p1=v1&p2=v2');
    }));

    it('should not include bulk request parameter', inject([EtagCacheService], (service: EtagCacheService) => {
      // given
      const url = '/web/foo?p1=v1&p2=v2&p3=v3';

      // when
      const result = service.buildKey(url, 'p2');

      // then
      expect(result).toBe('/web/foo?p1=v1&p3=v3');
    }));
  });

  describe('purge', () => {

    it('should expire outdated regular entries', () => {
      // given
      const key1 = 'cache-key-1';
      const key2 = 'cache-key-2';
      const cache = {};
      cache[key1] = {
        etag: 'etag',
        status: 200,
        body: 'v1',
        access: new Date().getTime() - EtagCacheService.CACHE_EXPIRY + 10
      };
      cache[key2] = {
        etag: 'etag',
        status: 200,
        body: 'v2',
        access: new Date().getTime() - EtagCacheService.CACHE_EXPIRY - 10
      };
      sessionStorage[EtagCacheService.CACHE_KEY] = JSON.stringify(cache);
      const service = TestBed.get(EtagCacheService);

      // when
      service.purge();
      expect(window.sessionStorage.setItem).toHaveBeenCalled();

      // then
      const result = JSON.parse(sessionStorage[EtagCacheService.CACHE_KEY]);
      expect(result[key1]).toBeDefined();
      expect(result[key2]).not.toBeDefined();
    });

    it('should cache read reset expiry', () => {
      // given
      const key = 'cache-key';
      const cache = {};
      cache[key] = {
        etag: 'etag',
        status: 200,
        body: 'v',
        access: new Date().getTime() - EtagCacheService.CACHE_EXPIRY - 10
      };
      sessionStorage[EtagCacheService.CACHE_KEY] = JSON.stringify(cache);
      const service = TestBed.get(EtagCacheService);

      // when
      service.getEntry(key);
      expect(window.sessionStorage.setItem).toHaveBeenCalled();
      service.purge();
      expect(window.sessionStorage.setItem).toHaveBeenCalled();

      // then
      const result = JSON.parse(sessionStorage[EtagCacheService.CACHE_KEY]);
      expect(result[key]).toBeDefined();
    });

    it('should expire outdated bulk ID entries', () => {
      // given
      const key = 'cache-key';
      const id1 = 'id-1';
      const id2 = 'id-2';
      const cache = {};
      cache[key] = {};
      cache[key][id1] = {
        etag: 'etag',
        status: 200,
        body: 'v1',
        access: new Date().getTime() - EtagCacheService.CACHE_EXPIRY + 10
      };
      cache[key][id2] = {
        etag: 'etag',
        status: 200,
        body: 'v2',
        access: new Date().getTime() - EtagCacheService.CACHE_EXPIRY - 10
      };
      sessionStorage[EtagCacheService.BULK_CACHE_KEY] = JSON.stringify(cache);
      const service = TestBed.get(EtagCacheService);

      // when
      service.purge();
      expect(window.sessionStorage.setItem).toHaveBeenCalled();

      // then
      const result = JSON.parse(sessionStorage[EtagCacheService.BULK_CACHE_KEY]);
      expect(result[key]).toBeDefined();
      expect(result[key][id1]).toBeDefined();
      expect(result[key][id2]).not.toBeDefined();
    });

    it('should expire outdated bulk entries', () => {
      // given
      const key = 'cache-key';
      const id = 'id';
      const cache = {};
      cache[key] = {};
      cache[key][id] = {
        etag: 'etag',
        status: 200,
        body: 'v',
        access: new Date().getTime() - EtagCacheService.CACHE_EXPIRY - 10
      };
      sessionStorage[EtagCacheService.BULK_CACHE_KEY] = JSON.stringify(cache);
      const service = TestBed.get(EtagCacheService);

      // when
      service.purge();
      expect(window.sessionStorage.setItem).toHaveBeenCalled();

      // then
      const result = JSON.parse(sessionStorage[EtagCacheService.BULK_CACHE_KEY]);
      expect(result[key]).not.toBeDefined();
    });

    it('should cache read reset bulk expiry', () => {
      // given
      const key = 'cache-key';
      const id = 'id';
      const cache = {};
      cache[key] = {};
      cache[key][id] = {
        etag: 'etag',
        status: 200,
        body: 'v',
        access: new Date().getTime() - EtagCacheService.CACHE_EXPIRY - 10
      };
      sessionStorage[EtagCacheService.BULK_CACHE_KEY] = JSON.stringify(cache);
      const service = TestBed.get(EtagCacheService);

      // when
      service.getBulkEntry(key, id);
      expect(window.sessionStorage.setItem).toHaveBeenCalled();
      service.purge();
      expect(window.sessionStorage.setItem).toHaveBeenCalled();

      // then
      const result = JSON.parse(sessionStorage[EtagCacheService.BULK_CACHE_KEY]);
      expect(result[key]).toBeDefined();
    });
  });

  describe('cache', () => {

    it('should store request data', inject([EtagCacheService], (service: EtagCacheService) => {
      // given
      const etag = 'abc';
      const status = 200;
      const url = '/web/foo';
      const body = 'body';

      // when
      service.putEntry(url, etag, status, body);
      expect(window.sessionStorage.setItem).toHaveBeenCalled();

      // then
      const result = JSON.parse(sessionStorage[EtagCacheService.CACHE_KEY]);
      expect(result[url]).toBeDefined();
      expect(result[url].etag).toBe(etag);
      expect(result[url].body).toBe(body);
      expect(result[url].status).toBe(status);
    }));

    it('should retrieve stored request data', inject([EtagCacheService], (service: EtagCacheService) => {
      // given
      const etag = 'abc';
      const status = 200;
      const url = '/web/foo';
      const body = 'body';

      service.putEntry(url, etag, status, body);

      // when
      const entry = service.getEntry(url);

      // then
      expect(entry).toBeDefined();
      expect(entry.etag).toBe(etag);
      expect(entry.body).toBe(body);
      expect(entry.status).toBe(status);
    }));

    it('should find stored request data', inject([EtagCacheService], (service: EtagCacheService) => {
      // given
      const etag = 'abc';
      const status = 200;
      const url = '/web/foo';
      const body = 'body';

      // then
      expect(service.isCached(url)).toBe(false);

      // when - again
      service.putEntry(url, etag, status, body);

      // then - again
      expect(service.isCached(url)).toBe(true);
    }));
  });

  describe('bulk cache', () => {

    it('should store request data', inject([EtagCacheService], (service: EtagCacheService) => {
      // given
      const etag = 'abc';
      const status = 200;
      const url = '/web/foo';
      const id = 'id';
      const body = 'body';

      // when
      service.putBulkEntry(url, id, etag, status, body);
      expect(window.sessionStorage.setItem).toHaveBeenCalled();

      // then
      const result = JSON.parse(sessionStorage[EtagCacheService.BULK_CACHE_KEY]);
      expect(result[url]).toBeDefined();
      expect(result[url][id]).toBeDefined();
      expect(result[url][id].etag).toBe(etag);
      expect(result[url][id].body).toBe(body);
      expect(result[url][id].status).toBe(status);
    }));

    it('should retrieve stored request data', inject([EtagCacheService], (service: EtagCacheService) => {
      // given
      const etag = 'abc';
      const status = 200;
      const url = '/web/foo';
      const id = 'id';
      const body = 'body';

      service.putBulkEntry(url, id, etag, status, body);

      // when
      const entry = service.getBulkEntry(url, id);

      // then
      expect(entry).toBeDefined();
      expect(entry.etag).toBe(etag);
      expect(entry.body).toBe(body);
      expect(entry.status).toBe(status);
    }));

    it('should find stored request data', inject([EtagCacheService], (service: EtagCacheService) => {
      // given
      const etag = 'abc';
      const status = 200;
      const url = '/web/foo';
      const id = 'id';
      const body = 'body';

      // then
      expect(service.isBulkCached(url)).toBe(false);
      expect(service.isBulkIdCached(url, id)).toBe(false);

      // when - again
      service.putBulkEntry(url, id, etag, status, body);

      // then - again
      expect(service.isBulkCached(url)).toBe(true);
      expect(service.isBulkIdCached(url, id)).toBe(true);
    }));
  });
});
