/**
 * Service for loading images with the correct orientation
 */
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
/**
 * Image loader service mainly used to correct image orientation by reading exif tag information
 * about the rotation: @see {@link http://sylvana.net/jpegcrop/exif_orientation.html}.
 * First the image is loaded via an HttpClient as a blob and piped into a FileReader.
 * A base64 string representation of the image is created and the raw data is checked for
 * an exif orientation tag (0x0112). If the image does not qualify (not a valid jpeg, no tag given or nor change in
 * orientation required) the base64 string is emitted.
 * If the image qualifies for correction, it is drawn onto a transformed invisible canvas which is then
 * converted back into a dataUrl (base64) and emitted.
 */
export class ImageLoaderService {
  constructor(private httpClient: HttpClient) {

  }

  /**
   * Loads an image and guarantees that the image has been loaded when the observable completes
   * @param source The url of the image to be loaded
   * @returns an observable HTMLImageElement
   */
  loadImage(source: string): Observable<HTMLImageElement> {
    const subject = new Subject<HTMLImageElement>();
    const image = new Image();
    image.onload = () => subject.next(image);
    image.onerror = err => subject.error(err);
    this.httpClient.get(source, {responseType: 'blob'}).subscribe(data => {
      const fileReader = new FileReader();
      fileReader.onloadend = () => {
        const base64 = this.createBase64String(data.type, fileReader.result as ArrayBufferLike);
        const orientation = this.findRotation(fileReader.result as ArrayBufferLike);
        this.correctOrientation(base64, orientation).subscribe(imageData => image.src = imageData);
      };
      fileReader.readAsArrayBuffer(data);
    });
    return subject;
  }

  /**
   * Creates a valid base64 string with data type from a data type string and a buffer
   * @param dataType The data type (Content-Type)
   * @param buffer The buffer containing the binary image data
   * @returns A base64 string
   */
  private createBase64String(dataType: string, buffer: ArrayBufferLike): string {
    return `data:${dataType};base64,${this.arrayBufferToBase64(buffer)}`;
  }

  /**
   * Converts an arraybuffer into the equivalent base64 representation
   * @param buffer The buffer object
   * @returns A base64 representation of the buffer data
   */
  private arrayBufferToBase64(buffer: ArrayBufferLike): string {
    let str = '';
    const bytes = new Uint8Array(buffer);
    const length = bytes.length;
    for (let i = 0; i < length; i++) {
      str += String.fromCharCode(bytes[i]);
    }
    return btoa(str);
  }

  /**
   * Takes a base64 representation of an image and the images current orientation number
   * @see {@link http://sylvana.net/jpegcrop/exif_orientation.html}
   * @param base64 The raw image data in base64
   * @param currentOrientation The image orientation number
   * @returns An observable that will emit a base64 string of the correctly oriented image
   */
  private correctOrientation(base64: string, currentOrientation: number): Observable<string> {
    // Rotation happens only between 2 and 8 (1 is correct orientation)
    if (!currentOrientation || currentOrientation < 2 || currentOrientation > 8) {
      return of(base64);
    }
    const subject = new Subject<string>();
    // Create a temporary image object to load the base64 into the canvas
    const image = new Image();
    image.onload = () => {
      // Create the temporary canvas
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d');

      // Switch height and width if necessary
      const imageWidth = image.width;
      const imageHeight = image.height;
      if (4 < currentOrientation && currentOrientation < 9) {
        canvas.width = imageHeight;
        canvas.height = imageWidth;
      } else {
        canvas.width = imageWidth;
        canvas.height = imageHeight;
      }

      switch (currentOrientation) {
        case 2:
          context.transform(-1, 0, 0, 1, imageWidth, 0);
          break;
        case 3:
          context.transform(-1, 0, 0, -1, imageWidth, imageHeight);
          break;
        case 4:
          context.transform(1, 0, 0, -1, 0, imageHeight);
          break;
        case 5:
          context.transform(0, 1, 1, 0, 0, 0);
          break;
        case 6:
          context.transform(0, 1, -1, 0, imageHeight, 0);
          break;
        case 7:
          context.transform(0, -1, -1, 0, imageHeight, imageWidth);
          break;
        case 8:
          context.transform(0, -1, 1, 0, 0, imageWidth);
          break;
        default:
          break;
      }
      // Draw to transformed canvas
      context.drawImage(image, 0, 0);
      // Emit new base64 string
      subject.next(canvas.toDataURL());
    };
    image.src = base64;
    return subject;
  }

  /**
   * Attempts to find the exif orientation tag and extract its value
   * @param rawData The image data in an array buffer
   * @returns The orientation or 0 if none is found
   */
  private findRotation(rawData: ArrayBufferLike): number {
    // Check if the data is valid
    if (!rawData || rawData.byteLength < 2) {
      return 0;
    }

    const dataView = new DataView(rawData);
    let index = 0;
    // Check for jpeg file marker
    const firstByte = dataView.getUint16(index);
    if (firstByte !== 0xFFD8) {
      return 0;
    }
    // Set the limit to look through the entire file
    let limit = dataView.byteLength - 2;
    while (index < limit) {
      const current = dataView.getUint16(index);
      index += 2;
      switch (current) {
        case 0xFFE1:
          // Marker for exif data length, this allows us to only look through the exif table
          limit = dataView.getUint16(index) - index;
          index += 2;
          break;
        case 0x0112:
          // Orientation tag found
          return dataView.getUint16(index + 6, false);
      }
    }
    return 0;
  }
}
