import {inject, TestBed} from '@angular/core/testing';
import {NGXLogger} from 'ngx-logger';
import {TranslationCompilerService} from './translation-compiler.service';

describe('TranslationCompilerService', () => {
  let translationCompilerService: TranslationCompilerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TranslationCompilerService, {
        provide: NGXLogger,
        useValue: jasmine.createSpyObj('NGXLogger', ['error'])
      }]
    });
    translationCompilerService = TestBed.get(TranslationCompilerService);
  });

  it('should be created', inject([TranslationCompilerService], (service: TranslationCompilerService) => {
    expect(service).toBeTruthy();
  }));

  it('should sanitize a compiled string', () => {
    // given
    const language = 'en';
    const translation = '<script>alert("Hello World");</script>';

    // when
    const result = translationCompilerService.compile(translation, language);

    // then
    expect(result({})).toBe('alert("Hello World");');
  });

  it('should not sanitize umlauts', () => {
    // given
    const language = 'de';
    const translation = 'Bloß keine Wörter mit Umlauten!';

    // when
    const result = translationCompilerService.compile(translation, language);

    // then
    expect(result({})).toBe('Bloß keine Wörter mit Umlauten!');
  });

  it('should sanitize a compiled object', () => {
    // given
    const language = 'en';
    const translations = {
      GREET: 'Hello World',
      ALERT: '<script>alert("Hello World");</script>'
    };

    // when
    const result = translationCompilerService.compileTranslations(translations, language);

    // then
    expect(result['GREET']({})).toBe('Hello World');
    expect(result['ALERT']({})).toBe('alert("Hello World");');
  });

  it('should support params for translation', () => {
    // given
    const language = 'en';
    const translation = 'Hello {key}';

    // when
    const result = translationCompilerService.compile(translation, language);

    // then
    expect(result({key: 'World'})).toBe('Hello World');
  });

  it('should support params for translations', () => {
    // given
    const language = 'en';
    const translations = {
      HELLO_WORLD: 'Hello {key}'
    };

    // when
    const result = translationCompilerService.compileTranslations(translations, language);

    // then
    expect(result['HELLO_WORLD']({key: 'World'})).toBe('Hello World');
  });

  it('should fail gracefully', () => {
    // given
    const language = 'en';
    const translations = {
      SUCCESS: 'Hello World',
      ERROR: '{count, plural, = 1{one} other{more'
    };

    // when
    const result = translationCompilerService.compileTranslations(translations, language);

    // then
    expect(result['SUCCESS']({})).toBe('Hello World');
    expect(result['ERROR']).toBeUndefined();
  });

  it('should handle pluralization for the current user', () => {
    // given
    const language = 'en';
    const translation = '{othersCount, plural, =1{{isLiked, select, true{like} other{likes}}} other{like}} this';

    // when
    const result = translationCompilerService.compile(translation, language);

    // then
    expect(result({isLiked: true})).toBe('like this');
  });

  it('should handle like pluralization for current user', () => {
    // given
    const language = 'en';
    const translation = '{othersCount, plural, =1{{isLiked, select, true{like} other{likes}}} other{like}} this';

    // when
    const result = translationCompilerService.compile(translation, language);

    // then
    expect(result({isLiked: true, othersCount: 0})).toBe('like this');
  });

  it('should handle like pluralization for current user', () => {
    // given
    const language = 'en';
    const translation = '{othersCount, plural, =1{{isLiked, select, true{like} other{likes}}} other{like}} this';

    // when
    const result = translationCompilerService.compile(translation, language);

    // then
    expect(result({isLiked: false, othersCount: 1})).toBe('likes this');
  });

  it('should handle like pluralization for current user and other user', () => {
    // given
    const language = 'en';
    const translation = '{othersCount, plural, =1{{isLiked, select, true{like} other{likes}}} other{like}} this';

    // when
    const result = translationCompilerService.compile(translation, language);

    // then
    expect(result({isLiked: true, othersCount: 1})).toBe('like this');
  });
});
