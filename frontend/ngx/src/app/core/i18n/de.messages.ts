import {Messages} from './messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'CLOSE.MODAL.ARIA': 'Overlay schließen',
    'COMMONS.SHARES.SUBMIT': 'Teilen',
    'ERRORS.STATUSCODE.NOT_FOUND.DEFAULT': 'Die angeforderten Daten konnten nicht gefunden werden oder du hast keine ausreichende Berechtigung.',
    'PREVIEW.LINK.DELETE.ARIA': 'Link Vorschau löschen',
    'PREVIEW.VIDEO.DELETE.ARIA': 'Video löschen',
    'PREVIEW.LINK.GOOGLE.DRIVE.TITLE': 'Google Drive',
    'PREVIEW.LINK.GOOGLE.TYPE.SPREADSHEET': 'Tabelle',
    'PREVIEW.LINK.GOOGLE.TYPE.DOCUMENT': 'Dokument',
    'PREVIEW.LINK.GOOGLE.TYPE.PRESENTATION': 'Präsentation',
    'PREVIEW.LINK.GOOGLE.TYPE.DRAWING': 'Zeichnung',
    'PREVIEW.LINK.GOOGLE.TYPE.MAPS': 'Map',
    'PREVIEW.LINK.GOOGLE.TYPE.FORMS': 'Formular'
  }
};
