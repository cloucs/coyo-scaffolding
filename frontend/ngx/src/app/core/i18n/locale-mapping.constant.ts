/**
 * Moment.js locale mappings.
 */
export const localeMapping: { [key: string]: string } = {
  en: 'en-gb',
  zh: 'zh-cn',
  no: 'nb',
  hy: 'hy-am'
};
