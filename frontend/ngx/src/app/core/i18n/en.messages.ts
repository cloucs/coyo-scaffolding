import {Messages} from './messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'CLOSE.MODAL.ARIA': 'Close overlay',
    'COMMONS.SHARES.SUBMIT': 'Share',
    'ERRORS.STATUSCODE.NOT_FOUND.DEFAULT': 'The requested data could not be found or you are not allowed to access.',
    'PREVIEW.LINK.DELETE.ARIA': 'Delete link preview',
    'PREVIEW.VIDEO.DELETE.ARIA': 'Delete video',
    'PREVIEW.LINK.GOOGLE.DRIVE.TITLE': 'Google Drive',
    'PREVIEW.LINK.GOOGLE.TYPE.SPREADSHEET': 'Spreadsheet',
    'PREVIEW.LINK.GOOGLE.TYPE.DOCUMENT': 'Document',
    'PREVIEW.LINK.GOOGLE.TYPE.PRESENTATION': 'Presentation',
    'PREVIEW.LINK.GOOGLE.TYPE.DRAWING': 'Drawing',
    'PREVIEW.LINK.GOOGLE.TYPE.MAPS': 'Map',
    'PREVIEW.LINK.GOOGLE.TYPE.FORMS': 'Form'
  }
};
