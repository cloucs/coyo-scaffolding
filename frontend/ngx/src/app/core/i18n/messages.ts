/**
 * Interface for defining translations
 *
 * Implementer must define a language and a message key to translation value map on the messages property.
 */
export interface Messages {
  lang: string;
  messages: { [key: string]: string; };
}
