import {TestBed} from '@angular/core/testing';
import {CaretService} from './caret.service';

describe('CaretService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CaretService = TestBed.get(CaretService);
    expect(service).toBeTruthy();
  });
});
