import {HttpErrorResponse} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Ng1CoyoConfig} from '@root/typings';
import {NG1_COYO_CONFIG, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import * as _ from 'lodash';
import {Observable, of} from 'rxjs';
import {switchMap} from 'rxjs/operators';

/**
 * Turns an http error response into an error message to be presented to the user.
 */
@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    private translateService: TranslateService,
    @Inject(NG1_STATE_SERVICE) private stateService: IStateService,
    @Inject(NG1_COYO_CONFIG) private coyoConfig: Ng1CoyoConfig) {
  }

  /**
   * Creates an error message. (The result is already translated).
   * Certain HTTP status codes are handled (401, 403, 503) as are coyo specific error codes.
   *
   * @param {HttpErrorResponse} error A response that represents an error or failure
   * @returns {Observable<string>} promise that resolves to a concrete message string
   */
  getMessage(error: HttpErrorResponse): Observable<string> {
    if (error.status === 401) {
      return this.translateService.get('ERRORS.UNAUTHORIZED');
    }

    if (error.status === 403) {
      return this.translateService.get('ERRORS.FORBIDDEN');
    }

    if (error.status === 503 || error.status === -1) {
      return this.translateService.get('ERRORS.SERVER_UNAVAILABLE');
    }

    const statusCode = _.get(error, 'error.errorStatus');
    const context = _.get(error, 'error.context', {});

    let key = 'ERRORS.STATUSCODE.UNKNOWN';

    if (statusCode) {

      const errorStatusCodes = ['NOT_FOUND', 'DELETED', 'LOCKED'];
      if (errorStatusCodes.indexOf(statusCode) < 0) {
        key = 'ERRORS.STATUSCODE.' + statusCode;
        return this.terminateTranslation('ERRORS.STATUSCODE.' + statusCode, context)
          // If translation is not found use unknown message key
          .pipe(switchMap(
            translation => translation === key ?
              this.terminateTranslation('ERRORS.STATUSCODE.UNKNOWN') : of(translation)));
      }

      const entityType = this.coyoConfig.entityTypes[_.get(error, 'error.context.entityType')];

      if (entityType && entityType.label) {
        return this.translateService.get(entityType.label).pipe(switchMap(
          (label: string) => {
            const translationContext: object = {};
            Object.assign(translationContext, context, {entityType: label});
            key = 'ERRORS.STATUSCODE.' + statusCode;
            return this.terminateTranslation(key, translationContext);
          }));
      } else {
        key = 'ERRORS.STATUSCODE.' + statusCode + '.DEFAULT';
      }
    }
    return this.terminateTranslation(key);
  }

  private terminateTranslation(key: string | string[], interpolateParams?: object): Observable<string> {
    if (interpolateParams) {
      return this.translateService.get(key, interpolateParams);
    } else {
      return this.translateService.get(key);
    }
  }

  /**
   * Redirect to an error state without changing the url.
   *
   * @param message The (already translated) error message to be displayed.
   * @param status Http status code (will affect the icon displayed)
   * @param buttons Configure the buttons visible on the error page (by default a 'home' link will be displayed).
   * Elements may be objects to configure a button (see details below or a string referencing a standard button config
   * (one of 'RETRY', 'CONFIGURE_BACKEND').
   */
  showErrorPage(message: string, status: number, buttons: string[]): void {
    this.stateService.go('error', {message, status, buttons}, {location: false, inherit: false});
  }

  /**
   * Convert the list of validation field errors from a REST error response into an object tree that can be
   * used with ng-messages.
   *
   * @param {HttpErrorResponse} error The http error response
   *
   * @returns {object} tree where the keys at the top level represent the field names and the second level
   * represents the validation error code, empty object if the response contained no field errors.
   */
  getValidationErrors(error: HttpErrorResponse): object {
    const fieldErrors = _.get(error, 'error.fieldErrors');
    if (fieldErrors) {
      return _.mapValues(_.groupBy(fieldErrors, 'key'), (item: any[]) => {
        const result = {};
        if (item.length > 0) {
          result[item[0].code] = true;
        }
        return result;
      });
    }
    return {};
  }
}
