import {ParticipantStatus} from '@domain/event/participant-status';
import {ParticipantsLimit} from '@domain/event/participants-limit';
import {SenderEventSettings} from '@domain/event/sender-event-settings';
import {Sender} from '@domain/sender/sender';
import {ShareableSender} from '@domain/sender/shareable-sender';

/**
 * Domain modal for Events
 */
export interface SenderEvent extends ShareableSender {
  creator: Sender;
  place: string;
  attendingCount: number;
  startDate: Date;
  endDate: Date;
  fullDay: boolean;
  settings?: SenderEventSettings;
  status?: ParticipantStatus;
  requestDefiniteAnswer?: boolean;
  limitedParticipants?: ParticipantsLimit;
}
