import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {CapabilitiesService} from '@domain/capability/capabilities/capabilities.service';
import {DomainService} from '@domain/domain/domain.service';
import {FilePreview} from '@domain/preview/file-preview';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {File} from '../file';

/**
 * Service for file requests
 */
@Injectable({
  providedIn: 'root'
})
export class FileService extends DomainService<File, File> {

  constructor(private capabilitiesService: CapabilitiesService,
              http: HttpClient,
              urlService: UrlService) {
    super(http, urlService);
  }

  protected getBaseUrl(): string {
    return '/web/senders/{senderId}/files';
  }

  /**
   * Returns the deep link URL for the given file.
   *
   * @param file The file.
   *
   * @returns the deep link URL.
   */
  getDeepUrl(file: File): string {
    return `/files/${file.senderId}/${file.id}/${file.name}`;
  }

  /**
   * Gets the file preview status
   *
   * @param url File previewUrl string to get parameters replaced
   * @param groupId Attached file groupId
   * @param fileId Attached file id
   *
   * @returns an 'Observable' with the attached file preview status
   */
  getPreviewStatus(url: string, groupId: string, fileId: string): Observable<string> {
    if (fileId !== undefined) {
      const httpOptions = {
        headers: new HttpHeaders({
          handleErrors: 'false'
        })
      };
      const processedUrl = url.replace('{{groupId}}', groupId)
        .replace('{{id}}', fileId) + '/preview-status';
      return this.http.get<{status: string}>(processedUrl, httpOptions).pipe(map(data => data.status));
    }
    return of('PROCESSING');
  }

  getImagePreviewUrl(url: string, groupId: string, file: FilePreview,
                     size: 'XS' | 'S' | 'M' | 'L' | 'XL' | 'XXL' | ''): Observable<string> {
    return this.capabilitiesService.previewImageFormat(file.contentType).pipe(map(format =>
      this.createPreviewUrl(url, groupId, file.id, size, format, file.modified, file.contentType)));
  }

  private createPreviewUrl(url: string, groupId: string, documentId: string, size: string, format: string,
                           modified: Date, contentType: string): string {
    const baseUrl = this.urlService.getBackendUrl()
      + this.urlService.insertPathVariablesIntoUrl(url, {groupId, id: documentId});
    return baseUrl + (baseUrl.indexOf('?') < 0 ? '?' : '&') + this.urlService.toMultiUrlParamString({
      modified: modified ? [modified + ''] : [] as string[],
      type: size ? [size] : [],
      format: (!size && contentType === 'image/gif') ? [contentType] : [format]
    });
  }
}
