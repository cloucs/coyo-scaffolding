import {HttpClientTestingModule} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {CapabilitiesService} from '@domain/capability/capabilities/capabilities.service';
import {FilePreview} from '@domain/preview/file-preview';
import * as _ from 'lodash';
import {of} from 'rxjs';
import {File} from '../file';
import {FileService} from './file.service';

describe('FileService', () => {
  let urlService: jasmine.SpyObj<UrlService>;
  let capabilitiesService: jasmine.SpyObj<CapabilitiesService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [FileService, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['join', 'insertPathVariablesIntoUrl', 'getBackendUrl',
          'toMultiUrlParamString'])
      }, {
        provide: CapabilitiesService,
        useValue: jasmine.createSpyObj('capabilitiesService', ['previewImageFormat'])
      }]
    });
    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
    capabilitiesService = TestBed.get(CapabilitiesService);
  });

  it('should be created', inject([FileService], (service: FileService) => {
    expect(service).toBeTruthy();
  }));

  it('should return the correct URL', inject([FileService], (service: FileService) => {
    // when
    const url = service.getUrl({senderId: 'senderID'});

    // then
    expect(url).toEqual('/web/senders/senderID/files');
  }));

  it('should return the deep link URL for a given file', inject([FileService], (service: FileService) => {
    // given
    const file = {id: 'ID', senderId: 'senderID', name: 'name'} as File;

    // when
    const url = service.getDeepUrl(file);

    // then
    expect(url).toEqual('/files/senderID/ID/name');
  }));

  it('should create a image preview url', inject([FileService], (service: FileService) => {
    // given
    const file = {
      groupId: 'group-id',
      id: 'id',
      previewUrl: '/{{group-id}}/test/{{id}}',
      contentType: 'document/word',
      modified: 12345
    } as unknown as FilePreview;
    const size = 'L';
    const imageFormat = 'image/jpeg';
    const backendUrl = 'http://test.com';
    capabilitiesService.previewImageFormat.and.returnValue(of(imageFormat));
    urlService.getBackendUrl.and.returnValue(backendUrl);
    urlService.insertPathVariablesIntoUrl.and.returnValue(`/${file.groupId}/test/${file.id}`);
    urlService.toMultiUrlParamString.and.returnValue(`format=${imageFormat}&type=${size}&modified=${file.modified}`);

    // when
    const result = service.getImagePreviewUrl(file.previewUrl, file.groupId, file, size);

    // then
    let called = false;
    result.subscribe(url => {
      expect(url).toBe('http://test.com/group-id/test/id?format=image/jpeg&type=L&modified=12345');
      called = true;
    });
    expect(urlService.toMultiUrlParamString).toHaveBeenCalledWith({
      modified: [file.modified + ''],
      type: [size],
      format: [imageFormat]
    });
    expect(called).toBeTruthy();
  }));
});
