import {BaseModel} from '@domain/base-model/base-model';
import {Target} from './target';

/**
 * Sender entity model
 */
export interface Sender extends BaseModel {
  displayName: string;
  displayNameInitials: string;
  color: string;
  slug: string;
  typeName: string;
  target: Target;
  public: boolean;
  _permissions: {[key: string]: boolean};
  externalWorkspaceMember?: boolean;
  imageUrls: {
    avatar: string;
    cover?: string;
  };
}
