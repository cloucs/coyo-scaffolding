/**
 * The target model
 */
export interface Target {
  name: string;
  params: {[key: string]: string};
}
