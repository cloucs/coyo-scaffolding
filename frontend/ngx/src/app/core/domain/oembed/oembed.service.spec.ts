import {inject, TestBed} from '@angular/core/testing';
import {OembedService} from './oembed.service';

describe('OembedService', () => {
  let container: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    container = {
      offsetWidth: 450
    } as HTMLElement;
  });

  it('should be created', () => {
    const service: OembedService = TestBed.get(OembedService);
    expect(service).toBeTruthy();
  });

  it('should match youtube video by url pattern', inject([OembedService], (service: OembedService) => {
    // given
    const url = 'https://www.youtube.com/watch?v=Bh4x2jCWsB4';
    const result = '<iframe webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" ' +
                   'src="//www.youtube-nocookie.com/embed/Bh4x2jCWsB4" width="450" height="252"></iframe>';

    // when
    const html = service.createByUrl(url, container);

    // then
    expect(html.outerHTML).toBe(result);
  }));

  it('should match instagram video by url pattern', inject([OembedService], (service: OembedService) => {
    // given
    const url = 'https://www.instagram.com/p/BzvbsK-g8yt/?utm_source=ig_web_copy_link';
    const result = '<iframe webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" ' +
                   'src="https://instagram.com/p/BzvbsK-g8yt/embed/" width="450" height="522" scrolling="no" allowtransparency="true"></iframe>';
    // when
    const html = service.createByUrl(url, container);

    // then
    expect(html.outerHTML).toBe(result);
  }));

  it('should match vine video by url pattern', inject([OembedService], (service: OembedService) => {
    // given
    const url = 'https://vine.co/v/MH0IJwlJFVz';
    const result = '<iframe webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" ' +
                   'src="//vine.co/v/MH0IJwlJFVz/embed/simple" width="450" height="450" class="vine-embed"></iframe>';

    // when
    const html = service.createByUrl(url, container);

    // then
    expect(html.outerHTML).toBe(result);
  }));

  it('should match vimeo video by url pattern', inject([OembedService], (service: OembedService) => {
    // given
    const url = 'https://vimeo.com/213100067';
    const result = '<iframe webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" ' +
                   'src="//player.vimeo.com/video/213100067" width="450" height="252"></iframe>';

    // when
    const html = service.createByUrl(url, container);

    // then
    expect(html.outerHTML).toBe(result);
  }));

  it('should match dailymotion video by url pattern', inject([OembedService], (service: OembedService) => {
    // given
    const url = 'https://www.dailymotion.com/video/x7n19xq';
    const result = '<iframe frameborder="0" src="//www.dailymotion.com/embed/video/x7n19xq" width="450" height="252"></iframe>';

    // when
    const html = service.createByUrl(url, container);

    // then
    expect(html.outerHTML).toBe(result);
  }));

  it('should match youku video  by url pattern', inject([OembedService], (service: OembedService) => {
    // given
    const url = 'https://v.youku.com/v_show/id_XNDM0ODk2MDA0NA==.html?spm=a2h0j.11185381.listitem_page1.5!8~A';
    const result = '<iframe webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" ' +
                   'src="//player.youku.com/embed/XNDM0ODk2MDA0NA" height="450" width="540"></iframe>';

    // when
    const html = service.createByUrl(url, container);

    // then
    expect(html.outerHTML).toBe(result);
  }));

  it('should match mp4 video by url pattern', inject([OembedService], (service: OembedService) => {
    // given
    const url = 'https://www.seitmitvideo.com/video.mp4';
    const result = '<video controls="" src="https://www.seitmitvideo.com/video.mp4" width="450" height="252"></video>';

    // when
    const html = service.createByUrl(url, container);

    // then
    expect(html.outerHTML).toBe(result);
  }));

  it('should match ogg video by url pattern', inject([OembedService], (service: OembedService) => {
    // given
    const url = 'https://www.seitmitvideo.com/video.ogg';
    const result = '<video controls="" src="https://www.seitmitvideo.com/video.ogg" width="450" height="252"></video>';

    // when
    const html = service.createByUrl(url, container);

    // then
    expect(html.outerHTML).toBe(result);
  }));

  it('should match webm video by url pattern', inject([OembedService], (service: OembedService) => {
    // given
    const url = 'https://www.seitmitvideo.com/video.webm';
    const result = '<video controls="" src="https://www.seitmitvideo.com/video.webm" width="450" height="252"></video>';

    // when
    const html = service.createByUrl(url, container);

    // then
    expect(html.outerHTML).toBe(result);
  }));

  it('should match videoCdn by url pattern', inject([OembedService], (service: OembedService) => {
    // given
    const url = 'https://e.video-cdn.net/video?video-id=9jZMQU5bXFhM1zvBZ9kNUy&player-id=4q8Fb_7qnb17zTCaRECe3D';
    const result = '<iframe webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" ' +
                   'src="https://e.video-cdn.net/video?video-id=9jZMQU5bXFhM1zvBZ9kNUy&amp;player-id=4q8Fb_7qnb17zTCaRECe3D" ' +
                   'width="450" height="252"></iframe>';

    // when
    const html = service.createByUrl(url, container);

    // then
    expect(html.outerHTML).toBe(result);
  }));
});
