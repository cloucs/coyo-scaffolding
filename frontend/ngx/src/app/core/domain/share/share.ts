import {BaseModel} from '../base-model/base-model';
import {Sender} from '../sender/sender';

/**
 * A share.
 */
export interface Share extends BaseModel {
  author: Sender;
  recipient: Sender;
  canAccessRecipient: boolean;
  stickyExpiry: number;
  unread: boolean;
  data: {
    message: string;
  };
  _permissions?: {
    sticky: boolean;
    delete: boolean;
    accessoriginalauthor: boolean;
  };
}
