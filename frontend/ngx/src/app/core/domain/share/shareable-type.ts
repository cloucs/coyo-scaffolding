/**
 * The type name of a shareable entity
 */
export type ShareableType = 'timeline-item' | 'blog-article' | 'wiki-article';
