import {BaseModel} from '@domain/base-model/base-model';
import {LaunchpadLink} from './launchpad-link';

/**
 * Represents the Launchpad categories
 */
export interface LaunchpadCategory extends BaseModel {
  name: string;
  links: LaunchpadLink[];
  _permissions?: {
    manage: boolean
  };
}
