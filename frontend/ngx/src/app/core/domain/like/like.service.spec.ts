import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {discardPeriodicTasks, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {CoyoConfig} from '@root/typings';
import {NG1_COYO_CONFIG} from '@upgrade/upgrade.module';
import * as _ from 'lodash';
import {skip} from 'rxjs/operators';
import {LikeInfo} from './like-info';
import {LikeState} from './like-state';
import {LikeService} from './like.service';

describe('LikeService', () => {
  let likeService: LikeService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let urlService: jasmine.SpyObj<UrlService>;
  const coyoConfig = {
    likeReloadIntervalMinutes: 1
  } as CoyoConfig;
  const senderId = 'sender-id';
  const targetId = 'target-id';
  const targetType = 'test-type';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LikeService, {
        provide: NG1_COYO_CONFIG, useValue: coyoConfig
      }, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['join'])
      }]
    });

    likeService = TestBed.get(LikeService);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);
    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should like a target', () => {
    // given
    const likeInfo = {
      count: 12,
      likedBySender: true
    } as LikeInfo;

    // when
    const observable = likeService.like(senderId, targetId, targetType);

    // then
    observable.subscribe(result => {
      expect(result).toBe(likeInfo);
    });
    const req = httpMock.expectOne(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`);
    expect(req.request.method).toBe('POST');
    req.flush(likeInfo);
  });

  it('should unlike a target', () => {
    // when
    const observable = likeService.unlike(senderId, targetId, targetType);

    // then
    observable.subscribe();
    const req = httpMock.expectOne(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`);
    expect(req.request.method).toBe('DELETE');
    req.flush(null);
  });

  it('should delay the like info request for ' + LikeService.THROTTLE + 'ms', fakeAsync(() => {
    // when
    const observable = likeService.getLikeTargetState$(senderId, targetId, targetType);

    // then
    const subscription = observable.subscribe();
    // => There should be no request because the debounce time is not yet over
    httpMock.verify();
    subscription.unsubscribe();
    discardPeriodicTasks();
  }));

  it('should send a bulked request after waiting ' + LikeService.THROTTLE + 'ms', fakeAsync(() => {
    // given
    const targetId2 = 'target-id2';
    const responseBody: {[key: string]: LikeInfo} = {};
    responseBody[targetId] = {
      count: 5
    } as LikeInfo;
    responseBody[targetId2] = {
      count: 2
    } as LikeInfo;

    // when
    const observable = likeService.getLikeTargetState$(senderId, targetId, targetType).pipe(skip(1));
    const subscription = observable.subscribe(result => expect(result.totalCount).toBe(5));
    const observable2 = likeService.getLikeTargetState$(senderId, targetId2, targetType).pipe(skip(1));
    const subscription2 = observable2.subscribe(result => expect(result.totalCount).toBe(2));
    tick(LikeService.THROTTLE);

    // then
    const request = httpMock.expectOne(req => req.url === `/web/like-targets/${targetType}` &&
      req.params.getAll('ids').indexOf(targetId) > -1 &&
      req.params.getAll('ids').indexOf(targetId2) > -1
    );
    request.flush(responseBody);

    tick();

    subscription.unsubscribe();
    subscription2.unsubscribe();
    discardPeriodicTasks();
  }));

  it('should send requests periodically', fakeAsync(() => {

    // We have to create a new service here to use fake async as the time is created in the constructor
    const service = new LikeService(httpClient, urlService, coyoConfig);

    // when
    const observable = service.getLikeTargetState$(senderId, targetId, targetType);
    const subscription = observable.subscribe();

    // then
    tick(LikeService.THROTTLE);
    httpMock.expectOne(req =>
        req.url === `/web/like-targets/${targetType}` &&
          req.params.getAll('ids').indexOf(targetId) > -1

    );
    httpMock.verify();
    tick(coyoConfig.likeReloadIntervalMinutes * 1000 * 60);
    tick(LikeService.THROTTLE);
    httpMock.expectOne(req =>  req.url === `/web/like-targets/${targetType}` &&
        req.params.getAll('ids').indexOf(targetId) > -1
    );

    subscription.unsubscribe();
    discardPeriodicTasks();
  }));

  it('should create a second request when a new target id comes in while requesting', fakeAsync(() => {
    // given
    const targetId2 = 'target-id2';
    const responseBody: {[key: string]: LikeInfo} = {};
    responseBody[targetId] = {
      count: 5
    } as LikeInfo;

    const responseBody2: {[key: string]: LikeInfo} = {};
    responseBody2[targetId2] = {
      count: 2
    } as LikeInfo;

    // when
    const observable = likeService.getLikeTargetState$(senderId, targetId, targetType);

    const subscription = observable.pipe(skip(1)).subscribe(result =>
      expect(result.totalCount).toBe(5)
    );
    tick(LikeService.THROTTLE);

    const observable2 = likeService.getLikeTargetState$(senderId, targetId2, targetType);

    const subscription2 = observable2.pipe(skip(1)).subscribe(result =>
      expect(result.totalCount).toBe(2)
    );

    const request = httpMock.expectOne(req => req.url === `/web/like-targets/${targetType}` &&
      req.params.getAll('ids').indexOf(targetId) > -1 &&
      req.params.getAll('ids').indexOf(targetId2) === -1
    );

    request.flush(responseBody);

    tick(LikeService.THROTTLE);

    // then
    const request2 = httpMock.expectOne(req => req.url === `/web/like-targets/${targetType}` &&
      req.params.getAll('ids').indexOf(targetId) === -1 &&
      req.params.getAll('ids').indexOf(targetId2) > -1
    );

    request2.flush(responseBody2);

    subscription.unsubscribe();
    subscription2.unsubscribe();
    discardPeriodicTasks();
  }));

  it('should emit a loading state on subscribing', fakeAsync(() => {
    // given
    let stateEmitted = false;

    // when
    const subscription = likeService.getLikeTargetState$(senderId, targetId, targetType)
      .subscribe(state => {
        stateEmitted = true;
        expect(state.isLoading).toBeTruthy();
    });
    tick();

    // then
    subscription.unsubscribe();
    expect(stateEmitted).toBeTruthy();
    discardPeriodicTasks();
  }));

  it('should emit changes to observers when liking', fakeAsync(() => {
    // given
    const states: LikeState[] = [];
    const likeInfo = {
      likedBySender: true
    } as LikeInfo;

    const subscription = likeService.getLikeTargetState$(senderId, targetId, targetType).pipe(skip(1))
      .subscribe(state => states.push(state));

    // when
    likeService.like(senderId, targetId, targetType).subscribe();

    const req = httpMock.expectOne(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`);

    req.flush(likeInfo);

    // then
    subscription.unsubscribe();
    expect(states.length).toBe(2);
    expect(states[0].isLoading).toBeTruthy();
    expect(states[1].isLiked).toBeTruthy();
    expect(states[1].isLoading).toBeFalsy();
    discardPeriodicTasks();
  }));

  it('should emit changes to observers when unliking', fakeAsync(() => {
    // given
    const states: LikeState[] = [];
    const likeInfo = {
      likedBySender: false
    } as LikeInfo;

    const subscription = likeService.getLikeTargetState$(senderId, targetId, targetType).pipe(skip(2))
      .subscribe(state => states.push(state));

    tick(LikeService.THROTTLE);
    const request = httpMock.expectOne(req =>
        req.url === `/web/like-targets/${targetType}` && req.params.getAll('ids').indexOf(targetId) > -1
    );

    request.flush({'target-id': {likedBySender: true}});

    // when
    likeService.unlike(senderId, targetId, targetType).subscribe();

    const request2 = httpMock.expectOne(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`);

    request2.flush(likeInfo);

    // then
    subscription.unsubscribe();
    expect(states.length).toBe(2);
    expect(states[0].isLoading).toBeTruthy();
    expect(states[0].isLiked).toBeTruthy();
    expect(states[1].isLiked).toBeFalsy();
    expect(states[1].isLoading).toBeFalsy();
    discardPeriodicTasks();
  }));

  it('should revert the loading state when error occurs', fakeAsync(() => {
    // given
    let stateEmitted = false;

    // when
    const subscription = likeService.getLikeTargetState$(senderId, targetId, targetType).pipe(skip(1))
      .subscribe(state => {
        stateEmitted = true;
        expect(state.isLoading).toBeFalsy();
      });

    tick(LikeService.THROTTLE);
    const request = httpMock.expectOne(req =>
      req.url === `/web/like-targets/${targetType}` && req.params.getAll('ids').indexOf(targetId) > -1
    );

    request.error(new ErrorEvent('test error'));

    // then
    subscription.unsubscribe();
    expect(stateEmitted).toBeTruthy();
  }));

  it('should call the right endpoint', () => {
    // when
    const result = likeService.getUrl({
      targetId,
      targetType
    });

    // then
    expect(result).toBe(`/web/like-targets/${targetType}/${targetId}/likes`);
  });

  it('should skip initial request when flag is given', fakeAsync(() => {
    // given

    // when
    likeService.getLikeTargetState$('sender-id', 'target-id', 'target-type', true);
    tick(LikeService.THROTTLE);

    // then
    httpMock.verify();
    discardPeriodicTasks();
  }));
});
