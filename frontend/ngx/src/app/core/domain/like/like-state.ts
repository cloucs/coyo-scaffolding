import {Sender} from '../sender/sender';

/**
 * State object for the likes
 */
export interface LikeState {
  isLoading: boolean;
  isLiked: boolean;
  total: Sender[];
  totalCount: number;
  others: Sender[];
  othersCount: number;
}
