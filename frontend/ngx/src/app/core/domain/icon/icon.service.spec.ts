import {TestBed} from '@angular/core/testing';
import {File} from '../file/file';
import {IconService} from './icon.service';

describe('IconService', () => {
  let iconService: IconService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    iconService = TestBed.get(IconService);
  });

  it('should be created', () => {
    expect(iconService).toBeTruthy();
  });

  it('upload failed icon', () => {
    // given
    const file = {uploadFailed: true} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-alert-circle-o upload-failed']);
  });

  it('no contentType icon', () => {
    // given
    const file = {contentType: ''} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-coyo zmdi-coyo-file']);
  });

  it('gsuite icon', () => {
    // given
    const file = {contentType: 'application/vnd.google-apps.audio', storage: 'G_SUITE'} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-coyo zmdi-coyo-file']);
  });

  it('msOffice DOC icon', () => {
    // given
    const file = {contentType: 'application/msword'} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-coyo zmdi-coyo-word']);
  });

  it('image icon', () => {
    // given
    const file = {contentType: 'image/jpeg'} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-coyo zmdi-coyo-image']);
  });

  it('video icon', () => {
    // given
    const file = {contentType: 'video/mp4'} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-coyo zmdi-coyo-video']);
  });

  it('pdf icon', () => {
    // given
    const file = {contentType: 'application/pdf'} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-coyo zmdi-coyo-pdf']);
  });

  it('plain text icon', () => {
    // given
    const file = {contentType: 'text/plain'} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-coyo zmdi-coyo-text']);
  });

  it('zip icon', () => {
    // given
    const file = {contentType: 'application/zip'} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-coyo zmdi-coyo-zip']);
  });

  it('default icon', () => {
    // given
    const file = {contentType: ''} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-coyo zmdi-coyo-file']);
  });

  it('not a file', () => {
    // given
    const file = undefined as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-coyo zmdi-coyo-file']);
  });

  it('file folder', () => {
    // given
    const file = {folder: 'folder'} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-folder-outline']);
  });

  it('file folder and appRoot', () => {
    // given
    const file = {folder: 'folder', appRoot: 'appRoot'} as File;
    // when
    const result = iconService.getFileIcons(file);
    // then
    expect(result).toEqual(['zmdi-folder-outline', 'zmdi-lock']);
  });

  it('should return default COYO file icon', () => {
    // when
    const result = iconService.getFileIconByMimeType('application/zip', 'G_SUITE');

    // then
    expect(result).toEqual('zmdi-coyo zmdi-coyo-file');
  });

  it('should return COYO icon if no storage given', () => {
    // when
    const result = iconService.getFileIconByMimeType('application/image', undefined);

    // then
    expect(result).toEqual('zmdi-coyo zmdi-coyo-image');
  });

  it('should return COYO default if no valid mime type given', () => {
    // when
    const result = iconService.getFileIconByMimeType('application/xyz', 'OFFICE_365');

    // then
    expect(result).toEqual('zmdi-coyo zmdi-coyo-file');
  });
});
