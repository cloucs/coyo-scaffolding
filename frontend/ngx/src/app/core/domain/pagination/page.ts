import {Order} from './order';

/**
 * A sublist of a list of objects.
 */
export interface Page<T> {

  /**
   * The elements of type `T` in this page.
   */
  content: T[];

  /**
   * Indicates whether this page is the first one.
   */
  first: boolean;

  /**
   * Indicates whether this page is the last one.
   */
  last: boolean;

  /**
   * The current page number.
   */
  number: number;

  /**
   * The number of elements currently on this page.
   */
  numberOfElements: number;

  /**
   * The current page size.
   */
  size: number;

  /**
   * The sort order of this page.
   */
  sort: Order[];

  /**
   * The total amount of elements.
   */
  totalElements: number;

  /**
   * The total amount of pages.
   */
  totalPages: number;
}
