/**
 * A theme
 */
export interface Theme {
  modified: Date;
  css: string;
  colors: { [key: string]: string };
  images: { [key: string]: string };
}
