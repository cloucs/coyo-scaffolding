import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {discardPeriodicTasks, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {SocketService} from '@core/socket/socket.service';
import * as _ from 'lodash';
import {of} from 'rxjs';
import {PresenceStatus} from './presence-status';
import {User} from './user';
import {UserService} from './user.service';

describe('UserService', () => {
  let httpTestingController: HttpTestingController;
  let urlService: jasmine.SpyObj<UrlService>;
  let socketService: jasmine.SpyObj<SocketService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService,
        {provide: UrlService, useValue: jasmine.createSpyObj('UrlService', ['join'])},
        {provide: SocketService, useValue: jasmine.createSpyObj('SocketService', ['listenTo$'])}
      ]
    });
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
    socketService = TestBed.get(SocketService);
    socketService.listenTo$.and.returnValue(of());
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('should return the correct URL', inject([UserService], (service: UserService) => {
    // when
    const url = service.getUrl();

    // then
    expect(url).toEqual('/web/users');
  }));

  it('should return the user online count', inject([UserService], (service: UserService) => {
    // given
    const response = {count: 3};

    // when
    const count$ = service.getUserOnlineCount();

    // then
    count$.subscribe(count => expect(count).toBe(3));
    const req = httpTestingController.expectOne(request => request.url === '/web/users/online/count');
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(response);
  }));

  it('should return a minimum online count of 1', inject([UserService], (service: UserService) => {
    // It happens that the websocket connection is not yet fully established when the
    // user requests the user online count via HTTP. Thus, his own connection is not
    // taken into account. We therefore want to return a minimum of 1.

    // given
    const response = {count: 0};

    // when
    const count$ = service.getUserOnlineCount();

    // then
    count$.subscribe(count => expect(count).toBe(1));
    const req = httpTestingController.expectOne(request => request.url === '/web/users/online/count');

    // finally
    req.flush(response);
  }));

  it('should return an initial user presence online status and update by websocket', fakeAsync(inject([UserService], (service: UserService) => {
    // given
    const user = {
      id: 'user-id'
    } as User;
    const response = {'user-id': {
      state: 'ONLINE',
      label: 'This is a test user'
    } as PresenceStatus};

    const newPresenceStatus = {
      state: 'BUSY',
      label: 'I am busy'
    } as PresenceStatus;
    socketService.listenTo$.and.returnValue(of({content: newPresenceStatus}));

    // when
    const presenceStatus$ = service.getPresenceStatus$(user);

    // then
    let invocationCount = 0;
    const subscription = presenceStatus$.subscribe(presenceStatus => {
      if (invocationCount === 0) {
        expect(presenceStatus.state).toBe('ONLINE');
        expect(presenceStatus.label).toBe('This is a test user');
      } else {
        expect(presenceStatus.state).toBe('BUSY');
        expect(presenceStatus.label).toBe('I am busy');
        subscription.unsubscribe();
      }
      invocationCount++;
    });
    tick(200);

    const req = httpTestingController.expectOne(request => request.urlWithParams === '/web/users/presence-status?userIds=user-id');

    // finally
    req.flush(response);
    discardPeriodicTasks();
  })));
});
