import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import * as _ from 'lodash';
import {Observable, of} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {CapabilityModel} from './../capability-model';

/**
 * Capabilities service providing information on attachments previews
 */
@Injectable({
  providedIn: 'root'
})
export class CapabilitiesService {

  previewTypes$: Observable<CapabilityModel>;

  constructor(
    @Inject(HttpClient) protected http: HttpClient,
    @Inject(UrlService) protected urlService: UrlService) {}

  /**
   * Determines if the image formate is png or jpeg
   *
   * @param contentType Attached file contentType
   *
   * @returns an 'Observable' with format details
   */
  previewImageFormat(contentType: string): Observable<string> {
    if (!contentType) {
      return of('');
    }
    return this.getPreviewTypes().pipe(map(contentTypes => {
      const arrayOfTypes = _.find(contentTypes, contentType);
      if (arrayOfTypes) {
        const format = _.find(arrayOfTypes[contentType], {details: 'image/png'}) ||
        _.find(arrayOfTypes[contentType], {details: 'image/jpeg'});
        return format.details as any as string;
      }
      return '';
    }));
  }

  /**
   * Determines if the image preview is available
   *
   * @param contentType Attached file contentType
   *
   * @returns an 'Observable' with the information if the preview image is available or not
   */
  imgAvailable(contentType: string): Observable<boolean> {
    return this.getPreviewTypes().pipe(map(contentTypes => {
      const arrayOfTypes = _.find(contentTypes, contentType);
      return arrayOfTypes &&
              (_.find(arrayOfTypes[contentType], {details: 'image/jpeg'}) || _.find(arrayOfTypes[contentType], {details: 'image/png'})) !== undefined;
    }));
  }

  /**
   * Determines if the pdf preview is available
   *
   * @param contentType Attached file contentType
   *
   * @returns an 'Observable' with the information if the pdf preview is available or not
   */
  pdfAvailable(contentType: string): Observable<boolean> {
    return this.getPreviewTypes().pipe(map(contentTypes => {
      const arrayOfTypes = _.find(contentTypes, contentType);
      return arrayOfTypes && (_.find(arrayOfTypes[contentType], {details: 'application/pdf'}) !== undefined) || contentType === 'application/pdf';
    }));
  }

  private getPreviewTypes(): Observable<CapabilityModel> {
    if (this.previewTypes$) {
      return this.previewTypes$;
    }
    this.previewTypes$ = this.http.get<CapabilityModel>('/web/capabilities/preview').pipe(shareReplay(1));
    return this.previewTypes$;
  }
}
