#!/bin/bash

set -e

rm /usr/local/apache2/htdocs/config.js

echo "window.Config = { applicationName: '${COYO_APP_NAME}', backendUrlStrategy: '${COYO_BACKEND_URL_STRATEGY}', backendUrl: '${COYO_BACKEND_URL}', likeReloadIntervalMinutes: ${COYO_LIKE_RELOAD_INTERVAL_MINUTES:-1}, minReconnectDelaySeconds: ${COYO_MIN_RECONNECT_DELAY_SECONDS:-30}, maxReconnectDelaySeconds: ${COYO_MAX_RECONNECT_DELAY_SECONDS:-30}, debug: false, enabledServices: ${ENABLED_SERVICES:-[]} };" > /usr/local/apache2/htdocs/config.js

# Start httpd
exec httpd-foreground
