/* eslint-disable */
/*! modernizr 3.6.0 (Custom Build) | MIT *
 * https://modernizr.com/download/?-bloburls-canvas-contenteditable-cookies-flexbox-history-localstorage-mediaqueries-sessionstorage-touchevents-video-websockets-setclasses !*/
 !function(e,t,n){function r(e,t){return typeof e===t}function o(){var e,t,n,o,i,a,s;for(var l in x)if(x.hasOwnProperty(l)){if(e=[],t=x[l],t.name&&(e.push(t.name.toLowerCase()),t.options&&t.options.aliases&&t.options.aliases.length))for(n=0;n<t.options.aliases.length;n++)e.push(t.options.aliases[n].toLowerCase());for(o=r(t.fn,"function")?t.fn():t.fn,i=0;i<e.length;i++)a=e[i],s=a.split("."),1===s.length?Modernizr[s[0]]=o:(!Modernizr[s[0]]||Modernizr[s[0]]instanceof Boolean||(Modernizr[s[0]]=new Boolean(Modernizr[s[0]])),Modernizr[s[0]][s[1]]=o),b.push((o?"":"no-")+s.join("-"))}}function i(e){var t=w.className,n=Modernizr._config.classPrefix||"";if(_&&(t=t.baseVal),Modernizr._config.enableJSClass){var r=new RegExp("(^|\\s)"+n+"no-js(\\s|$)");t=t.replace(r,"$1"+n+"js$2")}Modernizr._config.enableClasses&&(t+=" "+n+e.join(" "+n),_?w.className.baseVal=t:w.className=t)}function a(){return"function"!=typeof t.createElement?t.createElement(arguments[0]):_?t.createElementNS.call(t,"http://www.w3.org/2000/svg",arguments[0]):t.createElement.apply(t,arguments)}function s(e){return e.replace(/([a-z])-([a-z])/g,function(e,t,n){return t+n.toUpperCase()}).replace(/^-/,"")}function l(){var e=t.body;return e||(e=a(_?"svg":"body"),e.fake=!0),e}function c(e,n,r,o){var i,s,c,u,f="modernizr",d=a("div"),p=l();if(parseInt(r,10))for(;r--;)c=a("div"),c.id=o?o[r]:f+(r+1),d.appendChild(c);return i=a("style"),i.type="text/css",i.id="s"+f,(p.fake?p:d).appendChild(i),p.appendChild(d),i.styleSheet?i.styleSheet.cssText=e:i.appendChild(t.createTextNode(e)),d.id=f,p.fake&&(p.style.background="",p.style.overflow="hidden",u=w.style.overflow,w.style.overflow="hidden",w.appendChild(p)),s=n(d,e),p.fake?(p.parentNode.removeChild(p),w.style.overflow=u,w.offsetHeight):d.parentNode.removeChild(d),!!s}function u(e,t){return!!~(""+e).indexOf(t)}function f(e,t){return function(){return e.apply(t,arguments)}}function d(e,t,n){var o;for(var i in e)if(e[i]in t)return n===!1?e[i]:(o=t[e[i]],r(o,"function")?f(o,n||t):o);return!1}function p(e){return e.replace(/([A-Z])/g,function(e,t){return"-"+t.toLowerCase()}).replace(/^ms-/,"-ms-")}function v(t,n,r){var o;if("getComputedStyle"in e){o=getComputedStyle.call(e,t,n);var i=e.console;if(null!==o)r&&(o=o.getPropertyValue(r));else if(i){var a=i.error?"error":"log";i[a].call(i,"getComputedStyle returning null, its possible modernizr test results are inaccurate")}}else o=!n&&t.currentStyle&&t.currentStyle[r];return o}function m(t,r){var o=t.length;if("CSS"in e&&"supports"in e.CSS){for(;o--;)if(e.CSS.supports(p(t[o]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var i=[];o--;)i.push("("+p(t[o])+":"+r+")");return i=i.join(" or "),c("@supports ("+i+") { #modernizr { position: absolute; } }",function(e){return"absolute"==v(e,null,"position")})}return n}function y(e,t,o,i){function l(){f&&(delete j.style,delete j.modElem)}if(i=r(i,"undefined")?!1:i,!r(o,"undefined")){var c=m(e,o);if(!r(c,"undefined"))return c}for(var f,d,p,v,y,h=["modernizr","tspan","samp"];!j.style&&h.length;)f=!0,j.modElem=a(h.shift()),j.style=j.modElem.style;for(p=e.length,d=0;p>d;d++)if(v=e[d],y=j.style[v],u(v,"-")&&(v=s(v)),j.style[v]!==n){if(i||r(o,"undefined"))return l(),"pfx"==t?v:!0;try{j.style[v]=o}catch(g){}if(j.style[v]!=y)return l(),"pfx"==t?v:!0}return l(),!1}function h(e,t,n,o,i){var a=e.charAt(0).toUpperCase()+e.slice(1),s=(e+" "+E.join(a+" ")+a).split(" ");return r(t,"string")||r(t,"undefined")?y(s,t,o,i):(s=(e+" "+O.join(a+" ")+a).split(" "),d(s,t,n))}function g(e,t,r){return h(e,n,n,t,r)}var b=[],x=[],C={_version:"3.6.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,t){var n=this;setTimeout(function(){t(n[e])},0)},addTest:function(e,t,n){x.push({name:e,fn:t,options:n})},addAsyncTest:function(e){x.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=C,Modernizr=new Modernizr,Modernizr.addTest("cookies",function(){try{t.cookie="cookietest=1";var e=-1!=t.cookie.indexOf("cookietest=");return t.cookie="cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT",e}catch(n){return!1}}),Modernizr.addTest("history",function(){var t=navigator.userAgent;return-1===t.indexOf("Android 2.")&&-1===t.indexOf("Android 4.0")||-1===t.indexOf("Mobile Safari")||-1!==t.indexOf("Chrome")||-1!==t.indexOf("Windows Phone")||"file:"===location.protocol?e.history&&"pushState"in e.history:!1});var S=!1;try{S="WebSocket"in e&&2===e.WebSocket.CLOSING}catch(T){}Modernizr.addTest("websockets",S),Modernizr.addTest("localstorage",function(){var e="modernizr";try{return localStorage.setItem(e,e),localStorage.removeItem(e),!0}catch(t){return!1}}),Modernizr.addTest("sessionstorage",function(){var e="modernizr";try{return sessionStorage.setItem(e,e),sessionStorage.removeItem(e),!0}catch(t){return!1}});var w=t.documentElement,_="svg"===w.nodeName.toLowerCase();Modernizr.addTest("canvas",function(){var e=a("canvas");return!(!e.getContext||!e.getContext("2d"))}),Modernizr.addTest("contenteditable",function(){if("contentEditable"in w){var e=a("div");return e.contentEditable=!0,"true"===e.contentEditable}}),Modernizr.addTest("video",function(){var e=a("video"),t=!1;try{t=!!e.canPlayType,t&&(t=new Boolean(t),t.ogg=e.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),t.h264=e.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),t.webm=e.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,""),t.vp9=e.canPlayType('video/webm; codecs="vp9"').replace(/^no$/,""),t.hls=e.canPlayType('application/x-mpegURL; codecs="avc1.42E01E"').replace(/^no$/,""))}catch(n){}return t});var P=function(){var t=e.matchMedia||e.msMatchMedia;return t?function(e){var n=t(e);return n&&n.matches||!1}:function(t){var n=!1;return c("@media "+t+" { #modernizr { position: absolute; } }",function(t){n="absolute"==(e.getComputedStyle?e.getComputedStyle(t,null):t.currentStyle).position}),n}}();C.mq=P,Modernizr.addTest("mediaqueries",P("only all"));var k="Moz O ms Webkit",E=C._config.usePrefixes?k.split(" "):[];C._cssomPrefixes=E;var z=function(t){var r,o=A.length,i=e.CSSRule;if("undefined"==typeof i)return n;if(!t)return!1;if(t=t.replace(/^@/,""),r=t.replace(/-/g,"_").toUpperCase()+"_RULE",r in i)return"@"+t;for(var a=0;o>a;a++){var s=A[a],l=s.toUpperCase()+"_"+r;if(l in i)return"@-"+s.toLowerCase()+"-"+t}return!1};C.atRule=z;var O=C._config.usePrefixes?k.toLowerCase().split(" "):[];C._domPrefixes=O;var L={elem:a("modernizr")};Modernizr._q.push(function(){delete L.elem});var j={style:L.elem.style};Modernizr._q.unshift(function(){delete j.style}),C.testAllProps=h,C.testAllProps=g,Modernizr.addTest("flexbox",g("flexBasis","1px",!0));var N=C.prefixed=function(e,t,n){return 0===e.indexOf("@")?z(e):(-1!=e.indexOf("-")&&(e=s(e)),t?h(e,t,n):h(e,"pfx"))},R=N("URL",e,!1);R=R&&e[R],Modernizr.addTest("bloburls",R&&"revokeObjectURL"in R&&"createObjectURL"in R);var A=C._config.usePrefixes?" -webkit- -moz- -o- -ms- ".split(" "):["",""];C._prefixes=A;var U=C.testStyles=c;Modernizr.addTest("touchevents",function(){var n;if("ontouchstart"in e||e.DocumentTouch&&t instanceof DocumentTouch)n=!0;else{var r=["@media (",A.join("touch-enabled),("),"heartz",")","{#modernizr{top:9px;position:absolute}}"].join("");U(r,function(e){n=9===e.offsetTop})}return n}),o(),i(b),delete C.addTest,delete C.addAsyncTest;for(var $=0;$<Modernizr._q.length;$++)Modernizr._q[$]();e.Modernizr=Modernizr}(window,document);
window.Browser = {

  /**
   * Checks if the current browser is at least partially supported by the Coyo application.
   *
   * @return {boolean} True, if and only if the current browser is partially or fully supported.
   */
  isPartiallySupported: function () {
    return Modernizr.bloburls
        && Modernizr.cookies
        && Modernizr.flexbox
        && Modernizr.localstorage
        && Modernizr.mediaqueries
        && Modernizr.sessionstorage
        && Modernizr.websockets;
  },

  /**
   * Checks if the current browser is fully supported by the Coyo application.
   *
   * @return {boolean} True, if and only if the current browser is fully supported.
   */
  isFullySupported: function () {
    return this.isPartiallySupported()
        && Modernizr.canvas
        && Modernizr.contenteditable
        && Modernizr.history;
  },

  /**
   * Displays an error bar for partially supported browsers.
   */
  showErrorBar: function () {
    console.log('[Browser Support] ', Modernizr);
    var cls = document.body.getAttribute('class');
    document.body.setAttribute('class', 'browser-error-bar ' + (cls || ''));
    var ctn = document.body.innerHTML;
    document.body.innerHTML = '<p class="browser-warning" onclick="this.parentNode.removeChild(this)">' +
        '<span>You are using an outdated browser that is only partially supported by COYO. ' +
        'Please update your browser to avoid any runtime problems. For more information ' +
        'visit <a href="https://www.coyoapp.com/">https://www.coyoapp.com/</a> or contact the ' +
        '<a href="https://www.service.coyoapp.com/">COYO Service</a>.</span></p>' + ctn;
  },

  /**
   * Displays an error page for unsupported browsers.
   */
  showErrorPage: function () {
    console.log('[Browser Support] ', Modernizr);
    document.body.setAttribute('class', 'browser-error-page');
    document.body.removeAttribute('ng-class');
    document.body.innerHTML = '<h1>:(</h1>' +
        '<p>You are using an outdated browser that is not supported by COYO. ' +
        'Please update your browser and try again. For more information about ' +
        'visit <a href="https://www.coyoapp.com/">https://www.coyoapp.com/</a> or contact the ' +
        '<a href="https://www.service.coyoapp.com/">COYO Service</a>.</p>' +
        '<p><small>User Agent: ' + window.navigator.userAgent + '</small></p>';
  },

  /**
   * Returns whether the browser is the internet explorer
   */
  isInternetExplorer: function () {
    var ua = navigator.userAgent;
    /* MSIE used to detect old browsers and Trident used to newer ones*/
    return ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
  }
};
