/* eslint-disable angular/window-service */

// Import legacy vendor scripts in the correct order
window._ = require('lodash');
window.$ = window.jQuery = require('jquery');
window.angular = require('angular');
require('angular-animate');
require('angular-cookies');
require('angular-messages');
require('angular-sanitize');

window.numeral = require('numeral');
window.Stomp = require('exports-loader?window.Stomp!' + 'stompjs/lib/stomp.js');
window.SockJS = require('sockjs-client');
window.kalendae = require('kalendae');
window.marked = require('marked');
window.rxjs = require('../../../../ngx/node_modules/rxjs');

require('angularjs-rails-resource');
require('angularjs-rails-resource/extensions/snapshots.js');

require('picturefill');
require('angular-picture/src/angular-picture.js');

require('ngdraggable-coyo/ngDraggable.js');
require('angular-ui-tour');
window.moment = require('../../../../ngx/node_modules/moment');
require('../../../../ngx/node_modules/moment-timezone');

require('ng-error');
require('ng-tags-input');
require('angular-ui-bootstrap');
require('imports-loader?Tinycon=tinycon2!angular-tinycon/dist/angular-tinycon.min.js');
window.Swiper = require('swiper');
require('angular-swiper');
require('angular-bootstrap-colorpicker');
require('angularjs-scroll-glue');
require('ngclipboard');
require('script-loader!ui-cropper');
require('ng-focus-if');

require('angular-loading-bar');
require('angular-moment');
require('angular-ui-tree');
require('angular-aria');
require('ngstorage');
require('angular-ui-notification');
require('angular-translate');
require('angular-translate-handler-log');
require('angular-translate-interpolation-messageformat');
require('angular-elastic');
require('bootstrap-ui-datetime-picker');
require('scrollmonitor');
require('ng-file-upload');
require('ui-select');
require('angular-chart.js');
