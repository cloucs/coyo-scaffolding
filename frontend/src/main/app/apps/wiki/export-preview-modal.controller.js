(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.wiki')
      .controller('ExportPreviewModalController', ExportPreviewModalController);

  /**
   * Controller for viewing modal of article print preview.
   *
   * @requires $uibModalInstance
   * @requires $scope
   * @requires $window
   * @requires $timeout
   * @requires $document
   * @requires $http
   * @requires $translate
   * @requires coyo.widgets.api.widgetLayoutService
   * @requires exportPreviewContent
   * @requires wikiExportConfig
   */
  function ExportPreviewModalController($uibModalInstance, $scope, $window, $document, $timeout, $http,
                                        $translate, widgetLayoutService, exportPreviewContent, wikiExportConfig) {
    var vm = this;

    vm.$onInit = onInit;
    vm.fixedPreviewWidth = wikiExportConfig.pageWidth;
    vm.scale = wikiExportConfig.defaultScale;
    vm.layoutOptions = {print: true};
    vm.loading = true;

    vm.onImagesComplete = onImagesComplete;
    vm.print = print;
    vm.close = close;

    var elementWidth, articleImagesCompleteCounter, loadingWatcher;

    function print() {
      $window.print();
    }

    function close() {
      $uibModalInstance.dismiss();
    }

    function onImagesComplete() {
      articleImagesCompleteCounter++;
    }

    function _adaptScaling(elementWidth) {
      vm.scale = Math.min(elementWidth / vm.fixedPreviewWidth, wikiExportConfig.defaultScale);
    }

    function _managePacedLoading() {
      vm.showLoadingBar = true;
      vm.articlesLoadingBatchSize = Math.min(wikiExportConfig.previewRenderingBatchSize, vm.articles.length);
      vm.loadingInfo = $translate.instant('APP.WIKI.PRINT.LOADING',
          {current: vm.articlesLoadingBatchSize, total: vm.articles.length});
      vm.loadingPercentage = Math.round(vm.articlesLoadingBatchSize / vm.articles.length * 100) / 100;

      loadingWatcher = $scope.$watch(function () {
        return (vm.articlesLoadingBatchSize - articleImagesCompleteCounter) + $http.pendingRequests.length;
      }, function (pendingRequests) {
        if (pendingRequests <= 0) {
          if (vm.articlesLoadingBatchSize >= vm.articles.length) {
            _stopLoading();
            return;
          }
          vm.articlesLoadingBatchSize =
              Math.min(vm.articlesLoadingBatchSize + wikiExportConfig.previewRenderingBatchSize, vm.articles.length);
          vm.loadingInfo = $translate.instant('APP.WIKI.PRINT.LOADING',
              {current: vm.articlesLoadingBatchSize, total: vm.articles.length});
          vm.loadingPercentage = Math.round(vm.articlesLoadingBatchSize / vm.articles.length * 100) / 100;
        }
      });
    }

    function _stopLoading() {
      vm.loading = false;
      vm.showLoadingBar = false;
      _determinePreviewHeight();
      loadingWatcher();
    }

    function _determinePreviewHeight() {
      $timeout(function () {
        var previewElement = angular.element($document[0].querySelector('.print-preview'));
        vm.previewHeight = previewElement[0].scrollHeight;
      });
    }

    function _initArticles() {
      if (vm.articles.length === 1) {
        vm.selectedLanguage = exportPreviewContent.selectedLanguage;
        vm.showLoadingBar = false;
        widgetLayoutService.onload($scope).finally(function () {
          vm.loading = false;
        });
      } else {
        _managePacedLoading();
      }
    }

    function onInit() {
      vm.appId = exportPreviewContent.appId;
      vm.modalTitle = exportPreviewContent.modalTitle;
      vm.articleTitle = exportPreviewContent.articleTitle;
      vm.showLoadingBar = exportPreviewContent.showLoadingBar;
      vm.loadingInfo = $translate.instant('APP.WIKI.PRINT.PREPARING');
      vm.loadingPercentage = 0.01;
      articleImagesCompleteCounter = 0;

      if (angular.isArray(exportPreviewContent.articles)) {
        vm.articles = exportPreviewContent.articles;
        _initArticles();
      } else if (exportPreviewContent.articles.then) {
        exportPreviewContent.articles.then(function (articles) {
          vm.articles = articles;
          _initArticles();
        });
      }

      $uibModalInstance.rendered.then(function () {
        var modalElement = angular.element($document[0].querySelector('#wiki-article-export-modal'));
        elementWidth = modalElement.width();
        _adaptScaling(elementWidth);
        var resizeHandler = function () {
          if (modalElement.width() !== elementWidth) {
            elementWidth = modalElement.width();
            $scope.$apply(_adaptScaling(elementWidth));
          }
        };

        angular.element($window).on('resize.exportPreview', _.throttle(function () {
          $timeout(resizeHandler);
        }, 50));

        angular.element($window).on('orientationchange.exportPreview', function () {
          $timeout(resizeHandler);
        });
      });

      $uibModalInstance.closed.then(function () {
        angular.element($window).off('resize.exportPreview');
        angular.element($window).off('orientationchange.exportPreview');
      });
    }
  }
})(angular);
