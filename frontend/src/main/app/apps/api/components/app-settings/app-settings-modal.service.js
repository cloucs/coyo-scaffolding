(function (angular) {
  'use strict';

  angular.module('coyo.apps.api')
      .factory('appSettingsModalService', appSettingsModalService);

  /**
   * @ngdoc service
   * @name coyo.apps.api.appSettingsModalService
   *
   * @description
   * This service provides a method to open a modal dialog which uses
   * {@link coyo.apps.api.oyocAppSettings:oyocAppSettings appSettings} and
   * {@link commons.ui.oyocSettingsView:oyocSettingsView settingsView} to display the settings
   * of a passed app. Within this settings a user can edit the default settings (like 'name' and 'active'), all custom
   * settings and can delete the app.
   *
   * @requires appService
   * @requires modalService
   */
  function appSettingsModalService(modalService) {

    return {
      open: open
    };

    /**
     * @ngdoc method
     * @name coyo.apps.api.appSettingsModalService#open
     * @methodOf coyo.apps.api.appSettingsModalService
     *
     * @description
     * This method opens the modal which shows the settings of the passed app. Note that this service reloads the
     * active app, when the app itself changed.
     *
     * @param {object} app
     * The app to show the settings for. Note that the app gets reloaded as origin, when opening the modal in order to
     * get the untranslated app for edit view.
     *
     * @returns {object} A promise with the updated (or deleted) app. If the app got deleted a property "deleted" with
     * the value "true" is added to the returned object.
     */
    function open(app, sender) {

      return modalService.open({
        size: 'lg',
        templateUrl: 'app/apps/api/components/app-settings/app-settings-modal.html',
        controller: 'AppSettingsModalController',
        resolve: {
          sender: function () {
            return sender;
          },
          app: function () {
            return sender.getApp(app.id, {origin: true});
          }
        }
      }).result;
    }
  }

})(angular);
