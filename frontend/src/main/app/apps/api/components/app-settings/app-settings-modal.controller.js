(function (angular) {
  'use strict';

  angular.module('coyo.apps.api')
      .controller('AppSettingsModalController', AppSettingsModalController);

  function AppSettingsModalController(app, sender, $q, $log, $uibModalInstance, $rootScope, SenderModel, modalService) {
    var vm = this;
    vm.$onInit = onInit;

    vm.app = app;
    vm.sender = sender;

    vm.save = save;
    vm.confirmRemoveApp = confirmRemoveApp;
    vm.isTranslationRequired = isTranslationRequired;
    vm.updateValidity = updateValidity;
    vm.saveCallbacks = {
      onBeforeSave: function () {
        return $q.resolve();
      }
    };

    function save() {
      $log.debug('[AppSettingsModalController] Saving app', vm.app);
      vm.app.prepareTranslationsForSave(vm);
      vm.saveCallbacks.onBeforeSave().then(function () {
        return vm.app.save().then(function (app) {
          $rootScope.$emit('app:updated', app);
          $uibModalInstance.close(app);
        });
      });
    }

    function confirmRemoveApp(event) {
      event.preventDefault();
      var app = vm.app;

      modalService.open({
        size: 'md',
        templateUrl: 'app/apps/api/components/app-settings/app-delete-confirmation-modal.html',
        controller: 'AppDeleteConfirmationModalController',
        controllerAs: '$ctrl',
        resolve: {
          app: function () {
            return angular.copy(app);
          }
        }
      }).result.then(function (deleteAppFiles) {
        _remove(deleteAppFiles);
      });
    }

    function _remove(deleteAppFiles) {
      var senderModel = new SenderModel({id: vm.app.senderId});
      return senderModel.removeApp(vm.app.id, {deleteAppFiles: deleteAppFiles}).then(function () {
        vm.app.deleted = true;
        $rootScope.$emit('app:deleted', vm.app.id);
        $uibModalInstance.close(vm.app);
      });
    }

    function isTranslationRequired(language) {
      return vm.sender.isTranslationRequired(vm.languages, vm.currentLanguage, language);
    }

    function updateValidity(key, valid) {
      vm.languages[key].valid = valid;
    }

    function onInit() {
      vm.sender.initTranslations(vm);
      vm.currentLanguage = vm.defaultLanguage;
      // add default translation
      vm.languages[vm.defaultLanguage].translations.name = vm.app.name;
      // add additional translations
      _.forEach(_.pickBy(vm.app.translations, function (translations, language) {
        return _.has(vm.languages, language);
      }), function (translations, language) {
        _.merge(vm.languages[language].translations, translations);
      });
    }
  }

})(angular);
