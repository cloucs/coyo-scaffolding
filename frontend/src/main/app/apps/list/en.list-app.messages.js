(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.list')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "APP.LIST.SEARCH": "Search",
          "APP.LIST.ADD_ENTRY": "Create entry",
          "APP.LIST.ADD_ENTRY_BUTTON": "Create a new entry",
          "APP.LIST.ADD_ENTRY.MODAL.TITLE": "Create a new entry",
          "APP.LIST.ADD_ENTRY.SUCCESS": "Successfully created new entry",
          "APP.LIST.ADD_ENTRY.ERROR": "An error occurred creating a new item",
          "APP.LIST.CONTEXTMENU.OPEN": "Open detail view",
          "APP.LIST.DESCRIPTION": "Create and manage a dynamic list with custom fields and columns.",
          "APP.LIST.DETAIL.MODAL.TITLE.VIEW": "Entry details",
          "APP.LIST.DETAIL.MODAL.TITLE.EDIT": "Edit entry",
          "APP.LIST.DETAIL.MODAL.ENTRY": "Entry",
          "APP.LIST.DETAIL.MODAL.COMMENTS": "Comments",
          "APP.LIST.DETAIL.MODAL.HISTORY": "History",
          "APP.LIST.DETAIL.MODAL.HISTORY.AUTHOR": "Created by",
          "APP.LIST.DETAIL.MODAL.HISTORY.EDITOR": "Edited by",
          "APP.LIST.DETAIL.MODAL.EDIT": "Edit",
          "APP.LIST.ELEMENT": "Element name",
          "APP.LIST.ELEMENT.HELP": "Name of the element that is displayed in the list, e.g. 'Telephone Numbers', 'Customers' etc.",
          "APP.LIST.FIELDS.CONFIGURE.NO_FIELDS": "There are no fields configured for this app, yet. You will need to define fields to specify the content of your list. Start creating the first fields, now!",
          "APP.LIST.NAME": "List",
          "APP.LIST.MODAL.DELETE.TITLE": "Delete entry",
          "APP.LIST.MODAL.DELETE.TEXT": "Do you really want to delete this entry? The deletion can't be undone.",
          "APP.LIST.NO_ENTRIES": "This list doesn't have any entries yet.",
          "APP.LIST.NO_READ_PERMISSIONS": "You don't have the permission to see items of this list",
          "APP.LIST.NO_SEARCH_RESULTS": "No entries found",
          "APP.LIST.NOTIFICATIONS.LABEL": "New entry notification",
          "APP.LIST.NOTIFICATIONS.ALL": "All",
          "APP.LIST.NOTIFICATIONS.ADMIN": "Administrators",
          "APP.LIST.NOTIFICATIONS.NONE": "No one",
          "APP.LIST.PERMISSIONS": "Permissions",
          "APP.LIST.PERMISSIONS.ALL": "Yes, all",
          "APP.LIST.PERMISSIONS.OWN": "Yes, only their own",
          "APP.LIST.PERMISSIONS.NONE": "No, none",
          "APP.LIST.PERMISSIONS.HEADLINE": "Normal users are allowed to… ",
          "APP.LIST.PERMISSION_CREATE": "… create entries",
          "APP.LIST.PERMISSION_READ": "… view entries",
          "APP.LIST.PERMISSION_EDIT": "… edit entries",
          "APP.LIST.PERMISSION_DELETE": "… delete entries",
          "ENTITY_TYPE.LIST_ENTRY": "List entry",
          "ENTITY_TYPE.LIST_ENTRIES": "List entries",
          "NOTIFICATIONS.LIST.ENTRY.NEW": "*{s1}* created a new entry in *{s2}* > *{s3}*",
          "NOTIFICATIONS.LIST.ENTRY.UPDATED": "*{s1}* modified a list entry in *{s2}* > *{s3}*"
        });
        /* eslint-enable quotes */
      });
})(angular);
