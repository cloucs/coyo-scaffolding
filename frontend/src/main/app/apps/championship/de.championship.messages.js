(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.championship')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "APP.CHAMPIONSHIP.DATE": "Datum",
          "APP.CHAMPIONSHIP.DESCRIPTION": "Wer wird die meisten Ergebnisse richtig tippen? Messe dich mit deinen Kollegen!",
          "APP.CHAMPIONSHIP.FINAL_SCORE": "Endergebnis",
          "APP.CHAMPIONSHIP.GAME_SAVED": "Tipp gespeichert",
          "APP.CHAMPIONSHIP.GAME_NOT_SAVED": "Tipp nicht gespeichert",
          "APP.CHAMPIONSHIP.GAMES": "Tippabgabe",
          "APP.CHAMPIONSHIP.GAMES_PAST": "Vergangene Spiele",
          "APP.CHAMPIONSHIP.MATCHUP": "Begegnung",
          "APP.CHAMPIONSHIP.NAME": "Tippspiel",
          "APP.CHAMPIONSHIP.NO_GAMES": "Zur Zeit sind hier keine Spiele verfügbar.",
          "APP.CHAMPIONSHIP.NO_RANKING": "Es wurden noch keine Punkte vergeben.",
          "APP.CHAMPIONSHIP.RANKING": "Rangliste",
          "APP.CHAMPIONSHIP.RANKING.MATCHES": "Spielplan",
          "APP.CHAMPIONSHIP.RANKING.NAME": "Name",
          "APP.CHAMPIONSHIP.RANKING.POINTS": "Punkte",
          "APP.CHAMPIONSHIP.RANKING.POINTS_MOBILE": "P",
          "APP.CHAMPIONSHIP.RANKING.RANK": "Rang",
          "APP.CHAMPIONSHIP.RANKING.RANK_MOBILE": "#",
          "APP.CHAMPIONSHIP.SELECT_TEAM": "Team auswählen"
        });
        /* eslint-enable quotes */
      });

})(angular);
