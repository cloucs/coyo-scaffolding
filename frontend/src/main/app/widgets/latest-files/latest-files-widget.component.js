(function () {
  'use strict';

  angular
      .module('coyo.widgets.latestfiles')
      .component('coyoLatestFilesWidget', coyoLatestFilesWidget())
      .controller('LatestFilesWidgetController', LatestFilesWidgetController);

  function coyoLatestFilesWidget() {
    return {
      templateUrl: 'app/widgets/latest-files/latest-files-widget.html',
      bindings: {
        widget: '=',
        config: '<'
      },
      controller: 'LatestFilesWidgetController'
    };
  }

  function LatestFilesWidgetController(latestFilesWidgetService, widgetStatusService, $scope, fileAuthorService,
                                       fileDetailsModalService) {
    var vm = this;
    vm.loadFiles = loadFiles;
    vm.openDetails = openDetails;

    widgetStatusService.refreshOnSettingsChange($scope);

    function loadFiles() {
      var files = [];
      return latestFilesWidgetService.getLatestFiles(vm.widget.settings._app.id, vm.widget.settings._fileCount)
          .then(function (result) {
            vm.app = result.app;
            files = _.map(result.documents, function (doc) {
              doc.isNew = doc.created === doc.modified;
              return doc;
            });
            return fileAuthorService.loadFileAuthors(vm.widget.settings._app.senderId,
                vm.widget.settings._app.id, _.map(files, 'id'), result.app.settings.showAuthors);
          }).then(function (result) {
            _.forEach(files, function (file) {
              file.author = _.get(result, file.id);
            });
            return files;
          }).finally(function () {
            vm.files = files;
            vm.lastUpdate = new Date().getTime();
          });
    }

    function openDetails(file) {
      fileDetailsModalService.open(vm.files, _.findIndex(vm.files, {id: file.id}));
    }
  }
})();
