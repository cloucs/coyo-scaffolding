(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.latestfiles')
      .controller('LatestFilesWidgetSettingsController', LatestFilesWidgetSettingsController);

  function LatestFilesWidgetSettingsController($scope) {
    var vm = this;
    vm.model = $scope.model;

    vm.$onInit = _init;

    function _init() {
      vm.model.settings._fileCount = vm.model.settings._fileCount || 5;
    }

  }

})(angular);
