(function (angular) {
  'use strict';

  angular.module('coyo.widgets.latestfiles')
      .factory('latestFilesWidgetService', latestFilesWidgetService);

  /**
   * @ngdoc service
   * @name coyo.widgets.latestfiles.latestFilesWidgetService
   *
   * @description
   * Service for retrieving latest files
   *
   */
  function latestFilesWidgetService($http) {
    return {
      getLatestFiles: getLatestFiles
    };

    /**
     * @ngdoc method
     * @name coyo.widgets.latestfiles.latestFilesWidgetService#getLatestFiles
     * @methodOf coyo.widgets.latestfiles.latestFilesWidgetService
     *
     * @description
     * Retrieves the n latest modified files (n is provided by the count parameter) of the app with the given id.
     *
     * @param {string} appId
     * The app id
     * @param {int} count
     * The maximum number of files.
     *
     * @returns {promise} the http promise
     */
    function getLatestFiles(appId, count) {
      return $http.get('/web/widgets/latest-files', {params: {
        appId: appId,
        count: count
      }}).then(function (response) {
        return response.data;
      });
    }
  }

})(angular);
