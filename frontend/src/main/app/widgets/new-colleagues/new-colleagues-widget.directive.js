(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.newcolleagues')
      .component('coyoNewColleaguesWidget', newColleaguesWidget())
      .controller('NewColleaguesWidgetController', NewColleaguesWidgetController);

  /**
   * @ngdoc directive
   * @name coyo.widgets.blog.coyoNewColleaguesWidget:coyoNewColleaguesWidget
   * @element ANY
   * @restrict E
   * @scope
   *
   * @description
   * Renders the widget to show the new colleagues.
   *
   * @param {object} widget
   * The widget configuration
   */
  function newColleaguesWidget() {
    return {
      templateUrl: 'app/widgets/new-colleagues/new-colleagues-widget.html',
      bindings: {
        widget: '=',
        showWidget: '='
      },
      controller: 'NewColleaguesWidgetController'
    };
  }

  function NewColleaguesWidgetController(newColleaguesWidgetService, $rootScope, $scope) {
    var vm = this;

    vm.loadData = loadData;
    vm.$onInit = _init;

    function loadData() {
      return newColleaguesWidgetService.getNewColleagues().then(function (result) {
        vm.data = result;
        vm.showWidget.show = result.totalElements > 0;
        return result;
      });
    }

    function _init() {
      var unsubscribe = $rootScope.$on('currentUser.follow:update', function (event, subscription) {
        vm.data.number -= 1;
        vm.data.nextAppended().then(function (result) {
          result.content = _.uniqBy(result.content, 'id');
          _.remove(result.content, {id: subscription.userId});
        });
      });
      $scope.$on('$destroy', unsubscribe);
    }
  }

})(angular);
