(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.wikiarticle')
      .directive('coyoWikiArticleWidget', wikiArticleWidget)
      .controller('WikiArticleWidgetController', WikiArticleWidgetController);

  /**
   * @ngdoc directive
   * @name coyo.widgets.wikiarticle:coyoWikiArticleWidget
   * @element ANY
   * @restrict E
   * @scope
   *
   * @description
   * Renders the widget to show a single wiki article
   *
   * @param {object} widget
   * The widget configuration
   */
  function wikiArticleWidget() {
    return {
      restrict: 'E',
      templateUrl: 'app/widgets/wiki-article/wiki-article-widget.html',
      scope: {},
      bindToController: {
        widget: '<',
        config: '<'
      },
      controller: 'WikiArticleWidgetController',
      controllerAs: '$ctrl'
    };
  }

  function WikiArticleWidgetController($scope, WikiArticleWidgetModel, targetService, widgetStatusService) {
    var vm = this;
    vm.links = {};
    vm.loadArticle = loadArticle;

    widgetStatusService.refreshOnSettingsChange($scope);

    /* Method to load a single article */
    function loadArticle() {
      delete vm.widget.article;
      return WikiArticleWidgetModel.getArticle(vm.widget.settings._articleId).then(function (article) {
        vm.widget.article = article;
        vm.links.article = targetService.getLink(article.articleTarget);
        vm.links.sender = targetService.getLink(article.senderTarget);
        return article;
      }).catch(function () {
        // do a return so the widget status directive can do it's duty (display empty message)
        return;
      }).finally(function () {
        vm.lastUpdate = new Date().getTime();
      });
    }

    $scope.$watch(function () {
      return vm.widget.settings._articleId;
    }, vm.loadArticle);
  }

})(angular);
