(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.singlefile')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "WIDGET.SINGLEFILE.DESCRIPTION": "Displays a single file.",
          "WIDGET.SINGLEFILE.NAME": "Single file",
          "WIDGET.SINGLEFILE.NO_FILE": "File is not available.",
          "WIDGET.SINGLEFILE.OPEN_IN_FILELIBRARY": "Open in filelibrary",
          "WIDGET.SINGLEFILE.UNABLE_TO_LOAD": "Unable to load file",
          "WIDGET.SINGLEFILE.SETTINGS.FILE.BUTTON": "Select a file",
          "WIDGET.SINGLEFILE.SETTINGS.FILE.LABEL": "File",
          "WIDGET.SINGLEFILE.SETTINGS.FILE.HELP": "Select a file to be displayed in the widget.",
          "WIDGET.SINGLEFILE.SETTINGS.HIDE_DATE.LABEL": "Hide date",
          "WIDGET.SINGLEFILE.SETTINGS.HIDE_DATE.HELP": "Hide the Upload Date in the file description.",
          "WIDGET.SINGLEFILE.SETTINGS.HIDE_SENDER.LABEL": "Hide origin",
          "WIDGET.SINGLEFILE.SETTINGS.HIDE_SENDER.HELP": "Hide the origin in the file description.",
          "WIDGET.SINGLEFILE.SETTINGS.HIDE_PREVIEW.LABEL": "Hide preview image",
          "WIDGET.SINGLEFILE.SETTINGS.HIDE_PREVIEW.HELP": "Hide the preview image and show a file icon.",
          "WIDGET.SINGLEFILE.SETTINGS.HIDE_DOWNLOAD_LINK.LABEL": "Hide download link",
          "WIDGET.SINGLEFILE.SETTINGS.HIDE_DOWNLOAD_LINK.HELP": "Hide the download Link in the file description.",
          "WIDGET.SINGLEFILE.MODAL.FILE.SELECT": "Select a file"
        });
        /* eslint-enable quotes */
      });
})(angular);
