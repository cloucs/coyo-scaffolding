(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.interestingcolleagues', [
        'coyo.widgets.api',
        'commons.resource',
        'commons.target',
        'commons.i18n'
      ])
      .config(registerWidget);

  function registerWidget(widgetRegistryProvider) {
    widgetRegistryProvider.register({
      name: 'WIDGETS.INTERESTINGCOLLEAGUES.NAME',
      key: 'interestingcolleagues',
      icon: 'zmdi-share',
      categories: 'dynamic',
      directive: 'coyo-interesting-colleagues-widget',
      description: 'WIDGETS.INTERESTINGCOLLEAGUES.DESCRIPTION',
      titles: ['WIDGETS.INTERESTINGCOLLEAGUES.NAME'],
      whitelistExternal: false
    });
  }

})(angular);
