(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.code', [
        'coyo.widgets.api',
        'commons.i18n'
      ])
      .config(registerCodeWidget);

  function registerCodeWidget(widgetRegistryProvider) {
    widgetRegistryProvider.register({
      key: 'code',
      name: 'WIDGET.CODE.NAME',
      description: 'WIDGET.CODE.DESCRIPTION',
      icon: 'zmdi-code',
      categories: 'static',
      directive: 'coyo-code-widget',
      settings: {
        templateUrl: 'app/widgets/code/code-settings.html',
        controller: 'CodeWidgetSettingsController',
        controllerAs: '$ctrl',
      },
      renderOptions: {
        panels: {
          noPanel: true
        },
        panel: {
          noPanel: true
        }
      },
      alwaysRestricted: true,
      whitelistExternal: true
    });
  }

})(angular);
