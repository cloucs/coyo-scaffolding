(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.blog')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "WIDGETS.BLOG.DESCRIPTION": "Displays a list of latest blog articles, either globally or for a specific blog.",
          "WIDGETS.BLOG.NAME": "Latest blog articles",
          "WIDGETS.BLOG.NO_ARTICLES": "No article found",
          "WIDGETS.BLOG.NO_BLOG_APPS_FOUND": "No blog apps found.",
          "WIDGETS.BLOG.SETTINGS.ARTICLE_COUNT.HELP": "The maximum number of articles to display in the widget.",
          "WIDGETS.BLOG.SETTINGS.ARTICLE_COUNT.LABEL": "Number of articles",
          "WIDGETS.BLOG.SETTINGS.BLOG_APP.AT": "at",
          "WIDGETS.BLOG.SETTINGS.BLOG_APP.HELP": "Select the blog apps for which to display the latest articles.",
          "WIDGETS.BLOG.SETTINGS.BLOG_APP.IN": "in",
          "WIDGETS.BLOG.SETTINGS.BLOG_APP.LABEL": "Blog apps",
          "WIDGETS.BLOG.SETTINGS.BLOG_USE_ALL_BLOGS.LABEL": "All",
          "WIDGETS.BLOG.SETTINGS.BLOG_USE_SELECTED_BLOGS.LABEL": "Selected",
          "WIDGETS.BLOG.SETTINGS.BLOG_USE_SUBSCRIBED_BLOGS.LABEL": "Only subscribed",
          "WIDGETS.BLOG.SETTINGS.BLOG_SHOW_TEASER_IMAGE.HELP": "Shows the blogs teaser images within the widget.",
          "WIDGETS.BLOG.SETTINGS.BLOG_SHOW_TEASER_IMAGE.LABEL": "Show teaser images",
          "WIDGETS.BLOG.SETTINGS.BLOG_SOURCES.HELP": "The blogs from the selected option will be shown within the widget.",
          "WIDGETS.BLOG.SETTINGS.BLOG_SOURCES.LABEL": "Blogs"
        });
        /* eslint-enable quotes */
      });
})(angular);
