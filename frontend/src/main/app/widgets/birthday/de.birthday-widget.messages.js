(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.birthday')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "WIDGET.BIRTHDAY.NAME": "Anstehende Geburtstage",
          "WIDGET.BIRTHDAY.TODAY": "Heute",
          "WIDGET.BIRTHDAY.AGE": "Wird {years} Jahre alt.",
          "WIDGET.BIRTHDAY.DESCRIPTION": "Zeigt anstehende Geburtstage",
          "WIDGET.BIRTHDAY.NO_BIRTHDAYS": "Es gibt keine anstehenden Geburtstage",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY.AGE.LABEL": "Das Alter anzeigen",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY.DISPLAY.LABEL": "Anzeige",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_NUMBER.LABEL": "Anzahl der angezeigten Geburtstage",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_NUMBER.HELP": "Gibt an wie viele Geburtstage anfänglich in dem Widget angezeigt werden.",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_ALL.USERS": "Alle Kollegen",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_SUBSCRIBED.USERS": "Kollegen denen ich folge",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_ALL.USERS.HELP": "Wähle aus ob die Geburtstage aller Kollegen oder nur der Kollegen, denen du folgst, angezeigt werden sollen.",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_ADVANCED_DAYS.LABEL": "Anzahl Tage im Voraus",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_ADVANCED_DAYS.HELP": "Anzahl an Tagen bis zum Geburtstag, die berücksichtigt werden sollen."
        });
        /* eslint-enable quotes */
      });
})(angular);
