(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.birthday')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "WIDGET.BIRTHDAY.NAME": "Upcoming birthdays",
          "WIDGET.BIRTHDAY.TODAY": "Today",
          "WIDGET.BIRTHDAY.AGE": "Turns {years} years old",
          "WIDGET.BIRTHDAY.DESCRIPTION": "Displays upcoming birthdays",
          "WIDGET.BIRTHDAY.NO_BIRTHDAYS": "No upcoming birthdays",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY.AGE.LABEL": "Display a colleagues age",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY.DISPLAY.LABEL": "Display",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_NUMBER.LABEL": "Number of displayed birthdays",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_NUMBER.HELP": "Defines the number of birthdays that should be initially shown in the widget.",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_ALL.USERS": "All colleagues",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_SUBSCRIBED.USERS": "Followers only",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_ALL.USERS.HELP": "Choose whether birthdays of all colleagues or only colleagues you follow should be displayed.",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_ADVANCED_DAYS.LABEL": "Days in advance",
          "WIDGETS.BIRTHDAY.SETTINGS.BIRTHDAY_ADVANCED_DAYS.HELP": "Amount of days to be considered until the birthday."
        });
        /* eslint-enable quotes */
      });
})(angular);
