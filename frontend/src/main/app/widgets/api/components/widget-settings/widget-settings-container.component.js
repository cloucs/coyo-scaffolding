(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.api')
      .component('oyocWidgetSettingsContainer', widgetSettingsContainer())
      .controller('WidgetSettingsContainerController', widgetSettingsContainerController);

  function widgetSettingsContainer() {
    return {
      templateUrl: 'app/widgets/api/components/widget-settings/widget-settings-container.html',
      bindings: {
        legacyFormSet: '&',
        saveCallbacks: '<',
        widget: '=',
        config: '<'
      },
      controller: 'WidgetSettingsContainerController'
    };
  }

  function widgetSettingsContainerController($timeout, $scope) {
    var vm = this;
    vm.$onInit = function () {
      $scope.$watch($scope.ng1WidgetForm, function () {
        vm.legacyFormSet($scope.ng1WidgetForm);
      });
    };
  }
})(angular);
