(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.upcomingEvents')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "WIDGETS.UPCOMING.EVENTS.NAME": "Anstehende Events",
          "WIDGETS.UPCOMING.EVENTS.DESCRIPTION": "Zeigt anstehende Events an",
          "WIDGETS.UPCOMING.EVENTS.NO.UPCOMING.EVENTS": "Es gibt keine anstehenden Events",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.DISPLAY.ONGOING": "Zeige laufende Events an",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.EVENT_NUMBER.LABEL": "Anzahl der angezeigten Events",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.EVENTS.DISPLAY.LABEL": "Zeige Events",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.EVENTS_ALL": "Alle",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.HELP.EVENTS_ALL": "Alle für den Benutzer sichtbaren Events.",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.EVENTS_SELECTED": "Ausgewählte",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.HELP.EVENTS_SELECTED": "Alle Events einer ausgewählten Seite oder eines Workspaces.",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.EVENTS_SUBSCRIBED": "Alle abonnierten",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.HELP.EVENTS_SUBSCRIBED": "Alle Events, die der Benutzer abonniert hat."
        });
        /* eslint-enable quotes */
      });
})(angular);
