(function (angular) {
  'use strict';

  angular
      .module('coyo.admin.license')
      .controller('AdminSubscriptionController', AdminSubscriptionController);

  function AdminSubscriptionController($state, coyoUrls, LicenseModel) {
    var vm = this;
    vm.$onInit = _init;

    function _init() {
      LicenseModel.load().then(function (license) {
        if (license.environment === 'enterprise') {
          $state.go('admin.license.license');
        }

        vm.customerCenterLink = coyoUrls.customerCenter + '/order/' + license.orderId;

        vm.status = license.status || false;
        vm.validDays = license.validDays;
        vm.user = {
          current: license.currentUserSize,
          max: license.maxUserSize
        };
        vm.storage = {
          current: license.currentStorageSize,
          max: license.maxStorageSize
        };
      });
    }
  }
})(angular);
