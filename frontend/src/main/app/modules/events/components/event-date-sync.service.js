(function (angular) {
  'use strict';

  angular
      .module('coyo.events')
      .factory('eventDateSyncService', eventDateSyncService);

  /**
   * @ngdoc service
   * @name coyo.events.eventDateSyncService
   *
   * @description
   * This service update and sync date and time properties from a event date/time picker.
   * The start date and times can not be after the end date and time and vice versa.
   *
   * The dates object should have the format:
   * {
   *   startDate: Date,
   *   startTime: Date,
   *   endDate: Date,
   *   endTime: Date
   * }
   *
   * @requires $log
   * @requires moment
   */
  function eventDateSyncService($log, moment) {

    var timeOffset = 1; // default: 1 hour

    return {
      updateStartDate: updateStartDate,
      updateStartTime: updateStartTime,
      updateEndDate: updateEndDate,
      updateEndTime: updateEndTime
    };

    /**
     * @ngdoc function
     * @name coyo.events.eventDateSyncService#updateStartDate
     * @methodOf coyo.events.eventDateSyncService
     *
     * @description
     * Sync the start time and updates the end date if the end date is before the start date.
     *
     * @param {object} dates All start and end dates
     * @param {boolean} updateEnd Indicates if the end is bound to the start date or not.
     */
    function updateStartDate(dates, updateEnd) {
      _syncStartTime(dates);
      _syncStartDate(dates); // set time for startDate
      _updateEndDateIfBeforeStartDate(dates, updateEnd);
    }

    /**
     * @ngdoc function
     * @name coyo.events.eventDateSyncService#updateStartTime
     * @methodOf coyo.events.eventDateSyncService
     *
     * @description
     * Sync the start date and updates the end time if the end time is same or before the start time.
     *
     * @param {object} dates All start and end dates
     * @param {boolean} updateEnd Indicates if the end is bound to the start date or not.
     */
    function updateStartTime(dates, updateEnd) {
      _syncStartDate(dates);
      _updateEndTimeIfSameOrBeforeStartTime(dates, updateEnd);
    }

    /**
     * @ngdoc function
     * @name coyo.events.eventDateSyncService#updateEndDate
     * @methodOf coyo.events.eventDateSyncService
     *
     * @description
     * Sync the end time and updates the start date if the start date is after the end date.
     *
     * @param {object} dates {object} All start and end dates
     */
    function updateEndDate(dates) {
      _syncEndTime(dates);
      _syncEndDate(dates); // set time for endDate
      _updateStartDateIfAfterEndDate(dates);
    }

    /**
     * @ngdoc function
     * @name coyo.events.eventDateSyncService#updateEndTime
     * @methodOf coyo.events.eventDateSyncService
     *
     * @description
     * Sync the end date and updates the start time if the start time is same or after the end date.
     *
     * @param {object} dates All start and end dates
     */
    function updateEndTime(dates) {
      _syncEndTime(dates);
      _syncEndDate(dates);
      _updateStartTimeIfSameOrAfterEndTime(dates);
    }

    // -----------------------------------------------------------------------------------------------------------------

    function _syncStartDate(dates) {
      if (!_validDates(dates.startDate, dates.startTime)) {
        return;
      }
      var startDate = moment(dates.startDate).clone();
      var startTime = moment(dates.startTime).clone();
      if (!startDate.isSame(startTime)) {
        startDate.hours(startTime.hours()).minutes(startTime.minutes());
        dates.startDate = startDate.toDate();
      }
    }

    function _syncEndDate(dates) {
      if (!_validDates(dates.endDate, dates.endTime)) {
        return;
      }
      var endDate = moment(dates.endDate).clone();
      var endTime = moment(dates.endTime).clone();
      if (!endDate.isSame(endTime)) {
        endDate.hours(endTime.hours()).minutes(endTime.minutes());
        dates.endDate = endDate.toDate();
      }
    }

    function _syncStartTime(dates) {
      if (!_validDates(dates.startDate, dates.startTime)) {
        return;
      }
      var startDate = moment(dates.startDate);
      var startTime = moment(dates.startTime);
      if (!startDate.isSame(startTime)) {
        var newStartTime = startDate.clone();
        newStartTime.hours(startTime.hours()).minutes(startTime.minutes());
        dates.startTime = newStartTime.toDate();
      }
    }

    function _syncEndTime(dates) {
      if (!_validDates(dates.endDate, dates.endTime)) {
        return;
      }
      var endDate = moment(dates.endDate);
      var endTime = moment(dates.endTime);
      if (!endDate.isSame(endTime)) {
        var newEndTime = endDate.clone();
        newEndTime.hours(endTime.hours()).minutes(endTime.minutes());
        dates.endTime = newEndTime.toDate();
      }
    }

    function _updateStartDateIfAfterEndDate(dates) {
      if (!_validDates(dates.startDate, dates.endDate, dates.startTime)) {
        return;
      }
      var startDate = moment(dates.startDate);
      var endDate = moment(dates.endDate);
      if (startDate.startOf('day').isAfter(endDate.startOf('day'))) {
        var startTime = moment(dates.startTime);
        var newStartDate = endDate.clone();
        newStartDate.hours(startTime.hours(), startTime.minutes());
        dates.startDate = newStartDate.toDate();
        dates.startTime = newStartDate.toDate();
      }
      _updateStartTimeIfSameOrAfterEndTime(dates);
    }

    function _updateEndDateIfBeforeStartDate(dates, updateEnd) {
      if (!_validDates(dates.startDate, dates.endDate, dates.endTime)) {
        return;
      }
      var startDate = moment(dates.startDate);
      var endDate = moment(dates.endDate);
      if (updateEnd || endDate.startOf('day').isBefore(startDate.startOf('day'))) {
        var endTime = moment(dates.endTime);
        var newEndDate = startDate.clone();
        newEndDate.hours(endTime.hours(), endTime.minutes());
        dates.endDate = newEndDate.toDate();
        dates.endTime = newEndDate.toDate();
      }
      _updateEndTimeIfSameOrBeforeStartTime(dates, updateEnd);
    }

    function _updateStartTimeIfSameOrAfterEndTime(dates) {
      if (!_validDates(dates.startTime, dates.endTime)) {
        return;
      }
      var start = moment(dates.startTime);
      var end = moment(dates.endTime);
      if (start.isSameOrAfter(end)) {
        var newStartTime = end.clone().subtract(timeOffset, 'hour');
        dates.startDate = newStartTime.toDate();
        dates.startTime = newStartTime.toDate();
      }
    }

    function _updateEndTimeIfSameOrBeforeStartTime(dates, updateEnd) {
      if (!_validDates(dates.startTime, dates.endTime)) {
        return;
      }
      var start = moment(dates.startTime);
      var end = moment(dates.endTime);
      if (updateEnd || end.isSameOrBefore(start)) {
        var newEndTime = start.clone().add(timeOffset, 'hour');
        dates.endDate = newEndTime.toDate();
        dates.endTime = newEndTime.toDate();
      }
    }

    function _validDates() {
      var valid = true;
      for (var i = 0; i < arguments.length; i++) {
        if (valid) {
          var date = arguments[i];
          valid = date && angular.isFunction(date.getTime) && !isNaN(date.getTime()) && moment(date).isValid();
          if (!valid) {
            $log.error('[eventDateSyncService] Invalid date!', arguments[i]);
            break;
          }
        }
      }
      return valid;
    }
  }

})(angular);
