(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .controller('FileDetailsModalController', FileDetailsModalController);

  /**
   * @ngdoc controller
   * @name commons.ui.ManageTranslationsModalController
   *
   * @description
   * The controller of the manage translation modal.
   *
   * @requires $document
   * @requires $q
   * @requires $rootScope
   * @requires $filter
   * @requires $scope
   * @requires $uibModalInstance
   * @requires $timeout
   * @requires FileModel
   * @requires SenderModel
   * @requires CapabilitiesModel
   * @requires backendUrlService
   * @requires fileService
   * @requires coyoEndpoints
   * @requires files
   * @requires currentIndex
   * @requires linkToFileLibrary
   * @requires showAuthors
   * @requires fileAuthors
   * @requires appIdOrSlug
   * @requires filesTotal
   * @requires loadNext
   * @requires currentUser
   * @requires externalFileHandlerService
   */
  function FileDetailsModalController($document, $q, $rootScope, $scope, $filter, $uibModalInstance, FileModel,
                                      SenderModel, CapabilitiesModel, $timeout, backendUrlService, fileService,
                                      coyoEndpoints, files, currentIndex, linkToFileLibrary, showAuthors,
                                      fileAuthors, appIdOrSlug, filesTotal, loadNext, currentUser,
                                      externalFileHandlerService) {
    var vm = this;
    vm.$onInit = onInit;
    vm.next = next;
    vm.previous = previous;
    vm.hasPrevious = hasPrevious;
    vm.hasNext = hasNext;
    vm.updateFile = updateFile;
    vm.allowPDFPreview = allowPDFPreview;
    vm.currentUser = currentUser;
    var fileLockSubscription;

    /**
     * Rerenders the current file's details with loading information again from the backend.
     * Remembers that this file was updated so that this information can be passed on when the modal closes.
     */
    function updateFile() {
      _setCurrentFile(vm.file, true).then(function (result) {
        vm.updated.push(result);
      });
    }

    function hasNext() {
      return vm.currentIndex < vm.filesTotal - 1;
    }

    /**
     * Display next image from files array if there is one.
     */
    function next() {
      if (hasNext()) {
        vm.currentIndex++;
        if (vm.currentIndex >= vm.files.length) {
          loadNext().then(function (result) {
            vm.files.push.apply(vm.files, result);
            _setCurrentFile(vm.files[vm.currentIndex]);
          });
        } else {
          _setCurrentFile(vm.files[vm.currentIndex]);
        }
      }
    }

    function hasPrevious() {
      return vm.currentIndex > 0;
    }

    /**
     * Display previous image from files array if there is one.
     */
    function previous() {
      if (hasPrevious()) {
        vm.currentIndex--;
        _setCurrentFile(vm.files[vm.currentIndex]);
      }
    }

    function allowPDFPreview() {
      vm.viewPdfFullScreen = true;
      vm.previewOptions = Object.assign({}, vm.previewOptions, {showPdfMobile: true});
      vm.showPDFLoadingHint = false;
    }

    function disallowPdfPreview() {
      vm.previewOptions = Object.assign({}, vm.previewOptions, {showPdfMobile: false});
    }

    function _setCurrentFile(file, forceReload) {
      delete vm.file;
      vm.externalFile = file && externalFileHandlerService.isExternalFile(file);
      disallowPdfPreview();

      if (!forceReload && _isAllFileDataAvailable(file) || vm.externalFile) {
        if (!vm.externalFile) {
          vm.file = file;
          vm.file.type = $filter('fileTypeName')(vm.file.contentType);
          vm.previewUrl = file.previewUrl || coyoEndpoints.sender.preview;
          _determinePdfHintDisplay(vm.file);
          vm.downloadUrl = _getDownloadUrl(file);
          _subscribeToFileLock();
          return $q.resolve(vm.file);
        } else {
          return _populateExternalFileInformation(file, forceReload || vm.externalFile);
        }
      } else {
        var getFileWithPermissionsPromise = FileModel.getWithPermissions({senderId: file.senderId, id: file.id}, {},
            ['*']);
        var getSenderWithPermissionsPromise = SenderModel.getWithPermissions(file.senderId, {},
            ['manage', 'createFile']);
        return $q.all([getFileWithPermissionsPromise, getSenderWithPermissionsPromise]).then(function (result) {
          vm.file = result[0];
          vm.file.sender = result[1];
          vm.file.type = $filter('fileTypeName')(vm.file.contentType);
          vm.previewUrl = vm.file.previewUrl || coyoEndpoints.sender.preview;
          vm.files[vm.currentIndex] = angular.extend(vm.files[vm.currentIndex], vm.file);
          _determinePdfHintDisplay(vm.file);
          _subscribeToFileLock();
          return vm.file;
        });
      }
    }

    function _getDownloadUrl(file) {
      return backendUrlService.getUrl()
          + vm.previewUrl.replace('{{groupId}}', file.groupId || file.senderId).replace('{{id}}', file.id);
    }

    function _isAllFileDataAvailable(file) {
      return file.attachment || (file._permissions && file.contentType && file.sender);
    }

    function _subscribeToFileLock() {
      _cleanFileLockSubscription();
      fileLockSubscription = fileService.subscribeToLock(vm.file, function (result) {
        _.merge(vm.file, result);
      });
    }

    function _cleanFileLockSubscription() {
      if (fileLockSubscription) {
        fileLockSubscription();
      }
    }

    function _bindKeyUp() {
      $document.on('keydown.filedetails', _onKeyUp);
    }

    function _unbindKeyUp() {
      $document.off('keydown.filedetails', _onKeyUp);
    }

    function _closeModal() {
      $uibModalInstance.close(vm.updated);
    }

    function _onKeyUp(event) {
      if (event.target.type && event.target.type.includes('text') && event.target.value && event.which !== 27) {
        return;
      }
      if (event.which === 37) {
        $scope.$apply(function () {
          event.preventDefault();
          previous();
        });
      } else if (event.which === 39) {
        $scope.$apply(function () {
          event.preventDefault();
          next();
        });
      } else if (event.which === 27) {
        $scope.$apply(function () {
          event.preventDefault();
          _closeModal();
        });
      }
    }

    function _populateExternalFileInformation(attachment, forceReload) {
      if (!attachment.externalFileUrl || forceReload) {
        attachment.externalFileUrlLoading = true;
        return externalFileHandlerService
            .getExternalFileDetails(attachment)
            .then(function (fileDetails) {
              attachment.externalFileUrl = fileDetails.externalUrl;
              attachment.length = fileDetails.fileSize;
              attachment.canEdit = fileDetails.canEdit;
              if (fileDetails.exportLinks) {
                attachment.exportLinks = fileDetails.exportLinks;
                attachment.previewUrl = fileDetails.previewUrl;
              } else if (attachment.contentType.startsWith('image')) {
                attachment.previewUrl = fileDetails.previewUrl;
              } else if (attachment.contentType === 'application/pdf') {
                attachment.previewUrl = fileDetails.previewUrl;
              }
              vm.previewUrl = attachment.previewUrl;
              vm.file = attachment;
              vm.file.type = $filter('fileTypeName')(attachment.contentType);
              _determinePdfHintDisplay(attachment);
              return attachment;
            })
            .catch(function (e) {
              if (e.status === 404) {
                attachment.length = undefined;
                vm.fileErrors[attachment.id] = 'FILE_DETAILS.EXTERNAL_FILE_NOT_AVAILABLE';
              }
              vm.file = attachment;
              return attachment;
            })
            .finally(function () {
              delete attachment.externalFileUrlLoading;
              $timeout(function () {
                $scope.$digest();
              });
            });
      } else {
        return $q.reject();
      }
    }

    function _determinePdfHintDisplay(file) {
      vm.showPDFLoadingHint = false;

      var isMobile = $rootScope.screenSize.isXs || $rootScope.screenSize.isSm;
      if (isMobile && vm.previewUrl) {
        var previewContentType = _setContentTypeToPdfIfGoogleNativeFile(file.contentType);
        CapabilitiesModel.pdfAvailable(previewContentType).then(function (available) {
          vm.showPDFLoadingHint = available;
        });
      }
    }

    /**
     * This method is necessary in order to handle Google native file types like docs, spreadsheets and presentation
     * like PDF's, so that they are rendered by our PDF viewer.
     *
     * @param {string} contentType
     * @return {string} contentType
     * @private
     */
    function _setContentTypeToPdfIfGoogleNativeFile(contentType) {
      return externalFileHandlerService.isGoogleMediaType(contentType) ? 'application/pdf' : contentType;
    }

    function onInit() {
      vm.updated = [];
      vm.viewPdfFullScreen = false;
      vm.linkToFileLibrary = linkToFileLibrary;
      vm.showAuthors = showAuthors;
      vm.fileAuthors = fileAuthors;
      vm.appIdOrSlug = appIdOrSlug;
      vm.previewOptions = {
        showPdfDesktop: true,
        allowGifPlayback: false,
        showPdfMobile: false
      };
      vm.fileErrors = {};

      if (angular.isArray(files)) {
        vm.files = files;
      } else {
        vm.files = [files];
      }

      vm.filesTotal = (filesTotal && (filesTotal > vm.files.length) && loadNext) ? filesTotal : vm.files.length;
      vm.currentIndex = currentIndex || 0;
      _setCurrentFile(vm.files[vm.currentIndex]);
      _bindKeyUp();

      $scope.$on('$destroy', _cleanFileLockSubscription);
      $scope.$on('$destroy', _unbindKeyUp);
    }
  }
})(angular);
