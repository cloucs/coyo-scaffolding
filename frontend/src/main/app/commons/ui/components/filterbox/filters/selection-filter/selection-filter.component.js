(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .component('coyoSelectionFilter', selectionFilter())
      .controller('SelectionFilterController', SelectionFilterController);

  /**
   * @ngdoc directive
   * @name commons.ui.coyoSelectionFilter:coyoSelectionFilter
   *
   * @description
   * Filter for single or multiple selections.
   *
   * @param {object} filterModel
   * The filter model containing the selectable items.
   *
   * @param {string} titleKey translation key for the title.
   *
   * @param {string} titleValues translation values for the title.
   *
   * @param {string} textPrefix Translation prefix for the selectable item's text.
   *
   * @param {expression} onSelectionChange
   * Callback that will be invoked when selection changed.
   * A named parameter 'selected' will receive an array of the selected keys.
   * A named parameter 'toggled' will receive an array of the toggled keys.
   *
   * @param {expression} onSingleSelectionChange
   * Callback that will be invoked when selection changed and singleSelect is set to true.
   * A named parameter 'selected' will receive receive a single selected key.
   * A named parameter 'toggled' will receive the toggled key.
   *
   * @param {boolean} singleSelect
   * Decides whether there can be only one option selected.
   *
   * @param {boolean} hideAllOption Decides whether the all option is hidden.
   *
   * @param {string} cssClass Defines a css class passed to the coyo filter element.
   *
   * @param {number} limitTo Limits the number of shown selectable items.
   *
   * @param {number} orderBy Applies order by filter on selectable items.
   *
   * @param {boolean} showExpand Decides whether an expand button should be shown.
   *
   * @param {expression} onClickExpand Callback that will be invoked when clicked on the expand button.
   */
  function selectionFilter() {
    return {
      templateUrl: 'app/commons/ui/components/filterbox/filters/selection-filter/selection-filter.html',
      bindings: {
        filterModel: '<',
        titleKey: '@',
        titleValues: '<?',
        textPrefix: '@?',
        onSelectionChange: '&?',
        onSingleSelectionChange: '&?',
        singleSelect: '<?',
        hideAllOption: '<?',
        cssClass: '@?'
      },
      controller: 'SelectionFilterController'
    };
  }

  function SelectionFilterController() {
    var vm = this;
    var FILTER_PAGE_SIZE = 7;

    vm.clear = clear;
    vm.toggle = toggle;
    vm.$onInit = onInit;
    vm.onClickExpand = onClickExpand;

    vm.allIcon = vm.filterModel.allIcon || 'zmdi-check-all';

    function clear() {
      var toggledItemKeys = vm.filterModel.clearAll();
      selectionChanged(toggledItemKeys);
    }

    function toggle(key) {
      vm.filterModel.items.forEach(function (item) {
        if (key === item.key) {
          item.active = !item.active;
        } else if (vm.singleSelect) {
          item.active = false;
        }
      });

      vm.filterModel.updateAllActive();
      selectionChanged([key]);
    }

    function selectionChanged(toggledKeys) {
      var selected = vm.filterModel.getSelected();
      if (angular.isFunction(vm.onSelectionChange)) {
        vm.onSelectionChange({selected: selected, toggled: toggledKeys});
      }
      if (vm.singleSelect && angular.isFunction(vm.onSingleSelectionChange)) {
        var singleSelection = selected.length > 0 ? selected[0] : undefined;
        vm.onSingleSelectionChange({selected: singleSelection, toggled: toggledKeys[0]});
      }
    }

    function onClickExpand() {
      vm.limitTo += FILTER_PAGE_SIZE;
      vm.showExpand = _.size(vm.filterModel.items) > vm.limitTo;
    }

    function onInit() {
      vm.limitTo = FILTER_PAGE_SIZE;
      vm.showExpand = _.size(vm.filterModel.items) > FILTER_PAGE_SIZE;
      _moveActiveAggregationItemToTop();
    }

    function _moveActiveAggregationItemToTop() {
      var activeAggregations = _.filter(vm.filterModel.items, {active: true});
      if (_.size(activeAggregations)) {
        _.remove(vm.filterModel.items, function (item) {
          return _.find(activeAggregations, {key: item.key});
        });
        activeAggregations.reverse().forEach(function (activeAggregation) {
          vm.filterModel.items.unshift(activeAggregation);
        });
      }
    }
  }

})(angular);
