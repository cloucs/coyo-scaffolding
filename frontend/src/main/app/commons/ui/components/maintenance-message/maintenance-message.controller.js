(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .controller('MaintenanceMessageController', MaintenanceMessageController);

  function MaintenanceMessageController($rootScope, $scope, $q, $timeout, $sessionStorage,
                                        adminStates, authService, SettingsModel, socketService) {
    var vm = this;
    vm.$onInit = onInit;
    vm.close = close;
    var timeout;

    function updateDisplay() {
      if (vm.message.endDate) {
        if (timeout) {
          $timeout.cancel(timeout);
        }
        var endIn = vm.message.endDate - Date.now();
        if (endIn >= 0) {
          timeout = $timeout(function () {
            vm.display = false;
          }, endIn);
        }
      }
      var currentTime = new Date().getTime();

      authService.getUser().then(function (user) {
        var checkAdmin = vm.message.onlyAdmins !== 'true' ||
          (vm.message.onlyAdmins === 'true' && user.hasGlobalPermissions(vm.allAdminPermissions));
        var checkTime = !vm.message.endDate || currentTime <= vm.message.endDate;
        var checkMessage = vm.message.message && vm.message.message !== '';
        var checkHidden = angular.isUndefined($sessionStorage['maintenanceMessage.' + vm.message.id]);
        vm.display = checkMessage && checkAdmin && checkTime && checkHidden;
      });
    }

    function updateMessageFromSettings() {
      var promises = [SettingsModel.retrieveByKey('maintenanceMessage'),
        SettingsModel.retrieveByKey('maintenanceMessageOnlyAdmins'),
        SettingsModel.retrieveByKey('maintenanceMessageEndDate'),
        SettingsModel.retrieveByKey('maintenanceMessageId')];

      return $q.all(promises).then(function (values) {
        return {
          id: values[3],
          message: values[0],
          onlyAdmins: values[1],
          endDate: values[2]
        };
      });
    }

    function close() {
      $sessionStorage['maintenanceMessage.' + vm.message.id] = true;
      updateDisplay();
    }

    function onInit() {
      vm.message = {};
      vm.allAdminPermissions = _.map(adminStates, 'globalPermission').join(',');
      vm.display = false;

      updateMessageFromSettings().then(function (message) {
        vm.message = message;
        updateDisplay();
      });

      authService.getUser().then(function (user) {
        var destination = '/topic/setting';
        var listenerMessage = socketService.subscribe(destination, function (message) {
          vm.message.message = message.content.value;
          updateDisplay();
          $scope.$apply();
        }, {content: {key: 'maintenanceMessage'}}, user.tenant);
        var listenerMessageId = socketService.subscribe(destination, function (message) {
          vm.message.id = message.content.value;
          updateDisplay();
          $scope.$apply();
        }, {content: {key: 'maintenanceMessageId'}}, user.tenant);
        var listenerEndDate = socketService.subscribe(destination, function (message) {
          vm.message.endDate = message.content.value;
          updateDisplay();
          $scope.$apply();
        }, {content: {key: 'maintenanceMessageEndDate'}}, user.tenant);
        var listenerOnlyAdmins = socketService.subscribe(destination, function (message) {
          vm.message.onlyAdmins = message.content.value;
          updateDisplay();
          $scope.$apply();
        }, {content: {key: 'maintenanceMessageOnlyAdmins'}}, user.tenant);

        $scope.$on('$destroy', function () {
          listenerMessage();
          listenerMessageId();
          listenerEndDate();
          listenerOnlyAdmins();
          $timeout.cancel(timeout);
        });
      });
    }
  }

})(angular);
