(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .controller('AboutCoyoController', AboutCoyoController);

  /**
   * @ngdoc service
   * @name commons.ui.AboutCoyoController
   *
   * @description
   * Renders about coyo.
   */
  function AboutCoyoController(coyoConfig) {
    var vm = this;

    vm.version = coyoConfig.versionString();

    var heartClicks = 0;
    vm.heartFeatureToggleActivated = false;
    vm.heartClick = function () {
      heartClicks++;
      if (heartClicks >= 3) {
        vm.heartFeatureToggleActivated = true;
        sessionStorage.setItem('heart-feature-toggle-activated', 'true');
      }
    };
  }

})(angular);
