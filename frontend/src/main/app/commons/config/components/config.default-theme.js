(function (angular) {
  'use strict';

  angular
      .module('commons.config')
      .constant('defaultThemeColors', {
        'color-primary': '#374555',
        'color-secondary': '#242f39',
        'color-navbar-border': '#9bbf29',
        'coyo-navbar-text': '#ffffff',
        'coyo-navbar': '#374555',
        'coyo-navbar-active': '#26303B',
        'btn-primary-color': '#ffffff',
        'btn-primary-bg': '#374555',
        'color-background-main': '#e8e8e8',
        'text-color': '#333333',
        'link-color': '#317DC1'
      });

})(angular);
