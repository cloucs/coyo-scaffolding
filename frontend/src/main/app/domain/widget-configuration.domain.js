(function (angular) {
  'use strict';

  angular
      .module('coyo.domain')
      .factory('WidgetConfigurationModel', WidgetConfigurationModel);

  /**
   * @ngdoc service
   * @name coyo.domain.WidgetConfigurationModel
   *
   * @description
   * Domain model representation of widget configuration endpoint.
   *
   * @requires restResourceFactory
   * @requires restSerializer
   * @requires coyoEndpoints
   */
  function WidgetConfigurationModel(restResourceFactory, coyoEndpoints) {
    var WidgetConfiguration = restResourceFactory({
      url: coyoEndpoints.widgetConfigurations,
      idAttribute: 'key'
    });

    // class members
    angular.extend(WidgetConfiguration, {
      save: function (widgetConfig) {
        return this.$put(this.$url({
          key: widgetConfig.key
        }), widgetConfig).then(function (result) {
          return _.merge(widgetConfig, result);
        });
      }
    });

    return WidgetConfiguration;
  }

})(angular);
