(function (angular) {
  'use strict';

  angular
      .module('coyo.domain')
      .factory('LicenseModel', LicenseModel);

  /**
   * @ngdoc service
   * @name coyo.domain.LicenseModel
   *
   * @description
   * Domain model representation of license endpoint.
   *
   * @requires $q
   * @requires $rootScope
   * @requires restResourceFactory
   * @requires coyoEndpoints
   */
  function LicenseModel($q, $rootScope, restResourceFactory, coyoEndpoints) {
    var LicenseModel = restResourceFactory({
          url: coyoEndpoints.license
        }),
        cachedLicense = undefined;

    function _emitLicenseLoaded(license) {
      cachedLicense = license;
      $rootScope.$emit('license:loaded', license);
    }

    // class members
    angular.extend(LicenseModel, {

      /**
       * @ngdoc function
       * @name coyo.domain.LicenseModel#updateLicense
       * @methodOf coyo.domain.LicenseModel
       *
       * @description
       * Updates the license.
       *
       * @params {string} license The new license as encrypted string
       *
       * @returns {promise} A $http promise
       */
      updateLicense: function (license) {
        return LicenseModel.$put(LicenseModel.$url(), {license: license}).then(function (license) {
          _emitLicenseLoaded(license);
          return license;
        });
      },

      /**
       * @ngdoc function
       * @name coyo.domain.LicenseModel#load
       * @methodOf coyo.domain.LicenseModel
       *
       * @description
       * Loads the current license.
       *
       * @params {boolean} cached Re-trigger the last requested license
       *
       * @returns {promise} A $http promise
       */
      load: function (cached) {
        if (cached && cachedLicense) {
          _emitLicenseLoaded(cachedLicense);
          return $q.resolve();
        }
        return LicenseModel.get().then(function (license) {
          _emitLicenseLoaded(license);
          return license;
        });
      },

      /**
       * @ngdoc function
       * @name coyo.domain.LicenseModel#checkLicense
       * @methodOf coyo.domain.LicenseModel
       *
       * @description
       * Checks the current license.
       *
       * @returns {promise} A $http promise
       */
      checkLicense: function () {
        return LicenseModel.$get(LicenseModel.$url('valid'));
      }
    });

    // instance members
    angular.extend(LicenseModel.prototype, {});

    return LicenseModel;
  }

})(angular);
