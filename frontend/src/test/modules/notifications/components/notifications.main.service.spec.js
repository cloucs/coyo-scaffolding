(function () {
  'use strict';

  var moduleName = 'coyo.notifications';
  var targetName = 'notificationsMainService';

  describe('module: ' + moduleName, function () {
    var $httpBackend;
    var coyoEndpoints;
    var notificationsMainService;
    var backendUrlService;

    var notificationsReturn = [1, 2, 3];
    var statusReturn = {unseen: {ACTIVITY: 1, DISCUSSION: 1}};

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
      });

      inject(function ($injector, _$httpBackend_, _coyoEndpoints_, _notificationsMainService_, _backendUrlService_) {
        $httpBackend = _$httpBackend_;
        coyoEndpoints = _coyoEndpoints_;
        notificationsMainService = _notificationsMainService_;
        backendUrlService = _backendUrlService_;

        $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
      });
    });

    it('should be registered', function () {
      expect(notificationsMainService).not.toBeUndefined();
    });

    describe('service: ' + targetName, function () {

      /* getNotifications */
      it('should be possible to get notifications', function () {
        // given
        $httpBackend
            .expectGET(backendUrlService.getUrl() + coyoEndpoints.notification.notifications
                .replace('{category}', 'ACTIVITY') + '&_page=' + 0 + '&_pageSize=' + 20 + '&_orderBy=created,DESC')
            .respond(200, notificationsReturn);

        // when
        var _result = null;
        notificationsMainService.getNotifications('ACTIVITY', 0, 20).then(function (response) {
          _result = response;
        });
        $httpBackend.flush();

        // then
        expect(_result).toEqual(notificationsReturn);
      });

      /* getStatus */
      it('should be possible to get status', function () {
        // given
        $httpBackend
            .expectGET(backendUrlService.getUrl() + coyoEndpoints.notification.status)
            .respond(200, statusReturn);

        // when
        var _result = null;
        notificationsMainService.getStatus().then(function (status) {
          _result = status;
        });
        $httpBackend.flush();

        // then
        expect(_result).toEqual(statusReturn);
      });

      /* markAllSeen */
      it('should be possible to mark seen', function () {
        // given
        $httpBackend
            .expectPUT(backendUrlService.getUrl() + coyoEndpoints.notification.markAllSeen.replace('{category}', 'DISCUSSION'))
            .respond(200);

        // when
        notificationsMainService.markAllSeen('DISCUSSION');
        $httpBackend.flush();
      });

      /* markAllClicked */
      it('should be possible to mark clicked', function () {
        // given
        $httpBackend
            .expectPUT(backendUrlService.getUrl() + coyoEndpoints.notification.markAllClicked.replace('{category}', 'ACTIVITY'))
            .respond(200);

        // when
        notificationsMainService.markAllClicked('ACTIVITY');
        $httpBackend.flush();
      });
    });
  });
})();
