(function () {
  'use strict';

  var moduleName = 'coyo.messaging';
  var targetName = 'MessagingChannelController';

  describe('module: ' + moduleName, function () {
    var $controller;
    var $scope;
    var $q;
    var $rootScope;
    var $interval;
    var $timeout;
    var $element;
    var socketService;
    var tempUploadService;
    var MessageModel;
    var optimisticService;
    var messageHandler;
    var messageChannelUserService;
    var utilService;
    var userMock = {id: 123, displayName: 'First User'};
    var singleChannelMock = {
      id: 'channel-id-1',
      type: 'SINGLE'
    };
    var groupChannelMock = {
      id: 'channel-id-2',
      type: 'GROUP'
    };
    var optimisticTypeMessage = 'OPTIMISTIC_TYPE_MESSAGE';
    var optimisticStatus = {
      NEW: 'NEW',
      PENDING: 'PENDING',
      ERROR: 'ERROR'
    };

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $provide.constant('optimisticTypeMessage', optimisticTypeMessage);
        $provide.constant('optimisticStatus', optimisticStatus);
      });

      inject(function (_$controller_, _$rootScope_, _$interval_, _$timeout_, _$q_, _MessageModel_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        $rootScope = _$rootScope_;
        $interval = _$interval_;
        $timeout = _$timeout_;
        $element = jasmine.createSpyObj('$element', ['find']);
        $q = _$q_;
        socketService = jasmine.createSpyObj('socketService', ['subscribe']);
        socketService.subscribe.and.returnValue(_.noop);
        tempUploadService = jasmine.createSpyObj('tempUploadService', ['upload']);
        MessageModel = _MessageModel_;
        utilService = jasmine.createSpyObj('utilService', ['requireInternetConnection']);
        optimisticService = jasmine.createSpyObj('optimisticService', ['saveEntity', 'remove', 'find', 'findWith',
          'findAll', 'clear']);
        optimisticService.saveEntity = function (message) {
          return {
            then: function (callback) {
              return callback({
                entityId: 'optimistic-entity-id',
                entityType: optimisticTypeMessage,
                status: optimisticStatus.NEW,
                entity: message
              });
            }
          };
        };
        messageHandler = jasmine.createSpyObj('messageHandler', [
          'sendMessage', 'markAsRead', 'getAllMessagesFromStorage',
          'getNewMessagesFromStorage', 'getPendingMessagesFromStorage', 'resendPendingMessages', 'getLatestWorkingCopy',
          'removeLocalMessage', 'uploadFiles', 'cleanupLocalMessages'
        ]);
        messageHandler.cleanupLocalMessages = function () {
          return $q.when();
        };
        spyOn(MessageModel, 'pagedQuery').and.callThrough();
        messageChannelUserService = jasmine.createSpyObj('messageChannelUserService', ['fetchDisabledUsers']);
        messageChannelUserService.fetchDisabledUsers.and.returnValue($q.resolve({
          data: {}
        }));
      });
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl(channelType) {
        return $controller(targetName, {
          $element: $element,
          $scope: $scope,
          socketService: socketService,
          tempUploadService: tempUploadService,
          MessageModel: MessageModel,
          optimisticService: optimisticService,
          messageHandler: messageHandler,
          utilService: utilService,
          $interval: $interval,
          messageChannelUserService: messageChannelUserService
        }, {
          channel: channelType === 'GROUP' ? groupChannelMock : singleChannelMock,
          currentUser: userMock
        });
      }

      it('should init controller for a single channel', function () {
        // when
        var ctrl = buildCtrl();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.currentUser).toBe(userMock);
        expect(ctrl.channel).toBe(singleChannelMock);
        expect(ctrl.messages).toEqual([]);
        expect(ctrl.messageGroups).toEqual({});
        expect(ctrl.days).toEqual([]);
        expect(ctrl.disabledUsers).toEqual(jasmine.any(Object));
        expect(MessageModel.pagedQuery).toHaveBeenCalledTimes(1);
        expect(messageChannelUserService.fetchDisabledUsers).toHaveBeenCalledTimes(1);
        expect(socketService.subscribe)
            .toHaveBeenCalledWith('/user/topic/messaging', jasmine.any(Function), 'messageCreated');
      });

      it('should init controller for a group channel', function () {
        // when
        var ctrl = buildCtrl('GROUP');
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.currentUser).toBe(userMock);
        expect(ctrl.channel).toBe(groupChannelMock);
        expect(ctrl.messages).toEqual([]);
        expect(ctrl.messageGroups).toEqual({});
        expect(ctrl.days).toEqual([]);
        expect(ctrl.disabledUsers).toEqual(jasmine.any(Object));
        expect(MessageModel.pagedQuery).toHaveBeenCalledTimes(1);
        expect(messageChannelUserService.fetchDisabledUsers).toHaveBeenCalledTimes(0);
        expect(socketService.subscribe)
            .toHaveBeenCalledWith('/user/topic/messaging', jasmine.any(Function), 'messageCreated');
      });

      it('should load more', function () {
        // given
        var ctrl = buildCtrl();
        var msg1 = {id: 'msg-1', created: 2};
        var msg2 = {id: 'msg-2', created: 1};
        var page = {
          content: [msg1, msg2],
          totalElements: 1
        };

        var thenCb, finallyCb;
        MessageModel.pagedQuery.and.returnValue({
          then: function (cb) {
            thenCb = cb;
            return {
              'finally': function (cb) {
                finallyCb = cb;
              }
            };
          }
        });

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(MessageModel.pagedQuery).toHaveBeenCalledTimes(1);

        thenCb(page);
        expect(ctrl.loading).toBeTruthy();
        expect(ctrl.messages[0]).toBe(msg2);
        expect(ctrl.messages[1]).toBe(msg1);
        expect(ctrl.currentPage).toBe(page);
        expect(messageHandler.markAsRead).toHaveBeenCalledWith(userMock.id, singleChannelMock.id);

        finallyCb();
        expect(ctrl.loading).toBeFalsy();
      });

      it('should handle new message', function () {
        // given
        var newMessageCallback;
        socketService.subscribe.and.callFake(function (dest, callback, filter) {
          if (dest === '/user/topic/messaging' && filter === 'messageCreated') {
            newMessageCallback = callback;
          }
          return _.noop;
        });

        messageHandler.removeLocalMessage.and.returnValue($q.when());
        var ctrl = buildCtrl();
        ctrl.$onInit();
        $scope.$apply();

        var event = {
          content: {
            id: 'msg-3',
            created: new Date(),
            channelId: singleChannelMock.id
          }
        };

        // when
        newMessageCallback(event);
        $timeout.flush(1000);
        $scope.$apply();

        // then
        expect(ctrl.messages[0].id).toEqual(event.content.id);
      });

      it('should add new messages on reconnect', function () {
        // given
        var ctrl = buildCtrl();
        var channelId = 'channel-id';
        ctrl.channel = {id: channelId};
        ctrl.messages = [{id: 'old', channelId: channelId, created: new Date()}];
        ctrl.currentUser = {};
        ctrl.$onInit();
        $scope.$apply();
        MessageModel.pagedQuery.calls.reset();
        var updatedPage = {
          content: [
            {id: 'old', channelId: channelId, created: new Date()},
            {id: 'new1', channelId: channelId, created: new Date()},
            {id: 'new2', channelId: channelId, created: new Date()}
          ]
        };
        MessageModel.pagedQuery.and.returnValue($q.resolve(updatedPage));
        messageHandler.removeLocalMessage.and.returnValue($q.resolve());

        // when
        $rootScope.$emit('socketService:reconnected');
        $scope.$apply();
        $timeout.flush();

        // then
        expect(ctrl.messages.length).toBe(3);
        expect(ctrl.messages[0].id).toBe('new2');
        expect(ctrl.messages[1].id).toBe('new1');
        expect(ctrl.messages[2].id).toBe('old');
      });

      it('should trigger reload if reconnect loads a full page of new messages', function () {
        // given
        var ctrl = buildCtrl();
        var channelId = 'channel-id';
        ctrl.channel = {id: channelId};
        ctrl.messages = [{id: 'old', channelId: channelId, created: new Date()}];
        ctrl.currentUser = {};
        ctrl.$onInit();
        $scope.$apply();
        var updatedPage = {
          content: [
            {id: 'new1', channelId: channelId, created: new Date()},
            {id: 'new2', channelId: channelId, created: new Date()},
            {id: 'new3', channelId: channelId, created: new Date()},
            {id: 'new4', channelId: channelId, created: new Date()},
            {id: 'new5', channelId: channelId, created: new Date()},
            {id: 'new6', channelId: channelId, created: new Date()},
            {id: 'new7', channelId: channelId, created: new Date()},
            {id: 'new8', channelId: channelId, created: new Date()},
            {id: 'new9', channelId: channelId, created: new Date()},
            {id: 'new10', channelId: channelId, created: new Date()}
          ]
        };
        MessageModel.pagedQuery.and.returnValue($q.resolve(updatedPage));
        messageHandler.removeLocalMessage.and.returnValue($q.resolve());
        spyOn($scope, '$broadcast');

        // when
        $rootScope.$emit('socketService:reconnected');
        $scope.$apply();
        $timeout.flush();

        // then
        expect($scope.$broadcast).toHaveBeenCalledWith('messaging-channel:refresh');
      });

      it('should evaluate the current day', function () {
        // given
        var ctrl = buildCtrl();
        var testDate = new Date();

        // when
        var result = ctrl.isCurrentDay(testDate);

        // then
        expect(result).toBeTruthy();
      });

      it('should save new messages from today', function () {
        // given
        var ctrl = buildCtrl();
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        yesterday.setHours(0, 0, 0, 0);
        var today = new Date().setHours(0, 0, 0, 0);
        var message = {
          data: {
            message: 'message'
          },
          attachments: [{
            file: 'attachment'
          }],
          channelId: 'channel-id',
          clientMessageId: 'client-message-id'
        };
        spyOn(optimisticService, 'saveEntity').and.callThrough();
        messageHandler.removeLocalMessage.and.returnValue($q.when());
        messageHandler.sendMessage = function () {
          return $q.when(message);
        };

        // set values
        ctrl.$onInit();
        ctrl.days = [yesterday.valueOf()];
        ctrl.channel = {
          id: 'channel-id'
        };
        ctrl.messageGroups[yesterday.valueOf()] = [[{message: 1}, {message: 2}]];

        // when
        ctrl.saveMessage(message);
        $interval.flush(100);

        // then
        expect(_.keys(ctrl.messageGroups).length).toEqual(2);
        expect(ctrl.messageGroups[yesterday.valueOf()].length).toEqual(1);
        expect(ctrl.messageGroups[today.valueOf()].length).toEqual(1);
        expect(ctrl.days.length).toEqual(2);
      });

      it('should resend a failed message', function () {
        // given
        var ctrl = buildCtrl();
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        yesterday.setHours(0, 0, 0, 0);
        var todayDate = new Date();
        var today = angular.copy(todayDate).setHours(0, 0, 0, 0);
        var message = {
          data: {
            message: 'message'
          },
          attachments: [{
            file: 'attachment'
          }],
          created: new Date().setDate(yesterday.getDate() - 1),
          channelId: 'channel-id',
          clientMessageId: 'client-message-id',
          error: 'error'
        };
        spyOn(optimisticService, 'saveEntity').and.callThrough();
        utilService.requireInternetConnection.and.returnValue($q.resolve());
        optimisticService.findWith.and.returnValue({
          status: 'error',
          entity: {
            error: true
          }
        });
        messageHandler.sendMessage.and.returnValue($q.resolve());

        // set values
        ctrl.$onInit();
        ctrl.channel = {
          id: 'channel-id'
        };
        ctrl.messages.push(message);
        ctrl.messageGroups[yesterday.valueOf()] = [];
        ctrl.messageGroups[yesterday.valueOf()].push([message]);
        $scope.$apply();

        // when
        ctrl.onMessageClick(message);
        $scope.$apply();

        // then
        expect(ctrl.messageGroups[today.valueOf()][0].length).toEqual(1);
        expect(ctrl.messageGroups[today.valueOf()][0][0].created).toBeGreaterThan(todayDate);
        expect(ctrl.days.length).toEqual(1);
        expect(utilService.requireInternetConnection).toHaveBeenCalled();
        expect(optimisticService.saveEntity).toHaveBeenCalled();
        expect(messageHandler.sendMessage).toHaveBeenCalled();
      });
    });
  });
})();
