(function () {
  'use strict';

  var moduleName = 'coyo.events';
  var targetName = 'EventsListController';

  describe('module: ' + moduleName, function () {
    var $scope, $controller, $q, $sessionStorage, $state, $timeout, moment;
    var EventModelMock, Pageable;

    beforeEach(function () {
      module('commons.ui');

      module(moduleName);

      inject(function (_$rootScope_, _$controller_, _$q_, _Pageable_, _$timeout_, _moment_) {
        $scope = _$rootScope_.$new();
        $controller = _$controller_;
        $q = _$q_;
        $sessionStorage = {};
        $state = jasmine.createSpyObj('$state', ['go', 'transitionTo']);
        $timeout = _$timeout_;
        moment = _moment_;
        EventModelMock = jasmine.createSpyObj('EventModel', ['searchWithFilter', 'getAllParticipationStatuses']);
        Pageable = _Pageable_;
      });
      EventModelMock.getAllParticipationStatuses.and.returnValue(['ATTENDING', 'NOT_ATTENDING', 'PENDING']);
    });

    describe('controller: ' + targetName, function () {

      function buildController($stateParams) {
        var ctrl = $controller(targetName, {
          $sessionStorage: $sessionStorage,
          $state: $state,
          $stateParams: $stateParams || {},
          EventModel: EventModelMock,
          Pageable: Pageable
        });
        ctrl.$onInit();
        return ctrl;
      }

      it('should init and read from $sessionStorage', function () {
        // given
        $sessionStorage =
            {eventQuery: {term: 'term', from: '2010-10-10', to: '2010-10-20', participationStatus: 'PENDING'}};

        // when
        var ctrl = buildController();

        // then
        expect(ctrl.query.term).toEqual($sessionStorage.eventQuery.term);
        expect(ctrl.query.from).toEqual($sessionStorage.eventQuery.from);
        expect(ctrl.query.to).toEqual($sessionStorage.eventQuery.to);
        expect(ctrl.query.participationStatus).toEqual(['PENDING']);
        expect(ctrl.participationStatusFilter.getItem('ATTENDING').isSelected()).toBe(false);
        expect(ctrl.participationStatusFilter.getItem('NOT_ATTENDING').isSelected()).toBe(false);
        expect(ctrl.participationStatusFilter.getItem('PENDING').isSelected()).toBe(true);
        expect(ctrl.statusFilterModel.getItem('SUBSCRIBED').isSelected()).toBe(true);
      });

      it('should init and read from URL', function () {
        // given
        $sessionStorage = {eventQuery: {term: 'term', from: '2010-10-10', to: '2010-10-20'}};
        var $stateParams = {term: 'term2', from: '2010-11-10', to: '2010-11-20'};

        // when
        var ctrl = buildController($stateParams);

        // then
        expect(ctrl.query.term).toEqual($stateParams.term);
        expect(ctrl.query.from).toEqual($stateParams.from);
        expect(ctrl.query.to).toEqual($stateParams.to);
      });

      it('should load more', function () {
        // given
        var page = {
          content: [{id: 1, name: 'event1'}, {id: 2, name: 'event2'}, {id: 3, name: 'event3'}]
        };
        EventModelMock.searchWithFilter.and.returnValue($q.resolve(page));

        var ctrl = buildController();
        ctrl.query = {
          term: 'term',
          from: '2017.08.05',
          to: '2017.08.08'
        };

        // when
        $timeout.flush();

        // then
        expect($sessionStorage.eventQuery).toEqual(ctrl.query);
        expect(EventModelMock.searchWithFilter).toHaveBeenCalledWith(
            ctrl.query.term, '2017.08.05T00:00:00', '2017.08.08T23:59:59',
            new Pageable(0, 20, ['_score,DESC', 'displayName.sort']),
            {},
            ['displayName', 'description']
        );

        expect(ctrl.loading).toBeFalsy();
        expect(ctrl.currentPage).toEqual(page);
      });

      it('should search', function () {
        // given
        var term = 'term';
        var ctrl = buildController();
        EventModelMock.searchWithFilter.and.returnValue($q.reject());

        // when
        ctrl.search(term);

        // then
        expect(ctrl.query.term).toEqual(term);
        expect(EventModelMock.searchWithFilter).toHaveBeenCalled();
      });

      it('should not search while already loading', function () {
        // given
        var term = 'term';
        var ctrl = buildController();
        ctrl.currentPage = {};
        ctrl.loading = true;

        // when
        ctrl.search(term);

        // then
        expect(EventModelMock.searchWithFilter).not.toHaveBeenCalled();
      });

      it('should filter by date range', function () {
        // given
        var ctrl = buildController();
        EventModelMock.searchWithFilter.and.returnValue($q.reject());

        // when
        var from = '2010-10-10';
        var to = '2010-10-10';
        ctrl.filter([moment(from, 'YYYY-MM-DD'), moment(to, 'YYYY-MM-DD')]);

        // then
        expect(ctrl.query.from).toEqual(from);
        expect(ctrl.query.to).toEqual(to);
        expect(EventModelMock.searchWithFilter).toHaveBeenCalled();
      });

      it('should clear date range', function () {
        // given
        var ctrl = buildController();
        ctrl.dateRange = ['2010-10-10', '2010-10-10'];
        ctrl.query = {from: '2010-10-10', to: '2010-10-10'};
        EventModelMock.searchWithFilter.and.returnValue($q.reject());

        // when
        ctrl.clearDate();

        // then
        expect(ctrl.dateRange).toEqual([]);
        expect(ctrl.query.from).toEqual(null);
        expect(ctrl.query.to).toEqual(null);
        expect(EventModelMock.searchWithFilter).toHaveBeenCalled();
      });

      it('should filter by participation status', function () {
        // given
        var page = {
          content: [{id: 1, name: 'event1'}, {id: 2, name: 'event2'}, {id: 3, name: 'event3'}]
        };
        EventModelMock.searchWithFilter.and.returnValue($q.resolve(page));
        var ctrl = buildController();

        // when
        ctrl.setParticipationStatusFilter(['PENDING', 'NOT_ATTENDING']);
        $scope.$apply();

        // then
        expect(EventModelMock.searchWithFilter).toHaveBeenCalledWith(
            ctrl.query.term, undefined, undefined, new Pageable(0, 20, 'displayName.sort'),
            {participationStatus: ['PENDING', 'NOT_ATTENDING'], status: ['SUBSCRIBED']},
            ['displayName', 'description']
        );
        expect(ctrl.loading).toBeFalsy();
        expect(ctrl.currentPage).toEqual(page);
      });

      it('should filter by relevance', function () {
        // given
        var page = {
          content: [{id: 1, name: 'event1'}, {id: 2, name: 'event2'}, {id: 3, name: 'event3'}]
        };
        EventModelMock.searchWithFilter.and.returnValue($q.resolve(page));
        var ctrl = buildController();

        // when
        ctrl.setStatusFilter([]); // SUBSCRIBED is default, so we search for ALL instead
        $scope.$apply();

        // then
        expect(EventModelMock.searchWithFilter).toHaveBeenCalledWith(
            ctrl.query.term, undefined, undefined, new Pageable(0, 20, 'displayName.sort'),
            {status: []},
            ['displayName', 'description']
        );
        expect(ctrl.loading).toBeFalsy();
        expect(ctrl.currentPage).toEqual(page);
      });
    });
  });

})();
