(function () {
  'use strict';

  var moduleName = 'coyo.events';
  var targetName = 'EventsSettingsController';

  describe('module: ' + moduleName, function () {
    var $controller, $scope, $state, moment, modalService, $q;

    var event;
    var EventModel, coyoNotification, eventDateSyncService;
    var currentUser = {};
    var adminIds = ['id-0', 'id-1', 'id-2', 'id-3'];

    function buildController() {
      var ctrl = $controller(targetName, {
        $scope: $scope,
        $state: $state,
        event: event,
        currentUser: currentUser,
        adminIds: adminIds,
        moment: moment,
        EventModel: EventModel,
        coyoNotification: coyoNotification,
        modalService: modalService,
        eventDateSyncService: eventDateSyncService
      });
      ctrl.$onInit();
      return ctrl;
    }

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$rootScope_, _$q_, _moment_, _EventModel_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        EventModel = _EventModel_;
        $q = _$q_;
        moment = _moment_;

        $state = jasmine.createSpyObj('$state', ['go', 'href']);

        event = {
          id: '1',
          slug: 'event-slug',
          startDate: '2017-08-07 15:35:00',
          endDate: '2017-08-07 16:35:00',
          updateEvent: function () {
          },
          deleteEvent: function () {
          }
        };

        coyoNotification = jasmine.createSpyObj('coyoNotification', ['success']);
        modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);
      });
    });

    describe('controller: ' + targetName, function () {

      it('should update the event', function () {
        // given
        // ... see beforeEach()

        spyOn(event, 'updateEvent').and.returnValue({
          then: function (callback) {
            callback();
            return {
              catch: function () {
              }
            };
          }
        });

        // when
        var ctrl = buildController();
        $scope.$apply();
        ctrl.saveEvent({$valid: true});

        // then
        expect(event.updateEvent).toHaveBeenCalled();
        expect(coyoNotification.success).toHaveBeenCalledWith('MODULE.EVENTS.EDIT.SUCCESS');
        expect($state.go).toHaveBeenCalledWith('main.event.show.timeline', {idOrSlug: event.slug}, {reload: true});
      });

      it('should delete an event and redirect to event overview when cancelation is triggered', function () {
        // given

        modalService.confirmDelete.and.returnValue({result: $q.resolve()});
        spyOn(event, 'deleteEvent').and.returnValue($q.resolve());

        // when
        var ctrl = buildController();
        ctrl.deleteEvent();
        $scope.$apply();

        // then
        expect(event.deleteEvent).toHaveBeenCalled();
        expect(coyoNotification.success).toHaveBeenCalledWith('EVENT.DELETE.SUCCESS');
        expect($state.go).toHaveBeenCalledWith('main.event');
      });

    });
  });
})();
