(function () {
  'use strict';

  var moduleName = 'coyo.events';
  var controllerName = 'EventDateLabelController';

  describe('module: ' + moduleName, function () {
    var $controller;
    var $rootScope = {
      dateFormat: {long: 'MMMM Do YYYY'},
      timeFormat: {short: 'h:mma'}
    };

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_) {
        $controller = _$controller_;
      });
    });

    function buildController(event) {
      return $controller(controllerName, {
        $rootScope: $rootScope
      }, {
        event: event
      });
    }

    describe('controller: ' + controllerName, function () {

      it('should format non full day events (single day)', function () {
        // given
        var event = {
          fullDay: false,
          startDate: '2017-01-01T09:30:00',
          endDate: '2017-01-01T10:30:00'
        };

        // when
        var ctrl = buildController(event);
        ctrl.$onInit();

        // then
        expect(ctrl.startDate).toBeDefined();
        expect(ctrl.endDate).toBeDefined();
        expect(ctrl.startPattern).toBe('h:mma');
        expect(ctrl.endPattern).toBe('h:mma');
      });

      it('should format non full day events (multiple days)', function () {
        // given
        var event = {
          fullDay: false,
          startDate: '2017-01-01T09:30:00',
          endDate: '2017-01-02T10:30:00'
        };

        // when
        var ctrl = buildController(event);
        ctrl.$onInit();

        // then
        expect(ctrl.startDate).toBeDefined();
        expect(ctrl.endDate).toBeDefined();
        expect(ctrl.startPattern).toBe('h:mma');
        expect(ctrl.endPattern).toBe('MMMM Do YYYY, h:mma');
      });

      it('should format full day events (single day)', function () {
        // given
        var event = {
          fullDay: true,
          startDate: '2017-01-01T00:00:00',
          endDate: '2017-01-01T23:59:59'
        };

        // when
        var ctrl = buildController(event);
        ctrl.$onInit();

        // then
        expect(ctrl.startDate).toBeDefined();
        expect(ctrl.endDate).toBeDefined();
        expect(ctrl.startPattern).toBe('');
        expect(ctrl.endPattern).toBe('');
      });

      it('should format full day events (multiple days)', function () {
        // given
        var event = {
          fullDay: true,
          startDate: '2017-01-01T00:00:00',
          endDate: '2017-01-02T23:59:59'
        };

        // when
        var ctrl = buildController(event);
        ctrl.$onInit();

        // then
        expect(ctrl.startDate).toBeDefined();
        expect(ctrl.endDate).toBeDefined();
        expect(ctrl.startPattern).toBe('');
        expect(ctrl.endPattern).toBe('MMMM Do YYYY');
      });
    });
  });

})();
