(function () {
  'use strict';

  var moduleName = 'coyo.admin.settings';
  var controllerName = 'AdminGeneralSettingsController';

  describe('module: ' + moduleName, function () {

    var $scope, $q, $controller;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$rootScope_, _$q_, _$controller_) {
        $scope = _$rootScope_.$new();
        $q = _$q_;
        $controller = _$controller_;
      });
    });

    describe('controller: ' + controllerName, function () {
      var SettingsModel, settings;

      beforeEach(function () {
        settings = {};
        Object.setPrototypeOf(settings, jasmine.createSpyObj('settings', ['update', 'get']));
        settings.update.and.returnValue($q.resolve());
        settings.get.and.returnValue($q.resolve(settings));
        settings.networkName = 'Coyo';

        SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieve']);
        SettingsModel.retrieve.and.returnValue($q.resolve(settings));
      });

      function buildController() {
        return $controller(controllerName, {
          SettingsModel: SettingsModel,
          settings: settings
        });
      }

      describe('controller', function () {

        it('should init', function () {
          // given
          var ctrl = buildController();
          var result = {};

          // when
          expect(ctrl.transferObject).toEqual(result);
          ctrl.$onInit();
          $scope.$apply();

          // then
          expect(ctrl.transferObject).not.toBe(result);
        });

        it('should save settings', function () {
          // given
          var ctrl = buildController();

          ctrl.$onInit();
          $scope.$apply();

          // when
          ctrl.save();
          $scope.$apply();

          // then
          expect(settings.update).toHaveBeenCalled();
          expect(SettingsModel.retrieve).toHaveBeenCalledWith(true);
        });

        it('should correct generate transfer object (settings)', function () {
          // given
          var ctrl = buildController();

          // when
          ctrl.$onInit();
          $scope.$apply();

          var result = {
            networkName: '',
            trackingCode: '',
            subNavigationActive: '',
            deletedUserAnonymizationActive: '',
            deletedUserAnonymizationDelay: '',
            deletedUserDisplayName: '',
            defaultVisibilityPages: '',
            defaultVisibilityWorkspaces: '',
            userOfflineAfterCreationActive: '',
            multiLanguageActive: '',
            integrationType: ''
          };

          // then
          expect(Object.keys(ctrl.transferObject)).toEqual(Object.keys(result));
        });

      });
    });
  });
})();
