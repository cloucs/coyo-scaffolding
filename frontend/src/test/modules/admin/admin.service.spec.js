(function () {
  'use strict';

  var moduleName = 'coyo.admin';

  describe('module: ' + moduleName, function () {
    var $rootScope, $scope;
    var adminService;

    beforeEach(function () {
      module(moduleName);
    });

    beforeEach(inject(function (_$rootScope_, _adminService_) {
      $rootScope = _$rootScope_;
      $scope = _$rootScope_.$new();
      adminService = _adminService_;

      $rootScope.screenSize = {
        isXs: false,
        isSm: false,
        isMd: false,
        isLg: false
      };
    }));

    describe('service: adminService', function () {
      it('should initialize', function () {
        // then
        expect(adminService).toBeDefined();
      });

      it('should be mobile view when screensize is xs', function () {
        // given
        $rootScope.screenSize.isXs = true;

        // when
        var mobile = adminService.isMobile($rootScope.screenSize);

        // then
        expect(mobile).toBe(true);
      });

      it('should be mobile view when screensize is sm', function () {
        // given
        $rootScope.screenSize.isSm = true;

        // when
        var mobile = adminService.isMobile($rootScope.screenSize);

        // then
        expect(mobile).toBe(true);
      });

      it('should not be mobile view when screensize is other than xs or sm', function () {
        // given
        $rootScope.screenSize.isMd = true;

        // when
        var mobile = adminService.isMobile($rootScope.screenSize);

        // then
        expect(mobile).toBe(false);
      });

      it('should execute callback on screensize event', function () {
        // given
        var callback = jasmine.createSpy('Callback');
        $rootScope.screenSize.isSm = true;

        // when
        var result = adminService.initMobileCheck($scope, callback);
        $rootScope.screenSize.isSm = false;
        $rootScope.screenSize.isMd = true;
        $rootScope.$broadcast('screenSize:changed', $rootScope.screenSize);

        // then
        expect(result).toBe(true);
        expect(callback).toHaveBeenCalledWith(false, $rootScope.screenSize, jasmine.any(Object));
      });
    });
  });
})();
