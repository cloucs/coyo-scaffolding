(function () {
  'use strict';

  var moduleName = 'coyo.pages';
  var targetName = 'PagesListController';

  describe('module: ' + moduleName, function () {
    var $controller, $rootScope, $scope, $q, $sessionStorage, $state, $timeout;
    var PageModelMock, Pageable, appService, subscriptionsService, user, categories;

    beforeEach(function () {
      module('commons.ui');

      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
      });

      inject(function (_$rootScope_, _$controller_, _$q_, _Pageable_, _$timeout_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = _$rootScope_.$new();
        $q = _$q_;
        $sessionStorage = {};
        Pageable = _Pageable_;
        $timeout = _$timeout_;
        user = {id: 1, name: 'user'};
        categories = [{id: 1, name: 'cat1'}, {id: 2, name: 'cat2'}, {id: 3, name: 'cat3'}];
        $state = jasmine.createSpyObj('$state', ['transitionTo']);
        PageModelMock = jasmine.createSpyObj('PageModel', ['searchWithFilter']);
        appService = jasmine.createSpyObj('appService', ['redirectToApp', 'getAppLinkForCreatedApp']);
        subscriptionsService = jasmine.createSpyObj('subscriptionsService', ['getSubscriptions']);
      });
    });

    describe('controller: ' + targetName, function () {

      function buildController($stateParams) {
        return $controller(targetName, {
          $rootScope: $rootScope,
          $scope: $scope,
          $q: $q,
          $sessionStorage: $sessionStorage,
          $state: $state,
          $stateParams: $stateParams || {},
          PageModel: PageModelMock,
          Pageable: Pageable,
          currentUser: user,
          categories: categories,
          appService: appService,
          subscriptionsService: subscriptionsService
        });
      }

      it('should init and read from URL', function () {
        // given
        $sessionStorage = {pageQuery: {term: 'term'}};
        var $stateParams = {term: 'term', 'categories[]': 'cat1'};

        // when
        var ctrl = buildController($stateParams);
        ctrl.$onInit();

        // then
        expect(ctrl.query.term).toEqual($stateParams.term);
        expect(ctrl.query.filters.categories).toEqual($stateParams['categories[]']);
      });

      it('should init and read from $sessionStorage', function () {
        // given
        $sessionStorage = {pageQuery: {term: 'term'}};

        // when
        var ctrl = buildController();
        ctrl.$onInit();

        // then
        expect(ctrl.query.term).toEqual($sessionStorage.pageQuery.term);
      });

      it('should load more', function () {
        // given
        var page = {
          content: [{id: 1, name: 'page1'}, {id: 2, name: 'page2'}, {id: 3, name: 'page3'}],
          aggregations: {categories: [{key: 1, count: 2}], allCategories: [{key: 'allCategories', count: 3}]}
        };
        PageModelMock.searchWithFilter.and.returnValue($q.resolve(page));

        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.query = {term: 'term', filters: {categories: [1]}};

        // when
        $timeout.flush();

        // then
        expect($sessionStorage.pageQuery).toEqual(ctrl.query);
        expect(PageModelMock.searchWithFilter).toHaveBeenCalledWith(
            ctrl.query.term,
            new Pageable(0, 10, ['_score,DESC', 'displayName.sort']),
            ctrl.query.filters,
            ['displayName', 'description'],
            {categories: 0, allCategories: 1});

        expect(ctrl.loading).toBeFalse();
        expect(ctrl.currentPage).toEqual(page);
        expect(ctrl.categories[0].count).toEqual(2);
        expect(ctrl.categories[1].count).toEqual(0);
        expect(ctrl.categories[2].count).toEqual(0);
        expect(ctrl.totalCount).toEqual(3);
      });

      it('should check subscriptions', function () {
        // given
        var page = {
          content: [{id: 1, name: 'page1'}, {id: 2, name: 'page2'}, {id: 3, name: 'page3'}],
          aggregations: {categories: [{key: 1, count: 2}], allCategories: 1}
        };
        PageModelMock.searchWithFilter.and.returnValue($q.resolve(page));
        var subscriptions = [{targetId: 1, autoSubscribe: true}, {targetId: 2, autoSubscribe: false}];
        subscriptionsService.getSubscriptions.and.returnValue($q.resolve(subscriptions));

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $timeout.flush();

        var result1 = ctrl.isAutoSubscribe({id: 1});
        var result2 = ctrl.isAutoSubscribe({id: 2});
        var result3 = ctrl.isAutoSubscribe({id: 3});

        // then
        expect(result1).toBeTrue();
        expect(result2).toBeUndefined();
        expect(result3).toBeUndefined();
      });

      it('should search', function () {
        // given
        var term = 'new term';
        var ctrl = buildController();
        ctrl.$onInit();
        PageModelMock.searchWithFilter.and.returnValue($q.reject());

        // when
        ctrl.search(term);

        // then
        expect(ctrl.query.term).toEqual(term);
        expect(ctrl.query.filters.categories).toEqual([]);
        expect(PageModelMock.searchWithFilter).toHaveBeenCalled();
      });

      it('should not search when editing categories', function () {
        // given
        var term = 'new term';
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.editingCategory = true;
        ctrl.query = {term: 'term', filters: {categories: [1]}};
        PageModelMock.searchWithFilter.and.returnValue($q.reject());

        // when
        ctrl.search(term);

        // then
        expect(ctrl.query.term).toEqual('term');
        expect(ctrl.query.filters.categories).toEqual([1]);
        expect(PageModelMock.searchWithFilter).not.toHaveBeenCalled();
      });

      it('should load app link for an app of a page', function () {
        // given
        var page = {id: 'page-id'};
        var app = {id: 'app-id'};
        var url = '/some-url/app';
        appService.getAppLinkForCreatedApp.and.returnValue(url);
        var ctrl = buildController();

        // when
        var result = ctrl.getAppLink(app, page);

        // then
        expect(appService.getAppLinkForCreatedApp).toHaveBeenCalledWith(page, app);
        expect(result).toBe(url);
      });
    });
  });

})
();
