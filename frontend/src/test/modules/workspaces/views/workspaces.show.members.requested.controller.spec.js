(function () {
  'use strict';

  var moduleName = 'coyo.workspaces';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $rootScope, $scope, currentUser, workspace, memberships, event;

    beforeEach(module(moduleName));

    beforeEach(function () {
      currentUser = {
        id: 'USER-ID-ONE'
      };
      memberships = [{
        user: {
          id: 'USER-ID-ONE'
        }
      }, {
        user: {
          id: 'USER-ID-TWO'
        }
      }];
      workspace = jasmine.createSpyObj('WorkspaceModel', ['approveMember', 'removeMember', 'getRequested', 'getMembers']);
      event = jasmine.createSpyObj('$event', ['stopPropagation']);

      inject(function (_$controller_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
      });
    });

    var controllerName = 'WorkspaceMemberRequestedController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          currentUser: currentUser,
          workspace: workspace
        });
      }

      it('should load requested memberships', function () {
        // given
        var response = {
          content: memberships,
          totalElements: 2
        };
        workspace.getRequested.and.returnValue($q.resolve(response));
        var ctrl = buildController();

        // when
        $scope.$apply();

        // then
        expect(workspace.getRequested).toHaveBeenCalled();
        expect(ctrl.currentPage).toBe(response);
        expect(workspace.requestedCount).toBe(response.totalElements);
        expect(ctrl.loading).toBe(false);
      });

      it('should decline a membership request', function () {
        // given
        var response = {
          content: memberships,
          totalElements: 2
        };
        workspace.getRequested.and.returnValue($q.resolve(response));
        workspace.removeMember.and.returnValue($q.resolve());
        workspace.requestedCount = 2;

        var ctrl = buildController();

        // when
        ctrl.decline('USER-ID-TWO', event);
        $scope.$apply();

        // then
        expect(ctrl.currentPage.content).toBeArrayOfSize(1);
        expect(ctrl.currentPage.content[0].user.id).toBe('USER-ID-ONE');
        expect(workspace.requestedCount).toBe(1);
        expect(event.stopPropagation).toHaveBeenCalled();
      });

      it('should approve a membership request', function () {
        // given
        spyOn($rootScope, '$emit');
        var response = {
          content: memberships,
          totalElements: 2
        };
        workspace.getRequested.and.returnValue($q.resolve(response));
        workspace.approveMember.and.returnValue($q.resolve());
        workspace.requestedCount = 2;

        var ctrl = buildController();

        // when
        ctrl.approve('USER-ID-TWO', event);
        $scope.$apply();

        // then
        expect(ctrl.currentPage.content).toBeArrayOfSize(1);
        expect(ctrl.currentPage.content[0].user.id).toBe('USER-ID-ONE');
        expect(workspace.requestedCount).toBe(1);
        expect(event.stopPropagation).toHaveBeenCalled();
        expect($rootScope.$emit).toHaveBeenCalledWith('workspace.member:added', 'USER-ID-TWO');
      });

    });
  });

})();
