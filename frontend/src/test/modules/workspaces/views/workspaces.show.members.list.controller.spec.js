(function () {
  'use strict';

  var moduleName = 'coyo.workspaces';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $rootScope, $scope, $state, currentUser, workspace, memberships, event;

    beforeEach(module(moduleName));

    beforeEach(function () {
      currentUser = {
        id: 'USER-ID-ONE'
      };
      memberships = [{
        user: {
          id: 'USER-ID-ONE'
        }
      }, {
        user: {
          id: 'USER-ID-TWO'
        }
      }];
      workspace = jasmine.createSpyObj('WorkspaceModel', ['promote', 'demote', 'removeMember', 'getMembers']);
      event = jasmine.createSpyObj('$event', ['stopPropagation']);

      inject(function (_$controller_, _$q_, _$rootScope_, _$state_) {
        $controller = _$controller_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $state = _$state_;
      });
    });

    var controllerName = 'WorkspaceMemberListController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          currentUser: currentUser,
          workspace: workspace
        });
      }

      it('should load members', function () {
        // given
        var response = {
          content: memberships
        };
        workspace.getMembers.and.returnValue($q.resolve(response));
        var ctrl = buildController();

        // when
        $scope.$apply();

        // then
        expect(workspace.getMembers).toHaveBeenCalled();
        expect(ctrl.currentPage).toBe(response);
        expect(ctrl.loading).toBe(false);
      });

      it('should remove a member', function () {
        // given
        spyOn($rootScope, '$emit');
        var response = {
          content: memberships
        };
        workspace.getMembers.and.returnValue($q.resolve(response));
        workspace.removeMember.and.returnValue($q.resolve());

        var ctrl = buildController();

        // when
        ctrl.remove('USER-ID-TWO', event);
        $scope.$apply();

        // then
        expect(ctrl.currentPage.content).toBeArrayOfSize(1);
        expect(ctrl.currentPage.content[0].user.id).toBe('USER-ID-ONE');
        expect(event.stopPropagation).toHaveBeenCalled();
        expect($rootScope.$emit).toHaveBeenCalledWith('workspace.member:removed', 'USER-ID-TWO');
      });

      it('should promote a member', function () {
        // given
        spyOn($state, 'reload');
        var response = {
          content: memberships
        };
        workspace.getMembers.and.returnValue($q.resolve(response));
        workspace.promote.and.returnValue($q.resolve());

        var ctrl = buildController();

        // when
        ctrl.promote('USER-ID-ONE', event);
        $scope.$apply();

        // then
        expect(event.stopPropagation).toHaveBeenCalled();
        expect(workspace.promote).toHaveBeenCalledWith('USER-ID-ONE');
        expect($state.reload).toHaveBeenCalled();
      });

      it('should demote an admin', function () {
        // given
        spyOn($state, 'reload');
        var response = {
          content: memberships
        };
        workspace.getMembers.and.returnValue($q.resolve(response));
        workspace.demote.and.returnValue($q.resolve());

        var ctrl = buildController();

        // when
        ctrl.demote('USER-ID-ONE', event);
        $scope.$apply();

        // then
        expect(event.stopPropagation).toHaveBeenCalled();
        expect(workspace.demote).toHaveBeenCalledWith('USER-ID-ONE');
        expect($state.reload).toHaveBeenCalled();
      });

    });
  });

})();
