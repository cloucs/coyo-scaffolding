/* globals spyOn */

(function () {
  'use strict';

  var moduleName = 'coyo.registration';

  describe('module: ' + moduleName, function () {
    var $controller, $rootScope, $q, $scope, $state, authService, backendUrlService,
        RegistrationModel, passwordPattern, token;

    var EMAIL = 'not.your@business.com';

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$q_) {
      $controller = _$controller_;
      $rootScope = _$rootScope_;
      $q = _$q_;
      $scope = $rootScope.$new();
      authService = jasmine.createSpyObj('authService', ['isAuthenticated']);
      backendUrlService = jasmine.createSpyObj('backendUrlService', ['isSet']);
      $state = jasmine.createSpyObj('$state', ['go', 'transitionTo']);
      RegistrationModel = createResourceMock(['get', 'save']);
      passwordPattern = 'abc';
      token = 'someToken';
    }));

    var controllerName = 'RegisterExternalController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope,
          $state: $state,
          backendUrlService: backendUrlService,
          authService: authService,
          RegistrationModel: RegistrationModel,
          passwordPattern: passwordPattern,
          token: token
        });
      }

      describe('init', function () {

        it('should redirect to main if already logged in', function () {
          // given
          var ctrl = buildController();
          backendUrlService.isSet.and.returnValue(true);
          authService.isAuthenticated.and.returnValue(true);

          // when
          ctrl.$onInit();

          // then
          expect($state.go).toHaveBeenCalledWith('main');
        });

        it('should request registration data for the invitation token', function () {
          // given
          var ctrl = buildController();
          invitationTokenIsValid();

          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect(ctrl.loading).toBeFalsy();
          expect(RegistrationModel.get).toHaveBeenCalledWith(token);
        });

        it('should redirect to login if the user is already registered', function () {
          // given
          var ctrl = buildController();
          invitationTokenIsInvalid('ALREADY_REGISTERED');

          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect(RegistrationModel.get).toHaveBeenCalledWith(token);
          expect($state.go).toHaveBeenCalledWith('front.login', {errorCode: 'ALREADY_REGISTERED', username: EMAIL});
        });

        it('should show error if the invitation token has expired', function () {
          // given
          var ctrl = buildController();
          invitationTokenIsInvalid('INVITATION_EXPIRED');

          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect(ctrl.loading).toBeFalsy();
          expect(RegistrationModel.get).toHaveBeenCalledWith(token);
          expect(ctrl.user.email).toBe(EMAIL);
          expect(ctrl.error).toBe('MODULE.REGISTER-EXTERNAL.ERROR.INVITATION_EXPIRED');
        });

        it('should show unknown error if the error code is not set', function () {
          // given
          var ctrl = buildController();
          invitationTokenIsInvalid(null);

          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect(ctrl.loading).toBeFalsy();
          expect(RegistrationModel.get).toHaveBeenCalledWith(token);
          expect(ctrl.user.email).toBe(EMAIL);
          expect(ctrl.error).toBe('MODULE.REGISTER-EXTERNAL.ERROR.ERROR');
        });
      });

      describe('register', function () {

        it('should register an external user and redirect to login', function () {
          // given
          var ctrl = buildController();
          invitationTokenIsValid();
          ctrl.$onInit();
          $rootScope.$apply();
          RegistrationModel.save.and.returnValue($q.resolve());
          ctrl.user.password = 'myPass';

          // when
          ctrl.register();
          $rootScope.$apply();

          // then
          expect(ctrl.loading).toBeFalsy();
          expect(RegistrationModel.save).toHaveBeenCalled();
          expect($state.go).toHaveBeenCalledWith('front.login', {username: EMAIL, password: 'myPass'});
        });

        it('should show error if registration failed', function () {
          // given
          var ctrl = buildController();
          invitationTokenIsValid();
          ctrl.$onInit();
          $rootScope.$apply();
          RegistrationModel.save.and.returnValue($q.reject({data: {errorStatus: 'FOOBAR_HAPPENED'}}));
          ctrl.user.password = 'myPass';

          // when
          ctrl.register();
          $rootScope.$apply();

          // then
          expect(ctrl.loading).toBeFalsy();
          expect(RegistrationModel.save).toHaveBeenCalled();
          expect($state.go).not.toHaveBeenCalled();
          expect(ctrl.error).toBe('MODULE.REGISTER-EXTERNAL.ERROR.FOOBAR_HAPPENED');
        });
      });

      function invitationTokenIsValid() {
        backendUrlService.isSet.and.returnValue(true);
        authService.isAuthenticated.and.returnValue(false);
        RegistrationModel.get.and.returnValue($q.resolve({email: EMAIL}));
      }

      function invitationTokenIsInvalid(errorCode) {
        backendUrlService.isSet.and.returnValue(true);
        authService.isAuthenticated.and.returnValue(false);
        RegistrationModel.get.and.returnValue($q.reject({
          data: {
            errorStatus: errorCode,
            context: {
              email: EMAIL
            }
          },
          status: 400
        }));
      }
    });

    function createResourceMock(functionNames) {
      function Resource() {
        return Resource;
      }

      _.forEach(functionNames, function (fnName) {
        Resource[fnName] = function () {
        };
        spyOn(Resource, fnName);
      });
      return Resource;
    }
  });
})();
