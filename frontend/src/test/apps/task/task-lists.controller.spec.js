(function () {
  'use strict';

  var moduleName = 'coyo.apps.task';
  var controllerName = 'TaskListsController';

  describe('module: ' + moduleName, function () {
    var $controller, $rootScope, $scope, $state, TaskListModel, modalService, appRegistry, taskService;
    var app, root, lists;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$q_, _taskService_) {
      $controller = _$controller_;
      $rootScope = _$rootScope_;
      $scope = _$rootScope_.$new();
      taskService = _taskService_;

      $state = jasmine.createSpyObj('$state', ['go']);
      TaskListModel = jasmine.createSpyObj('TaskListModel', ['fromApp']);
      TaskListModel.fromApp.and.callFake(function (app, data) {
        var list = angular.extend(jasmine.createSpyObj('list', ['save']), app, data);
        list.save.and.returnValue(_$q_.resolve(angular.extend(list, {id: 'task-list-4'})));
        return list;
      });
      modalService = jasmine.createSpyObj('modalService', ['open']);
      modalService.open.and.callFake(function (data) {
        return {result: _$q_.resolve(data.resolve.list())};
      });
      appRegistry = jasmine.createSpyObj('appRegistry', ['getRootStateName']);
      root = 'main.page.show.apps.task';
      appRegistry.getRootStateName.and.returnValue(root);

      app = {senderId: 'senderId', appId: 'appId'};
      lists = [
        angular.extend(jasmine.createSpyObj('list', ['save']), {id: 'task-list-1', title: 'Task List #1'}),
        angular.extend(jasmine.createSpyObj('list', ['save']), {id: 'task-list-2', title: 'Task List #2'}),
        angular.extend(jasmine.createSpyObj('list', ['save']), {id: 'task-list-3', title: 'Task List #3'})];
      lists[0].save.and.returnValue(_$q_.resolve());
      lists[1].save.and.returnValue(_$q_.resolve());
      lists[2].save.and.returnValue(_$q_.resolve());
    }));

    function buildController() {
      return $controller(controllerName, {
        $rootScope: $rootScope,
        $scope: $scope,
        $state: $state,
        taskService: taskService,
        TaskListModel: TaskListModel,
        modalService: modalService,
        appRegistry: appRegistry,
        app: app,
        lists: lists
      });
    }

    describe('controller: ' + controllerName, function () {

      it('should create a list', function () {
        // given
        var ctrl = buildController();
        ctrl.listTitle = 'Task List #4';

        // when
        ctrl.createList();
        $rootScope.$apply();

        // then
        expect(lists.length).toEqual(4);
        expect(lists[3].id).toEqual('task-list-4');
        expect($state.go).toHaveBeenCalledWith('.list.details', {id: 'task-list-4'}, {relative: root});
      });

      it('should edit a list', function () {
        // given
        var ctrl = buildController();
        var list = lists[0];
        var $event = jasmine.createSpyObj('$event', ['preventDefault', 'stopImmediatePropagation']);

        // when
        ctrl.editList(list, $event);

        // then
        expect($event.preventDefault).toHaveBeenCalled();
        expect($event.stopImmediatePropagation).toHaveBeenCalled();
        $rootScope.$apply();
        expect(list.save).toHaveBeenCalled();
      });
    });
  });

})();
