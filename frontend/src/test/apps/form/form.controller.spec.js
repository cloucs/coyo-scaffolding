(function () {
  'use strict';

  var moduleName = 'coyo.apps.form';

  describe('module: ' + moduleName, function () {

    var $controller, app, fields, formService, $q, $scope, $state, createFieldModalService;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$q_, $rootScope) {
      $controller = _$controller_;
      $q = _$q_;
      $scope = $rootScope.$new();
      app = {
        id: 'app-id',
        senderId: 'sender-id',
        key: 'app-key',
        getWithPermissions: function () {
        }
      };
      fields = [{id: 'field-id'}];
      formService = jasmine.createSpyObj('formService', ['createEntry']);
      $state = jasmine.createSpyObj('$state', ['go']);
      createFieldModalService = jasmine.createSpyObj('createFieldModalService', ['open']);
    }));

    var controllerName = 'FormController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          app: app, fields: fields, formService: formService,
          $state: $state, createFieldModalService: createFieldModalService
        });
      }

      it('should initialize controller', function () {
        // given
        var ctrl = buildController();
        formService.createEntry.and.returnValue({});
        var permissions = {
          _permissions: {
            createEntry: true
          }
        };
        spyOn(app, 'getWithPermissions').and.returnValue($q.resolve(permissions));

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.app).toBe(app);
        expect(ctrl.fields).toBe(fields);
        expect(ctrl.formSubmitted).toBeFalsy();
        expect(ctrl.loading).toBeFalsy();
        expect(formService.createEntry).toHaveBeenCalledTimes(1);
      });

      it('should save the entry', function () {
        // given
        var ctrl = buildController();
        var entry = jasmine.createSpyObj('entry', ['save']);
        ctrl.app = app;
        var permissions = {
          _permissions: {
            createEntry: true
          }
        };

        ctrl.fields = fields;
        ctrl.entry = entry;

        entry.save.and.returnValue($q.resolve({}));
        spyOn(ctrl.app, 'getWithPermissions').and.returnValue($q.resolve(permissions));
        spyOn(angular, 'extend').and.returnValue($q.resolve());

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect(entry.save).toHaveBeenCalledTimes(1);
        expect(angular.extend).toHaveBeenCalled();
        expect(ctrl.formSubmitted).toBeTruthy();
      });

      it('should set formSubmitted to false when save was not successful', function () {
        // given
        var ctrl = buildController();
        var entry = jasmine.createSpyObj('entry', ['save']);
        ctrl.fields = fields;
        ctrl.app = app;
        ctrl.entry = entry;
        entry.save.and.returnValue($q.reject());

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect(entry.save).toHaveBeenCalledTimes(1);
        expect(ctrl.formSubmitted).toBeFalsy();
      });

      it('should go to field configuration', function () {
        // given
        var ctrl = buildController();
        createFieldModalService.open.and.returnValue($q.resolve({}));

        // when
        ctrl.createField();
        $scope.$apply();

        // then
        expect(createFieldModalService.open).toHaveBeenCalledTimes(1);
        expect($state.go).toHaveBeenCalledTimes(1);
      });
    });
  });
})();
