(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';

  describe('module: ' + moduleName, function () {

    var app, modalService;

    beforeEach(function () {

      app = jasmine.createSpyObj('app', ['save', 'prepareTranslationsForSave']);

      modalService = jasmine.createSpyObj('modalService', ['open']);
      modalService.open.and.returnValue({
        result: {
          then: function (callback) {
            return callback(app);
          }
        }
      });

      module(moduleName, function ($provide) {
        $provide.value('modalService', modalService);
      });

    });

    describe('controller: AppSettingsModalController', function () {

      var $scope, sender, $q, $controller, $log, $uibModalInstance, senderModel;

      beforeEach(function () {

        inject(function ($rootScope, _$q_, _$log_, _$controller_, _SenderModel_) {
          $scope = $rootScope.$new();
          $q = _$q_;
          $log = _$log_;
          $controller = _$controller_;
          sender = new _SenderModel_({
            id: 'SENDER-ID'
          });
        });

        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close']);

        spyOn(sender, 'initTranslations').and.callThrough();

        // required to mock functionality inside ctrl._remove()
        senderModel = jasmine.createSpyObj('senderModel', ['removeApp']);
        var senderDeferred = $q.defer();
        senderDeferred.resolve(app);
        senderModel.removeApp.and.returnValue(senderDeferred.promise);
      });

      function buildController() {
        return $controller('AppSettingsModalController', {
          app: app,
          sender: sender,
          $log: $log,
          $uibModalInstance: $uibModalInstance,
          SenderModel: function () {
            return senderModel;
          }
        });
      }

      it('should init controller with correct translation values', function () {
        // given
        spyOn(sender, 'isSenderTranslated').and.returnValue(true);
        spyOn(sender, 'getDefaultLanguage').and.returnValue('DE');
        sender.translations = {EN: {displayName: 'the-sender-name'}};
        app.name = 'der-app-name';
        app.translations = {EN: {name: 'the-app-name'}};

        // when
        var ctrl = buildController();
        ctrl.$onInit();

        // then
        expect(ctrl.isSenderTranslated).toBe(true);
        expect(sender.initTranslations).toHaveBeenCalledWith(ctrl);
        expect(ctrl.languages).toEqual(jasmine.objectContaining({
          EN: {active: true, translations: {name: 'the-app-name'}}
        }));
        expect(ctrl.languages).toEqual(jasmine.objectContaining({
          DE: {active: true, translations: {name: 'der-app-name'}}
        }));
      });

      it('should save app and close modal', function () {
        // given
        app.save.and.returnValue($q.resolve({
          id: 'TEST-ID'
        }));

        // when
        var ctrl = buildController();
        ctrl.save();
        $scope.$apply();

        // then
        expect(app.save).toHaveBeenCalled();
        expect($uibModalInstance.close).toHaveBeenCalledWith({id: 'TEST-ID'});
      });

      it('should delete app and close modal', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.confirmRemoveApp({preventDefault: _.noop});
        $scope.$apply();

        // then
        expect(senderModel.removeApp).toHaveBeenCalled();
        expect($uibModalInstance.close).toHaveBeenCalledWith(app);
        expect(app.deleted).toBe(true);
      });

      it('should update form validity state for specified language', function () {
        // given
        var ctrl = buildController();
        ctrl.languages = {
          DE: {valid: false}
        };

        // when
        ctrl.updateValidity('DE', true);

        // then
        expect(ctrl.languages.DE.valid).toBe(true);
      });

      it('should provide the multi language functionality required state', function () {
        // given
        spyOn(sender, 'isTranslationRequired').and.returnValue('anyBooleanValue');
        var ctrl = buildController();
        ctrl.languages = {
          EN: {name: 'the-name'}
        };
        ctrl.currentLanguage = 'EN';

        // when
        var actualResult = ctrl.isTranslationRequired('DE');

        // then
        expect(actualResult).toEqual('anyBooleanValue');
        expect(sender.isTranslationRequired).toHaveBeenCalled();
        var args = sender.isTranslationRequired.calls.mostRecent().args;
        expect(args[0]).toBe(ctrl.languages);
        expect(args[1]).toEqual('EN');
        expect(args[2]).toEqual('DE');
      });
    });
  });

})();
