(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';

  describe('module: ' + moduleName, function () {

    var appChooserModalService, modalService;

    beforeEach(function () {

      modalService = jasmine.createSpyObj('modalService', ['open']);
      modalService.open.and.returnValue({
        result: {
          then: angular.noop
        }
      });

      module(moduleName, function ($provide) {
        // provide dependencies for service
        $provide.value('modalService', modalService);
      });

      inject(function (_appChooserModalService_) {
        // inject dependencies for test
        appChooserModalService = _appChooserModalService_;
      });

    });

    describe('service: appChooserModalService', function () {

      it('should open modal', function () {
        // given
        var sender = {id: 'SENDER-ID'};
        var SenderModel = {
          get: function () {
            return sender;
          }
        };

        // when
        appChooserModalService.open(sender);

        // then
        expect(modalService.open).toHaveBeenCalled();
        var args = modalService.open.calls.mostRecent().args;
        expect(args[0].resolve.sender[1](SenderModel)).toEqual(sender);
      });

    });

  });

})();
