(function () {
  'use strict';

  var moduleName = 'coyo.apps.list';

  describe('module: ' + moduleName, function () {

    // the service's name
    var listService,
        ListEntryModel,
        fields,
        fieldValues;

    beforeEach(function () {

      module(moduleName);

      inject(function (_listService_, _ListEntryModel_) {
        listService = _listService_;
        ListEntryModel = _ListEntryModel_;
      });

      fields = [{
        id: 'field-test-1'
      }, {
        id: 'field-test-2'
      }];

      fieldValues = [{
        fieldId: 'field-test-1',
        value: undefined
      }, {
        fieldId: 'field-test-2',
        value: undefined
      }];
    });

    describe('Service: listService', function () {

      it('should create new entry with empty values array', function () {
        // given
        var expectedEntry = new ListEntryModel({
          values: []
        });

        // when
        var entry = listService.createEntry();

        // then
        expect(entry).toEqual(expectedEntry);
      });

      it('should initialize entry with values', function () {
        // given
        var entry = new ListEntryModel({
          values: [fieldValues[0]]
        });
        expect(entry.values).toEqual([fieldValues[0]]);

        // when
        var result = listService.initEntry(entry, fields);

        // then
        expect(result.values).toEqual(fieldValues);
      });

      it('should initialize multiple entries with values', function () {
        // given
        var entries = [new ListEntryModel({values: []}), new ListEntryModel({values: []})];

        // when
        var result = listService.initEntries(entries, fields);

        // then
        expect(result[0].values).toEqual(fieldValues);
        expect(result[1].values).toEqual(fieldValues);
      });

      it('should return field value from list entry', function () {
        // given
        fieldValues[1].value = 'Test';
        var entry = new ListEntryModel({
          values: fieldValues
        });

        // when
        var fieldValue = listService.getFieldValue(entry, 'field-test-2');

        // then
        expect(fieldValue).toBe(fieldValue);
      });

    });

  });
})();
