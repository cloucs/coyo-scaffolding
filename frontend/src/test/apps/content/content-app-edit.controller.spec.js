(function () {
  'use strict';

  var moduleName = 'coyo.apps.content';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $scope, $state, $timeout, app, sender, widgetLayoutService;

    beforeEach(function () {

      module(moduleName);

      inject(function (_$controller_, _$q_, $rootScope, _$timeout_) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = $rootScope.$new();
        $state = jasmine.createSpyObj('$state', ['go']);
        $timeout = _$timeout_;
        app = jasmine.createSpyObj('app', ['updateExistingTranslations', 'getTranslatedContent']);
        sender = {
          defaultLanguage: 'EN',
          translations: {DE: {}, EN: {}},
          getDefaultLanguage: function () {
            return 'EN';
          },
          isSenderTranslated: function () {
          },
          isTranslationRequired: function () {
          },
          getApp: function () {
            return $q.resolve(app);
          }
        };

        spyOn(sender, 'isSenderTranslated');
        spyOn(sender, 'isTranslationRequired');

        widgetLayoutService = jasmine.createSpyObj('widgetLayoutService', ['edit', 'save', 'cancel', 'onload']);
      });

    });

    var controllerName = 'ContentAppEditController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope,
          $state: $state,
          $timeout: $timeout,
          app: app,
          sender: sender,
          widgetLayoutService: widgetLayoutService
        });
      }

      it('should save', function () {
        // given
        var ctrl = buildController();
        widgetLayoutService.save.and.returnValue($q.resolve());
        app.updateExistingTranslations.and.returnValue($q.resolve());

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect($state.go).toHaveBeenCalledWith('^', {created: false});
      });

      it('should not save twice when rapidly called', function () {
        // given
        var ctrl = buildController();
        widgetLayoutService.save.and.returnValue($q.resolve());
        app.updateExistingTranslations.and.returnValue($q.resolve());

        // when
        ctrl.save();
        ctrl.save();
        $scope.$apply();

        // then
        expect($state.go).toHaveBeenCalledWith('^', {created: false});
        expect($state.go).toHaveBeenCalledTimes(1);
      });

      it('should cancel', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.cancel();
        $scope.$apply();

        // then
        expect(widgetLayoutService.cancel).toHaveBeenCalledWith($scope);
        expect($state.go).toHaveBeenCalledWith('^', {created: false});
      });

      it('should init', function () {
        // given
        var ctrl = buildController();
        app.getTranslatedContent.and.returnValue(['DE', 'EN']);
        widgetLayoutService.onload.and.returnValue($q.resolve());
        sender.isSenderTranslated.and.returnValue(true);

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.isSenderTranslated).toBe(true);
        expect(ctrl.defaultLanguage).toBe('EN');
        expect(ctrl.currentLanguage).toBe('EN');
        expect(ctrl.languages).toEqual(jasmine.objectContaining({
          DE: {
            'active': true,
            'translations': {
              'anyPlaceholder': 'anyValue'
            }
          }
        }));
        expect(ctrl.languages).toEqual(jasmine.objectContaining({
          EN: {
            'active': true,
            'translations': {
              'anyPlaceholder': 'anyValue'
            }
          }
        }));
        expect(widgetLayoutService.edit).toHaveBeenCalledWith($scope);
      });

      it('should delegate isTranslationRequired call to sender', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.isTranslationRequired('EN');

        // then
        expect(sender.isTranslationRequired).toHaveBeenCalledWith(ctrl.languages, ctrl.currentLanguage, 'EN');
      });

      it('should build layout name for default language', function () {
        // given
        widgetLayoutService.onload.and.returnValue($q.resolve());
        app.id = 'app-identifier';

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        var actualLayoutName = ctrl.buildLayoutName('EN');

        // then
        expect(actualLayoutName).toBe('app-content-app-identifier');
      });

      it('should build layout name for non default language', function () {
        // given
        widgetLayoutService.onload.and.returnValue($q.resolve());
        app.id = 'app-identifier';

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        var actualLayoutName = ctrl.buildLayoutName('DE');

        // then
        expect(actualLayoutName).toBe('app-content-app-identifier-DE');
      });
    });
  });

})();
