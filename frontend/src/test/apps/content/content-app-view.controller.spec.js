(function () {
  'use strict';

  var moduleName = 'coyo.apps.content';

  describe('module: ' + moduleName, function () {

    var $q, $rootScope, $controller, $stateParams, $state, app, sender, currentUser, SettingsModel;

    beforeEach(module(moduleName), function ($provide) {
      SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieve']);
      SettingsModel.retrieve.and.returnValue({
        then: function (callback) {
          callback({defaultLanguage: 'EN'});
        }
      });
      $provide.value('SettingsModel', SettingsModel);
    });

    beforeEach(inject(function (_$q_, _$rootScope_, _$controller_) {
      $q = _$q_;
      $rootScope = _$rootScope_;
      $controller = _$controller_;
      $state = jasmine.createSpyObj('$state', ['go']);
      currentUser = jasmine.createSpyObj('currentUser', ['getBestSuitableLanguage']);
      app = {
        id: 'app-identifier', getTranslatedContent: function () {
          return ['DE'];
        }
      };
      sender = {defaultLanguage: 'EN'};
    }));

    var controllerName = 'ContentAppViewController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $stateParams: $stateParams,
          $state: $state,
          app: app,
          sender: sender,
          currentUser: currentUser
        });
      }

      it('should init new created content app', function () {
        // given
        $stateParams = {created: true};
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect($state.go).toHaveBeenCalledWith('.edit', {created: true});
      });

      it('should init existing content app', function () {
        // given
        $stateParams = {created: false};
        var ctrl = buildController();
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve('EN'));

        // when
        ctrl.$onInit();
        $rootScope.$apply();

        // then
        expect(currentUser.getBestSuitableLanguage).toHaveBeenCalledWith(['DE', 'EN'], jasmine.any(Function));
      });

      it('should build layout name for default language', function () {
        // given
        $stateParams = {created: false};
        var ctrl = buildController();
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve('EN'));

        // when
        ctrl.$onInit();
        $rootScope.$apply();
        var actualLayoutName = ctrl.buildLayoutName();

        // then
        expect(actualLayoutName).toBe('app-content-app-identifier');
      });

      it('should build layout name with user language', function () {
        // given
        $stateParams = {created: false};
        var ctrl = buildController();
        currentUser.getBestSuitableLanguage.and.returnValue($q.resolve('DE'));

        // when
        ctrl.$onInit();
        $rootScope.$apply();
        var actualLayoutName = ctrl.buildLayoutName();

        // then
        expect(actualLayoutName).toBe('app-content-app-identifier-DE');
      });
    });
  });

})();
