(function () {
  'use strict';

  var moduleName = 'coyo.apps.forum';

  describe('module: ' + moduleName, function () {

    // the service's name
    var $scope, $q, forumThreadService, modalService, thread, answer;

    beforeEach(function () {
      thread = jasmine.createSpyObj('ForumThreadModel', ['setPinned', 'setClosed', 'delete']);
      thread.id = 'THREAD-ID';
      thread.title = 'THREAD-TITLE';
      answer = jasmine.createSpyObj('ForumThreadAnswerModel', ['delete']);
      answer.id = 'THREAD-ANSWER-ID';
      answer.typeName = 'forum-thread-answer';

      modalService = jasmine.createSpyObj('modalService', ['confirm', 'confirmDelete']);

      module(moduleName, function ($provide) {
        // provide dependencies for service
        $provide.value('modalService', modalService);
      });

      inject(function (_forumThreadService_, _$q_, $rootScope) {
        // inject dependencies for test
        forumThreadService = _forumThreadService_;
        $q = _$q_;
        $scope = $rootScope.$new();
      });

    });

    describe('Service: forumThreadService', function () {

      it('should pin a thread', function () {
        // given
        thread.setPinned.and.returnValue($q.resolve());

        // when
        forumThreadService.pin(thread);
        $scope.$apply();

        // then
        expect(thread.setPinned).toHaveBeenCalledWith(true);
      });

      it('should unpin a thread', function () {
        // given
        thread.setPinned.and.returnValue($q.resolve());

        // when
        forumThreadService.unpin(thread);
        $scope.$apply();

        // then
        expect(thread.setPinned).toHaveBeenCalledWith(false);
      });

      it('should close a thread', function () {
        // given
        modalService.confirm.and.returnValue({result: $q.resolve()});
        thread.setClosed.and.returnValue($q.resolve());

        // when
        forumThreadService.close(thread);
        $scope.$apply();

        // then
        expect(modalService.confirm).toHaveBeenCalled();
        expect(thread.setClosed).toHaveBeenCalledWith(true);
      });

      it('should reopen a thread', function () {
        // given
        modalService.confirm.and.returnValue({result: $q.resolve()});
        thread.setClosed.and.returnValue($q.resolve());

        // when
        forumThreadService.reopen(thread);
        $scope.$apply();

        // then
        expect(modalService.confirm).not.toHaveBeenCalled();
        expect(thread.setClosed).toHaveBeenCalledWith(false);
      });

      it('should delete a thread', function () {
        // given
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});
        thread.delete.and.returnValue($q.resolve());

        // when
        forumThreadService.delete(thread);
        $scope.$apply();

        // then
        expect(modalService.confirmDelete).toHaveBeenCalled();
        expect(thread.delete).toHaveBeenCalled();
      });

      it('should delete a thread answer', function () {
        // given
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});
        answer.delete.and.returnValue($q.resolve());

        // when
        forumThreadService.deleteAnswer(answer);
        $scope.$apply();

        // then
        expect(modalService.confirmDelete).toHaveBeenCalled();
        expect(answer.delete).toHaveBeenCalled();
      });

    });

  });
})();
