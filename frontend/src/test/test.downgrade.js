(function () {
  'use strict';

  var mockedFactory = _.constant({});

  var ngxErrorService = function () {
    return jasmine.createSpyObj('ngxErrorService', ['getMessage', 'suppressNotification',
      'showErrorPage', 'isNotificationSuppressed', 'getValidationErrors']);
  };

  // Add your downgraded service mocks here
  angular.module('commons.i18n').factory('translationService', mockedFactory);

  angular.module('commons.error').factory('ngxErrorService', ngxErrorService);
})();
