(function () {
  'use strict';

  var moduleName = 'coyo.widgets.api';

  describe('module: ' + moduleName, function () {

    var widgetLayoutService, $scope, $q, $timeout, $rootScope, stateLockService;

    beforeEach(function () {

      stateLockService = jasmine.createSpyObj('stateLockService', ['lock', 'unlock']);

      module(moduleName, function ($provide) {
        $provide.value('stateLockService', stateLockService);
      });

      inject(function (_widgetLayoutService_, _$q_, _$timeout_, _$rootScope_) {
        widgetLayoutService = _widgetLayoutService_;
        $q = _$q_;
        $timeout = _$timeout_;
        $rootScope = _$rootScope_;
        $scope = jasmine.createSpyObj('$scope', ['$broadcast', '$on']);
      });

    });

    describe('Service: widgetLayoutService', function () {

      it('should edit trigger event', function () {
        // when
        widgetLayoutService.edit($scope, true);

        // then
        expect($scope.$broadcast).toHaveBeenCalledWith('widget-slot:edit', true);
        expect($rootScope.globalEditMode).toBe(true);
        expect(stateLockService.lock).toHaveBeenCalled();
      });

      it('should cancel trigger event', function () {
        // when
        widgetLayoutService.cancel($scope, true);

        // then
        expect($rootScope.globalEditMode).toBe(false);
        expect($scope.$broadcast).toHaveBeenCalledWith('widget-slot:cancel', true);
        expect(stateLockService.unlock).toHaveBeenCalled();
      });

      it('should save successfully', function () {
        // given
        var deferred = $q.defer();
        $scope.$broadcast.and.callFake(function (event, promises) {
          promises.push(deferred.promise);
        });

        // when
        var thenCalled = false;
        var catchCalled = false;
        var finallyCalled = false;
        widgetLayoutService.save($scope, true).then(function () {
          thenCalled = true;
        }).catch(function () {
          catchCalled = true;
        }).finally(function () {
          finallyCalled = true;
        });

        $rootScope.$apply();
        $timeout.flush();

        // then
        expect(thenCalled).toBe(false);
        expect(catchCalled).toBe(false);
        expect(finallyCalled).toBe(false);

        // when
        deferred.resolve();
        $rootScope.$apply();

        // then
        expect(thenCalled).toBe(true);
        expect(catchCalled).toBe(false);
        expect(finallyCalled).toBe(true);
        expect($rootScope.globalEditMode).toBe(false);
        expect(stateLockService.unlock).toHaveBeenCalled();
      });

      it('should save with error', function () {
        // given
        var deferred = $q.defer();
        $scope.$broadcast.and.callFake(function (event, promises) {
          promises.push(deferred.promise);
        });
        $rootScope.globalEditMode = true;

        // when
        var thenCalled = false;
        var catchCalled = false;
        var finallyCalled = false;
        widgetLayoutService.save($scope, true).then(function () {
          thenCalled = true;
        }).catch(function () {
          catchCalled = true;
        }).finally(function () {
          finallyCalled = true;
        });

        $rootScope.$apply();
        $timeout.flush();

        // then
        expect(thenCalled).toBe(false);
        expect(catchCalled).toBe(false);
        expect(finallyCalled).toBe(false);

        // when
        deferred.reject();
        $rootScope.$apply();

        // then
        expect(thenCalled).toBe(false);
        expect(catchCalled).toBe(true);
        expect(finallyCalled).toBe(true);
        expect($rootScope.globalEditMode).toBe(true);
        expect(stateLockService.unlock).not.toHaveBeenCalled();
      });

      it('should save with non-rejecting error', function () {
        // given
        var deferred = $q.defer();
        $scope.$broadcast.and.callFake(function (event, promises) {
          promises.push(deferred.promise);
        });
        $scope.$on.and.callFake(function (event, cb) {
          if (event === 'widget-slot:save-error') {
            cb();
          }
        });
        $rootScope.globalEditMode = true;

        // when
        var thenCalled = false;
        var catchCalled = false;
        var finallyCalled = false;
        widgetLayoutService.save($scope, true).then(function () {
          thenCalled = true;
        }).catch(function () {
          catchCalled = true;
        }).finally(function () {
          finallyCalled = true;
        });

        $rootScope.$apply();
        $timeout.flush();

        // then
        expect(thenCalled).toBe(false);
        expect(catchCalled).toBe(false);
        expect(finallyCalled).toBe(false);

        // when
        deferred.resolve();
        $rootScope.$apply();

        // then
        expect(thenCalled).toBe(false);
        expect(catchCalled).toBe(true);
        expect(finallyCalled).toBe(true);
        expect($rootScope.globalEditMode).toBe(true);
        expect(stateLockService.unlock).not.toHaveBeenCalled();
      });

    });

  });
})();
