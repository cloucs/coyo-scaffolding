(function () {
  'use strict';

  var moduleName = 'coyo.widgets.api';
  var targetName = 'WidgetLayoutRowChooserController';

  describe('module: ' + moduleName, function () {
    var $controller;

    var $uibModalInstance = {
      close: angular.noop
    };

    var name = 'app-content-123';

    var layout = {
      name: name,
      settings: {
        rows: [
          {slots: [{name: 'slot1', cols: 12}]}
        ]
      },
      update: angular.noop
    };

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_) {
        $controller = _$controller_;
      });
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl(index) {
        return $controller(targetName, {
          layout: layout,
          index: index,
          $uibModalInstance: $uibModalInstance
        }, {});
      }

      it('should init controller', function () {
        // given

        // when
        var ctrl = buildCtrl(0);

        // then
        expect(ctrl.layout).toBe(layout);
        expect(ctrl.index).toBe(0);
        expect(ctrl.rowTypes.length).toBeGreaterThan(0);
      });

      it('should save new row', function () {
        // given
        var ctrl = buildCtrl(0);

        // when
        ctrl.save({
          slots: [1, 11]
        });

        // then
        expect(layout.settings.rows.length).toBe(2);
        expect(layout.settings.rows[0].slots[0].cols).toBe(1);
        expect(layout.settings.rows[0].slots[1].cols).toBe(11);
      });
    });
  });
})();
