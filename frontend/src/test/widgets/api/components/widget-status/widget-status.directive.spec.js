(function () {
  'use strict';

  var moduleName = 'coyo.widgets.api';
  var directiveName = 'coyoWidgetStatus';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $rootScope, errorService, $translate, $scope;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $q = _$q_;
        errorService = jasmine.createSpyObj('errorService', ['getMessage', 'suppressNotification']);
        errorService.getMessage.and.returnValue($q.resolve('Error Message'));
        $translate = function (key) {
          return $q.resolve(key);
        };
      }));

      var controllerName = 'WidgetStatusController';

      describe('controller: ' + controllerName, function () {

        function buildController(loadFn) {
          return $controller(controllerName, {
            errorService: errorService,
            $translate: $translate,
            $scope: $scope
          }, {loadData: loadFn});
        }

        it('should enable loading indicator on init', function () {
          // given
          var loadFn = function () {
            return $q.resolve();
          };

          // when
          var ctrl = buildController(loadFn);

          // then
          expect(ctrl.loading).toBe(true);
        });

        it('should disable loading indicator on success', function () {
          // given
          var ctrl = buildController(function () {
            return $q.resolve();
          });

          // when
          ctrl.loadData();
          $rootScope.$apply();

          // then
          expect(ctrl.loading).toBe(false);
        });

        it('should disable loading indicator on error', function () {
          // given
          var ctrl = buildController(function () {
            return $q.reject({});
          });

          // when
          ctrl.loadData();
          $rootScope.$apply();

          // then
          expect(ctrl.loading).toBe(false);
        });

        it('should suppress notifications on error', function () {
          // given
          var errorResponse = {};
          var ctrl = buildController(function () {
            return $q.reject(errorResponse);
          });

          // when
          ctrl.loadData();
          $rootScope.$apply();

          // then
          expect(errorService.suppressNotification).toHaveBeenCalledWith(errorResponse);
        });

        it('should enable permission error on 403 response', function () {
          // given
          var loadFn = function () {
            return $q.reject({status: 403});
          };

          // when
          var ctrl = buildController(loadFn);
          $rootScope.$apply();

          // then
          expect(ctrl.errorMessage).toBe('WIDGETS.PERMISSION_ERROR');
        });

        it('should not enable permission error on non-403 response', function () {
          // given
          var loadFn = function () {
            return $q.reject({status: 400});
          };

          // when
          var ctrl = buildController(loadFn);
          $rootScope.$apply();

          // then
          expect(ctrl.permissionsError).not.toBe(true);
        });

        it('should indicate no result on empty response', function () {
          // given
          var ctrl = buildController(function () {
            return $q.resolve();
          });

          // when
          ctrl.loadData();
          $rootScope.$apply();

          // then
          expect(ctrl.emptyResult).toBe(true);
        });

        it('should not indicate no result on non-empty response', function () {
          // given
          var ctrl = buildController(function () {
            return $q.resolve({});
          });

          // when
          ctrl.loadData();
          $rootScope.$apply();

          // then
          expect(ctrl.emptyResult).not.toBe(true);
        });

        it('should indicate no result on empty array response', function () {
          // given
          var ctrl = buildController(function () {
            return $q.resolve([]);
          });

          // when
          ctrl.loadData();
          $rootScope.$apply();

          // then
          expect(ctrl.emptyResult).toBe(true);
        });

        it('should not indicate no result on non-empty array response', function () {
          // given
          var ctrl = buildController(function () {
            return $q.resolve(['result']);
          });

          // when
          ctrl.loadData();
          $rootScope.$apply();

          // then
          expect(ctrl.emptyResult).not.toBe(true);
        });

        it('should indicate no result on empty content array response', function () {
          // given
          var ctrl = buildController(function () {
            return $q.resolve({content: []});
          });

          // when
          ctrl.loadData();
          $rootScope.$apply();

          // then
          expect(ctrl.emptyResult).toBe(true);
        });

        it('should not indicate no result on non-empty content array response', function () {
          // given
          var ctrl = buildController(function () {
            return $q.resolve({content: ['result']});
          });

          // when
          ctrl.loadData();
          $rootScope.$apply();

          // then
          expect(ctrl.emptyResult).not.toBe(true);
        });

        it('should reload data on event', function () {
          // given
          var loadFn = jasmine.createSpy('loadFn');
          loadFn.and.returnValue($q.resolve());
          buildController(loadFn);

          // when
          $scope.$broadcast('widgetStatus:refresh');

          // then
          expect(loadFn).toHaveBeenCalledTimes(2);
        });

      });

    });

  });
})();
