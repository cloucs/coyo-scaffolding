(function () {
  'use strict';

  var moduleName = 'coyo.widgets.api';

  describe('module: ' + moduleName, function () {

    var widgetRegistryProvider, widgetRegistry, WidgetConfigurationModel, $scope, $q;

    beforeEach(function () {

      // initialize the service provider by injecting it to a fake module's config block
      angular.module('module.test', ['coyo.widgets.api']);
      angular.module('module.test').config(function (_widgetRegistryProvider_) {
        widgetRegistryProvider = _widgetRegistryProvider_;

        widgetRegistryProvider.register({
          key: 'test-widget-one',
          name: 'Test Widget One',
          description: 'A test widget.',
          directive: 'example-widget-one',
          categories: 'personal',
          settings: {
            controller: 'TestWidgetSettingsController',
            templateUrl: 'test-widget-settings.html'
          }
        });

        widgetRegistryProvider.register({
          key: 'test-widget-two',
          name: 'Test Widget Two',
          description: 'Another test widget.',
          directive: 'example-widget-two',
          categories: 'dynamic'
        });

        widgetRegistryProvider.register({
          key: 'test-widget-three-whitelistExternal',
          name: 'Test Widget whitelistExternal',
          description: 'Another test widget. (whitelistExternal)',
          directive: 'example-widget-three',
          categories: 'dynamic',
          whitelistExternal: true
        });

      });

      WidgetConfigurationModel = jasmine.createSpyObj('WidgetConfigurationModel', ['query']);

      // initialize test.app injector
      module('module.test', function ($provide) {
        $provide.value('WidgetConfigurationModel', WidgetConfigurationModel);
      });
    });

    // inject the actual service
    beforeEach(inject(function (_widgetRegistry_, $rootScope, _$q_) {
      widgetRegistry = _widgetRegistry_;
      $q = _$q_;
      $scope = $rootScope.$new();
    }));

    describe('Service: widgetRegistry', function () {

      it('should get all registered widgets', function () {
        // when
        var widgets = widgetRegistry.getAll();

        // then
        expect(widgets).toBeArrayOfSize(3);

        expect(widgets[0].name).toBe('Test Widget One');
        expect(widgets[0].key).toBe('test-widget-one');
        expect(widgets[0].directive).toBe('example-widget-one');
        expect(widgets[0].settings.controller).toBe('TestWidgetSettingsController');
        expect(widgets[0].settings.templateUrl).toBe('test-widget-settings.html');

        expect(widgets[1].name).toBe('Test Widget Two');
        expect(widgets[1].key).toBe('test-widget-two');
        expect(widgets[1].directive).toBe('example-widget-two');
        expect(widgets[1].settings).toBeUndefined();
      });

      it('should return the config of one widget', function () {
        // given
        var key = 'test-widget-one';

        // when
        var config = widgetRegistry.get(key);

        // then
        expect(config.name).toBe('Test Widget One');
        expect(config.key).toBe('test-widget-one');
        expect(config.directive).toBe('example-widget-one');
        expect(config.settings.controller).toBe('TestWidgetSettingsController');
        expect(config.settings.templateUrl).toBe('test-widget-settings.html');
      });

      it('should include whitelistExternal attribute', function () {

        // given
        var key = 'test-widget-three-whitelistExternal';

        // when
        var config = widgetRegistry.get(key);

        // then
        expect(config.whitelistExternal).not.toBeUndefined();
        expect(config.whitelistExternal).toEqual(true);
      });

      it('should return null if requested widget is not available', function () {
        // when
        var widget = widgetRegistry.get('unknown');

        // then
        expect(widget).toBeNull();
      });

      it('should get enabled widgets', function () {
        // given
        var enabledWidgets = [];
        var deferred = $q.defer();
        deferred.resolve([{key: 'test-widget-one'}]);
        WidgetConfigurationModel.query.and.returnValue(deferred.promise);

        // when
        widgetRegistry.getEnabled().then(function (widgets) {
          enabledWidgets = widgets;
        });
        $scope.$apply();

        // then
        expect(WidgetConfigurationModel.query).toHaveBeenCalled();
        expect(enabledWidgets).toBeArrayOfSize(1);
        expect(enabledWidgets[0].key).toBe('test-widget-one');
      });

      it('should cache enabled widgets', function () {
        // given
        var firstCall = [];
        var secondCall = [];

        var deferred = $q.defer();
        deferred.resolve([{key: 'test-widget-one'}]);
        WidgetConfigurationModel.query.and.returnValue(deferred.promise);

        // when
        widgetRegistry.getEnabled().then(function (widgets) {
          firstCall = widgets;
        });
        widgetRegistry.getEnabled().then(function (widgets) {
          secondCall = widgets;
        });
        $scope.$apply();

        // then
        expect(WidgetConfigurationModel.query).toHaveBeenCalledTimes(1);
        expect(firstCall).toBeArrayOfSize(1);
        expect(firstCall[0].key).toBe('test-widget-one');
        expect(firstCall).toEqual(secondCall);
      });

      it('should reset enabled widgets if call failed', function () {
        // given
        var enabledWidgets = undefined;
        var errorResult = {};

        var deferred = $q.defer();
        deferred.reject({error: 'error'});
        WidgetConfigurationModel.query.and.returnValue(deferred.promise);

        // when
        widgetRegistry.getEnabled().then(function (widgets) {
          enabledWidgets = widgets;
        }, function (error) {
          errorResult = error;
        });
        $scope.$apply();

        // then
        expect(WidgetConfigurationModel.query).toHaveBeenCalled();
        expect(enabledWidgets).toBeUndefined();
        expect(errorResult).toEqual({error: 'error'});
      });

    });

  });

  describe('Module: ' + moduleName, function () {

    describe('Provider: widgetRegistryProvider', function () {

      // Create a fake module with the provider. You can use this method in any test
      function createModule(widgetConfig) {
        angular.module('module.test', ['coyo.widgets.api']);
        angular.module('module.test').config(function (_widgetRegistryProvider_) {
          // Call methods on the provider in config phase here
          _widgetRegistryProvider_.register(widgetConfig);
        });

        module('module.test');
        inject(function () {
        });
      }

      it('should register a valid widget', function () {
        // given
        var config = {
          key: 'test-widget',
          name: 'Test Widget',
          description: 'A test widget',
          directive: 'example-widget',
          categories: 'dynamic'
        };

        // when
        createModule(config);

        // then -> perform without error
      });

      it('should fail if key is missing', function () {
        // given
        var config = {
          name: 'Test Widget',
          description: 'A test widget',
          directive: 'example-widget',
          categories: 'dynamic'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "key" is required/);
      });

      it('should fail if name is missing', function () {
        // given
        var config = {
          key: 'test-widget',
          description: 'A test widget',
          directive: 'example-widget',
          categories: 'dynamic'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "name" is required/);
      });

      it('should fail if description is missing', function () {
        // given
        var config = {
          key: 'test-widget',
          name: 'Test Widget',
          directive: 'example-widget',
          categories: 'dynamic'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "description" is required/);
      });

      it('should fail if categories are missing', function () {
        // given
        var config = {
          key: 'test-widget',
          description: 'A test widget',
          name: 'Test Widget',
          directive: 'example-widget'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "categories" is required/);
      });

      it('should fail if directive is missing', function () {
        // given
        var config = {
          key: 'test-widget',
          name: 'Test Widget',
          description: 'A test widget',
          categories: 'dynamic'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "directive" is required/);
      });

      it('should fail if icon is not zmdi-prefixed', function () {
        // given
        var config = {
          key: 'test-widget',
          name: 'Test Widget',
          description: 'A test widget',
          directive: 'example-widget',
          icon: 'wrong-icon',
          categories: 'dynamic'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "icon" must be a Material Design Iconic Font class/);
      });

      it('should fail if skipOnCreate is not a boolean', function () {
        // given
        var config = {
          key: 'test-widget',
          name: 'Test Widget',
          description: 'A test widget',
          directive: 'example-widget',
          categories: 'dynamic',
          settings: {
            skipOnCreate: 'abc'
          }
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "skipOnCreate" must be a boolean/);
      });

    });

  });

})();
