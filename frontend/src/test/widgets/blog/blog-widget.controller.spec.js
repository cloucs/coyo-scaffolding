(function () {
  'use strict';

  var moduleName = 'coyo.widgets.blog';
  var directiveName = 'coyoBlogWidget';

  describe('module: ' + moduleName, function () {

    var $controller, BlogWidgetModel, targetService, $q, widget, $rootScope, $scope, widgetStatusService;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $rootScope.screenSize = {
          isXs: false,
          isSm: false,
          isMd: true,
          isLg: false,
          isRetina: true
        };
        $scope = $rootScope.$new();
        BlogWidgetModel = jasmine.createSpyObj('BlogWidgetModel', ['getLatest']);
        BlogWidgetModel.getLatest.and.returnValue($q.resolve());
        targetService = jasmine.createSpyObj('targetService', ['getLink']);
        targetService.getLink.and.returnValue('/some-link');
        widgetStatusService = jasmine.createSpyObj('widgetStatusService', ['refreshOnSettingsChange']);
        widget = {settings: {}};
      }));

      var controllerName = 'BlogWidgetController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {
            BlogWidgetModel: BlogWidgetModel,
            targetService: targetService,
            $scope: $scope,
            widgetStatusService: widgetStatusService
          }, {
            widget: widget
          });
        }

        it('should load articles', function () {
          // given
          var articles = ['article'];
          widget.settings._sourceSelection = 'ALL';
          widget.settings._articleCount = 10;
          widget.settings._showTeaserImage = false;
          widget.settings._app = [{'id': 'AppId'}];
          BlogWidgetModel.getLatest.and.returnValue($q.resolve(articles));
          var ctrl = buildController();

          // when
          ctrl.loadArticles();
          $rootScope.$apply();

          // then
          expect(ctrl.articles).toEqual(articles);
          expect(BlogWidgetModel.getLatest).toHaveBeenCalledWith('ALL', 10, false, ['AppId']);
        });

        it('should register reload on settings change', function () {
          // when
          buildController();

          // then
          expect(widgetStatusService.refreshOnSettingsChange).toHaveBeenCalledWith($scope);
        });

        it('should call targetService.getLink', function () {
          // given
          var articles = [{
            articleTarget: {params: {id: 'articleId'}},
            senderTarget: {params: {id: 'senderId'}},
            appTarget: {params: {id: 'appId'}}
          }];
          BlogWidgetModel.getLatest.and.returnValue($q.resolve(articles));
          var ctrl = buildController();

          // when
          ctrl.loadArticles();
          $rootScope.$apply();

          // then
          expect(targetService.getLink).toHaveBeenCalled();
        });

        it('should build link object', function () {
          // given
          var articleId = 'a9asd-123sf';
          var senderId = '98jsf-33agz';
          var appId = 'xczss-18hjk';
          var articles = [{
            articleTarget: {params: {id: articleId}},
            senderTarget: {params: {id: senderId}},
            appTarget: {params: {id: appId}}
          }];
          BlogWidgetModel.getLatest.and.returnValue($q.resolve(articles));
          var ctrl = buildController();

          // when
          ctrl.loadArticles();
          $rootScope.$apply();

          // then
          expect(ctrl.links).not.toBeUndefined();
          expect(ctrl.links.articles[articleId]).toEqual('/some-link');
          expect(ctrl.links.senders[senderId]).toEqual('/some-link');
          expect(ctrl.links.apps[appId]).toEqual('/some-link');
        });
      });

    });

  });
})();
