(function () {
  'use strict';

  var moduleName = 'coyo.widgets.poll';
  var directiveName = 'coyoPollWidget';

  describe('module: ' + moduleName, function () {

    var $controller, PollWidgetModel, modalService, $q, widget, $rootScope, $scope;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        PollWidgetModel = jasmine.createSpyObj('PollWidgetModel',
            ['getSelectedAnswers', 'selectAnswer', 'getVotes', 'getVoters', 'deleteAnswer']);
        PollWidgetModel.getSelectedAnswers.and.returnValue($q.resolve([{id: 'RealId1', optionId: '1'}]));
        PollWidgetModel.selectAnswer.and.returnValue($q.resolve({id: 'RealId2', optionId: '2'}));
        PollWidgetModel.getVotes.and.returnValue($q.resolve([{optionId: '1', count: 100}]));
        PollWidgetModel.getVoters.and.returnValue($q.resolve({}));
        PollWidgetModel.deleteAnswer.and.returnValue($q.resolve({}));
        modalService = jasmine.createSpyObj('modalService', ['open']);
        modalService.open.and.returnValue($q.resolve(true));
        widget = {
          isNew: function () {
            return false;
          },
          settings: {
            _options: [{
              answer: 'test', id: 1
            }, {answer: 'test1', id: 2}],
            _showResults: true,
            _anonymous: false,
            question: 'test',
            _maxAnswers: 2
          },
          id: 10
        };
      }));

      var controllerName = 'pollWidgetController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {
            PollWidgetModel: PollWidgetModel,
            modalService: modalService,
            $scope: $scope
          }, {
            widget: widget
          });
        }

        it('should load selected Answers by default', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();
          // when
          $rootScope.$apply();

          // then
          expect(ctrl.checkableOptions[0].checked).toEqual(true);
          expect(PollWidgetModel.getSelectedAnswers).toHaveBeenCalledWith(10);
        });

        it('should delete a selection', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();

          //when
          $rootScope.$apply();
          ctrl.toggleOption(ctrl.checkableOptions[0]);
          $rootScope.$apply();

          // then
          expect(ctrl.checkableOptions[0].checked).toEqual(false);
          expect(ctrl.answers.length).toBe(0);
          expect(PollWidgetModel.deleteAnswer).toHaveBeenCalledWith(10, 'RealId1');
        });

        it('should add an selected answer', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();

          //when
          $rootScope.$apply();
          ctrl.toggleOption(ctrl.checkableOptions[1]);
          $rootScope.$apply();
          // then
          expect(ctrl.checkableOptions[1].checked).toEqual(true);
          expect(PollWidgetModel.selectAnswer).toHaveBeenCalledWith(10, 2);
        });

        it('should get correct number of votes', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();

          $rootScope.$apply();

          // then
          expect(PollWidgetModel.getVotes).toHaveBeenCalledWith(10);
          expect(ctrl.checkableOptions[0].votes).toBe(100);
          expect(ctrl.checkableOptions[1].votes).toBe(0);
        });

        it('should show voters in a modal', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();

          $rootScope.$apply();
          ctrl.showVoters(ctrl.widget.settings._options[0]);
          $rootScope.$apply();

          // then
          expect(modalService.open).toHaveBeenCalled();
        });

      });

    });

  });
})();
