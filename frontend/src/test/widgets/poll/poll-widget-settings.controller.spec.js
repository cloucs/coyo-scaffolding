(function () {
  'use strict';

  var moduleName = 'coyo.widgets.poll';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, PollWidgetModel, $timeout, $document, widget, $q;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$timeout_, _$document_, _$q_) {
      $controller = _$controller_;
      $scope = _$rootScope_.$new();
      $timeout = _$timeout_;
      $document = _$document_;
      $q = _$q_;
      PollWidgetModel = jasmine.createSpyObj('PollWidgetModel', ['getVotes']);
      PollWidgetModel.getVotes.and.returnValue($q.resolve([{optionId: '1', count: 100}]));
      widget = {
        isNew: function () {
          return true;
        },
        settings: {}
      };
      $scope.model = widget;
      $scope.saveCallbacks = {
        onBeforeSave: function () {
          return $q.resolve;
        }
      };
    }));

    var controllerName = 'PollWidgetSettingsController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope,
          $timeout: $timeout,
          $document: $document,
          PollWidgetModel: PollWidgetModel,
          $q: $q
        });
      }

      it('should initialize default setting', function () {
        // given
        var ctrl = buildController();

        // then
        expect(ctrl.model.settings._options.length).toBe(1);
        expect(ctrl.model.settings._maxAnswers).toBe(1);
        expect(ctrl.model.settings._showResults).toBe(true);
        expect(PollWidgetModel.getVotes).not.toHaveBeenCalled();
      });

    });
  });

})();
