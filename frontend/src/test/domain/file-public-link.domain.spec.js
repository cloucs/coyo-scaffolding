(function () {
  'use strict';

  describe('domain: FilePublicLinkModel', function () {
    beforeEach(module('commons.target'));
    beforeEach(module('coyo.domain'));

    var $httpBackend, FilePublicLinkModel, backendUrlService, coyoEndpoints;

    beforeEach(inject(function (_$httpBackend_, _FilePublicLinkModel_, _backendUrlService_, _coyoEndpoints_) {
      $httpBackend = _$httpBackend_;
      FilePublicLinkModel = _FilePublicLinkModel_;
      backendUrlService = _backendUrlService_;
      coyoEndpoints = _coyoEndpoints_;
      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('class members', function () {

      it('should get a link', function () {
        // given
        var link = {
          token: 'UUID-of-token',
          fileId: 'UUID-of-file',
          senderId: 'UUID-of-sender',
          active: true
        };

        $httpBackend
            .expectGET(backendUrlService.getUrl() + coyoEndpoints.publicLink.getLink.replace('{{senderId}}', link.senderId).replace('{{fileId}}', link.fileId))
            .respond(200, link);

        // when
        var result;
        FilePublicLinkModel.getLink(link.senderId, link.fileId).then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toEqual(jasmine.objectContaining(link));
      });

      it('should get null on missing', function () {
        // given
        var fileId = 'UUID-of-file';
        var senderId = 'UUID-of-sender';

        $httpBackend
            .expectGET(backendUrlService.getUrl() + coyoEndpoints.publicLink.getLink.replace('{{senderId}}', senderId).replace('{{fileId}}', fileId))
            .respond(404);

        // when
        var result;
        FilePublicLinkModel.getLink(senderId, fileId).then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toBe(null);
      });

      it('should create a link', function () {
        // given
        var link = {
          token: 'UUID-of-token',
          fileId: 'UUID-of-file',
          senderId: 'UUID-of-sender',
          active: true
        };

        $httpBackend
            .expectPOST(backendUrlService.getUrl() + coyoEndpoints.publicLink.create.replace('{{senderId}}', link.senderId).replace('{{fileId}}', link.fileId))
            .respond(201, link);

        // when
        var result;
        FilePublicLinkModel.create(link.senderId, link.fileId).then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toEqual(jasmine.objectContaining(link));
      });

      it('should recreate a link', function () {
        // given
        var link = {
          token: 'UUID-of-token',
          fileId: 'UUID-of-file',
          senderId: 'UUID-of-sender',
          active: true
        };

        $httpBackend
            .expectPUT(backendUrlService.getUrl() + coyoEndpoints.publicLink.recreate.replace('{{token}}', link.token))
            .respond(200, link);

        // when
        var result;
        FilePublicLinkModel.recreate(link).then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toEqual(jasmine.objectContaining(link));
      });

      it('should call deactivate on active link', function () {
        // given
        var link = {
          token: 'UUID-of-token',
          active: true
        };

        $httpBackend
            .expectPUT(backendUrlService.getUrl() + coyoEndpoints.publicLink.deactivate.replace('{{token}}', link.token))
            .respond(200);

        // when
        var result;
        FilePublicLinkModel.deactivate(link).then(function (response) {
          result = response;
        });

        // then
        $httpBackend.flush();
        expect(result.active).toBe(false);
      });

      it('should call activate on inactive link', function () {
        // given
        var link = {
          token: 'UUID-of-token',
          active: true
        };

        $httpBackend
            .expectPUT(backendUrlService.getUrl() + coyoEndpoints.publicLink.activate.replace('{{token}}', link.token))
            .respond(200);

        // when
        var result;
        FilePublicLinkModel.activate(link).then(function (response) {
          result = response;
        });

        // then
        $httpBackend.flush();
        expect(result.active).toBe(true);
      });
    });
  });

}());
