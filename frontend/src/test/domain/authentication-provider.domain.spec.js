(function () {
  'use strict';

  describe('domain: AuthenticationProviderModel', function () {

    beforeEach(module('coyo.domain'));

    var $httpBackend, AuthenticationProviderModel, backendUrlService;

    beforeEach(inject(function (_$httpBackend_, _AuthenticationProviderModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      AuthenticationProviderModel = _AuthenticationProviderModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('class members', function () {

      it('should order configs', function () {
        // given
        var ids = ['id1', 'id2'];
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/auth/providers/action/order', function (data) {
          expect(angular.fromJson(data)).toEqual(ids);
          return true;
        }).respond(200, 'foo');

        // when
        var result = null;
        AuthenticationProviderModel.order(ids).then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toBe('foo');
      });

      it('should load all configs', function () {
        // given
        var config1 = {id: 'id1', name: 'name1'};
        var config2 = {id: 'id2', name: 'name2'};
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/auth/providers').respond(200, [config1, config2]);

        // when
        var result = null;
        AuthenticationProviderModel.query().then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toEqual([new AuthenticationProviderModel(config1), new AuthenticationProviderModel(config2)]);
      });

      it('should load single config', function () {
        // given
        var id = 'foo-id';
        var config = {id: id, name: 'foo-name'};
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/auth/providers/' + id).respond(200, config);

        // when
        var result = null;
        AuthenticationProviderModel.get(id).then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toEqual(new AuthenticationProviderModel(config));
      });
    });

    describe('instance members', function () {

      it('should activate config', function () {
        // given
        var id = 'foo-id';
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/auth/providers/' + id + '/activate').respond(200);

        // when
        var result = null;
        new AuthenticationProviderModel({id: id}).activate().then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result.id).toBe(id);
      });

      it('should deactivate config', function () {
        // given
        var id = 'foo-id';
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/auth/providers/' + id + '/deactivate').respond(200);

        // when
        var result = null;
        new AuthenticationProviderModel({id: id}).deactivate().then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result.id).toBe(id);
      });
    });
  });
}());
