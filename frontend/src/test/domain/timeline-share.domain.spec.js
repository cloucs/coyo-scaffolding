(function () {
  'use strict';

  describe('domain: TimelineShareModel', function () {

    beforeEach(module('coyo.domain'));

    var TimelineShareModel;

    beforeEach(inject(function (_TimelineShareModel_) {
      TimelineShareModel = _TimelineShareModel_;
    }));

    describe('instance members', function () {

      it('should remove current share from given list of shares', function () {
        // given
        var share = new TimelineShareModel({id: 123});
        var shares = [{id: 123}, {id: 234}, {id: 345}];

        // when
        share.removeFromShares(shares);

        // then
        expect(shares.length).toBe(2);
        expect(shares).not.toEqual(jasmine.arrayContaining([{id: 123}]));
      });
    });

  });
}());
