(function () {
  'use strict';

  describe('domain: GlobalAppModel', function () {

    beforeEach(module('coyo.domain'));

    var $httpBackend, GlobalAppModel, Pageable, backendUrlService;

    beforeEach(inject(function (_$httpBackend_, _GlobalAppModel_, _Pageable_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      Pageable = _Pageable_;
      GlobalAppModel = _GlobalAppModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('instance members', function () {

      it('pagedQuery finds by name and key', function () {
        // given
        var app1 = {id: 'app1'};
        var app2 = {id: 'app2'};
        var apps = [app1, app2];
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/apps?_page=1&_pageSize=2&key=appKey&name=searchName')
            .respond(200, {content: apps});

        // when
        var result = null;
        GlobalAppModel.pagedQuery(new Pageable(1, 2), {name: 'searchName', key: 'appKey'})
            .then(function (response) {
              result = response;
            });
        $httpBackend.flush();

        // then
        expect(result.content).toEqual([new GlobalAppModel(app1), new GlobalAppModel(app2)]);
      });
    });
  });
}());
