(function () {
  'use strict';

  var moduleName = 'coyo.app';

  describe('module: ' + moduleName, function () {

    var $timeout, SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieveByKey']), $rootScope;

    function mockTrackingCodeSettings(code) {
      SettingsModel.retrieveByKey.and.callFake(function (key) {
        return {
          then: function (cb) {
            cb(key === 'trackingCode' ? code : null);
          }
        };
      });
    }

    function expectTrackingCode(expectedCode) {
      expect($rootScope.trackingCode.$$unwrapTrustedValue()).toBe(expectedCode); // eslint-disable-line angular/no-private-call
    }

    beforeEach(function () {

      module(moduleName, function ($provide) {
        mockTrackingCodeSettings('initial-code');
        $provide.value('SettingsModel', SettingsModel);
      });

      inject(function (_$timeout_, _$rootScope_) {
        $timeout = _$timeout_;
        $rootScope = _$rootScope_;
      });

    });

    describe('application init', function () {

      it('should set tracking code on app init', function () {
        // when
        $timeout.flush();

        // then
        expectTrackingCode('initial-code');
      });

      it('should set tracking code after login', function () {
        // given
        var code = 'tracking code after login';
        mockTrackingCodeSettings(code);

        // when
        $rootScope.$emit('authService:login:success');
        $timeout.flush();

        // then
        expectTrackingCode(code);
      });

      it('should set tracking code on backend url change', function () {
        // given
        var code = 'tracking code after url change';
        mockTrackingCodeSettings(code);

        // when
        $rootScope.$emit('backendUrlService:url:updated');
        $timeout.flush();

        // then
        expectTrackingCode(code);
      });
    });

  });
})();
