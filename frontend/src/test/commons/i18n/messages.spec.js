(function () {
  'use strict';

  var moduleName = 'coyo.app';

  describe('module: ' + moduleName, function () {

    var translationRegistryProviderMock;

    beforeEach(function () {
      angular.module('translationRegistryProviderConfig', ['commons.i18n.custom'])
          .config(function (_translationRegistryProvider_) {
            translationRegistryProviderMock = _translationRegistryProvider_;
            spyOn(translationRegistryProviderMock, 'registerTranslations').and.callThrough();
          });
      module('translationRegistryProviderConfig');
      module(moduleName);
      inject();
    });

    describe('Service: translationRegistry', function () {

      it('should register same message keys in both languages', function () {
        // when
        var calls = _.groupBy(translationRegistryProviderMock.registerTranslations.calls.all(), 'args[0]');
        var messagesDe = _.flatMap(calls.de, function (call) {
          return _.keys(call.args[1]);
        });
        var messagesEn = _.flatMap(calls.en, function (call) {
          return _.keys(call.args[1]);
        });

        // then
        expect(translationRegistryProviderMock.registerTranslations).toHaveBeenCalled();
        expect(messagesDe.length).toBe(messagesEn.length);
        expect(_.difference(messagesDe, messagesEn)).toEqual([]);
      });

    });

  });
})();
