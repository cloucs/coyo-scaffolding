(function () {
  'use strict';

  var moduleName = 'commons.i18n.custom';

  describe('module: ' + moduleName, function () {
    var $scope, LanguagesModel, translationRegistry, $q, errorService;
    var coyoTranslationLoader, originalNavigatorLanguages;

    beforeAll(function () {
      originalNavigatorLanguages = navigator.languages;
    });

    afterAll(function () {
      Object.defineProperty(navigator, 'languages', {
        get: function () { return originalNavigatorLanguages; }
      });
    });

    beforeEach(function () {
      LanguagesModel = jasmine.createSpyObj('LanguagesModel', ['getTranslations', 'getDefaultLanguage']);
      translationRegistry = jasmine.createSpyObj('translationRegistry', ['getTranslationTables']);
      errorService = jasmine.createSpyObj('errorService', ['suppressNotification']);
      translationRegistry.getTranslationTables.and.returnValue({
        de: {
          'key1': 'original1',
          'key2': 'original2'
        },
        en: {
          'key3': 'original3'
        }
      });

      module(moduleName, function ($provide) {
        $provide.value('LanguagesModel', LanguagesModel);
        $provide.value('translationRegistry', translationRegistry);
        $provide.value('errorService', errorService);
      });
    });

    beforeEach(inject(function (_$rootScope_, _coyoTranslationLoader_, _$localStorage_, _$q_) {
      $scope = _$rootScope_.$new();
      coyoTranslationLoader = _coyoTranslationLoader_;
      $q = _$q_;

      LanguagesModel.getDefaultLanguage.and.returnValue($q.resolve({language: 'en'}));

      LanguagesModel.getTranslations.and.callFake(function (language) {
        if (language === 'de' || language === 'en') {
          return $q.resolve([{
            key: 'key1',
            translation: 'backend_overwrite1'
          }, {
            key: 'key4',
            translation: 'backend_overwrite4'
          }]);
        }
        if (language === 'aa') {
          return $q.resolve([{
            key: 'key3',
            translation: 'backend_user_language3'
          }]);
        }
        if (language === 'xy') {
          return $q.resolve([{
            key: 'key3',
            translation: 'backend_browser_language3'
          }]);
        } else {
          return $q.reject('no active language');
        }
      });
    }));

    describe('service: coyoTranslationLoader', function () {
      it('should initialize', function () {
        // then
        expect(coyoTranslationLoader).toBeDefined();
      });

      it('should get translations', function () {
        // given
        var translations = {};

        // when
        coyoTranslationLoader({key: 'de'}).then(function (data) {
          translations = data;
        });
        $scope.$apply();

        // then
        expect(LanguagesModel.getDefaultLanguage).not.toHaveBeenCalled();
        expect(LanguagesModel.getTranslations).toHaveBeenCalledWith('de');
        expect(Object.keys(translations)).toBeArrayOfSize(3);
        expect(translations.key1).toBe('backend_overwrite1');
        expect(translations.key2).toBe('original2');
        expect(translations.key4).toBe('backend_overwrite4');
        expect(translations.key3).toBeUndefined();
      });

      it('should get translations for special user language', function () {
        // given
        var translations = {};

        // when
        coyoTranslationLoader({key: 'aa'}).then(function (data) {
          translations = data;
        });
        $scope.$apply();

        // then
        expect(LanguagesModel.getDefaultLanguage).toHaveBeenCalled();
        expect(LanguagesModel.getTranslations.calls.allArgs()).toEqual([['aa'], ['en']]);
        expect(Object.keys(translations)).toBeArrayOfSize(3);
        expect(translations.key1).toBe('backend_overwrite1');
        expect(translations.key2).toBeUndefined();
        expect(translations.key3).toBe('backend_user_language3');
        expect(translations.key4).toBe('backend_overwrite4');
      });
    });
  });
})();
