(function () {
  'use strict';

  var moduleName = 'commons.auth';
  var targetName = 'authService';

  describe('module: ' + moduleName, function () {
    var $rootScope, $state;
    var $httpBackend;
    var $q;
    var $sessionStorage;
    var $timeout;
    var authService;
    var mobileEventsService;
    var UserModel;
    var backendUrlService;
    var socketService;
    var loginUrl, resetUrl;
    var loginResponse = {id: 'user-id'};
    var deeplinkService;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $state = jasmine.createSpyObj('$state', ['go']);
        $sessionStorage = jasmine.createSpyObj('$sessionStorage', ['$reset']);
        socketService = jasmine.createSpyObj('socketService', ['subscribe']);
        mobileEventsService = jasmine.createSpyObj('mobileEventsService', ['propagate']);
        deeplinkService = jasmine.createSpyObj('deeplinkService',
            ['getReturnToState', 'getReturnToStateParams', 'setReturnToState']);
        $provide.value('$state', $state);
        $provide.value('$sessionStorage', $sessionStorage);
        $provide.value('socketService', socketService);
        $provide.value('mobileEventsService', mobileEventsService);
        $provide.value('deeplinkService', deeplinkService);
      });

      inject(function ($injector, _$rootScope_, _authService_, _$q_, _UserModel_, _backendUrlService_, _$httpBackend_,
                       _$sessionStorage_, _$timeout_, _mobileEventsService_, _deeplinkService_) {
        $rootScope = _$rootScope_;
        $q = _$q_;
        authService = _authService_;
        UserModel = _UserModel_;
        backendUrlService = _backendUrlService_;
        $httpBackend = _$httpBackend_;
        $timeout = _$timeout_;
        $sessionStorage = _$sessionStorage_;
        mobileEventsService = _mobileEventsService_;
        deeplinkService = _deeplinkService_;

        $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});

        loginUrl = backendUrlService.getUrl() + '/web/auth/login';
        resetUrl = backendUrlService.getUrl() + '/web/reset-password';
      });
    });

    describe('service: ' + targetName, function () {

      /* login */
      it('should be possible to log in', function () {
        // given
        var success = false;
        var credentials = 'password=someSecret&username=someUser';
        var successEventFired = false;
        $rootScope.$on('authService:login:success', function () {
          successEventFired = true;
        });

        $httpBackend.expectPOST(loginUrl, credentials, function (headers) {
          return headers['Content-Type'] === 'application/x-www-form-urlencoded';
        }).respond(200, loginResponse);

        // when
        authService.login('someUser', 'someSecret').then(function () {
          // success
          success = true;
        }, function () {
          // error
        });
        $httpBackend.flush();
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();

        $rootScope.$digest(); // digest promises etc.
        $timeout.flush();

        // then
        expect(success).toEqual(true);
        expect(authService.isAuthenticated()).toEqual(true);
        expect(successEventFired).toBe(true);
        expect(authService.getCurrentUserId()).toEqual(loginResponse.id);
      });

      it('should clear session', function () {
        prepareLoggedInUser();

        authService.clearSession();

        expect(authService.isAuthenticated()).toEqual(false);
        expect($sessionStorage.$reset).toHaveBeenCalled();
        expect($rootScope.search.visible).toBeFalsy();
        expect($rootScope.search.term).toBe('');
        authService.getUser().then(function () {
          fail('Expected getUser() to fail');
        });
      });

      it('should clear session storage but not deep link information', function () {
        // given
        prepareLoggedInUser();
        var returnToState = 'main.test.state';
        var returnToParams = {params: 'test'};
        deeplinkService.getReturnToState.and.returnValue(returnToState);
        deeplinkService.getReturnToStateParams.and.returnValue(returnToParams);

        // when
        authService.clearSession();

        // then
        expect($sessionStorage.$reset).toHaveBeenCalledTimes(1);
        expect(deeplinkService.setReturnToState).toHaveBeenCalledWith(returnToState, returnToParams);
        expect($sessionStorage.$reset).toHaveBeenCalledBefore(deeplinkService.setReturnToState);
      });

      /* logout */
      it('should be possible to log out', function () {
        // given
        prepareLoggedInUser();

        // when
        mobileEventsService.propagate.calls.reset();
        $httpBackend.expectPOST('/web/auth/logout').respond(200);

        expect(authService.isAuthenticated()).toEqual(true); // should be authenticated = logged in
        authService.logout();
        $httpBackend.flush();
        $rootScope.$apply();
        $timeout.flush();

        // then
        expect(mobileEventsService.propagate).toHaveBeenCalledWith('authService:logout:before', undefined);
        expect(mobileEventsService.propagate).toHaveBeenCalledWith('authService:logout:success', undefined);
        expect($state.go).toHaveBeenCalledWith('front.logout-success');
        expect(authService.isAuthenticated()).toEqual(false); // should be logged out again
      });

      it('should delete csrfToken, after logout was successful', function () {
        // given
        $rootScope.csrfToken = '';
        $httpBackend.expectPOST('/web/auth/logout').respond(200);

        // when
        authService.logout();
        $httpBackend.flush();
        $rootScope.$apply();
        $timeout.flush();

        // then
        expect($rootScope.csrfToken).not.toBeDefined();
      });

      it('should delete csrfToken, after logout failed', function () {
        // given
        $rootScope.csrfToken = '';
        $httpBackend.expectPOST('/web/auth/logout').respond(403);

        // when
        authService.logout();
        $httpBackend.flush();
        $rootScope.$apply();
        $timeout.flush();

        // then
        expect($rootScope.csrfToken).not.toBeDefined();
      });

      /* isAuthenticated */
      it('should not authenticate the user by default', function () {
        expect(authService.isAuthenticated()).toEqual(false);
      });

      /* getUser */
      it('should be possible to get the user', function () {
        var success = false;
        expect(authService.isAuthenticated()).toEqual(false); // should be logged out
        authService.getUser().catch(function (error) {
          expect(error.length).toBeGreaterThan(0);
        });

        $httpBackend.expectPOST(loginUrl).respond(200, loginResponse);
        authService.login('someUser', 'someSecret').then(function () {
          // success
          success = true;
        }, function () {
          // error
        });
        $httpBackend.flush();
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();

        $rootScope.$digest(); // digest promises etc.
        expect(success).toEqual(true); // request should be successful

        expect(authService.isAuthenticated()).toEqual(true); // should be authenticated = logged in
        authService.getUser().then(function (user) {
          expect(user.id).not.toEqual(0);
        });
        authService.logout();
        expect(authService.isAuthenticated()).toEqual(false); // should be logged out again
        authService.getUser().catch(function (error) {
          expect(error.length).toBeGreaterThan(0);
        });
      });

      /* requestPassword */
      it('should request a password reset token', function () {
        $httpBackend.expectPOST(resetUrl, {
          username: 'username'
        }).respond(202);

        authService.requestPassword('username');

        $httpBackend.flush();
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      /* resetPassword */
      it('should reset the password using a reset token', function () {
        $httpBackend.expectPUT(resetUrl, {
          token: 'token',
          password: 'password'
        }).respond(200);

        authService.resetPassword('token', 'password');

        $httpBackend.flush();
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it('should subscribe and unsubscribe user update from sockets service', function () {
        // given
        var unsubscribeFnMock = jasmine.createSpy();
        socketService.subscribe.and.returnValue(unsubscribeFnMock);

        // when
        authService.subscribeToUserUpdate();
        authService.unsubscribeFromUserUpdate();

        // then
        expect(socketService.subscribe).toHaveBeenCalledWith('/user/topic/updated', jasmine.any(Function), 'userUpdated');
        expect(unsubscribeFnMock).toHaveBeenCalled();
      });

      describe('onGlobalPermissions', function () {
        var user, callback;

        beforeEach(function () {
          user = new UserModel({globalPermissions: ['P1']});
          spyOn(authService, 'getUser').and.returnValue($q.resolve(user));
          callback = jasmine.createSpy('callback');
        });

        it('should execute callback with permission match immediately', function () {
          // when
          authService.onGlobalPermissions('P1', callback);
          $rootScope.$apply();

          // then
          expect(callback).toHaveBeenCalledWith(true, user);
        });

        it('should execute callback with permission mismatch immediately', function () {
          // when
          authService.onGlobalPermissions('P2', callback);
          $rootScope.$apply();

          // then
          expect(callback).toHaveBeenCalledWith(false, user);
        });
      });
    });

    function prepareLoggedInUser() {
      var success = false;
      $httpBackend.expectPOST(loginUrl).respond(200, loginResponse);
      authService.login('someUser', 'someSecret').then(function () {
        // success
        success = true;
      }, function () {
        // error
      });
      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();

      $rootScope.$digest(); // digest promises etc.
      expect(success).toEqual(true); // request should be successful
      expect(authService.isAuthenticated()).toEqual(true);
    }
  });
})();
