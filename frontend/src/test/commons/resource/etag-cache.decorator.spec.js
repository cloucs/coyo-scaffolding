(function () {
  'use strict';

  var moduleName = 'commons.resource';

  describe('module: ' + moduleName, function () {

    var $http, $httpBackend, $rootScope, etagCacheService, $sessionStorage = {};

    beforeEach(function () {

      module(moduleName, function ($provide) {
        $provide.value('$sessionStorage', $sessionStorage);
      });

      inject(function (_$http_, _$httpBackend_, _$rootScope_, _etagCacheService_) {
        $http = _$http_;
        $httpBackend = _$httpBackend_;
        $rootScope = _$rootScope_;
        etagCacheService = _etagCacheService_;
      });

    });

    afterEach(function () {
      Object.keys($sessionStorage).forEach(function (key) {
        delete $sessionStorage[key];
      });
    });

    describe('Decorator: etagHttpDecorator / $http', function () {

      describe('regular requests', function () {

        it('should pass request when no caching is active', function () {
          // given
          var url = 'http://host/path';
          $httpBackend.expectGET(url).respond(200, 'payload');

          // when
          var result = null;
          var cacheFlag = undefined;
          $http.get(url, {originalUrl: url}).then(function (response) {
            result = response.data;
            cacheFlag = response.fromCache;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toBe('payload');
          expect(etagCacheService.isCached(url)).toBe(false);
          expect(cacheFlag).not.toBe(true);
        });

        it('should store response in cache when ETag header is present', function () {
          // given
          var url = 'http://host/path';
          $httpBackend.expectGET(url).respond(200, 'payload', {'ETag': '"abc"'});

          // when
          var result = null;
          $http.get(url, {originalUrl: url}).then(function (response) {
            result = response.data;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toBe('payload');
          expect(etagCacheService.isCached(url)).toBe(true);
          var cacheEntry = etagCacheService.get(url);
          expect(cacheEntry.etag).toBe('abc');
          expect(cacheEntry.data).toBe('payload');
          expect(cacheEntry.status).toBe(200);
        });

        it('should store response in cache when weak ETag header is present', function () {
          // given
          var url = 'http://host/path';
          $httpBackend.expectGET(url).respond(200, 'payload', {'ETag': 'W/"abc"'});

          // when
          var result = null;
          $http.get(url, {originalUrl: url}).then(function (response) {
            result = response.data;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toBe('payload');
          expect(etagCacheService.isCached(url)).toBe(true);
          var cacheEntry = etagCacheService.get(url);
          expect(cacheEntry.etag).toBe('abc');
          expect(cacheEntry.data).toBe('payload');
          expect(cacheEntry.status).toBe(200);
        });

        it('should send etag header when url is cached and return request result if backend does not know etag', function () {
          // given
          var url = 'http://host/path';
          var ifNoneMatchHeader = null;
          $httpBackend.expectGET(url, function (headers) {
            ifNoneMatchHeader = headers['If-None-Match'];
            return true;
          }).respond(200, 'payload');
          etagCacheService.store(url, 'cached-payload', 'abc', 200);

          // when
          var result = null;
          $http.get(url, {originalUrl: url}).then(function (response) {
            result = response.data;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toBe('payload');
          expect(ifNoneMatchHeader).toBe('"abc"');
        });

        it('should not add etag request header when cache is disabled', function () {
          // given
          var url = 'http://host/path';
          var ifNoneMatchHeader = null;
          $httpBackend.expectGET(url, function (headers) {
            ifNoneMatchHeader = headers['If-None-Match'];
            return true;
          }).respond(200, 'payload');
          etagCacheService.store(url, 'cached-payload', 'abc', 200);

          // when
          var result = null;
          $http.get(url, {
            originalUrl: url,
            etagCache: {
              disabled: true
            }
          }).then(function (response) {
            result = response.data;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toBe('payload');
          expect(ifNoneMatchHeader).not.toBeDefined();
        });

        it('should use value from cache on etag match', function () {
          // given
          var url = 'http://host/path';
          $httpBackend.expectGET(url).respond(304);
          etagCacheService.store(url, 'payload', 'abc', 200);

          // when
          var result = null;
          var cacheFlag = null;
          $http.get(url, {originalUrl: url}).then(function (response) {
            result = response.data;
            cacheFlag = response.fromCache;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toBe('payload');
          expect(cacheFlag).toBe(true);
        });

        it('should add cache flag to payload when configured', function () {
          // given
          var url = 'http://host/path';
          $httpBackend.expectGET(url).respond(304);
          etagCacheService.store(url, {'payload': 'foo'}, 'abc', 200);

          // when
          var result = null;
          $http.get(url, {
            originalUrl: url,
            etagCache: {
              addCacheFlagToPayload: true
            }
          }).then(function (response) {
            result = response.data;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result.payload).toBe('foo');
          expect(result._fromCache).toBe(true);
        });

        it('should not try to add cache flag to payload if not an object', function () {
          // given
          var url = 'http://host/path';
          $httpBackend.expectGET(url).respond(304);
          etagCacheService.store(url, 'cached-payload', 'abc', 200);

          // when
          var result = null;
          $http.get(url, {
            originalUrl: url,
            etagCache: {
              addCacheFlagToPayload: true
            }
          }).then(function (response) {
            result = response.data;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toBe('cached-payload');
        });

      });

      describe('bulk requests', function () {
        it('should store bulk response in cache when bulk ETag header is present', function () {
          // given
          var url = 'http://host/path';
          var payload = {
            id1: 'payload1',
            id2: 'payload2'
          };
          $httpBackend.expectGET(url).respond(200, payload,
              {'X-Bulk-ETag': '{"id1":"abc","id2":"def"}'});

          // when
          var result = null;
          $http.get(url, {originalUrl: url}).then(function (response) {
            result = response.data;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toEqual(payload);
          expect(etagCacheService.isBulkCached(url)).toBe(true);
          var cacheEntry1 = etagCacheService.getBulk(url, 'id1');
          expect(cacheEntry1.etag).toBe('abc');
          expect(cacheEntry1.data).toBe('payload1');
          var cacheEntry2 = etagCacheService.getBulk(url, 'id2');
          expect(cacheEntry2.etag).toBe('def');
          expect(cacheEntry2.data).toBe('payload2');
        });

        it('should send bulk etag header for cached ids when url is cached and return request result if backend does not know etag', function () {
          // given
          var url = 'http://host/path';
          var ifNoneMatchHeader = null;
          $httpBackend.expectGET(url + '?myIds=id1&myIds=id2', function (headers) {
            ifNoneMatchHeader = headers['X-Bulk-If-None-Match'];
            return true;
          }).respond(200, {
            'id1': 'payload1',
            'id2': 'payload2'
          });
          etagCacheService.storeBulk(url, 'id1', 'cached-payload1', 'abc');
          etagCacheService.storeBulk(url, 'id2', 'cached-payload2', 'def');

          // when
          var result = null;
          $http.get(url, {
            originalUrl: url,
            etagCache: {
              bulkParameter: 'myIds'
            },
            params: {
              myIds: ['id1', 'id2']
            }
          }).then(function (response) {
            result = response.data;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toEqual({'id1': 'payload1', 'id2': 'payload2'});
          expect(ifNoneMatchHeader).toEqual('{"id1":"abc","id2":"def"}');
        });

        it('should not add bulk etag request header if cache is disabled', function () {
          // given
          var url = 'http://host/path';
          var ifNoneMatchHeader = null;
          $httpBackend.expectGET(url + '?myIds=id1&myIds=id2', function (headers) {
            ifNoneMatchHeader = headers['X-Bulk-If-None-Match'];
            return true;
          }).respond(200, {
            'id1': 'payload1',
            'id2': 'payload2'
          });
          etagCacheService.storeBulk(url, 'id1', 'cached-payload1', 'abc');
          etagCacheService.storeBulk(url, 'id2', 'cached-payload2', 'def');

          // when
          var result = null;
          $http.get(url, {
            originalUrl: url,
            etagCache: {
              bulkParameter: 'myIds',
              disabled: true
            },
            params: {
              myIds: ['id1', 'id2']
            }
          }).then(function (response) {
            result = response.data;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toEqual({'id1': 'payload1', 'id2': 'payload2'});
          expect(ifNoneMatchHeader).not.toBeDefined();
        });

        it('should use value from cache on where bulk etag matches', function () {
          // given
          var url = 'http://host/path';
          $httpBackend.expectGET(url + '?myIds=id1&myIds=id2').respond(200, {
            'id2': 'payload2'
          }, {
            'X-Bulk-Status': '{"id1":304,"id2":200}'
          });
          etagCacheService.storeBulk(url, 'id1', 'cached-payload1', 'abc');
          etagCacheService.storeBulk(url, 'id2', 'cached-payload2', 'def');

          // when
          var result = null;
          var cacheFlags = null;
          $http.get(url, {
            originalUrl: url,
            etagCache: {
              bulkParameter: 'myIds'
            },
            params: {
              myIds: ['id1', 'id2']
            }
          }).then(function (response) {
            result = response.data;
            cacheFlags = response.fromCache;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toEqual({
            id1: 'cached-payload1',
            id2: 'payload2'
          });
          expect(cacheFlags).toEqual({
            id1: true
          });
        });

        it('should add cache flag to payload when configured', function () {
          // given
          var url = 'http://host/path';
          $httpBackend.expectGET(url + '?myIds=id1&myIds=id2').respond(200, {
            'id2': {payload: 'notcached2'}
          }, {
            'X-Bulk-Status': '{"id1":304,"id2":200}'
          });
          etagCacheService.storeBulk(url, 'id1', {payload: 'cached1'}, 'abc');
          etagCacheService.storeBulk(url, 'id2', {payload: 'cached2'}, 'def');

          // when
          var result = null;
          $http.get(url, {
            originalUrl: url,
            etagCache: {
              bulkParameter: 'myIds',
              addCacheFlagToPayload: true
            },
            params: {
              myIds: ['id1', 'id2']
            }
          }).then(function (response) {
            result = response.data;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toEqual({
            'id1': {payload: 'cached1', _fromCache: true},
            'id2': {payload: 'notcached2'}
          });
        });

        it('should not try to add cache flag to payload if not an object', function () {
          // given
          var url = 'http://host/path';
          $httpBackend.expectGET(url + '?myIds=id1').respond(200, {}, {
            'X-Bulk-Status': '{"id1":304}'
          });
          etagCacheService.storeBulk(url, 'id1', 'cached-payload', 'abc');

          // when
          var result = null;
          $http.get(url, {
            originalUrl: url,
            etagCache: {
              bulkParameter: 'myIds',
              addCacheFlagToPayload: true
            },
            params: {
              myIds: ['id1']
            }
          }).then(function (response) {
            result = response.data;
          });
          $rootScope.$apply();
          $httpBackend.flush();

          // then
          expect(result).toEqual({
            'id1': 'cached-payload'
          });
        });

      });
    });
  });
})();
