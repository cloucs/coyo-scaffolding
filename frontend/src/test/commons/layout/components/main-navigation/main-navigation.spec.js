(function () {
  'use strict';

  var moduleName = 'commons.layout';

  describe('module: ' + moduleName, function () {

    var $controller, $rootScope, $scope, sidebarService, authService, tourService;

    beforeEach(
        module(moduleName, function ($provide) {
          $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
          $provide.value('tourSelectionModal', jasmine.createSpyObj('tourSelectionModal', ['open']));
        })
    );

    beforeEach(inject(function (_$controller_, _$rootScope_, $q) {
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      $controller = _$controller_;
      sidebarService = jasmine.createSpyObj('sidebarService', ['open']);
      authService = jasmine.createSpyObj('authService', ['getUser', 'onGlobalPermissions']);
      authService.getUser.and.returnValue($q.resolve({
        name: 'Testuser',
        id: '123456'
      }));
      authService.onGlobalPermissions.and.callFake(function (permissionNames, callback) {
        callback(permissionNames === 'ACCESS_OWN_USER_PROFILE', {id: '123456'});
      });
      tourService = jasmine.createSpyObj('tourService', ['getTopics']);
      tourService.getTopics.and.returnValue([]);
    }));

    var controllerName = 'MainNavigationController';

    describe('directive: oyocMainNavigation', function () {

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {
            sidebarService: sidebarService,
            authService: authService,
            tourService: tourService,
            $scope: $scope,
            adminStates: [
              {globalPermission: 'P1'}, {globalPermission: 'P2'}
            ]
          });
        }

        it('should init', function () {
          // given
          var ctrl = buildController();

          // when
          $scope.$apply();

          // then
          expect(ctrl.searchVisible).toBeFalsy();
          expect(ctrl.allAdminPermissions).toBe('P1,P2');
          expect(ctrl.hasTourSteps).toBe(false);
        });

        it('should open menu', function () {
          // given
          var ctrl = buildController();

          // when
          ctrl.openMenu();

          // then
          expect(sidebarService.open).toHaveBeenCalledWith('menu');
        });

        it('should update tour steps', function () {
          // given
          var ctrl = buildController();
          tourService.getTopics.and.returnValue([{id: 'my-step'}]);

          // when
          $rootScope.$emit('tourService:stepsChanged');
          $rootScope.$apply();

          // then
          expect(ctrl.hasTourSteps).toBe(true);
        });
      });
    });
  });

})();
