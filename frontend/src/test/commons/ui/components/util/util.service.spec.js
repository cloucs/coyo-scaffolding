(function () {
  'use strict';

  var moduleName = 'coyo.base';

  describe('module: ' + moduleName, function () {
    var utilService;

    beforeEach(function () {
      module(moduleName);

      inject(function (_utilService_) {
        utilService = _utilService_;
      });
    });

    describe('service: utilService', function () {

      it('should match text links', function () {
        // given
        var linkText = 'Test test http://test.test. Testtext testtext https://test2.test test.';

        // when
        var matchTextLinks = utilService.matchTextLinks(linkText);

        // given
        expect(matchTextLinks.length).toBe(2);
        expect(matchTextLinks[0]).toBe('http://test.test');
        expect(matchTextLinks[1]).toBe('https://test2.test');
      });

      it('should return empty array if no link found', function () {
        // given
        var linkText = 'Test test http://test. Testtext testtext.';

        // when
        var matchTextLinks = utilService.matchTextLinks(linkText);

        // given
        expect(matchTextLinks).toEqual([]);
        expect(matchTextLinks.length).toBe(0);
      });
    });
  });
})();
