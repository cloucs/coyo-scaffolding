(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $rootScope, $scope, $controller, $transclude, $timeout;

    beforeEach(module(moduleName));

    beforeEach(inject(function ($injector, _$rootScope_, _$controller_, _$timeout_) {
      $rootScope = _$rootScope_;
      $rootScope.screenSize = {
        isXs: false,
        isSm: false,
        isMd: true,
        isLg: false,
        isRetina: true
      };
      $scope = $rootScope.$new();
      $controller = _$controller_;
      $timeout = _$timeout_;
      $transclude = jasmine.createSpyObj('$transclude', ['isSlotFilled']);
    }));

    describe('directive: filterbox', function () {

      var controllerName = 'FilterboxController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {
            $scope: $scope,
            $transclude: $transclude
          });
        }

        describe('controller init', function () {

          it('should init default values', function () {
            // when
            var ctrl = buildController();

            // then
            expect(ctrl.filterOpen).toBe(false);
            expect(ctrl.toggleFilter).toBeDefined();
          });

          it('should init showActions', function () {
            // given
            $transclude.isSlotFilled.and.callFake(function (slotName) {
              return slotName === 'fb-actions';
            });

            // when
            var ctrl = buildController();

            // then
            expect(ctrl.showActions).toBe(true);
            expect(ctrl.showCount).toBe(false);
            expect(ctrl.showFilter).toBe(false);
            expect(ctrl.showSearch).toBe(false);
          });

          it('should init showCount', function () {
            // given
            $transclude.isSlotFilled.and.callFake(function (slotName) {
              return slotName === 'fb-count';
            });

            // when
            var ctrl = buildController();

            // then
            expect(ctrl.showCount).toBe(true);
            expect(ctrl.showActions).toBe(false);
            expect(ctrl.showFilter).toBe(false);
            expect(ctrl.showSearch).toBe(false);
          });

          it('should init showFilter', function () {
            // given
            $transclude.isSlotFilled.and.callFake(function (slotName) {
              return slotName === 'fb-filter';
            });

            // when
            var ctrl = buildController();

            // then
            expect(ctrl.showCount).toBe(false);
            expect(ctrl.showActions).toBe(false);
            expect(ctrl.showFilter).toBe(true);
            expect(ctrl.showSearch).toBe(false);
          });

          it('should init showSearch', function () {
            // given
            $transclude.isSlotFilled.and.callFake(function (slotName) {
              return slotName === 'fb-search';
            });

            // when
            var ctrl = buildController();

            // then
            expect(ctrl.showCount).toBe(false);
            expect(ctrl.showActions).toBe(false);
            expect(ctrl.showFilter).toBe(false);
            expect(ctrl.showSearch).toBe(true);
          });
        });

        describe('controller active', function () {
          var ctrl;

          beforeEach(function () {
            ctrl = buildController();
          });

          it('should activate filter', function () {
            // when
            ctrl.toggleFilter();
            $timeout.flush();

            // then
            expect(ctrl.filterOpen).toBe(true);
          });

          it('should deactivate filter', function () {
            // given
            ctrl.filterOpen = true;

            // when
            ctrl.toggleFilter();
            $timeout.flush();

            // then
            expect(ctrl.filterOpen).toBe(false);
          });

          it('should deactivate filter even when inactive', function () {
            // given
            ctrl.showFilter = false;

            // when
            ctrl.toggleFilter(false);
            $timeout.flush();

            // then
            expect(ctrl.filterOpen).toBe(false);
          });
        });
      });
    });
  });
})();
