(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'coyo-sender-navigation-mobile';

  describe('module: ' + moduleName, function () {

    var $transclude, $controller, $rootScope, $scope, senderModel;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$rootScope_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = _$rootScope_.$new();

        $transclude = jasmine.createSpyObj('$transclude', ['isSlotFilled']);
        senderModel = jasmine.createSpyObj('SenderModel', ['updateNavigation']);
      }));

      var controllerName = 'SenderNavigationMobileController';

      describe('controller: ' + controllerName, function () {

        function buildController(sender, apps) {
          return $controller(controllerName, {
            $transclude: $transclude,
            $rootScope: $rootScope,
            $scope: $scope,
            SenderModel: function () {
              return senderModel;
            },
            appService: jasmine.createSpyObj('appService', ['addApp']),
            appRegistry: jasmine.createSpyObj('appRegistry', ['getRootStateName']),
            appSettingsModalService: jasmine.createSpyObj('appSettingsModalService', ['open'])
          }, {
            sender: sender,
            apps: apps,
            currentApp: apps[0]
          });
        }

        it('should sort apps', function () {
          // given
          var app1 = {id: 'APP-ID-1'};
          var app2 = {id: 'APP-ID-2'};
          var app3 = {id: 'APP-ID-3'};
          var app4 = {id: 'APP-ID-4'};
          var app5 = {id: 'APP-ID-5'};
          var app6 = {id: 'APP-ID-6'};
          var app7 = {id: 'APP-ID-7'};
          var group1 = {name: 'APP-GROUP-1', apps: [app1.id, app3.id]};
          var group2 = {name: 'APP-GROUP-2', apps: [app2.id, app4.id]};
          var group3 = {name: 'APP-GROUP-3', apps: []};
          var group4 = {name: 'APP-GROUP-4', apps: [app6.id, app5.id, app7.id]};
          var workspace = {id: 'WORKSPACE-ID', appNavigation: [group1, group2, group3, group4]};
          var ctrl = buildController(workspace, [app1, app2, app3, app4, app5, app6, app6]);

          // when
          // after init

          // then
          expect(ctrl.apps[0]).toEqual(app1);
          expect(ctrl.apps[1]).toEqual(app3);
          expect(ctrl.apps[2]).toEqual(app2);
          expect(ctrl.apps[3]).toEqual(app4);
          expect(ctrl.apps[4]).toEqual(app6);
          expect(ctrl.apps[5]).toEqual(app5);
          expect(ctrl.apps.length).toEqual(6);
        });
      });
    });
  });

})();
