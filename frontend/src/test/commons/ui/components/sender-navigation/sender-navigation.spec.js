(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'coyo-sender-navigation';

  describe('module: ' + moduleName, function () {

    var $controller, $rootScope, $scope, $q, senderNavigationUpdateService, $stateParams;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$q_, _$rootScope_, _$stateParams_) {
        $controller = _$controller_;
        $stateParams = _$stateParams_;
        $rootScope = _$rootScope_;
        $scope = _$rootScope_.$new();
        $q = _$q_;
        senderNavigationUpdateService = jasmine.createSpyObj('senderNavigationUpdateService',
            ['updateNavigation', 'prepareNavigationUpdateResponse']);
      }));

      var controllerName = 'SenderNavigationController';

      describe('controller: ' + controllerName, function () {

        function buildController(sender, apps) {
          return $controller(controllerName, {
            $rootScope: $rootScope,
            $scope: $scope,
            appRegistry: jasmine.createSpyObj('appRegistry', ['getRootStateName']),
            appSettingsModalService: jasmine.createSpyObj('appSettingsModalService', ['open']),
            senderNavigationUpdateService: senderNavigationUpdateService
          }, {
            sender: sender,
            appsStatus: {busy: false},
            apps: apps
          });
        }

        it('should only display visible apps', function () {
          // given
          var app = {id: 'APP-ID-ONE'};
          var workspace = {id: 'WORKSPACE-ID', appNavigation: [{name: 'Group', apps: [app.id, 'invalid-id']}]};
          var ctrl = buildController(workspace, [app]);

          // when
          var apps = ctrl.getAppsByGroup(workspace.appNavigation[0]);

          // then
          expect(apps).toEqual([app]);
        });

        it('should display app groups containing at least one active app', function () {
          // given
          var app1 = {id: 'APP-ID-1', active: true};
          var app2 = {id: 'APP-ID-2', active: false};
          var workspace = {id: 'WORKSPACE-ID', appNavigation: [{name: 'Group', apps: [app1.id, app2.id]}]};
          var ctrl = buildController(workspace, [app1, app2]);

          // when
          var containsActiveApps = ctrl.containsActiveApps(workspace.appNavigation[0]);

          // then
          expect(containsActiveApps).toEqual(true);
        });

        it('should not display app groups containing no active apps', function () {
          // given
          var app1 = {id: 'APP-ID-1', active: false};
          var app2 = {id: 'APP-ID-2', active: false};
          var workspace = {id: 'WORKSPACE-ID', appNavigation: [{name: 'Group', apps: [app1.id, app2.id]}]};
          var ctrl = buildController(workspace, [app1, app2]);

          // when
          var containsActiveApps = ctrl.containsActiveApps(workspace.appNavigation[0]);

          // then
          expect(containsActiveApps).toEqual(false);
        });

        it('should reorder apps via drag and drop', function () {
          // given
          var appOne = {id: 'APP-ID-ONE'};
          var appTwo = {id: 'APP-ID-TWO'};
          var workspace = {
            id: 'WORKSPACE-ID',
            appNavigation: [{name: '', apps: [appOne.id, appTwo.id]}],
            _permissions: {manageApps: true}
          };
          var ctrl = buildController(workspace, [appOne, appTwo]);

          senderNavigationUpdateService.updateNavigation.and.returnValue(
              $q.resolve('the-app-navigation-update-response'));
          senderNavigationUpdateService.prepareNavigationUpdateResponse.and.returnValue(
              [{name: '', apps: [appTwo.id, appOne.id]}]);

          // when
          ctrl.sender.appNavigation = [{name: '', apps: [appTwo.id, appOne.id]}];
          ctrl.treeOptions.dropped({
            source: {index: 0},
            dest: {index: 1}
          });
          $scope.$apply();

          // then
          expect(senderNavigationUpdateService.updateNavigation)
              .toHaveBeenCalledWith(ctrl.apps, [{name: '', apps: [appTwo.id, appOne.id]}], 'WORKSPACE-ID');
          expect(ctrl.sender.appNavigation[0]).toEqual({name: '', apps: [appTwo.id, appOne.id]});
          expect(ctrl.appsStatus.busy).toBe(false);
        });

        it('should update $stateParams to wiki if edit wiki is called and another app is marked as currentApp',
            function () {
              // given
              var app1 = {id: 'APP-ID-BLOG', slug: 'blogSlug', key: 'blog'};
              var app2 = {id: 'APP-ID-WIKI', slug: 'wikiSlug', key: 'wiki'};
              var page = {
                id: 'PAGE-ID',
                appNavigation: [{name: '', apps: [app1.id, app2.id]}],
                _permissions: {manageApps: true}
              };
              var eventSpyObj = jasmine.createSpyObj('$event', ['preventDefault', 'stopImmediatePropagation']);
              eventSpyObj.preventDefault.and.callThrough();
              eventSpyObj.stopImmediatePropagation.and.callThrough();
              var ctrl = buildController(page, [app1, app2]);

              ctrl.currentApp = app2.key;

              //--------------------------------------//
              // when
              ctrl.openSettings(app2, eventSpyObj);

              // then
              expect($stateParams.appIdOrSlug).toBe(app2.slug);

              //--------------------------------------//
              // when
              ctrl.openSettings(app1, eventSpyObj);

              // then
              expect($stateParams.appIdOrSlug).toBe(app1.slug);
            });
      });
    });
  });

})();
