(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'cleanPhoneNumber';

  describe('module: ' + moduleName, function () {
    var cleanPhoneNumber;

    beforeEach(function () {
      module(moduleName);

      inject(function (_cleanPhoneNumberFilter_) {
        cleanPhoneNumber = _cleanPhoneNumberFilter_;
      });
    });

    describe('filter: ' + targetName, function () {

      it('should be registered', function () {
        expect(cleanPhoneNumber).not.toBeUndefined();
      });

      it('should handle invalid inputs correctly', function () {
        expect(cleanPhoneNumber(undefined)).toBeUndefined();
        expect(cleanPhoneNumber(null)).toBeNull();
        expect(cleanPhoneNumber('')).toEqual('');
      });

      it('should clean phone number strings', function () {
        expect(cleanPhoneNumber('+49 (0)40 134527')).toEqual('+4940134527');
        expect(cleanPhoneNumber('+49 (1)40 134527')).toEqual('+49140134527');
        expect(cleanPhoneNumber('00 (0)40 134527')).toEqual('0040134527');
        expect(cleanPhoneNumber('0170 66.55.9-4')).toEqual('0170665594');
        expect(cleanPhoneNumber('1 (234) 567-1234')).toEqual('12345671234');
        expect(cleanPhoneNumber('1-234-567-8901 ext1234')).toEqual('123456789011234');
        expect(cleanPhoneNumber('1/234/567/8901')).toEqual('12345678901');
      });

    });
  });
})();
