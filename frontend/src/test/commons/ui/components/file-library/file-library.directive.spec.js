(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'FileLibraryController';

  describe('module: ' + moduleName, function () {
    var $controller, $q;
    var $log, $injector, $timeout, $scope, $rootScope;
    var Upload, modalService, coyoNotification, coyoConfig, authService, filenameModalService;
    var imageCropModal, FileModel, FolderModel, DocumentModel, coyoEndpoints, Pageable, fileDetailsModalService,
        fileAuthorService, editInOfficeService, fileService;
    var folder, backendUrlService, folderModel;

    beforeEach(function () {
      module(moduleName, function ($translateProvider) {
        $translateProvider.useLoader(null);
      });

      $log = jasmine.createSpyObj('$log', ['debug', 'warn']);
      Upload = jasmine.createSpyObj(Upload, ['upload', 'dataUrltoBlob']);
      modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);
      imageCropModal = jasmine.createSpyObj(imageCropModal, ['open']);
      coyoNotification = jasmine.createSpyObj('coyoNotification', ['error']);
      backendUrlService = jasmine.createSpyObj('backendUrlService', ['getUrl']);
      fileDetailsModalService = jasmine.createSpyObj('fileDetailsModalService', ['open']);
      editInOfficeService = jasmine.createSpyObj('editInOfficeService', ['canEditInOffice', 'editInOffice']);
      fileService = jasmine.createSpyObj('fileService', ['subscribeToLock']);
      coyoConfig = {
        fileLibrary: {
          senderTypes: {
            page: {
              model: {}
            },
            workspace: {
              model: {}
            }
          }
        }
      };
      FolderModel = function () {
        folderModel = jasmine.createSpyObj('FolderModel', ['createWithPermissions']);
        folderModel.createWithPermissions.and.returnValue({
          then: function (callback) {
            callback(folder);
            return {
              finally: function (finallyCallback) {
                finallyCallback();
              }
            };
          }
        });
        return folderModel;
      };

      folder = {id: 'FOLDER-iD', name: 'My New Folder'};

      inject(function (_$controller_, _$q_, _$rootScope_, _$injector_, _$timeout_, _authService_,
                       _filenameModalService_, _coyoEndpoints_, _Pageable_, _FileModel_, _DocumentModel_,
                       _fileAuthorService_) {
        $controller = _$controller_;
        $q = _$q_;

        $injector = _$injector_;
        $timeout = _$timeout_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        authService = _authService_;
        filenameModalService = _filenameModalService_;
        fileAuthorService = _fileAuthorService_;
        coyoEndpoints = _coyoEndpoints_;
        Pageable = _Pageable_;

        FileModel = _FileModel_;
        DocumentModel = _DocumentModel_;

        // mock screensize information
        $rootScope.screenSize = {
          isXs: false,
          isSm: false,
          isMd: true,
          isLg: false,
          isRetina: true
        };
        $scope.$parent.$ctrl = {
          app: {
            id: '123456789',
            slug: 'documents'
          }
        };
      });
    });

    describe('controller: ' + targetName, function () {

      function initCtrl() {
        var controller = $controller(targetName, {
          $log: $log,
          $injector: $injector,
          $timeout: $timeout,
          $scope: $scope,
          Upload: Upload,
          modalService: modalService,
          coyoNotification: coyoNotification,
          coyoConfig: coyoConfig,
          authService: authService,
          filenameModalService: filenameModalService,
          fileAuthorService: fileAuthorService,
          imageCropModal: imageCropModal,
          FileModel: FileModel,
          FolderModel: FolderModel,
          DocumentModel: DocumentModel,
          coyoEndpoints: coyoEndpoints,
          Pageable: Pageable,
          backendUrlService: backendUrlService,
          fileDetailsModalService: fileDetailsModalService,
          editInOfficeService: editInOfficeService,
          fileService: fileService
        });
        controller.$onInit();
        return controller;
      }

      it('should init', function () {
        // given
        spyOn(authService, 'getUser').and.callThrough();

        // when
        var ctrl = initCtrl();

        // then
        expect(ctrl).not.toBeUndefined();
        expect(ctrl.loading).toBeTruthy();
        expect(ctrl.senderTypes).not.toBeUndefined();
        expect(authService.getUser).toHaveBeenCalled();
        expect(ctrl.savingNewFolder).toBe(false);
      });

      it('should open home', function () {
        // given
        var ctrl = initCtrl();
        ctrl.view = 'foo';
        ctrl.files = [1, 2, 3];

        // when
        ctrl.openHome();

        // then
        expect(ctrl.files).toBeNull();
        expect(ctrl.loading).toBeFalsy();
        expect(ctrl.view).toBe('home');
      });

      it('should open senders', function () {
        // given
        var ctrl = initCtrl();
        ctrl.view = 'foo';
        ctrl.files = [1, 2, 3];

        var page = {
          content: ['a', 'b', 'c']
        };

        spyOn($injector, 'invoke').and.returnValue($q.resolve(page));

        // when
        ctrl.openSenders(coyoConfig.fileLibrary.senderTypes.page);
        $scope.$apply();

        // then
        expect(ctrl.view).toBe('senders');
        expect(ctrl.senderType).toBe(coyoConfig.fileLibrary.senderTypes.page);

        expect(ctrl.files).toBeNull();
        expect(ctrl.currentPage).toBe(page);
        expect(ctrl.senders).toEqual(page.content);
        expect(ctrl.loading).toBeFalsy();
      });

      it('should load file authors after loading files', function () {
        // given
        var ctrl = initCtrl();
        var fileId = '1234578797';
        var files = [{id: fileId, name: 'image-one', type: 'image/jpeg'}];
        var authors = {'1234578797': {displayName: 'Robert Lang'}};
        ctrl.files = files;
        ctrl.authors = {};
        var sender = {
          id: 'senderId',
          typeName: 'testType',
          _permissions: {createFile: true}
        };
        var page = {
          content: [files]
        };

        spyOn(FileModel, 'pagedQueryWithPermissions').and.returnValue($q.resolve(page));
        spyOn(fileAuthorService, 'loadFileAuthors');
        fileAuthorService.loadFileAuthors.and.returnValue($q.resolve(authors));
        spyOn(_, 'map').and.callFake(function () {
          return [fileId];
        });

        // when
        ctrl.openSender(sender);
        $scope.$apply();

        // then
        expect(fileAuthorService.loadFileAuthors).toHaveBeenCalledTimes(1);
        expect(fileAuthorService.loadFileAuthors)
            .toHaveBeenCalledWith(ctrl.sender.id, ctrl.appIdOrSlug, [fileId], ctrl.showAuthors);
        expect(ctrl.authors).toEqual(authors);
      });

      it('should not load file author when no files', function () {
        // given
        var ctrl = initCtrl();
        ctrl.files = [];
        var sender = {
          id: 'senderId',
          typeName: 'testType',
          _permissions: {createFile: true}
        };
        var page = {
          content: []
        };

        spyOn(FileModel, 'pagedQueryWithPermissions').and.returnValue($q.resolve(page));
        spyOn(fileAuthorService, 'loadFileAuthors');

        // when
        ctrl.openSender(sender);
        $scope.$apply();

        // then
        expect(fileAuthorService.loadFileAuthors).not.toHaveBeenCalled();
      });

      it('should not load author for folders', function () {
        // given
        var ctrl = initCtrl();
        var fileId1 = '1234578797';
        var fileId2 = '987654321';
        var folderId = '12345111178797';
        var files = [{id: fileId1, name: 'image-one', type: 'image/jpeg'},
          {id: fileId2, name: 'image-one', type: 'image/jpeg'}, {id: folderId, folder: true}];
        var authors = {'1234578797': {displayName: 'Robert Lang'}};
        ctrl.files = files;
        ctrl.authors = {};
        var sender = {
          id: 'senderId',
          typeName: 'testType',
          _permissions: {createFile: true}
        };
        var page = {
          content: files
        };

        spyOn(FileModel, 'pagedQueryWithPermissions').and.returnValue($q.resolve(page));
        spyOn(fileAuthorService, 'loadFileAuthors');
        fileAuthorService.loadFileAuthors.and.returnValue($q.resolve(authors));
        // spyOn(_, 'map').and.callFake(function () {
        // return [fileId];
        // });

        // when
        ctrl.openSender(sender);
        $scope.$apply();

        // then
        expect(fileAuthorService.loadFileAuthors)
            .toHaveBeenCalledWith(ctrl.sender.id, ctrl.appIdOrSlug, [fileId1, fileId2], ctrl.showAuthors);

      });

      it('should open root files of sender', function () {
        // given
        var ctrl = initCtrl();
        ctrl.view = 'foo';
        ctrl.files = [1, 2, 3];
        ctrl.senderTypes.testType = {label: 'test', plural: 'Tests', model: 'TestModel'};

        var sender = {
          id: 'senderId',
          typeName: 'testType',
          _permissions: {createFile: true}
        };

        var page = {
          content: ['a', 'b', 'c']
        };

        spyOn(FileModel, 'pagedQueryWithPermissions').and.returnValue($q.resolve(page));

        // when
        ctrl.openSender(sender);
        $scope.$apply();

        // then
        expect(ctrl.senders).toBeNull();
        expect(ctrl.currentPage).toBe(page);
        expect(ctrl.files).toEqual(page.content);
        expect(ctrl.loading).toBeFalsy();
        expect(ctrl.view).toBe('files');
        expect(ctrl.senderType).toEqual(ctrl.senderTypes.testType);
        expect(ctrl.parent).toBeNull();
        expect(ctrl.canManageFolder).toBe(true);
      });

      it('should open folder of sender', function () {
        // given
        var ctrl = initCtrl();
        ctrl.view = 'foo';
        ctrl.files = [1, 2, 3];
        ctrl.senderTypes.testType = {label: 'test', plural: 'Tests', model: 'TestModel'};

        var sender = {
          id: 'senderId',
          typeName: 'testType',
          _permissions: {manage: true}
        };

        var folder = {
          id: 'folder',
          folder: true
        };

        var folderPermissions = {
          id: 'folder',
          folder: true,
          _permissions: {manage: true}
        };

        var page = {
          content: ['a', 'b', 'c']
        };

        spyOn(FileModel, 'pagedQueryWithPermissions').and.returnValue($q.resolve(page));
        spyOn(FileModel, 'getWithPermissions').and.returnValue($q.resolve(folderPermissions));

        // when
        ctrl.handleClick(sender, folder);
        $scope.$apply();

        // then
        expect(ctrl.senders).toBeNull();
        expect(ctrl.currentPage).toBe(page);
        expect(ctrl.files).toEqual(page.content);
        expect(ctrl.loading).toBeFalsy();
        expect(ctrl.view).toBe('files');
        expect(ctrl.senderType).toEqual(ctrl.senderTypes.testType);
        expect(ctrl.parent).toBe(folderPermissions);
        expect(ctrl.canManageFolder).toBeTruthy();
      });

      it('should select a single file', function () {
        // given
        var options = {selectMode: 'single'};
        var sender = {id: 'SENDER-ID'};
        var file = {
          id: 'FILE-ID',
          folder: false
        };
        var ctrl = initCtrl();
        ctrl.sender = sender;
        ctrl.options = options;

        // when
        ctrl.handleClick(sender, file);
        $scope.$apply();

        // then
        expect(_.some(ctrl.selectedFiles, {id: file.id, senderId: sender.id, folder: false})).toBe(true);
      });

      it('should refresh the file model after a new versions was uploaded in file details view',
          function () {
            // given
            var options = {};
            var sender = {
              id: 'SENDER-ID',
              _permissions: {
                manage: true
              }
            };
            var file = {
              id: 'FILE-ID',
              folder: false,
              uploading: false,
              uploadFailed: false,
              size: 0
            };
            var ctrl = initCtrl();
            ctrl.sender = sender;
            ctrl.options = options;
            ctrl.files = [file];
            ctrl.currentPage = {
              last: false,
              totalElements: 2
            };

            fileDetailsModalService.open.and.returnValue({
              result: $q.resolve([{id: file.id, size: 42}])
            });

            // when
            ctrl.handleClick(sender, file);
            $scope.$apply();

            // then
            expect(ctrl.files[0].size).toBe(42);
          });

      it('should select multiple files', function () {
        // given
        var options = {selectMode: 'multiple'};
        var sender = {id: 'SENDER-ID'};
        var file1 = {
          id: 'FILE-ID-1',
          folder: false
        };
        var file2 = {
          id: 'FILE-ID-2',
          folder: false
        };
        var ctrl = initCtrl();
        ctrl.sender = sender;
        ctrl.options = options;

        // when
        ctrl.handleClick(sender, file1);
        ctrl.handleClick(sender, file2);
        $scope.$apply();

        // then
        expect(_.some(ctrl.selectedFiles,
            {id: file1.id, senderId: sender.id, folder: false},
            {id: file2.id, senderId: sender.id, folder: false})).toBe(true);
      });

      it('should select and deselect multiple files', function () {
        // given
        var options = {selectMode: 'multiple'};
        var sender = {id: 'SENDER-ID'};
        var file1 = {
          id: 'FILE-ID-1',
          folder: false
        };
        var file2 = {
          id: 'FILE-ID-2',
          folder: false
        };
        var ctrl = initCtrl();
        ctrl.sender = sender;
        ctrl.options = options;

        // when
        ctrl.handleClick(sender, file1); // select
        ctrl.handleClick(sender, file2); // select
        $scope.$apply();
        ctrl.handleClick(sender, file1); // deselect
        ctrl.handleClick(sender, file2); // deselect
        $scope.$apply();
        ctrl.handleClick(sender, file1); // select
        ctrl.handleClick(sender, file2); // select
        $scope.$apply();
        ctrl.handleClick(sender, file1); // deselect
        $scope.$apply();

        // then
        expect(_.some(ctrl.selectedFiles, {id: file1.id, senderId: sender.id, folder: false})).toBe(false);
        expect(_.some(ctrl.selectedFiles, {id: file2.id, senderId: sender.id, folder: false})).toBe(true);
      });

      it('should select all current files', function () {
        // given
        var options = {selectMode: 'multiple'};
        var sender = {id: 'SENDER-ID'};
        var file1 = {
          id: 'FILE-ID-1',
          folder: false
        };
        var file2 = {
          id: 'FILE-ID-2',
          folder: false
        };
        var folder = {
          id: 'FOLDER-ID-1',
          folder: true
        };

        var ctrl = initCtrl();
        ctrl.sender = sender;
        ctrl.options = options;
        ctrl.files = [folder, file1, file2];

        // when
        ctrl.selectAll();
        $scope.$apply();

        // then
        expect(ctrl.selectedFiles).toBeArrayOfSize(2);
        expect(ctrl.selectedFiles[0]).toBe(file1);
        expect(ctrl.selectedFiles[1]).toBe(file2);
        expect(_.some(ctrl.selectedFiles, {id: folder.id})).toBe(false);
      });

      it('should deselect all current files', function () {
        // given
        var options = {selectMode: 'multiple'};
        var sender = {id: 'SENDER-ID'};
        var file1 = {
          id: 'FILE-ID-1',
          folder: false
        };
        var file2 = {
          id: 'FILE-ID-2',
          folder: false
        };
        var folder = {
          id: 'FOLDER-ID-1',
          folder: true
        };

        var ctrl = initCtrl();
        ctrl.sender = sender;
        ctrl.options = options;
        ctrl.files = [folder, file1, file2];
        ctrl.selectedFiles = [file1, file2];

        // when
        ctrl.deselectAll();
        $scope.$apply();

        // then
        expect(ctrl.selectedFiles).toBeArrayOfSize(0);
      });

      it('should check whether a file is currently selected', function () {
        // given
        var options = {selectMode: 'multiple'};
        var sender = {id: 'SENDER-ID'};
        var file1 = {
          id: 'FILE-ID-1',
          folder: false
        };
        var file2 = {
          id: 'FILE-ID-2',
          folder: false
        };
        var file3 = {
          id: 'FILE-ID-3',
          folder: false
        };
        var folder = {
          id: 'FOLDER-ID-1',
          folder: true
        };

        var ctrl = initCtrl();
        ctrl.sender = sender;
        ctrl.options = options;
        ctrl.selectedFiles = [file1, file2];

        // when
        var file1Selected = ctrl.isSelected(file1);
        var file2Selected = ctrl.isSelected(file2);
        var file3Selected = ctrl.isSelected(file3);
        var folderSelected = ctrl.isSelected(folder);
        $scope.$apply();

        // then
        expect(file1Selected).toBeTruthy();
        expect(file2Selected).toBeTruthy();
        expect(file3Selected).toBeFalsy();
        expect(folderSelected).toBeFalsy();
      });

      it('should check whether a file is selectable', function () {
        // given
        var options = {
          selectMode: 'multiple',
          filterContentType: 'image'
        };
        var sender = {id: 'SENDER-ID'};
        var file1 = {
          id: 'FILE-ID-1',
          folder: false,
          contentType: 'image/png'
        };
        var file2 = {
          id: 'FILE-ID-2',
          folder: false,
          contentType: 'someOtherContentType'
        };
        var file3 = {
          id: 'FILE-ID-3',
          folder: false,
          contentType: 'image/jpg'
        };
        var folder = {
          id: 'FOLDER-ID-1',
          folder: true
        };

        var ctrl = initCtrl();
        ctrl.sender = sender;
        ctrl.options = options;
        ctrl.files = [folder, file1, file2, file3];

        // when
        var file1Selectable = ctrl.isSelectable(file1);
        var file2Selectable = ctrl.isSelectable(file2);
        var file3Selectable = ctrl.isSelectable(file3);
        var folderSelectable = ctrl.isSelectable(folder);
        $scope.$apply();

        // then
        expect(file1Selectable).toBe(true);
        expect(file2Selectable).toBe(false);
        expect(file3Selectable).toBe(true);
        expect(folderSelectable).toBe(true);
      });

      it('should return download url', function () {
        // given
        var ctrl = initCtrl();
        delete ctrl.selected;
        var backendUrl = 'http://foo.bar:1234';
        backendUrlService.getUrl.and.returnValue(backendUrl);

        var sender = {
          id: 'senderId'
        };

        var file = {
          id: 'fileId',
          folder: false
        };

        // when
        var url = ctrl.getDownloadUrl(sender, file);

        // then
        expect(url).toEqual(backendUrl + '/web/senders/' + sender.id + '/documents/' + file.id);
      });

      it('should load more senders', function () {
        // given
        var ctrl = initCtrl();
        ctrl.view = 'senders';
        ctrl.loading = false;
        ctrl.currentPage = {
          last: false
        };
        ctrl.senders = [1, 2, 3];
        ctrl.senderType = coyoConfig.fileLibrary.senderTypes.workspace;

        spyOn($injector, 'invoke').and.returnValue($q.resolve({
          content: [4, 5, 6]
        }));

        // when
        ctrl.loadMore();
        expect(ctrl.loading).toBe(true);
        $scope.$apply();

        // then
        expect(ctrl.senders).toEqual([1, 2, 3, 4, 5, 6]);
        expect(ctrl.loading).toBe(false);
      });

      it('should load more files', function () {
        // given
        var ctrl = initCtrl();
        ctrl.view = 'files';
        ctrl.loading = false;
        ctrl.currentPage = {
          last: false
        };
        ctrl.files = [1, 2, 3];
        ctrl.sender = {_permissions: {}};

        spyOn(FileModel, 'pagedQueryWithPermissions').and.returnValue($q.resolve({}));

        // when
        ctrl.loadMore();
        $scope.$apply();

        // then
        expect(FileModel.pagedQueryWithPermissions).toHaveBeenCalled();
      });

      it('should not load more when already loading', function () {
        // given
        var ctrl = initCtrl();
        ctrl.view = 'files';
        ctrl.loading = true;
        ctrl.currentPage = {
          last: false
        };
        ctrl.files = [1, 2, 3];
        ctrl.sender = {};

        spyOn(FileModel, 'pagedQuery');

        // when
        ctrl.loadMore();

        // then
        expect(FileModel.pagedQuery).not.toHaveBeenCalled();
      });

      it('should not load more when last page was reached', function () {
        // given
        var ctrl = initCtrl();
        ctrl.view = 'files';
        ctrl.loading = false;
        ctrl.currentPage = {
          last: true
        };
        ctrl.files = [1, 2, 3];
        ctrl.sender = {};

        spyOn(FileModel, 'pagedQuery');

        // when
        ctrl.loadMore();

        // then
        expect(FileModel.pagedQuery).not.toHaveBeenCalled();
      });

      it('should delete a file', function () {
        // given
        spyOn(FileModel, '$delete');
        FileModel.$delete.and.returnValue($q.resolve());
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});

        var fileOne = {id: 'file-one', name: 'file_one.txt'};
        var fileTwo = {id: 'file-two', name: 'file_two.txt'};

        var ctrl = initCtrl();
        ctrl.view = 'files';
        ctrl.sender = {id: 'sender-id'};
        ctrl.files = [fileOne, fileTwo];

        // when
        ctrl.deleteFile(fileOne);
        $scope.$apply();

        // then
        expect(FileModel.$delete).toHaveBeenCalled();

        var args = FileModel.$delete.calls.mostRecent().args;
        expect(args[0].indexOf(ctrl.sender.id)).toBeGreaterThan(-1);
        expect(args[0].indexOf(fileOne.id)).toBeGreaterThan(-1);

        expect(ctrl.files).toBeArrayOfSize(1);
        expect(_.some(ctrl.files, fileOne)).toBe(false);
        expect(_.some(ctrl.files, fileTwo)).toBe(true);
        expect(ctrl.loading).toBe(false);
      });

      it('should open a sender when passed', function () {
        // given
        var sender = {id: 'SENDER-ID'};
        spyOn(authService, 'getUser');
        spyOn(FileModel, 'pagedQueryWithPermissions').and.returnValue($q.resolve({}));
        authService.getUser.and.returnValue($q.resolve({id: 'USER-ID'}));

        // when
        var ctrl = initCtrl();
        ctrl.sender = sender;
        $scope.$apply();

        // then
        expect(FileModel.pagedQueryWithPermissions).toHaveBeenCalled();
        expect(ctrl.view).toBe('files');
        expect(ctrl.sender).toBe(sender);
      });

      it('should create a new folder', function () {
        // given
        var sender = {id: 'SENDER-ID'};
        var ctrl = initCtrl();
        ctrl.sender = sender;
        ctrl.files = [];
        _.set(folder, '_permissions.manage', true);

        // when
        ctrl.createFolder();
        ctrl.saveNewFolder(ctrl.newFolder, folder.name);
        $scope.$apply();

        // then
        expect(folderModel.createWithPermissions).toHaveBeenCalledWith(['manage']);
        expect(ctrl.newFolder).toBeNull();
        var newFolder = _.find(ctrl.files, {id: folder.id});
        expect(newFolder).toBeDefined();
        expect(newFolder._permissions.manage).toBe(true);
      });

      it('should not create a new folder if name is empty', function () {
        // given
        var sender = {id: 'SENDER-ID'};
        var ctrl = initCtrl();
        ctrl.sender = sender;
        ctrl.files = [];

        // when
        ctrl.createFolder();
        ctrl.newFolder.name = '';
        ctrl.saveNewFolder();

        // then
        expect(ctrl.newFolder).toBeNull();
        expect(ctrl.files).toBeEmptyArray();
      });

      it('should disable saving a new folder while saving one', function () {
        // given
        var sender = {id: 'SENDER-ID'};
        var ctrl = initCtrl();
        ctrl.sender = sender;

        ctrl.createFolder();
        folderModel.createWithPermissions = jasmine.createSpy('createWithPermissions');
        folderModel.createWithPermissions.and.returnValue($q.resolve());

        ctrl.saveNewFolder(ctrl.newFolder, folder.name);
        expect(ctrl.savingNewFolder).toBe(true);

        // when
        ctrl.saveNewFolder(ctrl.newFolder, folder.name);
        ctrl.saveNewFolder(ctrl.newFolder, folder.name);
        $scope.$apply();

        // then
        expect(ctrl.savingNewFolder).toBe(false);
        expect(folderModel.createWithPermissions.calls.count()).toBe(1);
      });

      it('should open a modal for creating a new folder on mobile', function () {
        // given
        var sender = {id: 'SENDER-ID'};
        var ctrl = initCtrl();
        ctrl.sender = sender;
        ctrl.files = [];

        spyOn(filenameModalService, 'open');
        filenameModalService.open.and.returnValue($q.resolve(folder.name));

        // when
        ctrl.openFolderModal();
        $scope.$apply();

        // then
        expect(ctrl.newFolder).toBeNull();
        expect(_.some(ctrl.files, {id: folder.id})).toBe(true);
      });

      it('should open a modal for renaming an existing folder on mobile', function () {
        // given
        var updatedFolder = {id: folder.id, name: 'New Name'};
        var ctrl = initCtrl();
        ctrl.sender = {id: 'SENDER-ID'};

        spyOn(filenameModalService, 'open');
        filenameModalService.open.and.returnValue($q.resolve(updatedFolder.name));

        spyOn(FileModel, 'rename');
        FileModel.rename.and.returnValue($q.resolve(updatedFolder));

        // when
        ctrl.openFilenameModal(folder);
        $scope.$apply();

        // then
        expect(filenameModalService.open).toHaveBeenCalledWith('FILE_LIBRARY.MODAL.TITLE.RENAME', folder.name);
        expect(FileModel.rename).toHaveBeenCalledWith(ctrl.sender.id, folder, updatedFolder.name);
      });

      it('should check for version of files', function () {
        // given
        var file = {name: 'file', type: 'some/type', id: 'FILEMODEL-ID'};
        var fileModel = {id: 'FILEMODEL-ID', contentType: 'some/type'};
        var ctrl = initCtrl();
        ctrl.sender = {id: 'SENDER-ID'};
        ctrl.files = [file];
        Upload.upload.and.returnValue($q.resolve({data: {size: 42}}));

        // when
        ctrl.uploadVersion(fileModel, file);
        $scope.$apply();
        $timeout.flush();

        // then
        expect(Upload.upload).toHaveBeenCalledTimes(1);
        expect(ctrl.files).toBeArrayOfSize(1);
        expect(fileModel.uploading).toBe(false);
        expect(fileModel.size).toBe(42);
        expect(ctrl.files[0]).toEqual({size: 42});
      });

      it('should automatically select uploaded files', function () {
        // given
        var sender = {id: 'SENDER-ID'};
        var files = [{name: 'file-one', type: 'some/type'}, {name: 'file-two', type: 'some/type'}];
        var options = {selectMode: 'multiple'};
        var ctrl = initCtrl();
        ctrl.files = [];
        ctrl.sender = sender;
        ctrl.options = options;
        Upload.upload.and.returnValue($q.resolve({data: {size: 42}}));

        // when
        ctrl.uploadFiles(files);
        $scope.$apply();
        $timeout.flush();

        // then
        expect(Upload.upload).toHaveBeenCalledTimes(2);
        expect(ctrl.files).toBeArrayOfSize(2);

        expect(ctrl.selectedFiles).toBeArrayOfSize(2);
        expect(ctrl.selectedFiles[0].name).toBe(files[0].name);
        expect(ctrl.selectedFiles[0].size).toBe(42);
        expect(ctrl.selectedFiles[1].name).toBe(files[1].name);
        expect(ctrl.selectedFiles[1].size).toBe(42);
      });

      it('should upload an image and open crop modal', function () {
        // given
        var sender = {id: 'SENDER-ID'};
        var parent = {id: 'PARENT-ID'};
        var files = [{name: 'image-one', type: 'image/jpeg'}];

        imageCropModal.open.and.returnValue($q.resolve(files[0]));
        Upload.upload.and.returnValue($q.resolve({data: {size: 42}}));
        Upload.dataUrltoBlob.and.returnValue({});

        var ctrl = initCtrl();
        ctrl.files = [];
        ctrl.sender = sender;
        ctrl.parent = parent;
        ctrl.uploadMultiple = false;
        _.set(ctrl.cropSettings, 'cropImage', true);

        // when
        ctrl.uploadFiles(files);
        $scope.$apply();
        $timeout.flush();

        // then
        expect(Upload.upload).toHaveBeenCalledTimes(1);
        expect(imageCropModal.open).toHaveBeenCalledTimes(1);
        expect(ctrl.files).toBeArrayOfSize(1);
        expect(ctrl.files[0].name).toBe(files[0].name);
        expect(ctrl.files[0].uploading).toBe(false);
        expect(ctrl.files[0].size).toBe(42);
      });

      it('should warn if both multiple file option and crop option is set', function () {
        // given
        var options = {uploadMultiple: true};
        var cropSettings = {cropImage: true};
        var sender = {id: 'SENDER-ID'};
        var parent = {id: 'PARENT-ID'};
        var files = [{name: 'image-one', type: 'image/jpeg'}, {name: 'image-two', type: 'image/jpeg'}];

        imageCropModal.open.and.returnValue($q.resolve(files[0]));
        Upload.upload.and.returnValue($q.resolve({data: {size: 42}}));
        Upload.dataUrltoBlob.and.returnValue({});

        var ctrl = initCtrl();
        ctrl.files = [];
        ctrl.sender = sender;
        ctrl.parent = parent;
        ctrl.options = options;
        ctrl.cropSettings = cropSettings;

        // when
        ctrl.uploadFiles(files);
        $scope.$apply();
        $timeout.flush();

        // then
        expect(Upload.upload).toHaveBeenCalledTimes(2);
        expect(imageCropModal.open).not.toHaveBeenCalled();
        expect($log.warn).toHaveBeenCalledTimes(2);
        expect(ctrl.files).toBeArrayOfSize(2);
      });

      it('should warn if crop option is set and file is not an image', function () {
        // given
        var sender = {id: 'SENDER-ID'};
        var parent = {id: 'PARENT-ID'};
        var files = [{name: 'image-one', type: 'some/type'}];

        imageCropModal.open.and.returnValue($q.resolve(files[0]));
        Upload.upload.and.returnValue($q.resolve({data: {size: 42}}));
        Upload.dataUrltoBlob.and.returnValue({});

        var ctrl = initCtrl();
        ctrl.files = [];
        ctrl.sender = sender;
        ctrl.parent = parent;
        ctrl.uploadMultiple = false;
        _.set(ctrl.cropSettings, 'cropImage', true);

        // when
        ctrl.uploadFiles(files);
        $scope.$apply();
        $timeout.flush();

        // then
        expect(Upload.upload).toHaveBeenCalledTimes(1);
        expect(imageCropModal.open).not.toHaveBeenCalled();
        expect($log.warn).toHaveBeenCalledTimes(1);
        expect(ctrl.files).toBeArrayOfSize(1);
      });

      it('should load file author after uploading file', function () {
        // given
        var sender = {id: 'SENDER-ID'};
        var parent = {id: 'PARENT-ID'};
        var fileId = '1234578797';
        var files = [{id: fileId, name: 'image-one', type: 'image/jpeg'}];
        var authors = {'1234578797': {displayName: 'Robert Lang'}};
        var appIdOrSlug = 'kjahsdhkjh12h3kh';

        imageCropModal.open.and.returnValue($q.resolve(files[0]));
        Upload.upload.and.returnValue($q.resolve({data: {size: 42}}));
        Upload.dataUrltoBlob.and.returnValue({});
        spyOn(fileAuthorService, 'loadFileAuthors');
        fileAuthorService.loadFileAuthors.and.returnValue($q.resolve(authors));
        spyOn(_, 'map').and.callFake(function () {
          return [fileId];
        });

        var ctrl = initCtrl();
        ctrl.files = [];
        ctrl.authors = {};
        ctrl.sender = sender;
        ctrl.parent = parent;
        ctrl.uploadMultiple = false;
        ctrl.appIdOrSlug = appIdOrSlug;
        ctrl.showAuthors = true;
        _.set(ctrl.cropSettings, 'cropImage', true);

        // when
        ctrl.uploadFiles(files);
        $scope.$apply();
        $timeout.flush();

        // then
        expect(fileAuthorService.loadFileAuthors).toHaveBeenCalledTimes(1);
        expect(fileAuthorService.loadFileAuthors)
            .toHaveBeenCalledWith(ctrl.sender.id, ctrl.appIdOrSlug, [fileId], ctrl.showAuthors);
        expect(ctrl.authors).toEqual(authors);
      });

      it('should handle dropped files', function () {
        // given
        var files = [{name: 'file-one', type: 'some/type'}, {name: 'file-two', type: 'some/type'}];
        var ctrl = initCtrl();
        ctrl.files = [];
        ctrl.sender = {id: 'SENDER-ID'};
        ctrl.parent = {id: 'PARENT-ID'};
        Upload.upload.and.returnValue($q.resolve({}));

        // when
        ctrl.droppedFiles = files;
        $scope.$apply();
        $timeout.flush();

        // then
        expect(Upload.upload).toHaveBeenCalledTimes(2);
        expect(ctrl.files).toBeArrayOfSize(2);
        expect(ctrl.files[0].name).toBe(files[1].name);
        expect(ctrl.files[0].uploading).toBe(false);
        expect(ctrl.files[1].name).toBe(files[0].name);
        expect(ctrl.files[1].uploading).toBe(false);
      });

      it('should toggle to rename mode', function () {
        // given
        var file = {
          id: 'FILE-ID',
          name: 'My File'
        };
        var ctrl = initCtrl();

        // when
        ctrl.toggleRename(file);

        // then
        expect(ctrl.newFilename).toBe(file.name);
        expect(file.editMode).toBe(true);
      });

      it('should rename a file or folder', function () {
        // given
        var newName = 'New Name';
        var updatedFolder = angular.extend({}, folder, {name: newName});

        var ctrl = initCtrl();
        ctrl.sender = {id: 'SENDER-ID'};
        ctrl.files = [folder];

        spyOn(FileModel, 'rename');
        FileModel.rename.and.returnValue($q.resolve(updatedFolder));

        // when
        ctrl.renameFile(folder, newName);
        $scope.$apply();

        // then
        expect(FileModel.rename).toHaveBeenCalledWith(ctrl.sender.id, folder, newName);
        expect(ctrl.files[0]).toEqual(updatedFolder);
        expect(ctrl.files[0].editMode).toBeUndefined();
      });

      it('should not rename a file if name has not changed', function () {
        // given
        var ctrl = initCtrl();
        spyOn(FileModel, 'rename');

        // when
        ctrl.renameFile(folder, folder.name);

        // then
        expect(FileModel.rename).not.toHaveBeenCalled();
        expect(folder.editMode).toBe(false);
      });

      it('should not rename a file if name is empty', function () {
        // given
        var ctrl = initCtrl();
        spyOn(FileModel, 'rename');

        // when
        ctrl.renameFile(folder, '');

        // then
        expect(FileModel.rename).not.toHaveBeenCalled();
        expect(folder.editMode).toBe(false);
      });

      it('should move file to folder', function () {
        // given
        var parent = {id: 'PARENT-ID', folder: true, childCount: 0};
        var fileToMove = {id: 'FILE-ID'};
        var fileMoved = {id: 'FILE-ID', parent: parent};

        var ctrl = initCtrl();
        ctrl.sender = {id: 'SENDER-ID'};
        ctrl.files = [fileToMove, parent];
        spyOn(FileModel, 'move').and.returnValue($q.resolve(fileMoved));

        // when
        ctrl.moveSelectedFiles(fileToMove, parent);

        // then
        expect(FileModel.move).toHaveBeenCalledTimes(1);
        expect(ctrl.loadingMove[fileToMove.id]).toBeTrue();

        $scope.$apply();

        expect(ctrl.loadingMove[fileToMove.id]).toBeUndefined();
        expect(ctrl.files).toBeArrayOfSize(1);
        expect(ctrl.files[0]).toBe(parent);
        expect(parent.childCount).toBe(1);
      });

      it('should move file to root folder', function () {
        // given
        var parent = {id: 'PARENT-ID', folder: true, childCount: 0};
        var fileToMove = {id: 'FILE-ID', parent: parent};
        var fileMoved = {id: 'FILE-ID', parent: null};

        var ctrl = initCtrl();
        ctrl.sender = {id: 'SENDER-ID'};
        ctrl.files = [fileToMove];
        spyOn(FileModel, 'move').and.returnValue($q.resolve(fileMoved));

        // when
        ctrl.moveSelectedFiles(fileToMove, null);

        // then
        expect(FileModel.move).toHaveBeenCalledTimes(1);
        expect(ctrl.loadingMove[fileToMove.id]).toBeTrue();

        $scope.$apply();

        expect(ctrl.loadingMove[fileToMove.id]).toBeUndefined();
        expect(ctrl.files).toBeArrayOfSize(0);
      });

      it('should not move file when loading', function () {
        // given
        var fileToMove = {id: 'FILE-ID'};

        var ctrl = initCtrl();
        ctrl.loadingMove[fileToMove.id] = true;
        spyOn(FileModel, 'move');

        // when
        ctrl.moveSelectedFiles(fileToMove, null);

        // then
        $scope.$apply();
        expect(FileModel.move).not.toHaveBeenCalled();
      });

      it('should not move file to invalid destination', function () {
        // given
        var fileToMove = {id: 'FILE-ID'};
        var parent = {id: 'PARENT-ID', folder: false};

        var ctrl = initCtrl();
        spyOn(FileModel, 'move');

        // when
        ctrl.moveSelectedFiles(fileToMove, parent);

        // then
        $scope.$apply();
        expect(FileModel.move).not.toHaveBeenCalled();
      });

      it('should not move file to same folder', function () {
        // given
        var fileToMove = {id: 'FILE-ID'};

        var ctrl = initCtrl();
        spyOn(FileModel, 'move');

        // when
        ctrl.moveSelectedFiles(fileToMove, fileToMove);

        // then
        $scope.$apply();
        expect(FileModel.move).not.toHaveBeenCalled();
      });

      it('should move selected files to folder', function () {
        // given
        var parent = {id: 'PARENT-ID', folder: true, childCount: 0};
        var fileToMove1 = {id: 'FILE-ID1'};
        var fileToMove2 = {id: 'FILE-ID2'};
        var fileMoved1 = {id: 'FILE-ID1', parent: parent};
        var fileMoved2 = {id: 'FILE-ID2', parent: parent};

        var ctrl = initCtrl();
        ctrl.sender = {id: 'SENDER-ID'};
        ctrl.files = [fileToMove1, fileToMove2, parent];
        ctrl.options.isMultipleActivatedByButton = true;
        spyOn(FileModel, 'moveMultiple').and.returnValue($q.resolve(
            [fileMoved1, fileMoved2]
        ));

        // when
        ctrl.selectedFiles = [fileToMove1, fileToMove2];
        ctrl.moveSelectedFiles(fileToMove1, parent);

        // then
        expect(FileModel.moveMultiple).toHaveBeenCalled();

        $scope.$apply();

        expect(ctrl.files).toBeArrayOfSize(1);
        expect(ctrl.files[0]).toBe(parent);
        expect(parent.childCount).toBe(2);
      });

      it('should set files loading when moving them', function () {
        // given
        var parent = {id: 'PARENT-ID', folder: true, childCount: 0};
        var fileToMove1 = {id: 'FILE-ID1'};
        var fileToMove2 = {id: 'FILE-ID2'};

        var ctrl = initCtrl();
        ctrl.sender = {id: 'SENDER-ID'};
        ctrl.files = [fileToMove1, fileToMove2, parent];
        ctrl.options.isMultipleActivatedByButton = true;

        // when
        ctrl.selectedFiles = [fileToMove1, fileToMove2];
        ctrl.moveSelectedFiles(null, parent);

        // then
        expect(ctrl.loadingMove[fileToMove1.id]).toBe(true);
        expect(ctrl.loadingMove[fileToMove2.id]).toBe(true);
      });

      it('should not move selected files when loading', function () {
        // given
        var parent = {id: 'PARENT-ID', folder: true, childCount: 0};
        var fileToMove1 = {id: 'FILE-ID1'};
        var fileToMove2 = {id: 'FILE-ID2'};
        var fileMoved2 = {id: 'FILE-ID2', parent: parent};

        var ctrl = initCtrl();
        ctrl.sender = {id: 'SENDER-ID'};
        ctrl.files = [fileToMove1, fileToMove2, parent];
        ctrl.loadingMove[fileToMove1.id] = true;
        ctrl.options.isMultipleActivatedByButton = true;
        spyOn(FileModel, 'moveMultiple').and.returnValue($q.resolve(
            [fileMoved2]
        ));

        // when
        ctrl.selectedFiles = [fileToMove1, fileToMove2];
        ctrl.moveSelectedFiles(null, parent);

        // then
        expect(FileModel.moveMultiple).toHaveBeenCalledWith(ctrl.sender.id, [fileToMove2.id], parent.id);

        $scope.$apply();

        expect(ctrl.files).toBeArrayOfSize(2);
        expect(ctrl.files[0]).toBe(fileToMove1);
        expect(ctrl.files[1]).toBe(parent);
        expect(parent.childCount).toBe(1);
      });

      it('should not move selected files into a document (not folder)', function () {
        // given
        var destinationFile = {id: 'DESTINATION-ID'};
        var fileToMove1 = {id: 'FILE-ID1'};
        var fileToMove2 = {id: 'FILE-ID2'};

        var ctrl = initCtrl();
        ctrl.sender = {id: 'SENDER-ID'};
        ctrl.files = [fileToMove1, fileToMove2, destinationFile];
        ctrl.options.isMultipleActivatedByButton = true;
        spyOn(FileModel, 'moveMultiple');

        // when
        ctrl.selectedFiles = [fileToMove1, fileToMove2];
        ctrl.moveSelectedFiles(null, destinationFile);

        // then
        expect(FileModel.moveMultiple).not.toHaveBeenCalled();

        $scope.$apply();

        expect(ctrl.files).toBeArrayOfSize(3);
        expect(ctrl.files[0]).toBe(fileToMove1);
        expect(ctrl.files[1]).toBe(fileToMove2);
        expect(ctrl.files[2]).toBe(destinationFile);
      });

      it('should not move selected files into themselves', function () {
        // given
        var parent = {id: 'PARENT-ID', folder: true, childCount: 0};
        var fileToMove1 = {id: 'FILE-ID1'};
        var fileMoved1 = {id: 'FILE-ID1', parent: parent};

        var ctrl = initCtrl();
        ctrl.sender = {id: 'SENDER-ID'};
        ctrl.files = [fileToMove1, parent];
        ctrl.options.isMultipleActivatedByButton = true;
        spyOn(FileModel, 'moveMultiple').and.returnValue($q.resolve(
            [fileMoved1]
        ));

        // when
        ctrl.selectedFiles = [fileToMove1, parent];
        ctrl.moveSelectedFiles(null, parent);

        // then
        expect(FileModel.moveMultiple).toHaveBeenCalledWith(ctrl.sender.id, [fileToMove1.id], parent.id);

        $scope.$apply();

        expect(ctrl.files).toBeArrayOfSize(1);
        expect(ctrl.files[0]).toBe(parent);
        expect(parent.childCount).toBe(1);
      });

      it('should enter selectMultiple activated by button mode', function () {
        // given
        var ctrl = initCtrl();

        // when
        ctrl.setSelectModeToMultiple();

        // then
        expect(ctrl.options.selectMode).toBe('multiple');
        expect(ctrl.options.isMultipleActivatedByButton).toBe(true);
      });

      it('should exit selectMultiple activated by button mode', function () {
        // given
        var ctrl = initCtrl();
        ctrl.options.selectMode = 'multiple';
        ctrl.options.isMultipleActivatedByButton = true;
        ctrl.selectedFiles = [{id: 'some-file-id'}];

        // when
        ctrl.exitSelectMultipleMode();

        // then
        expect(ctrl.options.selectMode).toBe(undefined);
        expect(ctrl.selectedFiles).toEqual([]);
        expect(ctrl.options.isMultipleActivatedByButton).toBe(false);
      });

    });
  });
})
();
