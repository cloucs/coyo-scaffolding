(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $controller, coyoNotification, file, $window;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_) {
      $controller = _$controller_;
      coyoNotification = jasmine.createSpyObj('coyoNotification', ['success']);
      file = {id: 'file-id', senderId: 'sender-id', displayName: 'display-name'};
      $window = {location: {host: 'testhost', protocol: 'http:'}, encodeURIComponent: function (str) {
        return str;
      }};
    }));

    var controllerName = 'CopyFileLinkController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {coyoNotification: coyoNotification, $window: $window}, {file: file});
      }

      it('should init link', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.link).toBe('http://testhost/files/sender-id/file-id/display-name');
      });

      it('should show success message', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.success();

        // then
        expect(coyoNotification.success).toHaveBeenCalled();
      });

    });
  });

})();
