(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $controller, widgetLayoutService, $q, $scope, $rootScope;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
      $controller = _$controller_;
      $q = _$q_;
      $rootScope = _$rootScope_;
      $scope = _$rootScope_.$new();
      widgetLayoutService = jasmine.createSpyObj('widgetLayoutService', ['save']);
    }));

    var controllerName = 'ViewEditOptionsBarController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          widgetLayoutService: widgetLayoutService,
          $scope: $scope,
          $rootScope: $rootScope
        });
      }

      it('should save widget layout', function () {
        // given
        var ctrl = buildController();
        widgetLayoutService.save.and.returnValue($q.resolve({}));

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect(widgetLayoutService.save).toHaveBeenCalled();
        expect(ctrl.loading).toBeFalsy();
      });

      it('should not save widget layout multiple times at once', function () {
        // given
        var ctrl = buildController();
        widgetLayoutService.save.and.returnValue($q.resolve({}));

        // when
        ctrl.save();
        ctrl.save();

        // then
        expect(widgetLayoutService.save).toHaveBeenCalledTimes(1);
        expect(ctrl.loading).toBeTruthy();
      });

    });
  });

})();
