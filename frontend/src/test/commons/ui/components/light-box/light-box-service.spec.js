(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var controllerName = 'LightBoxModalController';

  describe('module: ' + moduleName, function () {

    var $rootScope, $scope, $controller, $q, $uibModalInstance, $interval, $window, DocumentModel, coyoEndpoints, backendUrlService,
        contentTypeService;
    var documentModel;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$rootScope_, _$q_, _$controller_, _$interval_, _$window_, _backendUrlService_,
                       _contentTypeService_) {
        $rootScope = _$rootScope_;
        $q = _$q_;
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $interval = _$interval_;
        $window = _$window_;
        coyoEndpoints = {sender: {preview: '/test/sender/preview}'}};
        backendUrlService = _backendUrlService_;

        contentTypeService = _contentTypeService_;

        DocumentModel = function () {
          documentModel = jasmine.createSpyObj('DocumentModel', ['$url', 'getDownloadUrl']);
          documentModel.$url.and.returnValue('THE-URL');
          documentModel.getDownloadUrl.and.returnValue('DOWNLOAD-URL');
          return documentModel;
        };
        angular.extend(DocumentModel, jasmine.createSpyObj('DocumentModel', ['$url']));
        DocumentModel.$url.and.returnValue('THE-URL');
      });

      $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['open', 'close']);
    });

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope,
          $q: $q,
          $uibModalInstance: $uibModalInstance,
          $interval: $interval,
          $window: $window,
          DocumentModel: DocumentModel,
          coyoEndpoints: coyoEndpoints,
          backendUrlService: backendUrlService,
          contentTypeService: contentTypeService,
          model: {
            _media: [
              {id: 'id1', senderId: 'senderId1', contentType: 'image/jpg', sortOrderId: 0},
              {id: 'id2', senderId: 'senderId2', contentType: 'video/mp4', sortOrderId: 1},
              {id: 'id1', senderId: 'senderId1', contentType: 'image/jpg', sortOrderId: 2},
              {id: 'id3', senderId: 'senderId3', contentType: 'image/jpg', sortOrderId: 3}
            ]
          },
          initialMediaId: 'id1',
          initialSortOrderId: 0
        });
      }

      it('should browse through media with same file ids', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();

        // when
        ctrl.loadNext();
        $rootScope.$apply();

        // then
        expect(ctrl.currentMedia).toBeDefined();
        expect(ctrl.currentMedia.id).toBe('id2');
        expect(ctrl.currentMedia.senderId).toBe('senderId2');
        expect(ctrl.currentMedia.contentType).toBe('video/mp4');
        expect(ctrl.currentMedia.sortOrderId).toBe(1);

        // when
        ctrl.loadNext();
        $rootScope.$apply();

        // then
        expect(ctrl.currentMedia).toBeDefined();
        expect(ctrl.currentMedia.id).toBe('id1');
        expect(ctrl.currentMedia.senderId).toBe('senderId1');
        expect(ctrl.currentMedia.contentType).toBe('image/jpg');
        expect(ctrl.currentMedia.sortOrderId).toBe(2);

        // when
        ctrl.loadNext();
        $rootScope.$apply();

        // then
        expect(ctrl.currentMedia).toBeDefined();
        expect(ctrl.currentMedia.id).toBe('id3');
        expect(ctrl.currentMedia.senderId).toBe('senderId3');
        expect(ctrl.currentMedia.contentType).toBe('image/jpg');
        expect(ctrl.currentMedia.sortOrderId).toBe(3);
      });
    });
  });

})();
