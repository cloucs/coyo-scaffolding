(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var controllerName = 'FileDetailsController';

  describe('module: ' + moduleName, function () {
    var $controller, $scope, $q, DocumentModel;
    var sender, file;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        $q = _$q_;

        DocumentModel = jasmine.createSpyObj('DocumentModel', ['setDescription']);

        sender = {
          id: 'SENDER-ID',
          displayName: 'Max Mustermann'
        };

        file = {
          id: 'FILE-ID',
          name: 'file name',
          contentType: 'image'
        };
      });
    });

    describe('controller: ' + controllerName, function () {

      function initCtrl() {
        return $controller(controllerName, {
          DocumentModel: DocumentModel
        }, {
          sender: sender,
          file: file
        });
      }

      it('should init', function () {
        // given

        // when
        var ctrl = initCtrl();

        // then
        expect(ctrl).not.toBeUndefined();
        expect(ctrl.sender).toBe(sender);
        expect(ctrl.file).toBe(file);
        expect(ctrl.isEditable).toBeFalsy();
      });

      it('should save the description', function () {
        // given
        var ctrl = initCtrl();

        DocumentModel.setDescription.and.returnValue($q.resolve(angular.extend({
          description: 'file description'
        }, file)));

        // when
        ctrl.file.description = 'file description';
        ctrl.isEditable = true;
        ctrl.saveDescription();
        $scope.$apply();

        // then
        expect(DocumentModel.setDescription).toHaveBeenCalledWith('SENDER-ID', 'FILE-ID', 'file description');
        expect(ctrl.isEditable).toBeFalse();
        expect(ctrl.file.description).toBe('file description');
      });

      it('should reset the description', function () {
        // given
        var ctrl = initCtrl();

        // when
        ctrl.resetDescription();

        // then
        expect(ctrl.isEditable).toBeFalse();
      });
    });
  });
})();
