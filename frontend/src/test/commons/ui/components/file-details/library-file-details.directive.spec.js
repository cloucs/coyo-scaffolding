(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var controllerName = 'LibraryFileDetailsController';

  describe('module: ' + moduleName, function () {
    var $scope, $q, $timeout, $controller, $log, $uibModalStack, coyoEndpoints, modalService;
    var Upload, DocumentModel, backendUrlService, fileLibraryModalService, fileAuthorService, editInOfficeService;
    var currentUser, sender, file, updateFunc, coyoNotification, documentModel, documentVersion, versions;

    var version1 = {
      'id': '1',
      'author': {
        'id': '111',
        'displayName': 'Robert Lang'
      },
      'versionNumber': 0,
      'created': 1493277658976,
      'length': 827377
    };
    var version2 = {
      'id': '2',
      'author': {
        'id': '111',
        'displayName': 'Robert Lang'
      },
      'versionNumber': 1,
      'created': 1493277659546,
      'length': 827377
    };
    var version3 = {
      'id': '3',
      'author': {
        'id': '111',
        'displayName': 'Robert Lang'
      },
      'versionNumber': 3,
      'created': 1493277659546,
      'length': 827377
    };

    var authors = {
      'FILE_ID': {
        data: {
          displayName: 'HODOR'
        }
      }
    };
    var initialPreviewUrl = 'PREVIEW-URL';

    beforeEach(function () {
      module(moduleName);

      inject(function ($rootScope, _$controller_, _$q_, _$timeout_, _$log_, _coyoEndpoints_) {
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $q = _$q_;
        $timeout = _$timeout_;
        $log = _$log_;
        coyoEndpoints = _coyoEndpoints_;

        currentUser = {
          id: 'CURRENT-USER-ID',
          globalPermissions: [
            'ACCESS_FILES'
          ]
        };

        sender = {
          id: 'SENDER-ID',
          displayName: 'Max Mustermann',
          _permissions: {
            manage: true
          }
        };

        file = {
          id: 'FILE-ID',
          senderId: sender.id,
          sender: sender,
          name: 'file name',
          contentType: 'image'
        };

        versions = {
          content: [version3, version2, version1],
          _queryParams: {
            _page: 0,
            _pageSize: 5,
            _sort: 'created,desc'
          }
        };

        documentVersion = {
          id: 'VERSION-ID',
          versionNumber: 8
        };

        Upload = jasmine.createSpyObj(Upload, ['upload']);
        backendUrlService = jasmine.createSpyObj('backendUrlService', ['getUrl']);
        backendUrlService.getUrl.and.returnValue('BASE-URL');

        modalService = jasmine.createSpyObj('modalService', ['confirm']);
        modalService.confirm.and.returnValue({result: $q.resolve()});
        fileLibraryModalService = jasmine.createSpyObj('fileLibraryModalService', ['open']);
        $uibModalStack = jasmine.createSpyObj('$uibModalStack', ['getTop', 'dismiss']);
        $uibModalStack.getTop.and.returnValue({key: 1});
        coyoNotification = jasmine.createSpyObj('coyoNotification', ['error']);
        fileAuthorService = jasmine.createSpyObj('fileAuthorService', ['loadVersionAuthors']);
        fileAuthorService.loadVersionAuthors.and.returnValue($q.resolve(authors));

        documentModel = jasmine.createSpyObj('DocumentModel',
            ['$url', 'getDownloadUrl', 'getDownloadUrlForVersion', 'getVersionPreviewUrl', 'restoreVersion',
              'getVersions']);
        documentModel.$url.and.returnValue('THE-URL');
        documentModel.getDownloadUrl.and.returnValue('/DOWNLOAD-URL');
        documentModel.getDownloadUrlForVersion.and.returnValue('versionId');
        documentModel.getVersionPreviewUrl.and.returnValue('THE-VERSION-URL');
        documentModel.restoreVersion.and.returnValue($q.resolve(file));
        documentModel.getVersions.and.returnValue($q.resolve(versions));
        DocumentModel = function (args) {
          _.merge(documentModel, args);
          return documentModel;
        };
        angular.extend(DocumentModel, jasmine.createSpyObj('DocumentModel', ['$url']));
        DocumentModel.$url.and.returnValue('THE-URL');

        editInOfficeService = {
          canEditInOffice: function () {
          },
          editInOffice: function () {
          }
        };

        updateFunc = jasmine.createSpy('update');
      });
    });

    describe('controller: ' + controllerName, function () {

      function initCtrl() {
        return $controller(controllerName, {
          $q: $q,
          $log: $log,
          $timeout: $timeout,
          $scope: $scope,
          $uibModalStack: $uibModalStack,
          Upload: Upload,
          DocumentModel: DocumentModel,
          fileAuthorService: fileAuthorService,
          coyoNotification: coyoNotification,
          coyoEndpoints: coyoEndpoints,
          backendUrlService: backendUrlService,
          editInOfficeService: editInOfficeService,
          modalService: modalService,
          fileLibraryModalService: fileLibraryModalService
        }, {
          file: file,
          fileAuthor: undefined,
          showAuthors: true,
          appIdOrSlug: 'documents',
          linkToFileLibrary: true,
          previewUrl: initialPreviewUrl,
          updateFileFunc: updateFunc,
          currentUser: currentUser
        });
      }

      it('should init', function () {
        // when
        var ctrl = initCtrl();
        ctrl.$onInit();

        // then
        expect(ctrl).not.toBeUndefined();
        expect(ctrl.file).toBe(file);
        expect(ctrl.linkToFileLibrary).toBe(true);
        expect(ctrl.showAuthors).toBe(true);
        expect(ctrl.appIdOrSlug).toBe('documents');
        expect(ctrl.previewUrl).toBe(initialPreviewUrl);
        expect(ctrl.updateFileFunc).toBeDefined();
        expect(ctrl.downloadUrl).toBe('BASE-URL/DOWNLOAD-URL');
        expect(ctrl.canEditInOffice).toBeDefined();
        expect(ctrl.editInOffice).toBeDefined();

        expect(backendUrlService.getUrl).toHaveBeenCalled();
        expect(documentModel.getDownloadUrl).toHaveBeenCalled();
      });

      it('should open file library', function () {
        // given
        var ctrl = initCtrl();
        ctrl.$onInit();
        file.parent = {id: 'parent-id'};

        // when
        ctrl.openFileLibrary();

        // then
        expect($uibModalStack.getTop).toHaveBeenCalled();
        expect($uibModalStack.dismiss).toHaveBeenCalled();
        expect(fileLibraryModalService.open).toHaveBeenCalledWith(sender, {
          highlightedFileId: file.id,
          initialFolder: file.parent
        });
      });

      it('should unlock a file', function () {
        // given
        var ctrl = initCtrl();
        ctrl.$onInit();
        file.unlock = jasmine.createSpy('unlock');

        // when
        ctrl.unlockFile();
        $scope.$apply();

        // then
        expect(modalService.confirm).toHaveBeenCalled();
        expect(file.unlock).toHaveBeenCalled();
      });

      it('should check for version of files', function () {
        // given
        var file = {name: 'file.type', type: 'some/type', id: 'FILEMODEL-ID'};
        var fileModel = {id: 'FILEMODEL-ID', contentType: 'some/type', extension: 'type'};
        var ctrl = initCtrl();
        ctrl.$onInit();
        var resultFile = {size: 42};
        ctrl.sender = {id: 'SENDER-ID'};
        Upload.upload.and.returnValue($q.resolve({data: resultFile}));

        // when
        ctrl.uploadVersion(fileModel, file);
        $scope.$apply();
        $timeout.flush();

        // then
        expect(Upload.upload).toHaveBeenCalledTimes(1);
        expect(fileModel.size).toBe(42);
        expect(updateFunc).toHaveBeenCalled();
      });

      it('should download version of files', function () {
        // given
        var versionId = 2;
        var ctrl = initCtrl();
        ctrl.$onInit();

        // when
        ctrl.getDownloadUrlForVersion(versionId);
        $scope.$apply();
        $timeout.flush();

        // then
        expect(documentModel.getDownloadUrlForVersion).toHaveBeenCalledWith(versionId);
      });

      it('should initially not change predefined previewUrl', function () {
        // given
        var ctrl = initCtrl();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.previewUrl).toBe(initialPreviewUrl);
      });

      it('should load preview image for a document version', function () {
        // given
        var ctrl = initCtrl();

        // when
        ctrl.$onInit();
        ctrl.selectVersion(documentVersion);

        // then
        expect(documentModel.getVersionPreviewUrl).toHaveBeenCalledWith(documentVersion);
        expect(ctrl.currentVersion.id).toBe(documentVersion.id);
        expect(ctrl.previewUrl).toBe('THE-VERSION-URL');
      });

      it('should restore an old version', function () {
        // given
        var ctrl = initCtrl();
        ctrl.$onInit();

        // when
        ctrl.restoreVersion(version1);
        $scope.$apply();

        // then
        expect(documentModel.restoreVersion).toHaveBeenCalledWith(version1.id);
        expect(updateFunc).toHaveBeenCalled();
      });

      it('should load versions and version authors when switching to history tab for first time, '
          + 'should not update previewUrl', function () {
        // given
        var ctrl = initCtrl();
        ctrl.$onInit();
        ctrl.versionAuthors = {};

        // when
        ctrl.switchTab(ctrl.tabs.HISTORY);
        $scope.$apply();

        // then
        expect(documentModel.getVersions).toHaveBeenCalledWith({_page: 0, _pageSize: 5, _sort: 'created,desc'});
        expect(fileAuthorService.loadVersionAuthors)
            .toHaveBeenCalledWith(sender.id, file.id, ctrl.appIdOrSlug, ctrl.showAuthors, sender._permissions.manage);
        expect(documentModel.getVersionPreviewUrl).not.toHaveBeenCalled();
        expect(ctrl.versionAuthors).toBe(authors);
        expect(ctrl.currentVersion).toBe(version3);
        expect(ctrl.currentVersionNumber).toBe(3);
        expect(ctrl.previewUrl).toBe(initialPreviewUrl);
      });

      it('should load first version page and show preview of latest version when switching tab from old version page',
          function () {
            // given
            var ctrl = initCtrl();
            ctrl.$onInit();
            ctrl.currentVersion = version1;
            ctrl.currentVersionNumber = 1;
            ctrl.versions = versions;
            ctrl.versions.first = false;

            // when
            ctrl.switchTab(ctrl.tabs.HISTORY);
            $scope.$apply();

            // then
            expect(fileAuthorService.loadVersionAuthors)
                .toHaveBeenCalledWith(sender.id, file.id, ctrl.appIdOrSlug, ctrl.showAuthors,
                    sender._permissions.manage);
            expect(documentModel.getVersionPreviewUrl).toHaveBeenCalledWith(version3);
            expect(ctrl.currentVersion).toBe(version3);
            expect(ctrl.currentVersionNumber).toBe(3);
            expect(ctrl.previewUrl).toBe('THE-VERSION-URL');
          });

      it('should not load versions again but only show latest version when switching tabs from first versions page',
          function () {
            // given
            var ctrl = initCtrl();
            ctrl.$onInit();
            ctrl.currentVersion = version1;
            ctrl.currentVersionNumber = 1;
            ctrl.versions = versions;
            ctrl.versions.first = true;

            // when
            ctrl.switchTab(ctrl.tabs.INFORMATION);
            $scope.$apply();

            // then
            expect(documentModel.getVersions).not.toHaveBeenCalled();
            expect(fileAuthorService.loadVersionAuthors).not.toHaveBeenCalled();
            expect(documentModel.getVersionPreviewUrl).toHaveBeenCalledWith(version3);
            expect(ctrl.currentVersion).toBe(version3);
            expect(ctrl.currentVersionNumber).toBe(3);
            expect(ctrl.previewUrl).toBe('THE-VERSION-URL');
          });
    });
  });
})();
