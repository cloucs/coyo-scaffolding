(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var serviceName = 'titleService';

  describe('module: ' + moduleName, function () {

    var titleService;
    var $rootScope, $scope, $document, $sanitize;

    beforeEach(function () {
      $document = jasmine.createSpyObj('$document', ['find']);
      $document.find.and.returnValue({
        html: function () {}
      });
      $sanitize = jasmine.createSpy('$sanitize');

      module(moduleName, function ($provide) {
        $provide.value('$document', $document);
        $provide.value('$sanitize', $sanitize);
      });

      inject(function (_titleService_, _$rootScope_) {
        titleService = _titleService_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
      });
    });

    describe('service: ' + serviceName, function () {
      it('should restore the title', function () {
        // given
        titleService.set('TEST', false);
        $scope.$apply();

        // when
        titleService.restore(); // We cannot set the title variable from outside...

        // then
        expect(titleService.get()).toBe('TEST');
        expect($document.find).toHaveBeenCalled();
        expect($sanitize).toHaveBeenCalled();
      });

      it('should set the title', function () {
        // given

        // when
        titleService.set('title <script>console.warn(\'>>> script executed!\');</script>', false);
        $scope.$apply();

        // then
        expect($document.find).toHaveBeenCalled();
        expect($sanitize).toHaveBeenCalled();
      });
    });

  });

})();
