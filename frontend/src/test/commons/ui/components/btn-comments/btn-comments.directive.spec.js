(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {
    var $scope, $q, $controller, $state;
    var authService, CommentModel;

    var target = {
      id: 'myTarget',
      typeName: 'myTargetType'
    };
    var currentUser = {
      id: 123
    };
    var url = '/some-url';

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$q_, _$controller_, _authService_, _CommentModel_) {
      $scope = _$rootScope_.$new();
      $q = _$q_;
      $controller = _$controller_;
      authService = _authService_;
      CommentModel = _CommentModel_;

      $state = jasmine.createSpyObj('$state', ['href']);
      $state.href.and.returnValue(url);
    }));

    describe('directive: coyoBtnComments', function () {

      describe('controller: BtnCommentsController', function () {

        function buildController() {
          var ctrlFn = $controller('BtnCommentsController', {
            authService: authService,
            CommentModel: CommentModel,
            $state: $state
          }, true);
          ctrlFn.instance.target = target;
          ctrlFn.instance.onClick = angular.noop;
          return ctrlFn();
        }

        it('should init default values', function () {
          // given
          spyOn(authService, 'getUser').and.returnValue($q.resolve(currentUser));
          spyOn(CommentModel, 'getInfo').and.returnValue($q.resolve(2));

          // when
          var ctrl = buildController();
          $scope.$apply();

          // then
          expect(ctrl.isLoading).toBeFalse();
          expect(ctrl.count).toBe(2);
          expect(ctrl.link).toEqual(url);
          expect(authService.getUser).toHaveBeenCalled();
          expect(CommentModel.getInfo).toHaveBeenCalled();
        });
      });
    });
  });
})();
