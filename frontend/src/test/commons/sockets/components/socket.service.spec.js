(function () {
  'use strict';

  var moduleName = 'commons.sockets';
  var targetName = 'socketService';

  describe('module: ' + moduleName, function () {
    var $rootScope, $timeout, SockJS, Stomp, authService, backendUrlService, csrfService, client, $localStorage;
    var socketService;
    var socketConfig = {
      autoConnect: false
    };
    var socketReconnectDelays = {
      GENERAL_RECONNECT_DELAY: 10001
    };

    var currentUser;
    var backendUrl = 'http://example.com';
    var csrfToken = 'token';

    beforeEach(function () {
      module(moduleName, function ($provide) {
        // uncomment for debug
        // $provide.value('$log', console);

        currentUser = jasmine.createSpyObj('currentUser', ['hasGlobalPermissions']);
        currentUser.hasGlobalPermissions.and.returnValue(true);

        authService = jasmine.createSpyObj('authService',
            ['isAuthenticated', 'getUser', 'subscribeToUserUpdate', 'validateUserId', 'getCurrentUserId']);
        authService.isAuthenticated.and.returnValue(true);
        authService.getUser.and.returnValue({
          then: function (callback) {
            callback(currentUser);
          }
        });
        authService.getCurrentUserId.and.returnValue('user-id');
        $provide.value('authService', authService);

        backendUrlService = jasmine.createSpyObj('backendUrlService', ['getUrl']);
        backendUrlService.getUrl.and.callFake(_.constant(backendUrl));
        $provide.value('backendUrlService', backendUrlService);

        csrfService = jasmine.createSpyObj('csrfService', ['getToken']);
        csrfService.getToken.and.returnValue({
          then: function (callback) {
            callback(csrfToken);
            return {
              'catch': function (callback) {
                if (!csrfToken) {
                  callback();
                }
              }
            };
          }
        });
        $provide.value('csrfService', csrfService);

        client = jasmine.createSpyObj('client', ['connect', 'disconnect', 'send', 'subscribe', '_transmit']);

        SockJS = jasmine.createSpy('SockJS').and.callFake(_.identity);
        $provide.constant('SockJS', SockJS);

        Stomp = jasmine.createSpyObj('Stomp', ['over']);
        Stomp.over.and.callFake(function (ws) {
          client.ws = ws;
          client.heartbeat = {};
          return client;
        });
        $provide.constant('Stomp', Stomp);

        $provide.constant('socketConfig', socketConfig);
        $provide.constant('socketReconnectDelays', socketReconnectDelays);

        $localStorage = {clientId: 'client-id', userId: 'user-id'};
        $provide.value('$localStorage', $localStorage);
      });

      inject(function (_$rootScope_, _$timeout_, _socketConfig_, _socketService_) {
        $rootScope = _$rootScope_;
        $timeout = _$timeout_;
        socketConfig = _socketConfig_;
        socketService = _socketService_;
      });
    });

    it('should be registered', function () {
      expect(socketService).toBeDefined();
    });

    describe('service: ' + targetName, function () {

      it('should connect', function () {
        // given
        spyOn($rootScope, '$emit');
        var response = {headers: 'success'};
        client.connect.and.callFake(function (args, onConnect) {
          client.connected = true;
          onConnect(response);
        });

        // when
        var result;
        var promise = socketService.connect().then(function (data) {
          result = data;
        });

        // then
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:connecting', 0);
        expect(SockJS).toHaveBeenCalledWith('http://example.com/web/ws?_csrf=token');
        expect(Stomp.over).toHaveBeenCalled();
        expect(client.debug).toBeFalse();
        expect(client.connect).toHaveBeenCalledWith(
            {
              login: '',
              passcode: '',
              'X-CSRF-TOKEN': 'token',
              'X-Coyo-Client-ID': 'client-id',
              'X-Coyo-Current-User': 'user-id'
            },
            jasmine.any(Function),
            jasmine.any(Function));
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:connected');
        expect(promise).toBeDefined();

        $rootScope.$apply();

        expect(result).toEqual(response.headers);
      });

      it('should not connect when connecting', function () {
        // given
        client.connect.and.stub();

        // when
        socketService.connect();
        var promise = socketService.connect();

        // then
        expect(client.connect.calls.count()).toBe(1);
        expect(promise).toBeDefined();
      });

      it('should not connect when connected', function () {
        // given
        var response = {headers: 'success'};
        client.connect.and.callFake(function (args, onConnect) {
          client.connected = true;
          onConnect(response);
        });

        // when
        socketService.connect();

        var result;
        var promise = socketService.connect().then(function (data) {
          result = data;
        });

        // then
        expect(client.connect.calls.count()).toBe(1);
        expect(promise).toBeDefined();

        $rootScope.$apply();

        expect(result).toEqual(response.headers);
      });

      it('should not connect when not authenticated', function () {
        // given
        spyOn($rootScope, '$emit');
        authService.isAuthenticated.and.returnValue(false);

        // when
        var result;
        var promise = socketService.connect().catch(function (data) {
          result = data;
        });

        // then
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:offline', null);
        expect(client.connect).not.toHaveBeenCalled();
        expect(promise).toBeDefined();

        $rootScope.$apply();

        expect(result).toBeNull();
      });

      it('should not connect when CSRF retrieval fails', function () {
        // given
        spyOn($rootScope, '$emit');
        var response = 'ERROR';
        csrfService.getToken.and.returnValue({
          then: function () {
            return {
              'catch': function (callback) {
                callback(response);
              }
            };
          }
        });

        // when
        var result;
        var promise = socketService.connect().catch(function (data) {
          result = data;
        });

        // then
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:offline', response);
        expect(client.connect).not.toHaveBeenCalled();
        expect(promise).toBeDefined();

        $rootScope.$apply();

        expect(result).toEqual(response);
      });

      it('should reconnect when client connection fails', function () {
        // given
        spyOn($rootScope, '$emit');
        var response = 'ERROR';
        client.connect.and.callFake(function (args, onConnect, onError) {
          client.connected = false;
          onError(response);
        });

        // when
        var promise = socketService.connect();

        expect(client.connect).toHaveBeenCalled();
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:connecting', 0);
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:disconnected');
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:sleep', 0);

        client.connect.calls.reset();
        $rootScope.$emit.calls.reset();
        $timeout.flush();

        // init retry
        expect(client.connect).toHaveBeenCalled();
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:connecting', 1);
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:disconnected');
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:sleep', socketReconnectDelays.GENERAL_RECONNECT_DELAY);

        client.connect.calls.reset();
        $rootScope.$emit.calls.reset();
        $timeout.flush();

        // 1st retry
        expect(client.connect).toHaveBeenCalled();
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:connecting', 2);
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:disconnected');
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:sleep', socketReconnectDelays.GENERAL_RECONNECT_DELAY);

        client.connect.calls.reset();
        $rootScope.$emit.calls.reset();
        $timeout.flush();

        // 2nd retry
        expect(client.connect).toHaveBeenCalled();
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:connecting', 3);
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:disconnected');
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:sleep', socketReconnectDelays.GENERAL_RECONNECT_DELAY);

        client.connect.calls.reset();
        $rootScope.$emit.calls.reset();
        client.connect.and.callFake(function (args, onConnect) {
          client.connected = true;
          onConnect({status: 'success', headers: {}});
        });
        $timeout.flush();
        $timeout.flush();

        // successful reconnect
        expect(client.connect).toHaveBeenCalled();
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:connecting', 4);
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:reconnected');

        expect(promise).toBeDefined();

        $rootScope.$apply();
      });

      it('should disconnect on too many requests', function () {
        // given
        spyOn($rootScope, '$emit');
        client.connect.and.callFake(function (args, onConnect) {
          client.connected = true;
          onConnect({body: 'TOO_MANY_REQUESTS'});
        });

        // when
        socketService.connect();

        // then
        expect(client.disconnect).toHaveBeenCalled();
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:connecting', 2);
        $timeout.flush();
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:disconnected');
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:sleep', socketReconnectDelays.GENERAL_RECONNECT_DELAY);
        $timeout.flush(socketReconnectDelays.GENERAL_RECONNECT_DELAY);
        expect($rootScope.$emit).toHaveBeenCalledWith('socketService:connecting', 3);
      });

      it('should subscribe and unsubscribe', function () {
        // given
        var response = {headers: 'success'};
        client.connect.and.callFake(function (args, onConnect) {
          client.connected = true;
          onConnect(response);
        });
        client.subscribe.and.returnValue({
          id: 123
        });

        // when
        var unsubscribeFn = socketService.subscribe('destination', angular.noop);

        // then
        expect(client.subscribe).not.toHaveBeenCalledWith('destination', jasmine.any(Function));

        socketService.connect();
        $timeout.flush();

        expect(client.subscribe).toHaveBeenCalledWith('destination', jasmine.any(Function), {});

        // when - again
        unsubscribeFn();

        // then - again
        expect(client._transmit).toHaveBeenCalledWith('UNSUBSCRIBE', {id: 123});
      });

      it('should subscribe and add selector', function () {
        // given
        var response = {headers: 'success'};
        client.connect.and.callFake(function (args, onConnect) {
          client.connected = true;
          onConnect(response);
        });
        var subscription = jasmine.createSpyObj('subscription', ['unsubscribe']);
        client.subscribe.and.returnValue(subscription);

        // when
        socketService.subscribe('destination', angular.noop, null, '1234');

        // then
        expect(client.subscribe).not.toHaveBeenCalledWith('destination', jasmine.any(Function), {});

        socketService.connect();
        $timeout.flush();

        expect(client.subscribe).toHaveBeenCalledWith('destination', jasmine.any(Function), {selector: 'headers.toSubscribe==\'1234\''});
      });

      it('should subscribe only once to each destination and still call each registered callback that passes the filter', function () {
        // given
        var response = {headers: 'success'};
        client.connect.and.callFake(function (args, onConnect) {
          client.connected = true;
          onConnect(response);
        });
        var subscription = jasmine.createSpyObj('subscription', ['unsubscribe']);
        client.subscribe.and.returnValue(subscription);
        socketService.connect();
        $timeout.flush();
        client.subscribe.calls.reset();

        // when
        var callback1 = false, callback2 = false, callback3 = false, callback4 = false, callback5 = false;
        socketService.subscribe('destination', function () {
          callback1 = true;
        });
        socketService.subscribe('destination', function () {
          callback2 = true;
        }, {event: 'pass'});
        socketService.subscribe('destination', function () {
          callback3 = true;
        }, {event: 'no-pass'});
        socketService.subscribe('destination', function () {
          callback4 = true;
        }, 'pass');
        socketService.subscribe('destination', function () {
          callback5 = true;
        }, function () { return true; });
        socketService.connect();

        // then
        expect(client.subscribe).toHaveBeenCalledTimes(1);
        client.subscribe.calls.mostRecent().args[1]({body: {event: 'pass'}});
        expect(callback1).toBe(true);
        expect(callback2).toBe(true);
        expect(callback3).toBe(false);
        expect(callback4).toBe(true);
        expect(callback5).toBe(true);
      });

      it('should receiveFrom', function () {
        // given
        var response = {headers: 'success'};
        client.connect.and.callFake(function (args, onConnect) {
          client.connected = true;
          onConnect(response);
        });

        // when
        socketService.receiveFrom('destination');
        $timeout.flush();
        $rootScope.$apply();

        // then
        expect(client.subscribe).toHaveBeenCalledWith('destination', jasmine.any(Function));
      });

      it('should disconnect', function () {
        // given
        var response = {headers: 'success'};
        client.connect.and.callFake(function (args, onConnect) {
          client.connected = true;
          onConnect(response);
        });
        client.disconnect.and.callFake(function () {
          client.connected = false;
        });

        // when
        socketService.connect();

        // then
        expect(socketService.isConnected()).toBeTrue();

        // when
        socketService.disconnect();

        // then
        expect(socketService.isConnected()).toBeFalse();
      });

      it('should subscribe and unsubscribe with token', function () {
        // given
        var response = {headers: 'success'};
        client.connect.and.callFake(function (args, onConnect) {
          client.connected = true;
          onConnect(response);
        });
        client.subscribe.and.returnValue({
          id: 123
        });

        // when
        var unsubscribeFn = socketService.subscribe('destination', angular.noop, 'deleted', '123', 'token');

        // then
        expect(client.subscribe).not.toHaveBeenCalledWith('destination', jasmine.any(Function));

        socketService.connect();
        $timeout.flush();

        expect(client.subscribe).toHaveBeenCalledWith('destination', jasmine.any(Function), {selector: 'headers.toSubscribe==\'123\'', subscriptionToken: 'token'});

        // when - again
        unsubscribeFn();

        // then - again
        expect(client._transmit).toHaveBeenCalledWith('UNSUBSCRIBE', {id: 123, selector: 'headers.toSubscribe==\'123\'', simpDestination: 'destination'});
      });
    });

    it('should reconnect with subscription token', function () {
      // given
      var response = {headers: 'success', body: 'reconnectToken'};
      client.connect.and.callFake(function (args, onConnect) {
        client.connected = true;
        onConnect(response);
      });
      client.subscribe.and.returnValue({
        id: 123
      });
      socketService.subscribe('destination', angular.noop, 'deleted', '123', 'token');
      socketService.connect();
      $timeout.flush();

      expect(client.subscribe).toHaveBeenCalledWith('destination', jasmine.any(Function), {selector: 'headers.toSubscribe==\'123\'', subscriptionToken: 'token'});
      socketService.disconnect();
      $timeout.flush();
      // when
      socketService.reconnect();
      $timeout.flush();

      // then - again
      expect(client.subscribe).toHaveBeenCalledWith('destination', jasmine.any(Function), {selector: 'headers.toSubscribe==\'123\'', subscriptionToken: 'reconnectToken'});
    });
  });

})();
