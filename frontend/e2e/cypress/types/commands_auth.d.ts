// ***********************************************************
// Custom commands related to authentication
// ***********************************************************

declare namespace Cypress {

    interface Chainable<Subject = any> {

        /**
         * Retrieves a new CSRF token and returns it. The token is also provided as
         * a context variable named `csrf`.
         *
         * @example
         *    cy.csrf();
         */
        csrf(): Chainable<string>;

        /**
         * Perform request using сыка token of the current user
         * @param method can be any http method (GET, POST, PUT, DELETE)
         * @param path is a url path
         * @param body optional and contains a body of the request
         */
        requestAuth(method: HttpMethod, path: string, body?: RequestBody): Chainable<Response>

        /**
         * Get bearer token via API
         * @param {string} username can be any available username
         * @param {string} password is the corresponding password
         */
        apiBearerToken(username: string, password: string): Cypress.Chainable<Response>;

        /**
         * Command for logging in via API
         * @param {string} username can be any available username
         * @param {string} password is the corresponding password
         * If left empty the given user in the cypress.json will taken, usually Robert Lang
         *
         */
        apiLogin(username?: string, password?: string): Cypress.Chainable<Response>;

        /**
         * Logout via API
         *
         */
        apiLogout(): Cypress.Chainable<Response>;
    }
}
