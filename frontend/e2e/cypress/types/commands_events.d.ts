// ***********************************************************
// Custom commands related to events
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Command for creating a event via UI
     * @param eventName string, the event will be created with this name
     * @param host string, if set up the event will be created with a host different from the current user
     * @param isPrivate boolean, if set up and true, the event will be created as private with additional step to invite attendees. If not/false, public event will be created
     * @param invitee string, if set up for private event, the event will be created with inviting this user to an event. Will be ignored if set up for public event
     */
    createEvent(eventName: string, isPrivate?: boolean, host?: string, invitee?: string): Cypress.Chainable<void>;
  }
}