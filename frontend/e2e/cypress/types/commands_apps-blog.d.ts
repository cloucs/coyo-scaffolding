// ***********************************************************
// Custom commands related to coyo workspaces
// ***********************************************************

declare namespace Cypress {

    interface Chainable<Subject = any> {

        /**
         * Command for creating a blog app
         * @param {string} name defines the name of the blog app
         * @param {boolean} active defines if the app is active or not
         * @param {string} authors defines who can write blog articles and can be 'admins', 'everyone', or 'custom'
         * @param {string} publishers defines who can edit and publish articles and can be 'admins', 'everyone', or 'custom'
         * @param {boolean} comments defines if user can comment blog articles
         * @param {boolean} share defines if the articles get automatically shared
         *
         */

        createBlogApp(name: string, active: boolean, authors: string, publishers: string, comments: boolean, share: boolean): Chainable<any>;

        /**
         * Command for creating a blog article
         * @param {string} title defines the blog title
         * @param {string} text defines the blog text
         * @param {string} teaserText define the blog teaser text
         * @param {boolean} displayTeaser asd Display the teaser before the blog article.
         * @param {string} status can be 'draft','published', or 'published-at'
         * @param {string} author 'user' 'pageworkspace'
         *
         */

        createBlogArticle(title: string, text: string, teaserText: string, displayTeaser: boolean, status: string, author: string): Chainable<any>;

        /**
         * Command for creating a blog app via api
         * @param {string} sender defines the sender id for the page or workspace the app should be created for
         * @param {string} name defines the name of the blog app
         * @param {boolean} active defines if the app is active or not
         *
         */

        apiCreateBlogApp(sender: string, name: string, active: boolean): Chainable<any>;
    }
}