describe('Tour check', function () {

  beforeEach(function () {
    cy.clearCookies();
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.clearLocalStorage().apiLogin('rl', 'demo')
        .apiLanguage('EN').apiEnableTour();
    cy.visit('/home/timeline');
  });

  afterEach(function () {
    cy.apiLogout();
  });

  it('should check and click through the tour for the timeline', function () {
    cy.tour('timeline');
  });

  it('should check and click through the tour for pages', function () {
    cy.tour('pages');
  });

  it('should check and click through the tour for workspaces', function () {
    cy.tour('workspaces');
  });

  it('should check and click through the tour for events', function () {
    cy.tour('events'); //FAILS here now as starts at step 2/3
  });

  it('should check and click through the tour for the search', function () {
    cy.tour('search');
  });

  it('should check and click through the tour for colleagues', function () {
    cy.tour('colleagues');
  });

  it('should check and click through the tour for the profile', function () {
    cy.tour('profile');
  });
});
