/// <reference types="Cypress" />

describe('Restricted timeline post', function () {

    beforeEach(function () {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        cy.clearLocalStorage().apiLogin('rl', 'demo')
            .apiLanguage('EN')
            .apiDismissTour();
        cy.visit('/home/timeline');
    });

    afterEach(function () {
        cy.apiLogout();
    });

    it('should create a restricted timeline post', () => {
        const timelinePostMessage = 'Qa timeline-post for create restricted post test ' + Date.now();

        cy.server();
        cy.route('GET', '/web/timeline-items/new?**').as('getNewTimelineItems');
        cy.route('GET', '/web/users/*/subscriptions**').as('getSubscriptions');
        cy.route('POST', '/web/timeline-items**').as('postTimelineItems');

        cy.get('[data-test=textarea-timeline-post]')
            .type(timelinePostMessage);
        cy.get('[data-test=timeline-lock-btn]')
            .click();
        cy.get('[data-test=button-timeline-submit]')
            .click();
        cy.wait('@postTimelineItems');
        cy.wait('@getNewTimelineItems');
        cy.wait('@getSubscriptions');
        cy.get('[data-test=timeline-item]').eq(0).as('post')
            .should('contain', timelinePostMessage).then(() => {
            cy.get('@post').within(() => {
                cy.get('[data-test=timeline-social-actions]').should('not.exist'); //TODO false positive, it just is not yet loaded
                cy.get('[data-test=like-button]').should('not.exist'); //TODO false positive, it just is not yet loaded
                cy.get('[data-test=share-button]').should('not.exist'); //TODO false positive, it just is not yet loaded
                cy.get('[data-test=functional-user-chooser]').should('not.exist'); //TODO false positive, it just is not yet loaded
                cy.get('[data-test=timeline-item-help]').should('be.visible');
            });
        });
    });

    it('should be possible to edit an own restricted timeline post', () => {
        const newPost = 'qa timeline-post for edit test ' + Date.now();
        const editPost = ' edited';
        cy.apiCreateTimelinePost(newPost, true).apiShowNewTimelinePosts();

        cy.server();
        cy.route('PUT', '/web/timeline-items/**').as('putEditTimelineItem');

        cy.get('[data-test=timeline-stream]').contains('[data-test=timeline-item]', newPost).as('post').then(($post) => {
            cy.wrap($post).find('[data-test=button-timeline-entry-edit-dropdown]')
                .click()
                .get('[data-test=button-timeline-post-edit]')
                .click()
                .get('[data-test=input-timeline-edit]')
                .type(editPost);
            cy.get('[data-test=button-timeline-edit-save]')
                .click();
            cy.wait('@putEditTimelineItem').its('status').should('eq', 200);
            cy.get('@post')
                .should('contain', newPost + editPost);
        })
    });

    it('should not be possible to comment on a restricted post', () => {
        const timelinePostMessage = 'Qa timeline-post for comment to restricted post test ' + Date.now();
        cy.apiCreateTimelinePost(timelinePostMessage, true).apiShowNewTimelinePosts();

        cy.get('[data-test=timeline-stream]').contains('[data-test=timeline-item]', timelinePostMessage).as('post').within(() => {
            cy.get('[data-test=timeline-comment-message]')
                .should('not.exist');
        })
    });

    it('should not be possible to like a restricted timeline post', () => {
        const timelinePostMessage = 'qa timeline-post for like to restricted post test ' + Date.now();
        cy.apiCreateTimelinePost(timelinePostMessage, true).apiShowNewTimelinePosts();
        cy.server();
        cy.route('GET', '/web/like-targets/timeline-item**').as('getLikeTimelineItem');

        cy.get('[data-test=timeline-stream]').contains('[data-test=timeline-item]', timelinePostMessage).as('post').within(() => {
            cy.wait('@getLikeTimelineItem').its('status').should('eq', 200);
            cy.get('[data-test=like-button]')
                .should('not.exist');
        });
    });

    it('should be possible to delete a restricted timeline post', () => {
        const timelinePostMessage = 'qa timeline-post for delete restricted post test ' + Date.now();
        cy.apiCreateTimelinePost(timelinePostMessage, true).apiShowNewTimelinePosts();

        cy.server();
        cy.route('DELETE', '/web/timeline-items/**').as('deleteTimelineItem');

        cy.get('[data-test=timeline-stream]').contains('[data-test=timeline-item]', timelinePostMessage).within(() => {
            cy.get('[data-test=button-timeline-entry-edit-dropdown]')
                .click()
        });
        cy.get('button[data-test=delete-post-button]')
            .click();
        cy.wait('@deleteTimelineItem');
        cy.get('[data-test=timeline-item]').should('not.contain', timelinePostMessage);
    });
});