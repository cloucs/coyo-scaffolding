describe('Timeline post', function () {

    beforeEach(function () {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        cy.clearLocalStorage().apiLogin('rl', 'demo')
            .apiLanguage('EN')
            .apiDismissTour();
        cy.visit('/home/timeline');
    });

    afterEach(function () {
        cy.apiLogout();
    });

    it('should create a timeline entry', () => {
        const timelinePostMessage = 'Qa timeline-post for create test ' + Date.now();

        cy.server();
        cy.route('GET', '/web/timeline-items/new?**').as('getNewTimelineItems');
        cy.route('GET', '/web/users/*/subscriptions**').as('getSubscriptions');
        cy.route('POST', '/web/timeline-items**').as('postTimelineItems');

        cy.get('[data-test=textarea-timeline-post]')
            .type(timelinePostMessage);
        cy.get('[data-test=button-timeline-submit]')
            .click();
        cy.wait('@postTimelineItems').its('status').should('eq', 201);
        cy.wait('@getNewTimelineItems').its('status').should('eq', 200);
        cy.wait('@getSubscriptions').its('status').should('eq', 200);
        cy.get('[data-test="timeline-stream"]').get('[data-test=timeline-item]').eq(0).as('post')
            .should('contain', timelinePostMessage).then(() => {
            cy.get('@post').within(() => {
                cy.get('[data-test=timeline-social-actions]').should('be.visible');
                cy.get('[data-test=like-button]').should('be.visible');
                cy.get('[data-test=share-button]').should('be.visible');
                cy.get('[data-test=functional-user-chooser]').should('be.visible');
                cy.get('[data-test=timeline-item-help]').should('not.exist');
            });
        });
    });

    it('should create a simple timeline post with an emoji', () => {
        const timelinePostMessage = 'Qa timeline-post for emoji test';
        const smile = "😀";  //TODO improve to use smile by data-test property and then probably try each time different smile

        cy.server();
        cy.route('GET', '/web/timeline-items/new?**').as('getNewTimelineItems');
        cy.route('GET', '/web/users/*/subscriptions**').as('getSubscriptions');
        cy.route('POST', '/web/timeline-items**').as('postTimelineItems');

        cy.get('[data-test=timeline-form]').within(() => {
            cy.get('[data-test=textarea-timeline-post]')
                .type(timelinePostMessage);
            cy.get('[data-test=timeline-emoji-picker]').click();
            cy.get('span[title="Smileys & People"]').click(); //TODO improve selector not to rely on code
            cy.get(`button[aria-label="${smile}"]`).click(); //TODO improve selector not to rely on code
            cy.get('[data-test=button-timeline-submit]')
                .click();
        });
        cy.wait('@postTimelineItems').its('status').should('eq', 201);
        cy.wait('@getNewTimelineItems').its('status').should('eq', 200);
        cy.wait('@getSubscriptions').its('status').should('eq', 200);
        cy.get('[data-test="timeline-stream"]').get('[data-test=timeline-item]').eq(0)
            .should('contain', timelinePostMessage + smile);
    });

    it('should create a timeline post with another sender', () => {
        const timelinePostMessage = 'Qa timeline-post for another sender test ' + Date.now();
        cy.apiCreateTimelinePost(timelinePostMessage, false).apiShowNewTimelinePosts();
        const host: string = 'Company News';

        cy.server();
        cy.route('GET', '/web/timeline-items/new?**').as('getNewTimelineItems');
        cy.route('GET', '/web/users/*/subscriptions**').as('getSubscriptions');
        cy.route('POST', '/web/timeline-items**').as('postTimelineItems');

        cy.get('[data-test=timeline-form]').within(() => {
            cy.get('[data-test=textarea-timeline-post]')
                .type(timelinePostMessage)
                .get('[data-test=functional-user-chooser]').click()
                .get('[data-test=functional-user-chooser]').contains(host).scrollIntoView().click()
                .get('[data-test=button-timeline-submit]').click();
        });
        cy.get('[data-test="show-new-timeline-items"]').click();
        cy.wait('@postTimelineItems').its('status').should('eq', 201);
        cy.wait('@getNewTimelineItems').its('status').should('eq', 200);
        cy.wait('@getSubscriptions').its('status').should('eq', 200);
        cy.get('[data-test="timeline-stream"]')
            .should('contain', timelinePostMessage);
    });

    xit('should create a post with a file and a link', () => { //TODO add function to upload file before the test for library not to be empty
        const timelinePostMessage = 'Qa timeline-post for post with attachment test ' + Date.now();

        cy.server();
        cy.route('GET', '/web/timeline-items/new?**').as('getNewTimelineItems');
        cy.route('GET', '/web/users/*/subscriptions**').as('getSubscriptions');
        cy.route('POST', '/web/timeline-items**').as('postTimelineItems');

        cy.get('[data-test=timeline-form]').within(() => {
            cy.get('[data-test=textarea-timeline-post]')
                .type(timelinePostMessage)
                .get('[data-test=timeline-attachment-btn]').click();
            cy.get('[data-test=open-file-library]').click();
        });
        cy.get('[data-test=file-table-row]').first().click();
        cy.get('[data-test=submit-selected-file]').click();
        cy.get('[data-test=timeline-form]').within(() => {
            cy.get('[data-test=coyo-attachment]').should('be.visible');
            cy.get('[data-test=button-timeline-submit]')
                .click();
        });
        cy.wait('@postTimelineItems').its('status').should('eq', 201);
        cy.wait('@getNewTimelineItems').its('status').should('eq', 200);
        cy.wait('@getSubscriptions').its('status').should('eq', 200);
        cy.get('[data-test=timeline-stream]').contains('coyo-timeline-item', timelinePostMessage).within(() => {
            cy.get('[data-test=file-preview__preview-image]').should('be.visible');  //TODO is it enough to check whether the image is shown
        });
    });

    it('should edit an own timeline entry', () => {
        const newPost = 'qa timeline-post for edit test ' + Date.now();
        const editPost = ' edited';
        cy.apiCreateTimelinePost(newPost, false).apiShowNewTimelinePosts();

        cy.server();
        cy.route('PUT', '/web/timeline-items/**').as('putEditTimelineItem');

        cy.get('[data-test=timeline-item]').contains(newPost).parents('[data-test=timeline-item]').as('post').then(($post) => {
            cy.wrap($post).find('[data-test=button-timeline-entry-edit-dropdown]')
                .click()
                .get('[data-test=button-timeline-post-edit]')
                .click()
                .get('[data-test=input-timeline-edit]')
                .type(editPost);
            cy.get('[data-test=button-timeline-edit-save]')
                .click();
            cy.wait('@putEditTimelineItem').its('status').should('eq', 200);
            cy.get('@post')
                .should('contain', newPost + editPost);
        })
    });

    it('should delete a timeline entry', () => {
        const timelinePostMessage = 'Qa timeline-post for delete test ' + Date.now();
        cy.apiCreateTimelinePost(timelinePostMessage, false).apiShowNewTimelinePosts();

        cy.server();
        cy.route('DELETE', '/web/timeline-items/**').as('deleteTimelineItem');

        cy.get('[data-test=timeline-item]').contains(timelinePostMessage).parents('[data-test=timeline-item]').within(() => {
            cy.get('[data-test=button-timeline-entry-edit-dropdown]')
                .click()
        });
        cy.get('[data-test=delete-post-button]')
            .click();
        cy.wait('@deleteTimelineItem').its('status').should('eq', 204);
        cy.get('[data-test=timeline-item]').should('not.contain', timelinePostMessage);
    });
});