describe('Page creation', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.clearLocalStorage().apiLogin('rl', 'demo')
        .apiLanguage('EN')
        .apiDismissTour();
    cy.visit('/home/timeline');
  });

  afterEach(function () {
    cy.apiLogout();
  });

  it('should create a private page without auto-subscription', function () {
    const visibility = 'off';
    const pageName = 'qa-page-' + visibility + '-' + Date.now();
    cy.createPage(pageName, 'private', visibility);
    cy.get('[data-test=text-page-title]')
        .contains(pageName);
    //TODO add verification that page is private and user is not subscribed to it
  });

  it('should create a public page with auto-subscribe for everyone', function () {
    const visibility = 'everyone';
    const pageName = 'qa-page-' + visibility + '-' + Date.now();
    cy.createPage(pageName, 'public', visibility);
    cy.get('[data-test=text-page-title]')
        .contains(pageName);
    //TODO add verification that page is public and user is subscribed to it
  });

  it('should create a private page with auto-subscribe for selected users', function () {
    const visibility = 'selected';
    const pageName = 'qa-page-' + visibility + '-' + Date.now();
    cy.createPage(pageName, 'private', visibility);
    cy.get('[data-test=text-page-title]')
        .contains(pageName);
    //TODO add verification that page is private and selected users are subscribed to it
  });
});
