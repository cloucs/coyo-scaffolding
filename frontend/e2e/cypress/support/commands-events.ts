// ***********************************************************
// Custom commands related to events
// ***********************************************************

/**
 * Command for creating a event via UI
 * @param eventName string, the event will be created with this name
 * @param host string, if set up the event will be created with a host different from the current user
 * @param isPrivate boolean, if set up and true, the event will be created as private with additional step to invite attendees. If not/false, public event will be created
 * @param invitee string, if set up for private event, the event will be created with inviting this user to an event. Will be ignored if set up for public event
 */

Cypress.Commands.add('createEvent', function (eventName: string, isPrivate=false, host?: string, invitee='Ian Bold') {
    // Create an event
    cy.server();
    cy.route('POST', '/web/events').as('postEvents');

    cy.get('[data-test=navigation-events]')
        .click();
    cy.url()
        .should('contain', Cypress.config().baseUrl + '/events');
    cy.get('[data-test=create-event-menu]')
        .click();

    let eventType: string;
    switch (isPrivate) {
        case true:
            eventType = 'Private event';
            break;
        case false:
            eventType = 'Public event';
            break;
        default:
            eventType = 'Public event';
    }

    cy.get('.cdk-overlay-pane').get('button').contains(eventType).click();
    cy.url()
        .should('contain', Cypress.config().baseUrl + '/events/create');

    cy.get('[data-test=event-create-input-event-name]')
        .click()
        .type(eventName).blur();
    cy.get('[data-test=event-create-checkbox-full-day]').click();

    if (host) {
        cy.get('[data-test=select-sender-component]').click();
        cy.get('.ng-dropdown-panel').contains(host)
            .click();
    }
    cy.get('[data-test=event-create-advanced-settings]').within(() => {
        cy.get('[data-test=event-create-show-participants-checkbox]').click();
        cy.get('[data-test=event-create-definite-answer-checkbox]').click();
        cy.get('[data-test=event-create-limited-participants-checkbox]').click();
        cy.get('[data-test=event-create-limited-participants-input]').click()
            .clear()
            .type('10').blur();
    });

    cy.get('[data-test=event-create-submit-button]')
        .click();

    if (isPrivate === true) {
        if (!invitee) {
            invitee = 'Ian Bold';
        }
        cy.get('[data-test=button-user-chooser]').click();
        cy.get('[data-test=list-user-chooser-user]').get('[data-test=user-chooser-element]').contains(invitee).scrollIntoView()
            .click();
        cy.get('[data-test=button-user-chooser-submit]').click();
        cy.get('[data-test="button-event-create-event"]').click();
    }
});
