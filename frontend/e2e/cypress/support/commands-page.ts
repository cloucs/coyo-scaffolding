// ***********************************************************
// Custom commands related to coyo pages
//
// Commands defined in this file may utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// Ideally, these commands utilize only variables defined in
// authentication commands at `./commands-auth.ts`,
// ***********************************************************

/**
 * Command for creating a page via UI
 * @param {string} pageName is a custom name for the page
 * @param {string} visibility can be 'public' or 'private'
 * @param {string} subscription can be 'off', 'everyone', or 'selected'
 *
 */

Cypress.Commands.add('createPage', function (pageName, visibility, subscription) {
    cy.server();
    cy.route('GET', '/web/pages/check-name**').as('getNameCheck');
    cy.route('GET', '/web/users**').as('getUsers');

    // Visit page overview and start creation
    cy.get('[data-test=navigation-pages]')
        .click();
    cy.url()
        .should('contain', Cypress.config().baseUrl + '/pages');
    cy.get('[data-test=button-create-page]').eq(0)
        .click();
    cy.url()
        .should('contain', Cypress.config().baseUrl + '/pages/create');

    // Setup the new page
    cy.get('[data-test=input-name]')
        .type(pageName);
    cy.wait('@getNameCheck');
    cy.get('[data-test="button-page-creation-general submit"]')
        .click();

    // Set visibility
    cy.get('[data-test=radio-page-' + visibility + ']')
        .check();
    cy.get('[data-test="button-page-creation-access submit"]')
        .click();

    // Set Auto-subscription
    cy.get('[data-test=radio-subscription-' + subscription + ']', {timeout: 5500})
        .check();
    if (subscription === 'selected') {
        cy.get('[data-test=button-select-user] > [data-test=button-user-chooser]')
            .click();
        cy.wait('@getUsers');

        cy.get('[data-test=user-chooser-element]').eq(0)
            .click();
        cy.get('[data-test=button-user-chooser-submit]')
            .click();
    }
    cy.get('[data-test="button-page-creation-subscription submit"]')
        .click();
});

/**
 * Command for creating a page via API
 * @param {string} name describes the name of the page
 * @param {string} visibility can be 'public' or 'private'
 *
 */

Cypress.Commands.add('apiCreatePage', function (name, visibility) {
    return cy.apiBearerToken('robert.lang@coyo4.com', 'demo').then((bearerToken) => cy.request({
        url: Cypress.env('backendUrl') + '/api/pages',
        method: 'POST',
        auth: {
            bearer: bearerToken
        },
        body: {
            name: name || Cypress.faker.lorem.words(),
            visibility: visibility || 'PUBLIC',
            adminIds: [this.user.id]
        }
    })).its('body');
});
