// ***********************************************************
// Custom commands related to the tour
// ***********************************************************

/**
 * Command for click through each tour via UI
 * @param {string} target can be 'timeline', 'pages', 'workspaces', 'events', 'search', 'colleagues', or 'profile'
 *
 */
Cypress.Commands.add('tour', function (target) {
  // Pressing escape before changing the target in case the previous test fails and tour stays active
  let tourStep = 1;
  const maxTourSteps = {
    timeline: 12,
    pages: 3,
    workspaces: 5,
    events: 3,
    search: 2,
    colleagues: 3,
    profile: 4
  };
  let lastTourStep = maxTourSteps[target];

  cy.get('body')
      .type('{esc}');
  switch (target) {
    case 'timeline':
      tourStep = tourStep - 1;
      lastTourStep = lastTourStep - 1;
      cy.url()
          .should('eq', Cypress.config().baseUrl + '/home/' + target);
      break;
    case 'search':
      cy.get('[data-test=button-tour-close]').click();
      cy.get('[data-test=navigation-search-icon]')
          .click();
      cy.get('[data-test=navigation-global-search-input]')
          .type('COYO{enter}');
      cy.url()
          .should('eq', Cypress.config().baseUrl + '/search?term=COYO');
      break;
    case 'profile':
      cy.get('[data-test=button-tour-close]').click();
      cy.get('[data-test=navigation-dropdown]')
          .click();
      cy.get('[data-test=navigation-profile]')
          .click();
      cy.url()
          .should('contain', Cypress.config().baseUrl + '/profile');
      break;
    default:
      cy.get('[data-test=button-tour-close]').click();
      cy.get(`[data-test=navigation-${target}]`)
          .click();
      cy.url()
          .should('contain', Cypress.config().baseUrl + '/' + target);
  }

  cy.wait(1000); // Actually necessary to check if the tour starts with step one
  cy.get('[data-test=text-tour-steps]').then((stepsText) => {
    // Check if tour begins with first step
    const firstTourStep = parseInt(stepsText.text().split('/')[0]);
    expect(firstTourStep).to.equal(1);
    // Set maximum tour steps and click through each tour


    for (tourStep; tourStep < lastTourStep; tourStep++) {
      cy.get('[data-test=button-tour-next]').eq(tourStep)
          .click();
      cy.wait(500); // Since there is a small animation between each tour step cypress finds two buttons, which does not happen with the wait
    }

    cy.get('[data-test=button-tour-finish]').eq(tourStep)
        .click();
  });
});
