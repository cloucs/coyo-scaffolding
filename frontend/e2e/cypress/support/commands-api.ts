// ***********************************************************
// Custom commands done via API
// ***********************************************************

// Get client secret via API
Cypress.Commands.add('getClientSecret', function () {
  cy.request({
    method: 'GET',
    url: Cypress.env('backendUrl') + '/web/api-clients'
  }).then((response) => {
    cy.wrap({
      clientId: response.body.content[0].clientId,
      clientSecret: response.body.content[0].clientSecret
    });
  });
});

/**
 * Command for setting a users language via API
 * @param {string} should be "DE" or "EN" or any language available for the instance
 *
 */
// Set language via API
Cypress.Commands.add('apiLanguage', function (language: string): any {
  return cy.requestAuth('PUT', '/web/users/' + this.user.id + '/language', {
    language: language
  });
});

/**
 * Dismisses the COYO tour for the given topics.
 *
 * @param {string[]} [topics] The list of tour topics to be dismissed.
 * If left empty all possible tours will be dismissed
 * @example
 *    cy.dismissTour();
 *    cy.dismissTour(['timeline', 'pages', 'workspaces']);
 */

Cypress.Commands.add('apiDismissTour', function (topics) {
  return cy.requestAuth('PUT', '/web/users/' + this.user.id + '/tour', {
    visited: topics || [
      'navigation',
      'messaging',
      'timeline',
      'pages',
      'workspaces',
      'events',
      'profile',
      'colleagues'
    ]
  });
});

/**
 * Command for enabling the tour via API
 * All possible tours will be enabled
 *
 */

Cypress.Commands.add('apiEnableTour', function () {
  return cy.requestAuth('PUT', '/web/users/' + this.user.id + '/tour', {
    visited: []
  });
});

