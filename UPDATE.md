# How to update the Scaffolding Project to a new version of COYO

## Backend

### Step 1: 3rd-party-library updates

Check versions of dependencies in the following files:

- build.gradle

### Step 2: COYO library update

Change the version of variables of type "x.y.z-BETA/RELEASE" in the following files:

- COYO-SCAFFOLDING/backend/build.gradle
- COYO-SCAFFOLDING/tools/dev/.env
- COYO-SCAFFOLDING/tools/run/.env

Check for new variables/environment variables in the following files compared to the file COYO/run/.env:

- COYO-SCAFFOLDING/tools/dev/.env
- COYO-SCAFFOLDING/tools/run/.env

## Step 3: Check docker setup

Compare the environment variables and containers in the following files compared to the file COYO/tools/dev/docker-compose.dev.yml:

- COYO-SCAFFOLDING/tools/dev/docker-compose.dev.yml
- COYO-SCAFFOLDING/tools/dev/docker-compose.dev.backend.yml
- COYO-SCAFFOLDING/tools/dev/docker-compose.dev.frontend.yml

Compare the environment variables and containers in the following files compared to the file COYO/run/docker-compose.tmpl:

- COYO-SCAFFOLDING/tools/run/docker-compose.yml

## Frontend

This is done automatically by the CI server.
